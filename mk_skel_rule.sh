#!/usr/bin/env bash
if [ ${#} -ne 2 ]; then
    echo "Must pass two, and only two arguments on the command line."
    exit 1
fi

LEFT="${1}"
LEFT=$(echo ${LEFT} | tr '[:upper:]' '[:lower:]')
CLEFT=$(echo $(tr a-z A-Z <<< ${LEFT:0:1})${LEFT:1})

RIGHT="${2}"
RIGHT=$(echo ${RIGHT} | tr '[:upper:]' '[:lower:]')
CRIGHT=$(echo $(tr a-z A-Z <<< ${RIGHT:0:1})${RIGHT:1})


echo "L::${CLEFT}::${LEFT}:: R::${CRIGHT}::${RIGHT}::"

SPACE=$(basename $(pwd))

cat <<EOF > src/nord/${SPACE}/rules/${CLEFT}${CRIGHT}.py
"""
Copyright $(date +%Y) NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--
"""
import nord.nord.Rule as Rule
from nord.nord.exceptions import RuntimeException as RuntimeException
import sys


class ${CLEFT}${CRIGHT}(Rule):
    """
    The ${CLEFT}${CRIGHT} rule
    """

    def apply(self):
        """
        Takes the cargo off the edge and adds it to the target builtin's
        set of arguments.
        """
        raise RuntimeException(_("NOT IMPLEMENTED: {} ${CLEFT}${CRIGHT} {}").format(self.edge.get_source(), self.edge.get_target()))  # noqa: F821, E501

        # Sample code below:

        if self.edge.has_cargo():
            if hasattr(self.edge, 'delegate'):
                self.edge.apply_delegate(self)
            else:
                self.edge.get_target().set_arg(self.edge, self.edge.get_cargo())
            return True
        else:
            raise RuntimeException(_("Delete target of pass, but nothing passed: {}").format(self.edge.get_target()))  # noqa: F821, E501


sys.modules[__name__] = ${CLEFT}${CRIGHT}
EOF

#
# Try to find a completing rule which pairs with this rule
# If one exists, generate a simple test
# 

FOUND="NO"
NODES=""
if [ -f src/nord/${SPACE}/edges/${CLEFT}.py ] && [ -f src/nord/${SPACE}/nodes/${CRIGHT}.py ]; then
    # this is an edge to node rule, and the edge and node exist already

        EDGE=${CLEFT}

        cat <<EOF >> tmp_${$}_import_nodes.py
import nord.${SPACE}.nodes.${CRIGHT} as ${CRIGHT}
EOF
        cat <<EOF >> tmp_${$}_import_edges.py
import nord.${SPACE}.edges.${CLEFT} as ${CLEFT}
EOF

    for RULE in $(ls src/nord/${SPACE}/rules/*${CLEFT}.py); do
        FOUND="LEFT"
        NODE=$(echo $(basename ${RULE}) | sed 's/\.py//' | sed "s/${CLEFT}//")
        if [ "" = "${NODES}" ]; then
            NODES="${NODE}"
        else
            NODES="${NODES} ${NODE}"
        fi
        cat <<EOF >> tmp_${$}_import_nodes.py
import nord.${SPACE}.nodes.${NODE} as ${NODE}
EOF
    done
fi

if [ -f src/nord/${SPACE}/nodes/${CLEFT}.py ] && [ -f src/nord/${SPACE}/edges/${CRIGHT}.py ] && [ "${FOUND}" = "NO" ]; then
    # this is an node to edge rule, and the edge and node exist already

        EDGE="${CRIGHT}"

        cat <<EOF >> tmp_${$}_import_edges.py
import nord.${SPACE}.edges.${CRIGHT} as ${CRIGHT}
EOF
        cat <<EOF >> tmp_${$}_import_nodes.py
import nord.${SPACE}.nodes.${CLEFT} as ${CLEFT}
EOF

    for RULE in $(ls src/nord/${SPACE}/rules/${CRIGHT}*.py); do
        FOUND="RIGHT"
        NODE=$(echo $(basename ${RULE}) | sed 's/\.py//' | sed "s/${EDGE}//")

        if [ "" = "${NODES}" ]; then
            NODES="${NODE}"
        else
            NODES="${NODES} ${NODE}"
        fi
        cat <<EOF >> tmp_${$}_import_nodes.py
import nord.${SPACE}.nodes.${NODE} as ${NODE}
EOF
    done
fi

if [ "${FOUND}" = "NO" ]; then
    echo "Could not find any rule pairs to test, exiting"
    rm -f tmp_${$}*.py
    cat <<EOF > tests/rules/${CLEFT}${CRIGHT}_test.py
"""
Copyright $(date +%Y) NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""


def test_rule_${CLEFT}${CRIGHT}():
    assert 1 == 2
EOF

    exit 1
fi

cat <<EOF > tests/rules/${CLEFT}${CRIGHT}_test.py
"""
Copyright $(date +%Y) NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""
from nord.nord import Runtime as RT
import nord.nord.Map as Map
EOF

cat tmp_${$}_import_nodes.py >> tests/rules/${CLEFT}${CRIGHT}_test.py
cat tmp_${$}_import_edges.py >> tests/rules/${CLEFT}${CRIGHT}_test.py
rm tmp_${$}*.py

cat <<EOF >> tests/rules/${CLEFT}${CRIGHT}_test.py
Runtime = RT.Runtime

EOF

if [ "${FOUND}" = "LEFT" ]; then
    for NODE in ${NODES}; do

        cat <<EOF >> tests/rules/${CLEFT}${CRIGHT}_test.py

# LEFT

def test_${NODE}_${CLEFT}${CRIGHT}():
    Na = ${NODE}(Map(None))
    Na.id = 1
    Nb = ${CRIGHT}(Map(None))
    Nb.id = 2
    rt = Runtime()
    e = ${EDGE}(Map(None))
    e.set_nodes(Na, Nb, '${EDGE}')
    e.follow(rt)

    assert repr(e) == "${EDGE}: [Node(${NODE}): <id: 1>]  --${EDGE}-->  [Node(${CRIGHT}): <id: 2>]**"

EOF
    done
else
    for NODE in ${NODES}; do

        cat <<EOF >> tests/rules/${CLEFT}${CRIGHT}_test.py

# RIGHT

def test_${CLEFT}${CRIGHT}_${NODE}():
    Na = ${CLEFT}(Map(None))
    Na.id = 1
    Nb = ${NODE}(Map(None))
    Nb.id = 2
    rt = Runtime()
    e = ${EDGE}(Map(None))
    e.set_nodes(Na, Nb, '${EDGE}')
    e.follow(rt)

    assert repr(e) == "${EDGE}: [Node(${CLEFT}): <id: 1>]  --${EDGE}-->  [Node(${NODE}): <id: 2>]**"

EOF
    done

fi

echo ${0} ${*} >> .mk_skel_gen.log