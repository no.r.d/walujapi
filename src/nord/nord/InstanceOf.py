"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

import nord.nord.Node as Node
import sys
import nord.sigurd.utils.namegen as ng


class InstanceOf(Node):
    """
    Subclasses of InstanceOf will be able to be represented
    and connected in different ways, multiple times in a given Map,
    yet, the internal state (and all non-edge functionality) will
    affect the same main instance.

    So, not quite like an instance of a class in OOP. In this case an instance
    is primarily a graph artifact which enables the same node to be employed
    differently im multiple locations.
    """

    def add_instance(self, node_dict):
        """"""
        tmp = self.__class__(self._graph)
        tmp.apply(node_dict, write_direct=True)
        rv = self._connect_instance(tmp)
        rv.set_parent(self)
        return rv

    def _connect_instance(self, instance):
        # wrap all of self's non-blacklisted (runtime, map, and edge related)
        # methods such that calls to the instance implementation
        # of those methods, actually call the parent (self)
        blacklist = ['actual_rem_target',
                     'actual_rem_source',
                     'apply',
                     'check_property',
                     'clear_blocking',
                     'clear_exe',
                     'clear_preempted',
                     'connect_allowed',
                     'delete',
                     'disconnect',
                     'execute',
                     'flag_blocking',
                     'flag_exe',
                     'flag_preempted',
                     'get_source_rules',
                     'get_sources',
                     'get_space',
                     'get_target_rules',
                     'get_target_edges',
                     'get_targets',
                     'get_unfollowed_sources',
                     'get_unfollowed_targets',
                     'follow_unfollowed_targets',
                     'schedule_unexec_targets',
                     'has_unexecuted_sources',
                     'is_blocked',
                     'has_executed',
                     'has_in_edges',
                     'has_out_edges',
                     'install_as_target',
                     'install_as_source',
                     'is_preempted',
                     'json_serialize',
                     'next_after_execute',
                     'prep_to_execute',
                     'rem_source',
                     'rem_target',
                     'rem_all_targets',
                     'remove',
                     'reset_source_edges',
                     'reset_target_edges',
                     'set_followed',
                     'set_source',
                     'set_target',
                     'set_unfollowed',
                     'set_untracked_attr',
                     'to_dict',
                     'set_parent',
                     'targets',
                     'set_breakpoint',
                     'unset_breakpoint',
                     'breakpoint_set',
                     'change_attribute']
        for func in [f for f in dir(self)
                     if f not in blacklist and not
                     f.startswith('_') and callable(getattr(self, f))]:

            def wrap(parent, funcname):
                func = getattr(parent, funcname)

                def instance_parent_wrapped(*args, **kwargs):
                    return func(*args, **kwargs)
                return instance_parent_wrapped
            setattr(instance, func, wrap(self, func))

        return instance

    def set_parent(self, parent):
        """
        This method is used to establish the relationship
        between the instance and its parent, as well as the
        essentials of the instance itself
        """
        self.set_untracked_attr('instanceof', parent.id)
        if not hasattr(self, 'id'):
            self.id = ng.id()
        self._parent = parent
        if not hasattr(parent, "_instances"):
            parent._instances = []
        if self not in parent._instances:
            parent._instances.append(self)

    def get_parent(self):
        """
        Return the parent instance so it can be
        interrogated and used should it be set.
        """
        if hasattr(self, '_parent'):
            return self._parent
        else:
            return None

    def get_all_instance_targets(self, rulename):
        """
        Return a list of all the targets of all the instances
        of this node which are of edges of the passed rulename
        """
        rv = []
        if hasattr(self, '_parent'):
            worknode = self._parent
        else:
            worknode = self
        if hasattr(worknode, '_instances'):
            for node in worknode._instances:
                rv += node.get_targets(rulename)
        return rv

    def get_all_instance_sources(self, rulename):
        """
        Return a list of all the sources of all the instances
        of this node which are of edges of the passed rulename
        """
        rv = []
        if hasattr(self, '_parent'):
            worknode = self._parent
        else:
            worknode = self
        if hasattr(worknode, '_instances'):
            for node in worknode._instances:
                rv += node.get_sources(rulename)
        return rv

    def change_attribute(self, attrname, value):
        """
        This is probably some of the worst spagetti code there could be...

        because breakpoints need to function on instances, and
        how breakpoints use the CDC mechanism to sync between the
        front and backend, the cdc mechanisms need special treatment
        to apply the changes to the correct place....
        """
        if attrname == '_breakpoint_set':
            setattr(self, attrname, value)
            for handler in self._change_attr_handlers.get(attrname, []):
                handler(attrname, value)
        else:
            """
            Through some convoluted initialization scenario, the pointed to
            (instanced) element ends up calling the instance's change_attribute
            method via the CDC pathways when running the graph, so
            we just call the original method here. Really, this is super obfuscated
            and smelly code. But, unfortunately, it seems to work, and doesn't break any
            tests...
            """
            super().change_attribute(attrname, value)


sys.modules[__name__] = InstanceOf
