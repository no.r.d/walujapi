"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

The base class for the primary construct of the Graph "language", the Node.

Nodes are the vertices of the graph. Nodes should perform most of the work
in Nord. Nodes are intended / expected to be subclassed. This module just provides
the common capabilities to support the runtime and fundamental operations
all subclasses should have. Subclasses are welcome to override nearly any of the
methods as appropriate.

Lifecycle of a Node relative to the operational map/graph:

1. Created - it is added to the graph via sigurd, or backend process
2. [*optional] updated - its properties are updated external to the operational graph
3. [*optional] executed - the node is executed by the runtime 0-n times
4. [*optional] updated - its properties are updated external to the operational graph
5. Save - the node is saved when the operational map/graph is saved
6. Load - the node is loaded with the operational map/graph
7. [*optional] updated - its properties are updated external to the operational graph
8. [*optional] executed - the node is executed by the runtime 0-n times
9. [*optional] updated - its properties are updated external to the operational graph
10. Deleted - the node is removed from the map/graph
11. [*optional] - the node removal is undone (then goto 1)
"""

import sys
import nord.nord.Base as Base
import nord.nord.RunState as RunState
import nord.nord.edges.Flow as Flow
import nord.sigurd.utils.namegen as ng
import nord.nord.utils.SpaceManager as use
from nord.nord.exceptions import UseException


class Node(Base):
    """
    Nodes are the vertices of the graph.

    They are connected by edges.

    A Node is the base class for all map objects. The primary usage of a node
    is to create subclasses of it. Data, Static and Container are examples of
    direct Node subclasses.

    >>> Node() #doctest: +ELLIPSIS
    Node:

    """

    # Define configuration defaults at
    # the class level like the (un-implemented) example below
    # _CI_Enforce_flow_preemption = True

    # For RuntimeStateMachine based runtime to know how to treat this
    # node when not targetted by an event
    _CI_continue_exec_wo_event = True

    # Event emitting nodes should set this to True
    # so the runtime can treat flow edges from them
    # differently than non-event emitting flow edges
    # namely, the initial execution of event emitters
    # needs to be permitted so the correct state
    # data can be established in event path nodes
    _CI_event_emitting_node = False

    def __init__(self, graph):
        """
        All nodes and edges are associated with a graph.

        When they are created, changed, or removed, the graph
        is notified, such that it can inform any and all clients.
        """
        self.__dict__['_graph'] = graph  # set like this so as to allow change detection to work
        self.__dict__['id'] = ng.id()
        self.__dict__["type"] = self.__class__.__name__.lower()
        self.__dict__["_remove_attr_handlers"] = {}
        self.__dict__["_change_attr_handlers"] = {}
        self.__dict__["_breakpoint_set"] = False
        if graph is not None:
            self._graph.ccap_node_added(self)

    def set_breakpoint(self):
        self._breakpoint_set = True

    def unset_breakpoint(self):
        self._breakpoint_set = False

    def breakpoint_set(self):
        return self.__dict__["_breakpoint_set"]

    def __setattr__(self, attrib, value):
        """
        if not a private item, (name starts with _), tell the graph this node has changed

        The risks here are that a value could be changed that is pointed to by the
        node's attribute, rather than changing the attribute itself. There are probably
        tons of bugs down this road...
        """
        if not attrib.startswith('_') or attrib == '_breakpoint_set':
            prev = None
            if hasattr(self, attrib):
                prev = getattr(self, attrib)
                if prev == value:
                    return
        self.__dict__[attrib] = value
        if (not attrib.startswith('_') or attrib == '_breakpoint_set') and self._graph is not None:
            self._graph.ccap_node_changed(self, attrib, prev, value, noparse=attrib == "position")

    def add_rem_attr_handler(self, attrname, handler):
        """
        Add the given handler if it is not already to the
        list of handlers to be called when the given attribute
        is removed
        """
        if attrname not in self._remove_attr_handlers:
            self._remove_attr_handlers[attrname] = []
        if handler not in self._remove_attr_handlers[attrname]:
            self._remove_attr_handlers[attrname].append(handler)

    def add_chg_attr_handler(self, attrname, handler):
        """
        Add the given handler if it is not already to the
        list of handlers to be called when the given attribute
        is changed
        """
        if attrname not in self._change_attr_handlers:
            self._change_attr_handlers[attrname] = []
        if handler not in self._change_attr_handlers[attrname]:
            self._change_attr_handlers[attrname].append(handler)

    def remove_attribute(self, attrname):
        if hasattr(self, attrname):
            delattr(self, attrname)
        for handler in self._remove_attr_handlers.get(attrname, []):
            handler(attrname)

    def change_attribute(self, attrname, value):
        setattr(self, attrname, value)
        for handler in self._change_attr_handlers.get(attrname, []):
            handler(attrname, value)

    def set_untracked_attr(self, attribute, value):
        """
        Used to associate values with a Node which will not
        be tracked by the change capture __setattr__ system.
        """
        self.__dict__[attribute] = value

    def __delattr__(self, attrib):
        if not attrib.startswith('_') and self._graph is not None:
            prev = None
            if hasattr(self, attrib):
                prev = getattr(self, attrib)
            self._graph.ccap_node_changed(self, attrib, prev, rem_attr=True)
        del self.__dict__[attrib]

    def get_graph(self):
        """
        Return the graph object with which this node is associated
        """
        return self._graph

    def apply(self, data,  write_direct=False):
        super().apply(data, write_direct)
        if self._graph is not None:
            self._graph.set_node(self.id, self)

    def handles_events(self):
        """Check if this node is configured to receive events."""
        return hasattr(self, 'handle_event')

    def flag_preempted(self):
        """Mark the node as pre-empted."""
        RunState.s(self).set_preempted()

    def is_preempted(self):
        """Check if this node is pre-empted"""
        return RunState.s(self).preempted()

    def clear_preempted(self):
        """Revert the node to the non-preempted state."""
        RunState.s(self).unset_preempted()

    def flag_exe(self, runtime):
        """Mark the node as executed."""
        RunState.s(self).set_executed(runtime)

    def get_name(self):
        """
        Returns the name of this node. If a name does not yet exist, one is
        created and stored with the node.
        """
        if not hasattr(self, "name"):
            raise Exception("I wanted to know if any node's had to create their names here?")
            self.name = ng.get()
        return self.name

    def get_id(self):
        """
        Returns the ID of this node.
        """
        return self.id

    def get_unfollowed_targets(self, runtime=None):
        """
        get the unfollowed targets, including the set of followed/unfollowed
        if the execution counter is greater than that of the candidate edges.
        """
        if not hasattr(self, '_unfollowedTargets') or not hasattr(self, '_followedTargets'):
            return []
        return self._get_unfollowed(self._unfollowedTargets, self._followedTargets, runtime)

    def get_unfollowed_sources(self, runtime=None):
        """
        get the unfollowed sources, including the set of followed/unfollowed
        if the execution counter is greater than that of the candidate edges.
        """
        if not hasattr(self, '_unfollowedSources') or not hasattr(self, '_followedSources'):
            return []
        return self._get_unfollowed(self._unfollowedSources, self._followedSources, runtime)

    def _get_unfollowed(self, uns, notuns, runtime):
        """
        The generic function implementing the logic of
        considering followed/unfollowed edges with regards
        to the runtime's execution counter. (This is primarily to
        facilitate flow edges)
        """
        rv = [x for x in uns if runtime is None or not x.been_followed(runtime)]
        if runtime:
            rv += [x for x in notuns if not x.been_followed(runtime)]
        return rv

    def flag_followed(self, runtime):
        """
        Mark all outbound edges as followed, without actually
        following them.
        """
        if hasattr(self, '_unfollowedTargets'):
            # unfollowed = self._unfollowedTargets.copy()
            unfollowed = self.get_unfollowed_targets(runtime)
            for edge in sorted(unfollowed):
                edge.flag_followed(runtime)

    def clear_exe(self, runtime):
        """Revert the node to the un-executed state."""
        RunState.s(self).unset_executed(runtime)

    def has_executed(self):
        """Return if this node has executed."""
        return RunState.s(self).executed()

    def flag_blocking(self):
        """Mark the node as blocked (for input perhaps)."""
        RunState.s(self).set_blocked()

    def clear_blocking(self):
        """Revert the node to the un-blocked state."""
        RunState.s(self).unset_blocked()

    def is_blocked(self):
        """Return if the node is blocked (for input perhaps)."""
        return RunState.s(self).blocked()

    def execute(self, runtime):
        """
        Execute this node.

        This method can be overridden by sub-classes of Node
        to perform whatever action it is that node is intended to perform.

        The runtime will execute this method, passing an instance of itself.

        A node will be executed after all its source edges have been followed, but
        before any of its target edges are.
        """
        self.flag_exe(runtime)

    def set_target(self, tgt, rule, edge):
        """
        Establish requested connection from this node to the Target.

        The set_target method takes a Node instance and rule name as arguments
        and establishes a connection between the two nodes.

        >>> src = Node()
        >>> tgt = Node()
        >>> src.set_target(tgt, "null", Edge())

        """
        if hasattr(edge, '_operate_in_reverse'):
            self.install_as_source(tgt, rule, edge)
        else:
            self.install_as_target(tgt, rule, edge)

        tgt.set_source(self, rule, edge)

    def install_as_target(self, tgt, rule, edge):
        if not hasattr(self, "_targetRules"):
            self._targetRules = dict()

        if not hasattr(self, "_targetNodes"):
            self._targetNodes = dict()

        if not hasattr(self, '_unfollowedTargets'):
            self._unfollowedTargets = list()
            self._followedTargets = list()

        if rule not in self._targetRules:
            self._targetRules[rule] = list()

        if tgt not in self._targetNodes:
            self._targetNodes[tgt] = dict()

        if rule not in self._targetNodes[tgt]:
            self._targetNodes[tgt][rule] = list()

        if edge not in self._targetNodes[tgt][rule]:
            self._targetNodes[tgt][rule].append(edge)

        if edge not in self._targetRules[rule]:
            self._targetRules[rule].append(edge)

        if edge not in self._unfollowedTargets:
            self._unfollowedTargets.append(edge)

    def rem_target(self, tgt, edge):
        if hasattr(edge, '_operate_in_reverse'):
            self.actual_rem_source(tgt, edge)
        else:
            self.actual_rem_target(tgt, edge)

    def rem_source(self, src, edge):
        if hasattr(edge, '_operate_in_reverse'):
            self.actual_rem_target(src, edge)
        else:
            self.actual_rem_source(src, edge)

    def actual_rem_target(self, tgt, edge):
        """
        Remove data traces of the passed edge to the target.

        The _[un]followed* lists are operational, and not
        dealt with with this function.
        """
        if hasattr(self, '_targetRules'):
            if edge.rule in self._targetRules and\
               edge in self._targetRules[edge.rule]:
                self._targetRules[edge.rule].remove(edge)

            if edge.rule in self._targetRules and\
                    len(self._targetRules[edge.rule]) == 0:
                del self._targetRules[edge.rule]

        if hasattr(self, '_targetNodes') and\
           tgt in self._targetNodes and\
           edge.rule in self._targetNodes[tgt] and\
           edge in self._targetNodes[tgt][edge.rule]:
            self._targetNodes[tgt][edge.rule].remove(edge)

            if len(self._targetNodes[tgt][edge.rule]) == 0:
                del self._targetNodes[tgt][edge.rule]

        tgt.rem_source(self, edge)

    def set_source(self, src, rule, edge):
        """
        Establish this node's connection back to the source.

        The set_source method takes a Node instance and rule name as arguments
        and establishes a connection back to the source.

        >>> src = Node()
        >>> tgt = Node()
        >>> tgt.set_source(src, "null", Edge())

        """
        if hasattr(edge, '_operate_in_reverse'):
            self.install_as_target(src, rule, edge)
        else:
            self.install_as_source(src, rule, edge)

    def install_as_source(self, src, rule, edge):
        if not hasattr(self, "_sourceRules"):
            self._sourceRules = dict()

        if not hasattr(self, "_sourceNodes"):
            self._sourceNodes = dict()

        if not hasattr(self, '_unfollowedSources'):
            self._unfollowedSources = list()
            self._followedSources = list()

        if rule not in self._sourceRules:
            self._sourceRules[rule] = list()

        if src not in self._sourceNodes:
            self._sourceNodes[src] = dict()

        if rule not in self._sourceNodes[src]:
            self._sourceNodes[src][rule] = list()

        if edge not in self._sourceNodes[src][rule]:
            self._sourceNodes[src][rule].append(edge)

        if edge not in self._sourceRules[rule]:
            self._sourceRules[rule].append(edge)

        if edge not in self._unfollowedSources:
            self._unfollowedSources.append(edge)

    def actual_rem_source(self, src, edge):
        """
        Remove data traces of the passed edge from the target.

        The _[un]followed* lists are operational, and not
        dealt with with this function. (perhaps they should be?)
        """
        if hasattr(self, '_sourceRules'):
            if edge.rule in self._sourceRules and\
               edge in self._sourceRules[edge.rule]:
                self._sourceRules[edge.rule].remove(edge)

            if edge.rule in self._sourceRules and\
                    len(self._sourceRules[edge.rule]) == 0:
                del self._sourceRules[edge.rule]

        if hasattr(self, '_sourceNodes') and\
           src in self._sourceNodes and\
           edge.rule in self._sourceNodes[src] and\
           edge in self._sourceNodes[src][edge.rule]:
            self._sourceNodes[src][edge.rule].remove(edge)

            if len(self._sourceNodes[src][edge.rule]) == 0:
                del self._sourceNodes[src][edge.rule]

    def targets(self, node, rule):
        """
        Interrogate set of targets to find out if the provided node is targetted
        by this node with the requested rule.
        """
        return hasattr(self, '_targetNodes') and\
            node in self._targetNodes and\
            rule in self._targetNodes[node]

    def get_target_edges(self, node, rule):
        """
        Return the list of edges where this node targets
        the passed node with the requested rule.
        """
        return self._targetNodes[node][rule]

    def get_target_rules(self):
        """
        Return a list of all the rules that this Node applies to its edges out.

        >>> src = Node()
        >>> tgt = Node()
        >>> src.set_target(tgt, "null", Edge())
        >>> src.get_target_rules()
        dict_keys(['null'])
        """
        if hasattr(self, "_targetRules"):
            return self._targetRules.keys()
        else:
            return []

    def get_targets(self, rule):
        """
        Get the list of _targetRules for the specified Rule.

        >>> src = Node()
        >>> tgt = Node()
        >>> src.set_target(tgt, "null", Edge())
        >>> src.get_targets("null")
        [Edge: [None]  --Edge-->  [None]]
        >>> src.get_targets("other")
        []
        """
        if hasattr(self, "_targetRules") and rule in self._targetRules:
            return self._targetRules[rule]
        else:
            return []

    def rem_all_targets(self, rule):
        wrk = [x for x in self.get_targets(rule)]
        for edge in wrk:
            self.rem_target(edge.target, edge)

    def get_source_rules(self):
        """
        Return a list of all the rules that apply to this Node.

        >>> src = Node()
        >>> tgt = Node()
        >>> src.set_target(tgt, "null", Edge())
        >>> tgt.get_source_rules()
        dict_keys(['null'])
        """
        if hasattr(self, "_sourceRules"):
            return self._sourceRules.keys()
        else:
            return []

    def get_sources(self, rule):
        """
        Get the list of _sourceRules for the specified Rule.

        >>> src = Node()
        >>> tgt = Node()
        >>> src.set_target(tgt, "null", Edge())
        >>> tgt.get_sources("null") #doctest: +NORMALIZE_WHITESPACE
        [Edge: [None]  --Edge-->  [None]]
        >>> src.get_targets("other")
        []
        """
        if hasattr(self, "_sourceRules") and rule in self._sourceRules:
            return self._sourceRules[rule]
        else:
            return []

    # def get_unfollowed_sources(self):
    #     """Return list of any unfollowed sources."""
    #     if not hasattr(self, '_unfollowedSources'):
    #         return []
    #     else:
    #         return self._unfollowedSources

    # def get_unfollowed_targets(self):
    #     """Return list of any unfollowed targets."""
    #     if not hasattr(self, '_unfollowedTargets'):
    #         return []
    #     else:
    #         return self._unfollowedTargets

    def eligible_for(self, rulename):
        """
        Check to see if this node can be a recipient of
        rules of the given type
        """
        target = self.__class__.__name__.capitalize()
        trulename = f'rules.{rulename.capitalize()}{target}'
        orig_space = use.current_space()
        space = self.get_space()
        try:
            use.space(space)
            use.module(f"nord.{space}.{trulename}")
            rv = True
        except UseException:
            rv = False

        use.space(orig_space)
        return rv

    def connect_allowed(self, other, edge_type, direction, delegate=None):
        """Provide an overridable means to children who may want to prevent connections."""
        return True

    def determine_event_hold_candidacy(self, runtime, node, direction='UP'):
        """
        look up the graph to determine if connected nodes are EHC
        or should be set as such.

        Traverse up the graph until:
           1) an EHC is detected upstream
           2) An un-sourced node is reached
           3) An event expecting node is found.

        Then, traverse down the graph, setting EHC (if upstream conditions dictate) until:
            1) A target-less node is reached
            2)  A node with EHC is detected
            3) An event expecting node is found (not sure what would happen here....)
        """
        if RunState.s(node).nord_ehc_inf_recurse_brake():
            if hasattr(node, '_ehc_enabled'):
                if node._ehc_enabled:
                    return node._event_sources
                else:
                    return node._ehc_enabled
            else:
                return False

        RunState.s(node).set_nord_ehc_inf_recurse_brake()
        if hasattr(node, '_ehc_enabled') and direction == 'UP':
            RunState.s(node).del_nord_ehc_inf_recurse_brake()
            if node._ehc_enabled:
                return node._event_sources
            else:
                return node._ehc_enabled

        if hasattr(node, 'handle_event'):
            node._ehc_enabled = True
            if not hasattr(node, '_event_sources'):
                node._event_sources = []
            node._event_sources += [node.id]
            # return True

        nf_sources = 0
        flow_sources = 0
        eventing_sources = 0
        non_eventing_sources = 0
        evid = None

        # Evaluate all upstream nodes.
        if node.get_source_rules():
            for rule in node.get_source_rules():
                if rule != 'flow' and not rule.endswith('flow'):
                    nf_sources += len(node.get_sources(rule))
                if rule == 'flow' and not rule.endswith('flow'):
                    flow_sources += 1
                for edge in node.get_sources(rule):
                    evid = self.determine_event_hold_candidacy(runtime, edge.get_source())
                    if evid:
                        eventing_sources += 1
                        if not hasattr(node, '_event_sources'):
                            node._event_sources = []
                        # Add any new eventing node id's to the list of potential event sources.
                        node._event_sources += list(set(evid) - set(node._event_sources))
                    else:
                        non_eventing_sources += 1

        if not hasattr(node, '_ehc_enabled') or not node._ehc_enabled:
            if eventing_sources:
                node._ehc_enabled = True
            else:
                node._ehc_enabled = False

        # call all the downstream nodes as well.
        if node.get_target_rules():
            for rule in node.get_target_rules():
                for edge in node.get_targets(rule):
                    if not hasattr(node, '_event_sources') or edge.get_target().id not in node._event_sources:
                        self.determine_event_hold_candidacy(runtime, edge.get_target(), direction="DOWN")
                    else:
                        pass

        if hasattr(node, '_event_sources'):
            evid = node._event_sources

        RunState.s(node).del_nord_ehc_inf_recurse_brake()
        return evid

    def event_hold_candidate(self, runtime):
        """
        Evaluates this node, and and connected nodes if needed,
        to determine if it should not execute because it must wait
        for an upstream event.

        Child classes can affect the behavior of this
        method by explicitly setting their own
        value for:
        _ehc_enabled

        The existance of _ehc_enabled prevents the graph traversal
        process from trying to identify candidacy to hold processing
        while waiting for an upstream event.
        """
        rv = False
        if not hasattr(self, '_ehc_enabled'):
            self.determine_event_hold_candidacy(runtime, self)
        rv = self._ehc_enabled
        return rv

    def set_followed(self, edge, source):
        """
        Mark the passed edge for this node as followed.

        if this Node is the source of the edge
        update the targets lists, if this node is the
        target, updates the sources lists
        """
        if source:
            if not hasattr(self, '_unfollowedTargets'):
                return
            if edge not in self._unfollowedTargets:
                # at one point it was thought this should be an exceptional case...
                pass
            else:
                self._unfollowedTargets.remove(edge)
                self._followedTargets.append(edge)
        else:
            if not hasattr(self, '_unfollowedSources'):
                return
            if edge not in self._unfollowedSources:
                # at one point it was thought this should be an exceptional case...
                pass
            else:
                self._unfollowedSources.remove(edge)
                self._followedSources.append(edge)

    def set_unfollowed(self, edge, source):
        """Mark the passed edge for this node as not followed."""
        if source:
            if not hasattr(self, '_unfollowedTargets'):
                return
            if edge not in self._unfollowedTargets:
                self._unfollowedTargets.append(edge)
            if self._followedTargets and edge in self._followedTargets:
                self._followedTargets.remove(edge)
        else:
            if not hasattr(self, '_unfollowedSources'):
                return
            if edge not in self._unfollowedSources:
                self._unfollowedSources.append(edge)
            if self._followedSources and edge in self._followedSources:
                self._followedSources.remove(edge)

    def emits_events(self):
        """
        The default node class does not emit events. Nodes which de emit events
        should have the configuration item event_emitting_node set to True
        """
        return self.get_cfg_value('event_emitting_node')

    def is_ehc_or_runnable(self, runtime):
        if self.event_hold_candidate(runtime):
            if runtime._in_event and \
                    hasattr(self, '_event_sources') and \
                    runtime._in_event in self._event_sources:
                return True
            else:
                return False
        else:
            return True

    def in_event_path(self, runtime):
        if self.event_hold_candidate(runtime):
            if runtime._in_event and \
                    hasattr(self, '_event_sources') and \
                    runtime._in_event in self._event_sources:
                return True
            else:
                return False
        else:
            return False

    def reset_source_edges(self):
        """
        Move all the followed source edges
        to the end of the unfollowed list. But,
        do so in a way that places the followed
        edges after the residual unfollowed edges.

        No idea why this ordering is important...
        """
        if hasattr(self, '_followedSources'):
            for edge in self._followedSources:
                if edge not in self._unfollowedSources:
                    self._unfollowedSources.append(edge)
                else:
                    self._unfollowedSources.remove(edge)
                    self._unfollowedSources.append(edge)
            self._followedSources.clear()

    def reset_target_edges(self, recurse=False, clear_exe=False):
        """
        Move all the followed target edges
        to the end of the unfollowed list. But,
        do so in a way that places the followed
        edges after the residual unfollowed edges.

        No idea why this ordering is important...

        If recurse is set to true, have all followed targets
        recursively reset all their target edges as well.
        """

        targets_to_recurse = []
        if hasattr(self, '_followedTargets'):
            for edge in [x for x in self._followedTargets]:
                # if edge not in self._unfollowedTargets:
                #     self._unfollowedTargets.append(edge)
                # else:
                #     self._unfollowedTargets.remove(edge)
                #     self._unfollowedTargets.append(edge)
                if recurse:
                    targets_to_recurse.append(edge.get_target())
                edge.clear_followed()
            self._followedTargets.clear()
            if clear_exe and hasattr(self, '_current_runtime'):
                self.clear_exe(self._current_runtime)
            if recurse:
                for target in targets_to_recurse:
                    target.reset_target_edges(recurse=True, clear_exe=clear_exe)

    def has_unexecuted_sources(self):
        rv = False
        for rule in self.get_source_rules():
            if rule.endswith("flow"):
                """
                Skip flow rules, except those from eventing nodes
                """
                for edge in self.get_sources(rule):
                    if edge.get_source().emits_events():
                        if hasattr(self, '_current_runtime'):
                            if self._current_runtime.is_handling_event():
                                """
                                We are handling an event, so, only flow rules from the event source
                                should be considered
                                """
                                if not self._current_runtime.in_event_path(edge.get_source()):
                                    continue
                                # to be here means we are downgraph of the eventing node
                                if not edge.get_source().has_executed():
                                    return True
                            else:
                                """
                                If we are not in an event, but the source is in the eventpath
                                (which it seems should be false...)
                                """
                                if not edge.get_source().has_executed():
                                    if self._current_runtime.in_event_path(edge.get_source()):
                                        continue
                                    return True
            else:
                for edge in self.get_sources(rule):
                    source = edge.get_source()
                    if not source.has_executed():
                        # skip non-event path nodes when runtime is
                        # handling an event
                        if hasattr(self, '_current_runtime') and\
                                self._current_runtime and\
                                not self._current_runtime.in_event_path(source) and\
                                self._current_runtime.is_handling_event():
                            continue
                        return True
        return rv

    def schedule_unexec_sources(self, runtime, verbose=False):
        """
        Schedule un-executed sources for subsequent execution.

        This node should be pre-empted by the Runtime State Machine.
        """

        if not hasattr(self, '_unfollowedSources'):
            return

        has_unexecuted_sources = False
        unfollowed = self.get_unfollowed_sources(runtime)
        to_run_sources = []

        for edge in sorted(unfollowed):
            if not isinstance(edge, Flow) or edge.get_source().emits_events():
                """
                For non-flow edges, inject the sources
                which have not yet been executed, to ensure
                that when we follow those edges, the sources
                have been populated.

                IFF all the sources to this node
                have been executed, follow all the un-followed
                edges. Otherwise, let the runtime execute
                the nodes that need to be run first.
                """
                if not edge.source_has_executed():
                    if runtime.is_handling_event():
                        # only schedule in-event-path
                        # nodes
                        if runtime.in_event_path(edge.get_source()):
                            to_run_sources.append(edge.get_source())
                            RunState.s(self).set_preempted()
                            has_unexecuted_sources = True
                    else:
                        to_run_sources.append(edge.get_source())
                        # runtime.setNextNode(edge.source, 'UP')
                        RunState.s(self).set_preempted()
                        has_unexecuted_sources = True

        # Sort the the inbound nodes so that execution order is deterministic
        # Then add them to the runtime.
        # NOTE: This direction may need to be reversed
        for node in sorted(to_run_sources):  # this relies upon nord.nord.Base
            runtime.setNextNode(node, 'UP')

        if not has_unexecuted_sources:
            for edge in sorted(unfollowed):
                if not isinstance(edge, Flow) or edge.get_source().emits_events():
                    # if not edge.get_source().is_ehc_or_runnable(runtime):
                    #     continue
                    if runtime.is_handling_event() and not runtime.in_event_path(edge.get_source()):
                        continue
                    edge.follow(runtime, verbose)
                # else:
                #     # it is a flow type edge. Flag as followed
                #     edge.flag_followed(runtime)
            if self.get_unfollowed_sources():
                # If we got here, there are sources which appear not to
                # have run next_after_execute to follow all their target edges...
                # this seems to happen when the current node is replaced by another, like when a
                # shoshoni reference swaps the target for itself
                unfollowed = self.get_unfollowed_sources()
                for edge in sorted(unfollowed):
                    if not isinstance(edge, Flow):
                        if not edge.follow(runtime, verbose):
                            self.flag_preempted()
                            break

        self.reset_target_edges()
        return
















    def prep_to_execute(self, runtime, verbose):
        # Save the runtime so it can be referenced
        # in subsequent calls.
        self._current_runtime = runtime
        return True

        """
        Run the needed preparation steps on this node before it is executed.

        This is called prior to execution. The general case
        is to make sure all edges targeting this node, except
        for flow nodes have been followed, and for flow edges
        which target this node, mark them as followed
        as we are now executing them.

        Also, flag all downstream edges as unfollowed.  NOTE: I see no evidence of this happening here. Should I?

        If never reviewed, review all sources to see if this node is downstream of an event listener.
        If it is downstream of an event listener, check to see
        if the triggering event node ID is decorating the prior node????

        Look at all flow sources. If any are event eligible,
        then, check to see if we are being fired by the event?????

        TODO: figure out if the event is the correct
        """

        # Save the runtime so it can be referenced
        # in subsequent calls.
        self._current_runtime = runtime

        if not hasattr(self, '_unfollowedSources'):
            return

        has_unexecuted_sources = False
        # unfollowed = self._unfollowedSources.copy()
        unfollowed = self.get_unfollowed_sources(runtime)
        event_path_node = []
        to_run_sources = []

        # Here is what using  a configuration item might look like:
        # if self.get_cfg_value('Enforce_flow_preemption', search=False):
        #     """
        #     This node is configured to only run from a flow edge, if there is a
        #     flow edge targeting this node. So, if we have an unfollowed flow,
        #     pre-empt if we've gotten here.
        #     """
        #     for edge in unfollowed:
        #         if isinstance(edge, Flow):
        #             # this edge is by it's existence in unfollowed
        #             # an unfollowed flow edge.
        #             RunState.s(self).set_preempted()
        #             runtime.setNextNode(edge.get_source(), 'UP')
        #             return
        # This was unimplemented because it was too hard to get right
        # at the time of creation, but left in (commented out) to illustrate a use of
        # a configuration item.

        for edge in sorted(unfollowed):
            if not isinstance(edge, Flow):
                """
                For non-flow edges, inject the sources
                which have not yet been executed, to ensure
                that when we follow those edges, the sources
                have been populated.

                IFF all the sources to this node
                have been executed, follow all the un-followed
                edges. Otherwise, let the runtime execute
                the nodes that need to be run first.
                """
                if not edge.source_has_executed():
                    # If we are reacting to an event, ensure that the
                    # upstream being considered is also a candidate
                    # for this event, otherwise, skip it.
                    if not edge.get_source().is_ehc_or_runnable(runtime):
                        continue
                    elif edge.get_source().in_event_path(runtime):
                        # set the nodes "in the event path" aside
                        # so they will be executed after any non-event path
                        # nodes.
                        event_path_node.append(edge.get_source())
                    else:
                        to_run_sources.append(edge.get_source())
                        # runtime.setNextNode(edge.source, 'UP')
                    RunState.s(self).set_preempted()
                    has_unexecuted_sources = True

        for node in event_path_node:
            # add the event path nodes to the end of the list
            # of nodes to execute. Ideally, there would be only
            # one .... But, I don't think this will get called....
            # unless we pre-empt the eventing node/path
            # such that all it's children and their siblings
            # run first. ... somehow
            # runtime.setNextNode(node, 'UP')
            to_run_sources.append(self)

        # Sort the the inbound nodes so that execution order is deterministic
        # Then add them to the runtime.
        # NOTE: This direction may need to be reversed
        for node in sorted(to_run_sources):  # this relies upon nord.nord.Base
            runtime.setNextNode(node, 'UP')

        if not has_unexecuted_sources:
            for edge in sorted(unfollowed):
                if not isinstance(edge, Flow):
                    if not edge.get_source().is_ehc_or_runnable(runtime):
                        continue
                    edge.follow(runtime, verbose)
                # else:
                #     # it is a flow type edge. Flag as followed
                #     edge.flag_followed(runtime)
            if self._unfollowedSources:
                # If we got here, there are sources which appear not to
                # have run next_after_execute to follow all their target edges...
                # this seems to happen when the current node is replaced by another, like when a
                # shoshoni reference swaps the target for itself
                unfollowed = self._unfollowedSources
                for edge in sorted(unfollowed):
                    if not isinstance(edge, Flow):
                        if not edge.follow(runtime, verbose):
                            self.flag_preempted()
                            break

        self.reset_target_edges()
        return

    def get_op_runtime(self):
        """
        Return the runtime set by
        the most recent call to prep_to_execute.
        """
        if hasattr(self, '_current_runtime'):
            return self._current_runtime
        elif hasattr(self, '_run_state'):
            self._current_runtime = RunState.s(self).runtime()
            return self._current_runtime
        else:
            return None

    def follow_unfollowed_targets(self, runtime, verbose=False):
        """
        Follow all the unexecuted target edges
        """
        if not hasattr(self, '_unfollowedTargets'):
            return []

        rv = []

        # unfollowed = self._unfollowedTargets.copy()
        unfollowed = self.get_unfollowed_targets(runtime)
        flows = []
        for edge in sorted(unfollowed):
            if not isinstance(edge, Flow):
                """
                For non-flow edges, inject the targets
                which have not yet been executed

                IFF all the targets from this node
                have been executed, follow all the un-followed
                edges. Otherwise, let the runtime execute
                the nodes that need to be run next.
                """
                if not edge.target_has_executed():
                    edge.follow(runtime, verbose)
                    # Perhaps this needs to be the raw target? rather than apparent...
                    # as the raw target is truly pointed at by the edge...
                    rv.append(edge.get_target())
            else:
                flows.append(edge)

        # But, if there are still TBE flow's, those are followed last
        for edge in flows:
            edge.follow(runtime, verbose)

        return rv

    def schedule_unexec_targets(self, runtime, targets=None):
        """
        Schedule un-executed targets. A shortcut is provided to
        facilitate only evaluating the outbound edges once in
        follow_unfollowed_targets. Targets list should NOT be
        created other than by follow_unfollowed_targets.
        """
        if targets is not None:
            for target in targets:
                runtime.setNextNode(target, 'DOWN')
        else:
            unfollowed = self.get_unfollowed_targets(runtime)
            for edge in sorted(unfollowed):
                if not isinstance(edge, Flow):
                    """
                    For non-flow edges, inject the targets
                    which have not yet been executed
                    """
                    if not edge.target_has_executed():
                        runtime.setNextNode(edge.get_target(), 'DOWN')

    def next_after_execute(self, runtime, verbose, affect_schedule=True):
        """
        Run the post-execution steps on this node.

        This is called after execution, and potentially after
        deferral, to set up the runtime state for the coming call to
        next
        """
        if hasattr(self, '_unfollowedSources'):
            # unfollowed = self._unfollowedSources.copy()
            unfollowed = self.get_unfollowed_sources(runtime)
            for edge in sorted(unfollowed):
                if isinstance(edge, Flow):
                    edge.flag_followed(runtime)

        if not hasattr(self, '_unfollowedTargets'):
            return

        has_unexecuted_targets = False
        # unfollowed = self._unfollowedTargets.copy()
        unfollowed = self.get_unfollowed_targets(runtime)
        for edge in sorted(unfollowed):
            if not isinstance(edge, Flow):
                """
                For non-flow edges, inject the targets
                which have not yet been executed

                IFF all the targets from this node
                have been executed, follow all the un-followed
                edges. Otherwise, let the runtime execute
                the nodes that need to be run next.
                """
                if not edge.target_has_executed() and affect_schedule:
                    runtime.setNextNode(edge.get_target(), 'DOWN')
                    has_unexecuted_targets = True

        # This conditional allows the runtime to loop, by
        # re-setting flow edges which were set as followed
        # by nodes that were reached by other means.
        # which is a solution to the chicken and egg problem
        # flow edges induces. The logic directly below
        # as the continuation of that logic. Meaning,
        # if a Node's out flow had been flagged as
        # followed, it will now increment the counter
        # and reset it's target out edges
        # so the target can be run again.
        # NOTE: THIS LOGIC BELONGS IN FLOW EDGES...
        # has_followed_outflows = False
        # followed = self._followedTargets.copy()
        # for edge in sorted(followed):
        #     if isinstance(edge, Flow):
        #         has_followed_outflows = True
        #         break

        if not has_unexecuted_targets:
            for edge in sorted(unfollowed):
                if not isinstance(edge, Flow):
                    edge.follow(runtime, verbose)

        # But, if there are still TBE flow's, those are followed first
            for edge in sorted(unfollowed):
                if isinstance(edge, Flow):
                    # edge.flag_followed(runtime)
                    edge.follow(runtime, verbose)
                    pass

        # THIS LOOKS WRONG, FLOW TYPE EDGES SHOULD TAKE CARE OF THIS
        # lastly, if we are looping, increment the execution counter
        # and, reset the downstream edges by calling flow.follow
        # if has_followed_outflows:
        #     runtime.incr_exec_count()
        #     for edge in followed:
        #         edge.follow(runtime, verbose)
        #     # Mark this node as executed at the new exec level
        #     self.flag_exe(runtime)

    def has_out_edges(self):
        """
        Return if this node is the source of any edges.

        This checks to see if there are any edges from this
        node that haven't yet been followed (that should have been)
        """
        if hasattr(self, '_unfollowedTargets') and len(self._unfollowedTargets) > 0:
            return True
        elif hasattr(self, '_unfollowedTargets') and len(self._unfollowedTargets) == 0:
            return False

        # if for some reason _unfollowedTargets does not exist
        # return the truthiness of the check for no target rules
        # instead....
        return len(self.get_target_rules()) != 0

    def has_in_edges(self):
        """
        Return if this node is the source of any edges.

        This checks to see if there are any edges from this
        node that haven't yet been followed (that should have been)
        """
        if hasattr(self, '_unfollowedSources') and len(self._unfollowedSources) > 0:
            return True
        elif hasattr(self, '_unfollowedSources') and len(self._unfollowedSources) == 0:
            return False

        # if for some reason _unfollowedTargets does not exist
        # return the truthiness of the check for no target rules
        # instead....
        return len(self.get_target_rules()) != 0

    def get_space(self):
        """Return the imported Base's spacename which should be specific to each extension."""
        return self._spacename

    def handle_visible_request(self, data):
        """
        An overridable method called when the front-end/visualization
        requests node specific information.

        By default, it just returns itself as a dict.
        """
        return self.to_dict(include_contents=True)

    def disconnect(self):
        """
        Completely disconnects this node from others, and, removes
        all the edges.
        """
        for rule in list(self.get_source_rules()):
            for edge in list(self.get_sources(rule)):
                edge.disconnect()

        for rule in list(self.get_target_rules()):
            for edge in list(self.get_targets(rule)):
                edge.disconnect()

        if hasattr(self, '_followedSources'):
            del self._followedSources[:]
        if hasattr(self, '_unfollowedSources'):
            del self._unfollowedSources[:]
        if hasattr(self, '_followedTargets'):
            del self._followedTargets[:]
        if hasattr(self, '_unfollowedTargets'):
            del self._unfollowedTargets[:]

    def delete(self):
        """
        Remove the node from the Graph/Map.

        This will trigger the map change tracker to
        communicate this node as deleted. Unfortunately,
        __del__ is not able to provide this service because
        MapChanges maintains references to all new and changed nodes.
        """
        if self._graph:
            self._graph.ccap_node_deleted(self)

    def to_dict(self, include_contents=False):
        """Return a dictionary of all simple datatype members."""
        rv = {}
        # save attrs allows "_" prefixed variables to be saved to the map file
        # yet, not be processed by any other methods which ignore "_*" attributes.
        for k in (k for k in sorted(self.__dict__.keys())
                  if (not k.startswith("_") or (hasattr(self, "_save_attrs") and
                                                k in self._save_attrs))):
            v = getattr(self, k)
            if type(v) in (str, int, float, bool):
                rv[k] = v
            """
            The visualization member of the object
            is established by Sigurd, and dump_to_dict method is
            defined in callbackmanager.py (the parent to all node visualizations)
            """
            if k == "visualization":
                self.visualization.dump_to_dict(rv)  # pragma: no cover # must be covered by sigurd

        # in case we are operating in a headless fashion (no associated visualization),
        # position is important, and is filtered out as it is stored as a list
        # so, look for it and capture it.
        if hasattr(self, 'position'):
            rv['position'] = self.position

        if self._spacename != "nord":
            rv["space"] = self._spacename

        return rv

    def debug_to_dict(self):
        """Return a dictionary of all simple datatype members."""
        rv = {}

        rv["string"] = repr(self)
        if not hasattr(self, '_unfollowedSources'):
            rv["unfollowed_sources"] = '-'
        else:
            rv["unfollowed_sources"] = str(len(self._unfollowedSources))
        if not hasattr(self, '_unfollowedTargets'):
            rv["unfollowed_targets"] = '-'
        else:
            rv["unfollowed_targets"] = str(len(self._unfollowedTargets))
        if not hasattr(self, '_followedSources'):
            rv["followed_sources"] = '-'
        else:
            rv["followed_sources"] = str(len(self._followedSources))
        if not hasattr(self, '_followedTargets'):
            rv["followed_targets"] = '-'
        else:
            rv["followed_targets"] = str(len(self._followedTargets))

        if self.get_op_runtime():
            rv["rt_exec_cnt"] = self.get_op_runtime().get_exec_count()
        else:
            rv["rt_exec_cnt"] = "??"

        if hasattr(self, '_run_state') and hasattr(self._run_state, '_executed'):
            rv["exec_count"] = self._run_state._executed
        else:
            rv["exec_count"] = "-"

        rv["executed"] = self.has_executed()

        if hasattr(self, 'value'):
            rv["value"] = str(self.value)
        return rv

        for k in sorted(self.__dict__.keys()):
            v = getattr(self, k)
            if type(v) in (str, int, float, bool):
                rv[k] = v
            else:
                rv[k] = str(v)
            """
            The visualization member of the object
            is established by Sigurd, and dump_to_dict method is
            defined in callbackmanager.py (the parent to all node visualizations)
            """
            if k == "visualization":
                self.visualization.dump_to_dict(rv)  # pragma: no cover # must be covered by sigurd

        # in case we are operating in a headless fashion (no associated visualization),
        # position is important, and is filtered out as it is stored as a list
        # so, look for it and capture it.
        if hasattr(self, 'position'):
            rv['position'] = self.position

        if self._spacename != "nord":
            rv["space"] = self._spacename

        return rv

    def render_dbg_text(self, txt, dat):
        """
        Given the dict of dbg info from the backend,
        return a renderable string and whether this node has been executed
        """
        if "value" in dat:
            val = f"\nvalue: {dat['value']}"
        else:
            val = ''
        nus = dat['unfollowed_sources']
        nfs = dat['followed_sources']
        nut = dat['unfollowed_targets']
        nft = dat['followed_targets']
        pfx = ""
        sfx = ""
        if dat["executed"]:
            pfx = "--------------\n"
            sfx = "\n--------------"
        else:
            pfx = ".   .   .   .\n"
            sfx = "\n.   .   .   ."
        txt = f"{pfx}{dat['exec_count']}/{dat['rt_exec_cnt']}  self/rt exe cnt\n{nus}/{nfs}  srcs un/followed\n{nut}/{nft}  tgts un/followed{val}{sfx}\n\n" + txt  # noqa: E501
        return txt, dat["executed"]

    def __str__(self):
        """Stringify the most basic identifying information about the node."""
        rv = ""
        for k in (k for k in sorted(self.__dict__.keys()) if k in ('id', 'name')):
            if rv == "":
                rv = "{}: {}".format(k, self.__dict__[k])
            else:
                rv = "{}, {}: {}".format(rv, k, self.__dict__[k])
        return "Node({}): <{}>".format(type(self).__name__, rv)

    def __repr__(self):
        """Return the full representation of the node."""
        rv = "{}:".format(type(self).__name__)
        for a in (a for a in sorted(self.__dict__.keys())
                  if a not in ('_sourceRules', '_targetRules', '_sourceNodes', '_targetNodes') and
                  not a.startswith('_')):
            rv = "{}\n    .{} = {}".format(rv, a, self.__dict__[a])

        if hasattr(self, "_sourceRules"):
            rv = "{}\n  Sources".format(rv)
            for r in self._sourceRules.keys():
                for e in self._sourceRules[r]:
                    orphaned = ""
                    if not e.been_followed():
                        orphaned = "-!!-"
                    rv = "{}\n     - {} <{}- {}"\
                         .format(rv, type(e).__name__, orphaned, str(e.source))

        if hasattr(self, "_targetRules"):
            rv = "{}\n  Targets".format(rv)
            for r in self._targetRules.keys():
                for e in self._targetRules[r]:
                    rv = "{}\n     - {} -> {}".format(rv, type(e).__name__, str(e.target))

        return rv


sys.modules[__name__] = Node
