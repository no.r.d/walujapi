"""
Copyright 2020 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

The Map is the analog of program, script, module and/or library from other languages.

Map'self are graph'self made up of nodes connected by edges which have rules relating
the types of nodes with the type of edge. Map'self can reference other maps.
"""

from collections import OrderedDict
from datetime import datetime
from nord.sushrut.utils import make_transportable


def merge(change_dict_a, change_dict_b):
    """
    THIS IMPLEMENTATION IS PROBABLY WRONG!
    """
    if change_dict_a is None and change_dict_b is None:
        return {}
    elif change_dict_a is not None and change_dict_b is None:
        return change_dict_a
    elif change_dict_a is None and change_dict_b is not None:
        return change_dict_b
    else:
        if len(change_dict_a) == 0 and len(change_dict_b) == 0:
            return {}
        if len(change_dict_a) == 0:
            return change_dict_b
        if len(change_dict_b) == 0:
            return change_dict_a

    raise Exception("MERGE CHANGES NOT IMPLEMENTED")


class MapChanges():
    def __init__(self, graph):
        # Need to think about re-adding a node or edge after deletion
        # also, the data structure used to manage the changes
        # will need some performance tuning consideration
        self._added_nodes = OrderedDict()
        self._deleted_nodes = OrderedDict()
        self._changed_nodes = OrderedDict()
        self._added_edges = OrderedDict()
        self._deleted_edges = OrderedDict()
        self._list_of_changes = list()
        self.graph = graph

    def add_node(self, node):
        ct = "add_node"
        change = MapChange(ct, node)
        self._added_nodes[node] = change
        self._list_of_changes.append(change)

    def change_node(self, node, attrib, prev, curr, noparse):
        ct = "change_node"
        if node in self._added_nodes:
            """Node is new to this change-set, so, skip tracking changes"""
            return
        change = MapChange(ct, node, attrib, prev, curr, noparse=noparse)
        if node not in self._changed_nodes:
            self._changed_nodes[node] = []
        self._changed_nodes[node].append(change)
        self._list_of_changes.append(change)

    def delete_node(self, node):
        ct = "delete_node"
        change = MapChange(ct, node)
        self._deleted_nodes[node] = change
        self._list_of_changes.append(change)

    def add_edge(self, edge):
        ct = "add_edge"
        change = MapChange(ct, edge)
        self._added_edges[edge] = change
        self._list_of_changes.append(change)

    def delete_edge(self, edge):
        ct = "delete_edge"
        change = MapChange(ct, edge)
        self._deleted_edges[edge] = change
        self._list_of_changes.append(change)

    def get_changes(self, retain_changes=False, guid=None):
        rv = []
        for c in self._list_of_changes:
            x = c.to_dict()
            if guid is not None:
                x["change_src_id"] = guid
            rv.append(x)
        if not retain_changes:
            self._changed_nodes.clear()
            self._deleted_nodes.clear()
            self._deleted_edges.clear()
            self._added_edges.clear()
            self._added_nodes.clear()
            self._list_of_changes.clear()
        return rv

    def has_changes(self):
        return len(self._list_of_changes) > 0


class MapChange():
    """
    a dict (and therefore JSON) serializable
    class used to track the changes to a map
    """
    def __init__(self, change_type, changed_entity, attrib=None, prev=None, curr=None, rem_attr=False, noparse=False):
        self.when = datetime.now()
        self.change_type = change_type
        self.changed_entity = changed_entity
        if attrib:
            self.attribute = attrib
            self.prev = prev
            self.curr = curr
            self.rem_attr = rem_attr
        self.noparse = noparse

    def __repr__(self):
        return f"Map ∆: {self.change_type} [{self.changed_entity}]"

    def to_dict(self):
        rv = {
            "timestamp": self.when.timestamp(),
            "when": self.when.isoformat(),
        }
        if hasattr(self, 'attribute'):
            rv['attribute'] = self.attribute
            if not self.noparse:
                rv["prior"] = make_transportable(self.prev)
                rv['change'] = make_transportable(self.curr)
            else:
                rv["prior"] = self.prev
                rv['change'] = self.curr

            rv['delattr'] = self.rem_attr
        rv["change_type"] = self.change_type
        if self.change_type.endswith('_edge'):
            rv["item_id"] = (self.changed_entity.source.id,
                             self.changed_entity.target.id,
                             self.changed_entity.rule)
        else:
            rv["item_id"] = self.changed_entity.id
        if self.change_type.startswith('add') or self.change_type.startswith('delete'):
            rv["entity"] = self.changed_entity.to_dict()
        return rv
