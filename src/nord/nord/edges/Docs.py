"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

import nord.nord.Edge as Edge
import sys


class Docs(Edge):
    """
    Docs is the connection between vertices which is connects a comment to a Node

    >>> Docs() #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE
    Docs: [None]  --Docs-->  [None]
    """
    def traverse(s, srule, trule):  # pragma: no cover
        """Do nothing at all. Not called, as follow is overridden"""
        return True

    def follow(s, runtime, verbose=False):
        """
        Overrides the edge follow so as not to do anything at all
        as Docs is used to connect comments to other Nodes. Comments should do nothing at all
        """
        s.flag_followed(runtime)


sys.modules[__name__] = Docs
