"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

import nord.nord.Edge as Edge
import sys


class Flow(Edge):
    """
    Flow is the connection between vertices that controls the execution of the
    map. At each step in the flow all the target's non-flow relationships are
    evaluated and potentially executed.

    >>> Flow() #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE
    Flow: [None]  --Flow-->  [None]
    """
    def follow(s, runtime, verbose=False):
        """
        Flow is a very generic edge. It requires no Rules since it just moves the runtime
        from one node to the next, regardless of the node subclass
        """
        if verbose:
            print("   ---> Flow following: {}".format(s))

        if not hasattr(s, '_downpath'):
            downpath, cycle_to_me = s.detect_cycle(s.get_target(), path=[])
            if not cycle_to_me:
                s._downpath = [s.get_target()]
            else:
                s._downpath = downpath

        for node in s._downpath:
            node.reset_target_edges(clear_exe=True)
            node.clear_exe(runtime)

        # When looping via a conditional, if one branch does not
        # does not return to the source conditional, then
        # we need to mark the source as executed. Else, it can be
        # run again, causing an infinite loop
        if len(s._downpath) == 1:
            s.get_source().flag_exe(runtime)

        runtime.setNextNode(s.get_target(), 'DOWN')
        return

        """
        NOTE: This premature return may not be fully tested.... It's been a long time since
        this change has been looked at.
        """

        s.get_target().reset_target_edges(recurse=True, clear_exe=True)
        runtime.setNextNode(s.target, 'DOWN')
        # The flow edges should increment the exec_counter, not the node
        runtime.incr_exec_count()
        s.get_source().flag_exe(runtime)

    def detect_cycle(self, start, path=[]):
        """
        Develop a list of all downstream nodes, and, if a cycle to the starting point
        is detected return True with the list of nodes.
        """
        if path and start == path[0]:
            return path, True
        elif path and start in path:
            return path, False
        path.append(start)
        rv = False
        for rule in list(start.get_target_rules()):
            for edge in list(start.get_targets(rule)):
                _, cycle = self.detect_cycle(edge.get_target(), path)
                if cycle:
                    rv = True
        return path, rv


sys.modules[__name__] = Flow
