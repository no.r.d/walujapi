"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

import nord.nord.Edge as Edge
import nord.nord.Property as Property
import sys


class Pass(Edge):
    """
    Pass is the connection between vertices which is used send the source node
    to the target for the target to operate upon or manipulate

    >>> Pass() #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE
    Pass: [None]  --Pass-->  [None]
    """
    def __init__(self, graph):
        super().__init__(graph)
        self._properties = {
            'ord_name': Property(self, 'ord_name', default=None, description=_("""The name of the argument in the target to which the source is passed""")),
            'ord_pos': Property(self, 'ord_pos', default=None, description=_("""The numeric position of the argument in the target to which the source is passed"""))
        }


sys.modules[__name__] = Pass
