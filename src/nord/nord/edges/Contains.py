"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--
Edges of this type indicate that the target node is contained within the source.
"""
import nord.nord.Edge as Edge
import nord.nord.utils.SpaceManager as use
from nord.nord.exceptions import RuntimeException as RuntimeException
from nord.nord.exceptions import UseException as UseException
import sys


class Contains(Edge):
    """
    The edge type whose relationship of one node within another.

    Contains is the connection between vertices which indicates if a Node is within
    another node.

    >>> Contains() #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE
    Contains: [None]  --Contains-->  [None]
    """

    def follow(self, runtime, verbose=False):
        """
        Following a contains edge instantiates the rule for the target vertex type.

        The container overrides the base Edge follow method because
        the container is always the cargo passed to the target
        which, generally, will not be used by the target
        """
        if verbose:
            print("   ---> following (contained) : {}".format(self))

        working_space = use.current_space()
        tgt_modname = "{}{}".format(type(self).__name__, type(self.target).__name__)
        if hasattr(self.target, 'space'):
            provider = self.target.space
        elif hasattr(self.target, '_spacename'):
            provider = self.target._spacename
        else:
            provider = 'nord'
        use.space(provider)
        mod = use.module('nord.{}.rules.{}'.format(provider, tgt_modname))
        if mod is not None:

            trule = mod(self.target, runtime, self)

        else:
            raise RuntimeException(  # pragma: nocover Contains_test.test_follow_exception hits this
                "Executing node: {} - No tgt rule execution source module named '{}.rules.{}' available"  # noqa: E501
                .format(str(runtime.current), provider, tgt_modname))

        srule = None
        src_modname = "{}{}".format(type(self.source).__name__, type(self).__name__)
        if not src_modname.startswith('Container'):
            # We want to skip src rule module detection
            # for Containers
            if hasattr(self.target, 'space'):
                provider = self.target.space
            elif hasattr(self.target, '_spacename'):
                provider = self.target._spacename
            else:
                provider = 'nord'
            use.space(provider)
            try:
                mod = use.module('nord.{}.rules.{}'.format(provider, src_modname))
                if mod is not None:

                    srule = mod(self.target, runtime, self)
            except UseException:
                pass

        rv = True
        if srule is not None:
            rv = srule.apply()
        else:
            self.set_cargo(self.source)
        if rv:
            rv = trule.apply()

        use.space(working_space)

        if rv:
            self.flag_followed(runtime)
        return rv


sys.modules[__name__] = Contains
