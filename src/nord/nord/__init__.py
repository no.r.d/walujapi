"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

:mod:`nord` -- No Round Data
============================

.. module: nord
   :platform: Unix, Windows, Android
   :synopsis: Environment which provides users the ability to instruct the system using a graph or map.
.. moduleauthor:: Nate Byrnes <nate@qabal.org>


"""
# Base types
__all__ = ['Config',  # noqa: F405
           'Edge',
           'Map',
           'Node',
           'InstanceOf',
           'Property',
           'Rule',
           'Runtime',
           'exceptions',
           'nodes',
           'rules',
           'edges',
           'utils',
           'version']
# from nord.Edge import Edge
# import nord.nord.Map as Map
# import nord.nord.Runtime as Runtime

# Node sub types
# from nord.nodes import *

# Edge sub types
# from nord.edges import *

# Rule sub types ... really the implementation of the logic of NoRD
# from nord.rules import *

# Bring in all the utils modules
# from nord.nord.utils import *

import nord.nord.Strings  # noqa: F402, F401
from nord.nord.nodes import *  # noqa: E402, F403
