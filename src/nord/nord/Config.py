"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--
Configuration management for the runtime.
"""

import nord.nord.utils.SpaceManager as use

max_path_length = 1000
max_in_events = 10000
max_out_events = 10000


def add_to_file_path(uri):
    """Add to the current space's file and path searchpaths."""
    use.add_location(uri, 'files')
    use.add_location(uri, 'path')


def add_to_code_path(uri):
    """Add to the current space's code path."""
    use.add_location(uri, 'code')


class ConfigItem():
    """
    Configuration Item objects provide the means
    to establish, update and retrieve configuration
    data.

    The life cycle of a configuration item is as follows:
    1) Declare - The class instance interested in being configured
        create's an instance of a ConfigItem and adds it to itself via the
        inherited Configurable methods
    """
    def __init__(self, name, value, space="nord"):
        global Config
        self.name = name
        self.default_value = value
        self.type = str(type(self.default_value))
        self.space = space
        self.model = "Unknown"
        self.tags = ["advanced"]  # of advanced and basic
        self.visibility = ["map"]  # of map, node, and edge
        self.render = ["dynamic"]  # dynamic, decoration, and node
        self.override_by = ["none"]  # map, parentmap, node and edge
        self.value = None

    def get_value(self):
        if self.value is None:
            return self.default_value
        else:
            return self.value

    def set_value(self, value):
        if str(type(value)) == str(type(self.default_value)):
            self.value = value
        else:
            raise AttributeError("IMPLEMENTATION ERROR: "
                                 f"Incompatible config value attempted '{type(value)}' for {self.name}")


class Configurable():
    """
    Provide standardized methods to subclasses in order
    to standardize and expose access to configuration
    variables.

    NOTE: This approach feels like it might not be optimal,
    or, even needed. It could be possible to take advantage
    of the naming convention and python's introspection
    ability, and dispense with the methods and class hierarchy.
    """
    def extract_config_items(self):
        """
        Given an instance of a Configurable,
        extract all the _CI_* class variables
        and establish them as the default
        values for Configuration Items
        belonging to this instance.
        """
        if not hasattr(self, '_configuration_values'):
            self._configuration_values = {}

        for itmname in dir(self.__class__):
            if itmname.startswith('_CI_'):
                name = itmname.replace('_CI_', '')
                self.set_cfg_value(name, getattr(self, itmname), initialize=True)

    def load_config(self, indict={}):
        raise Exception("NOT IMPLEMENTED")
        if "^config" not in indict:
            return

    def save_config(self, outdict):
        outdict["^config"] = {}
        raise Exception("NOT IMPLEMENTED")

    def get_cfg_value(self, name, space=None, map=None, runtime=None, search=True):
        if not hasattr(self, '_configuration_values'):
            self.extract_config_items()
        if name in self._configuration_values:
            return self._configuration_values[name].get_value()
        if search:
            return Config.get_cfg_value(name, space, map, runtime)
        else:
            return None

    def set_cfg_value(self, name, value, initialize=False):
        if not hasattr(self, '_configuration_values'):
            if not hasattr(self, "space"):
                space = "nord"
            else:
                space = self.space
            self.extract_config_items(space)
        if name not in self._configuration_values and not initialize:
            raise AttributeError(f"IMPLEMENTATION ERROR: Config::{name} is unknown")
        elif name not in self._configuration_values:
            if not hasattr(self, "space"):
                space = "nord"
            else:
                space = self.space
            self._configuration_values[name] = ConfigItem(name, value, space=space)
        else:
            self._configuration_values[name].set_value(value)

    def set_cfg_item(self, name, ci):
        pass


class Configuration():
    """
    Configuration is a class which exposes runtime affectable
    configuration. This is intended to be different than
    the *.conf files in that the .conf files are intended to be distrubution
    and release managed, whereas Configuration is intended to expose
    choices to the user. Certainly there will be some overlap between
    these functionalities. It could even be that the Configuration class
    will come to operate upon the things in the .conf files...

    Eventually, Configuration should replace the globalVars used by Sigurd
    as well.
    """
    def __init__(self):
        self.maps = {}
        self.universal = {}
        self.spaces = {}
        self.global_config = Configurable()

    def populate_from_map(self, inmap):
        """
        Given a map, extract and organize the configurables
        from the nodes, edges, and any included maps.

        Raise exceptions on any config-key redundancy.
        """
        raise Exception("NOT IMPLEMENTED")

    def populate_from_space(self, space):
        """
        Load the configuration variables defined by a space
        at the space (aka extension) level.
        """
        raise Exception("NOT IMPLEMENTED")

    def get_cfg_value(self, name, space, graph, runtime):
        if graph in self.maps:
            return self.maps[graph].get_cfg_value(name, search=False)
        if space in self.spaces:
            return self.spaces[space].get_cfg_value(name, space, search=False)
        return self.global_config.get_cfg_value(name, search=False)

    def set_cfg_value(self, space, name, ci):
        if space not in self.spaces:
            self.spaces[space] = Configurable()
            self.spaces[space].set_cfg_item(name, ci)


Config = Configuration()
