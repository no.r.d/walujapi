"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

import nord.nord.Rule as Rule
import nord.nord.utils.SpaceManager as use
import nord.nord.Map as Map
import json
import sys


class GraphUse(Rule):
    """
    The ContainerUse rule adds the nodes within the source container
    to the "namespace" of the target container
    """

    def apply(s):
        """
        Adds the source container's contents to the target containers namespace

        """
        karte = Map(json.load(use.file(s.edge.get_source().get_property('mapfile'))))
        s.edge.set_cargo(karte)
        return True


sys.modules[__name__] = GraphUse
