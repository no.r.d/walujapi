"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

import nord.nord.Rule as Rule
from nord.nord.exceptions import RuntimeException as RuntimeException
import sys


class DataPass(Rule):
    """
    DataPass is the subclass of Rule which calls a Data Node's get method to retrieve
    the Data Node's value and place it on the edge for delivery

    >>> DataPass(None, None, None) #doctest: +ELLIPSIS
    <__main__.DataPass object at 0x...>
    """

    def apply(s):
        """
        Data Nodes place it's value on the edge for delivery to the target
        """
        if hasattr(s.edge.get_source(), 'get'):
            s.edge.set_cargo(s.edge.get_source().get())
            return True
        elif hasattr(s.edge.get_source(), 'value'):
            s.edge.set_cargo(s.edge.get_source().value)
            return True
        else:
            raise RuntimeException(_("Data asked to pass, "  # noqa: F821
                                     "but has nothing to pass: {}").format(s.edge.get_source()))


sys.modules[__name__] = DataPass
