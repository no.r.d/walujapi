"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

import nord.nord.Rule as Rule
from nord.nord.exceptions import RuntimeException as RuntimeException
import sys


class ContainerPass(Rule):
    """
    The ContainerPass rule adds the nodes within the source container
    to the "namespace" of the target container
    """

    def apply(s):
        """
        Adds the source container's contents to the target containers namespace

        """
        #s.edge.set_cargo( s.edge.get_source().cargo )
        # this would be set by the Return Node

        if s.edge.has_cargo():
            s.edge.set_cargo( s.edge.get_cargo() )
            return True
        else:
            raise RuntimeException(_("Container asked to pass, but has nothing to pass: {}").format(s.edge.get_source()))


sys.modules[__name__] = ContainerPass
