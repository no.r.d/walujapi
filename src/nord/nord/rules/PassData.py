"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

import nord.nord.Rule as Rule
from nord.nord.exceptions import RuntimeException as RuntimeException
import sys


class PassData(Rule):
    """
    The PassData rule
    """

    def apply(s):
        """
        Takes the cargo off the edge and adds it to the target builtin's
        set of arguments.
        """
        if s.edge.has_cargo():
            s.edge.get_target().set(s.edge.get_cargo())
            return True
        else:
            raise RuntimeException(_("Builtin target of pass,"  # noqa: F821
                                     f" but nothing passed: {s.edge.get_target()}"))


sys.modules[__name__] = PassData
