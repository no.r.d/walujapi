"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

import nord.nord.Rule as Rule
from nord.nord.exceptions import RuntimeException as RuntimeException
import sys


class BuiltinPass(Rule):
    """
    The BuiltinPass rule adds assigns the result of the
    builtin function to the edge cargo.
    """

    def apply(s):
        """
        Set's the result of the builtin function
        to the edge's cargo.
        """
        if hasattr(s.edge.get_source(), '_returned_value'):
            s.edge.set_cargo(s.edge.get_source()._returned_value)
            return True
        else:
            raise RuntimeException(_("Builtin asked to assign, "  # noqa: F821
                                     "but has nothing to assign: {}").format(s.edge.get_source()))


sys.modules[__name__] = BuiltinPass
