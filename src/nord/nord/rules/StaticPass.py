"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

import nord.nord.Rule as Rule
from nord.nord.exceptions import RuntimeException as RuntimeException
import sys


class StaticPass(Rule):
    """
    StaticPass is the subclass of Rule which calls a Data Node's Set method to assign
    the value from the source

    Every edge should be executable via two rules
    If An edge looks something like:
    Node(Data) <--Edge(Assign)-- Node(Static)

    Then there should be the following rules:

    DataAssign
    AssignStatic

    Then, each edge will evaluate the nodes belonging to it
    and dynamically call the rule modules, should they exist.


    >>> StaticPass(None, None, None) #doctest: +ELLIPSIS
    <__main__.StaticPass object at 0x...>
    """

    def apply(s):
        """
        Static Nodes can only ever be sources. So, all a StaticPass rule can do is
        place it's cargo on the edge for delivery to the target
        """
        if hasattr(s.edge.get_source(), 'value'):
            s.edge.set_cargo( s.edge.get_source().get() )
            return True
        else:
            raise RuntimeException(_("Static asked to pass, but has nothing to pass: {}").format(s.edge.get_source()))


sys.modules[__name__] = StaticPass
