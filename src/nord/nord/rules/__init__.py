"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

__all__ = ['AssignData', 'CommentDocs', 'ContainerAssign',
           'ContainerContains', 'ContainerPass',
           'ContainerUse', 'ContainsAnomaly', 'ContainsBuiltin',
           'ContainsComment', 'ContainsContainer', 'ContainsData',
           'ContainsReference', 'ContainsReturn', 'ContainsStatic',
           'DataAssign', 'DataPass', 'ExternalUse', 'GraphUse', 'PassBuiltin',
           'PassContainer', 'PassReference', 'PassReturn', 'BuiltinPass',
           'BuiltinAssign', 'ReferenceAssign', 'ReferencePass', 'StaticAssign',
           'StaticPass', 'UseContainer']
