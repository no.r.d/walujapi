"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

import nord.nord.Rule as Rule
import sys


class AssignData(Rule):
    """
    AssignData is the subclass of Rule which calls a Data Node's Set method to assign
    the value from the source

    >>> AssignData(None, None, None) #doctest: +ELLIPSIS
    <__main__.AssignData object at 0x...>
    """

    def apply(s):
        """
        Performs the assignment. Takes the edge's cargo and calls the Data's set
        method with that value as the argument

        In this test, AssignData is called by e.follow as the edge is of type assign
        and its target is of type Data. This test also tests the DataAssign apply method
        as well as a simple Edge.follow()

        >>> from ..nodes.Data import Data
        >>> from ..Runtime import Runtime
        >>> from ..edges.Assign import Assign
        >>> Na = Data()
        >>> Na.set("Larry")
        >>> Nb = Data()
        >>> rt = Runtime()
        >>> e = Assign()
        >>> e.set_nodes(Na, Nb, "RuleName")
        >>> e.follow(rt)
        >>> Nb.get() #doctest: +ELLIPSIS
        'Larry'

        """
        if hasattr(s.edge.get_target(), "set"):
            s.edge.get_target().set(s.edge.get_cargo())
            return True
        else:  # pragma: no cover ## So far the only way I've found to test
            # this is to put a deliberately incorrect rule module inplace.
            raise Exception(
                "Attempted to assign a value to a non-assignable node: {} -> {}"
                .format(str(s.source),
                        str(s.target)))


sys.modules[__name__] = AssignData
