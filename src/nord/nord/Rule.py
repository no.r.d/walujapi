"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

Rules implement the nodetype logic for each edge end.

Rules can be considered the third core concept of the Nord graphical
language. Nodes have edges connecting them, Edges have rules specific to
the types of nodes they are connecting.
"""

import sys
import nord.nord.Base as Base


class Rule(Base):
    """
    Rules constrain the relationships between Nodes and Edges.

    Subclasses of Rule are used to implement the behavior
    when executing a map (which is a graph built with NoRD semantics)

    >>> Rule(None, None, None) #doctest: +ELLIPSIS
    <__main__.Rule object at 0x...>
    """

    def __init__(self, node, runtime, edge):
        """Basic constructer for rules."""
        self.node = node
        self.edge = edge
        self.runtime = runtime


sys.modules[__name__] = Rule
