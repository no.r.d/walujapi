"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

The heart of Nord's execution engine.

The Runtime executes frames. Frames are stacked. A Frame
has a list of nodes to execute. The runtime is steppable.
"""

import nord.nord.Config as config
import nord.nord.RunState as RS
import nord.nord.RuntimeStateMachine as RSM
import nord.nord.Frame as Frame
import sys
import asyncio
import time
from collections import deque
from collections.abc import Iterable
import nord.pasion.managers.sessionmanager as sm
from nord.nord.utils.Debugger import tb
import nord.nord.version as version


class Runtime(object):
    """
    Runtimes are the base class which facilitates the execution of the program.

    Runtimes control the execution of a map by following the Flow edges, and
    executing rules on nodes as the edges connecting the nodes expect.

    The Runtime keeps track of which edges it has followed.

    >>> Runtime() #doctest: +ELLIPSIS
    <__main__.Runtime object at 0x...>
    """

    def __init__(self, raise_python_exc=False):
        """Initialize the runtime."""
        self.current = None
        self.debug = False
        self.breakpoint_detected = False
        self.stack = list()
        self.stackPos = 0
        self.input_requests = {}
        self.input_provided = {}
        self.output_to_send = {}
        self.econsession = sm.get_current_session()
        self._exec_count = 0
        self.verbose = False
        self.raise_anomaly_as_pyexc = raise_python_exc

        """
        The *events queue's are used to
        buffer events requested by front end user's
        and the responses in order until the
        appropriate time to process them.

        in_events are used to drive node execution
        once the current map iteration has completed
        (presumably, the same _exec_count)

        The _in_event item indicates the ID of the node which triggered the current
        execution.
        """
        self.in_events = deque(maxlen=config.max_in_events)
        self.out_events = deque(maxlen=config.max_out_events)
        self._in_event = None

    def get_heading(self):
        """
        Return from which direction the current node was proposed for execution.

        So, DOWN means that a source node to the current one, submitted this
        node for execution. UP means that a target of this node submitted this node for execution.
        """
        if self.current:
            return self.current.get_current_direction()
        return 'DOWN'

    def add_in_event(self, event):
        """
        append the new event to the queue
        of inbound events.
        """
        self.in_events.append(event)
        self.stopped = False
        self.paused = True

    def next_from_in_events(self):
        """
        Set up this runtime instance to
        next run the requested node in the event.

        In the event of an error, stop the runtime
        by returning True
        """
        if len(self.in_events) == 0:
            return False
        event = self.in_events.pop()
        if 'node_id' not in event:
            self.anomaly = Exception('Runtime Error: malformed event')
            return True
        if event['node_id'] not in self.map.nodes:
            self.anomaly = Exception('Runtime Error: unknown node')
            return True
        node = self.map.nodes[event['node_id']]
        if not node.handles_events():
            self.anomaly = Exception('Runtime Error: event sent to node which cannot process it.')
            return True
        self._in_event = event['node_id']
        return node.handle_event(self, event)

    def is_handling_event(self):
        """
        enable callers to tell if the runtime is in event handling mode.
        """
        return self._in_event is not None

    def has_pending_event(self):
        return len(self.in_events) > 0

    def in_this_node_event(self, node):
        """
        Help nodes determine if the runtime is processing an
        event specific to them.
        """
        if self._in_event and self._in_event == node.id:
            return True
        else:
            return False

    def in_event_path(self, node):
        """
        Detect if the given node has been annotated as
        downstream of the current event node, thus
        being in the current node's event path.
        """
        if not hasattr(self, '_in_event'):
            return False
        else:
            # check if the node's event path annotation includes the event node id
            return RS.s(node).downstream_of_eventor(self._in_event)

    def in_any_event_path(self, node):
        """
        Detect if the given node has been annotated as
        downstream of the any event node.
        """
        if not hasattr(self, '_in_event'):
            return False
        else:
            # check if the node's event path annotation includes the event node id
            return RS.s(node).has_eventor_upstream()

    def get_exec_count(self):
        return self._exec_count

    def incr_exec_count(self):
        self._exec_count += 1

    def set_input(self, nodeid, content):
        """Cache the provided content for the node with provided id."""
        if nodeid in self.map.nodes:
            if nodeid not in self.input_provided:
                self.input_provided[nodeid] = deque()
            self.input_provided[nodeid].insert(0, content)
            if nodeid in self.input_requests:
                del self.input_requests[nodeid]

    def has_input(self, nodeid):
        """Check to see if there is input available for the requested node."""
        return nodeid in self.input_provided and len(self.input_provided[nodeid]) > 0

    def get_input(self, nodeid):
        """Return the earliest input for the requesting node."""
        if nodeid in self.input_provided:
            if len(self.input_provided[nodeid]) > 0:
                rv = self.input_provided[nodeid].pop()
                if len(self.input_provided[nodeid]) == 0:
                    del self.input_provided[nodeid]
                return rv
            else:
                return None
        else:
            self.input_requests[nodeid] = True
            return None

    def wants_input(self, nodeid):
        """Check to see if the node has requested input."""
        if nodeid in self.input_requests:
            return True
        else:
            return False

    def input_wanters(self):
        """Return the list of nodes waiting for input."""
        return [x for x in list(self.input_requests.keys()) if x not in self.input_provided or self.input_provided[x]]

    def set_output(self, nodeid, content):
        """Cache the provided content for the node with provided id."""
        if nodeid in self.map.nodes:
            if nodeid not in self.output_to_send:
                self.output_to_send[nodeid] = list()
            self.output_to_send[nodeid].insert(0, content)

    def get_output(self, nodeid):
        """Return the earliest input for the requesting node."""
        if nodeid in self.output_to_send and self.output_to_send[nodeid]:
            rv = self.output_to_send[nodeid].pop()
            if len(self.output_to_send[nodeid]) == 0:
                del self.output_to_send[nodeid]
            return rv
        else:
            return None

    def has_output(self, nodeid):
        """Check to see if there is output available for the requested node."""
        return nodeid in self.output_to_send and len(self.output_to_send[nodeid]) > 0

    def get_outputters(self):
        """Return node id's for all nodes with output."""
        rv = list(self.output_to_send.keys())
        return rv

    def add_out_event(self, event):
        if 'node_id' not in event:
            self.anomaly = Exception('Runtime Error: malformed out event: missing node_id')
            return
        if 'event_type' not in event:
            self.anomaly = Exception('Runtime Error: malformed out event: missing type')
            return
        if 'payload' not in event:
            self.anomaly = Exception('Runtime Error: malformed out event: missing payload')
            return

        self.out_events.append(event)

    def get_out_events(self):
        if len(self.out_events) == 0:
            return []
        rv = list(self.out_events)
        self.out_events.clear()
        return rv

    def initialize(self, inmap, verbose=False):
        """The first container node (typically Main) in a Map initializes the runtime."""
        self.map = inmap
        self.paused = False
        self.path = deque(maxlen=config.max_path_length)
        self.anomaly = None
        self.stopped = False
        self.verbose = verbose
        self._exec_count = 0
        """
        To enhance testing capabilities,
        clear out current should it be there.
        """
        if hasattr(self, "current"):
            if self.current is not None:
                del self.current
            self.current = None
        # self.prep_top_of_stack()

    def setNextNode(self, nxt, direction):
        """Set the passed node to be the next executed by the runtime."""
        if self.current is None:
            self.current = Frame()
        self.current.inject_node(nxt, direction)

    # def pushNextNode(self, nxt):
        # """
        # """
        # self.nextNode.insert(0, nxt)

    def appendNextNode(self, nxt, direction):
        """Add passed node te the end of the current frame's list."""
        if self.current is None:
            self.current = Frame()
            self.current.inject_node(nxt, direction)
        else:
            self.current.append_node(nxt, direction)

    def topframe_append_node(self, nxt, direction):
        """Add passed node to the top frame's list of nodes."""
        self.stack[-1].append_node(nxt, direction)
        if not self.stack[-1].has_started():
            self.stack[-1].set_next()

    def replace_current_node(self, nxt, direction='DOWN'):
        """Replace the current frame pointer node with the provided one."""
        if self.current is None:
            self.current = Frame()
        self.current.replace_current(nxt, direction)

    def setReturn(self, retval):
        """
        Set the return value.

        This is deprecated and need be removed along with it's test code.
        """
        self.current.set_return(retval)

    def setAnomaly(self, exc):
        """TDB."""
        self.anomaly = exc

    def frame_next(self):
        """
        Execute all the non-Flow edge-rules affecting this node (recursively if need be).

        First all the non-flow rules for edges targeting this node are assembled and initialized

        Then all the non-flow rules for edges leaving this node are assembled and initialized

        Then, all the source rules are executed, followed by all the target rules.
        **NOTE** There should probably be a means of ordering which rules are
                 executed in what order for both sources and targets


        """
        try:
            # self.export_as_graphviz(directory="/tmp/nord_dbg")
            # self.export_as_graphviz()
            rv, state = self.current.run_node(self)
        except Exception as e:
            print(f"{self.current.current} threw exception:\n{e}")

            last_type, last_value, exc_traceback = sys.exc_info()
            tb(exc_traceback, last_type, last_value)

            self.anomaly = e
            self.stopped = True
            return False, RSM.ANOMALY

        # check to see if the stack has grown any
        # If so, process that before continuing the current Node
        if rv and len(self.stack) > self.stackPos:
            self.prep_top_of_stack()
            return rv, state

        if self.stack and self.current.done():
            # we're still in the stack someplace, but, should be done
            # with this frame, so, on the next iteration, drop back a level
            self.frame_finished()
        elif not self.stack and self.current.done():  # pragma: nocover ... reachable???
            # we're out of stack and out of edges to follow, so, we're done..
            # self.prep_next()
            self.current = None
        # elif self.current.has_out_edges():
        #    self.current = tmp_current

        return rv, state

    def runstate(self):
        """Helper method to return the runstate interrogator."""
        return RS

        ############################

        # TODO: anomaly and state setting as well
        # as how those constructs / concepts interact or are implemented by the runtime

        ############################

    def pushStack(self):

        """
        Append the current frame'self state, and a new list of next nodes to the stack.

        This prepares the stack to move into a new container such that
        the runtime can get back to the current state when this frame
        is completed.
        """
        self.current.set_deferred()
        nf = Frame()
        # nf.append_node(startNode)
        self.stack.append(nf)

    def prep_top_of_stack(self):
        """Ensure that the state is set up to run the topmost layer of the stack."""
        self.stackPos = len(self.stack) - 1
        self.current = self.stack[-1]

    def set_frame_return(self, val):
        """
        Set the current Frame's return value (presently ignored)
        AND make the frame as done
        """
        if self.current:
            self.current.set_return(val)
            self.current.set_done()

    def frame_finished(self):
        """Upon completion of a frame, deal with the stack as needed."""
        if not self.stack:
            self.current = None
            return
        else:
            last_rv = self.stack[-1].get_return()
            del self.stack[-1]
            if self.stack:
                self.stackPos = len(self.stack) - 1
                self.current = self.stack[-1]
                self.current.set_return(last_rv)
                # Frame return values are redundant with Return Edges

    def keep_nexting(self, skipbreakpoint=False):
        """
        Idea is that breakpoints should only stop the runtime if in debug mode.

        if there are no breakpoints, and in debug mode, do not iterate the runtime.
        """
        rv = True
        if self.debug:
            if self.breakpoint_detected:
                if self.current and \
                        self.current.current and \
                        self.current.current.breakpoint_set() and \
                        not skipbreakpoint:
                    print(f"Breakpoint hit in: {self.current.current}")
                    rv = False
            else:
                rv = False
        if self.stopped or self.paused:
            rv = False
        return rv

    def begin(self, start=None, debug=False):
        """
        Begin will start the execution of the map.

        If the input node is iterable, inject all
        the nodes in the list.

        If in debug mode, look for a breakpoint.
        If there is one, set runtime flag such that
        we will begin execution all the way to the breakpoint
        """
        if debug:
            # detect any set breakpoints
            all_reviewed = True
            for k, n in self.map.nodes.items():
                if n.breakpoint_set():
                    all_reviewed = False
                    self.breakpoint_detected = True
                    break

            if all_reviewed:
                self.breakpoint_detected = False

        self.current = Frame()
        if start is None:
            self.map.detect_start_node()
            node = self.map.start_node
        else:
            node = start
        if isinstance(node, Iterable):
            for n in node:
                self.current.append_node(n, 'DOWN')
        else:
            self.current.inject_node(node, 'DOWN')
        self.stack.append(self.current)
        self.current.set_next()
        self.debug = debug
        self.stopped = False
        self.paused = False

        while self.keep_nexting() and\
                len(self.input_wanters()) == 0:
            sleeptime = self.next()
            time.sleep(sleeptime)

        RS.s(self.map).set_walujapi_once_through()

    async def continue_async(self):
        """Resume execution of the map."""
        # when continuing, we need to ignore the breakpoint at which we were stopped
        skip_bkp = True
        # we also need to pretend that a breakpoint has been
        # found such that the keep_nexting logic
        # will permit continuation, in the event there was not a breakpoint
        self.breakpoint_detected = True
        # it might be better to incorporate the breakpoint logic into RSM ....
        while self.keep_nexting(skipbreakpoint=skip_bkp):
            skip_bkp = False
            sleeptime = self.next()
            await asyncio.sleep(sleeptime)
        RS.s(self.map).set_walujapi_once_through()

    def continue_exec(self):
        """Resume execution of the map."""
        skip_bkp = True
        self.breakpoint_detected = True
        while self.keep_nexting(skipbreakpoint=skip_bkp) and\
                len(self.input_wanters()) == 0:
            skip_bkp = False
            sleeptime = self.next()
            time.sleep(sleeptime)
        RS.s(self.map).set_walujapi_once_through()

    async def begin_async(self, start=None, debug=False):
        """Begin will start the execution of the map."""
        self.current = Frame()
        if start is None:
            self.map.detect_start_node()
            node = self.map.start_node
        else:
            node = start
        if isinstance(node, Iterable):
            for n in node:
                self.current.append_node(n, 'DOWN')
        else:
            self.current.inject_node(node, 'DOWN')
        self.stack.append(self.current)
        self.current.set_next()
        self.debug = debug

        while self.keep_nexting():
            sleeptime = self.next()
            await asyncio.sleep(sleeptime)
        RS.s(self.map).set_walujapi_once_through()

    def next(self):
        """
        Perform the next step in the execution flow.

        Next will evaluate the current condition of the current node, and flow to the next node
        as indicated, or indicate completion of the map, or if an anomaly has occurred set the
        anomaly condition and indicate the map has terminated

        If the runtime is not paused or in debug mode, next will call the next node in the flow
        """
        # if current is not set, check to see if there are inbound events, and if the graph has
        # run through it's warmup run.
        if self.current is None:
            self.incr_exec_count()
            if not RS.s(self.map).walujapi_once_through() and self.in_events:
                # If this map has not been flagged as executed
                # find any and all possible start nodes that have
                # not been run yet, and queue them for execution.
                startable_nodes, found_any = self.map.get_sourceless_nodes()
                for node in startable_nodes:
                    self.setNextNode(node, 'DOWN')
                self.stopped = False
                RS.s(self.map).set_walujapi_once_through()
                return 0.0

            # That is, unless there are some events to process
            if len(self.in_events) and self._exec_count:
                if self.next_from_in_events():
                    self.stopped = True
            else:
                self.stopped = True

            return 0.0

        # This will bill the user
        # if a session is established, and
        # in the case of insufficient funds, will sleep
        # until funds come available.
        if not sm.consumesw(1.0) or not sm.consumehw(1.0):
            self.setAnomaly(Exception("Insufficient Funds"))
            return 5.0










        direction = self.get_heading()
        path_current = self.current.current
        deferred, rsm_state = self.frame_next()







        # if self.current.is_blocked():
        #     # If run_node returns true, it is still blocked, so return, else, continue
        #     if self.current.run_node(self):
        #         self.path.append((self.current.current, 'BLOCKED', '---'))
        #         return 0.0
        #     else:
        #         # the node was blocked, but completed...
        #         self.current.set_next()

        # path_current = self.current.current
        # # presently the current frame prep_next is a no-op
        # self.current.prep_next()
        # # frame next will execute the next node in the frame
        # deferred = self.frame_next()








        # add the current node to the list of traversed nodes, and
        # removes oldest elements on the path according to the configuration
        # NOTE: It is not presently known if all Nodes
        # will make in onto this path, see Frame.set_next
        if path_current:
            status = ''
            if deferred:
                status = 'DEFERRED'
            if self.anomaly:
                status += ' ANOMALY'
            if RS.s(path_current).preempted():
                status += ' PREEMPTED'
            if RS.s(path_current).executed():
                status += ' EXECUTED'
            if rsm_state == RSM.DONE:
                status += ' DONE'
            if rsm_state == RSM.SCHEDULE_NEXT:
                status += ' SCHED-NEXT'
            if rsm_state == RSM.ANOMALY:
                status += ' rsmANOMALY'
            if rsm_state == RSM.BLOCKED:
                status += ' BLOCKED'

            self.path.append((path_current, status, direction))

        if self.anomaly is not None:
            print("Runtime Anomaly '{}' raised".format(self.anomaly))
            self.stopped = True
            self.current = None
            if self.raise_anomaly_as_pyexc:
                raise RuntimeError(self.anomaly)

        if not deferred and self.current and not self.is_blocked():
            self.current.set_next()

        return 0.0

    def pause(self):
        """
        Set the paused flag.

        This means calls to next will not proceed through the flow without interaction.
        """
        self.paused = True

    def unpause(self):
        """
        Unset the paused flag.
        """
        self.paused = False

    def resume(self):
        """Unset the paused flag and continue the execution."""
        self.paused = False
        while self.keep_nexting():
            sleeptime = self.next()
            time.sleep(sleeptime)

    def stop(self):
        """Do nothing ATT."""

    def finish(self):
        """Unpause and turn off debugging such that execution can continue."""
        self.paused = False
        self.debug = False
        self.stopped = True

    def finalize(self):
        """Do nothing ATT."""

    def get_current_node(self):
        """Return the current node (the next to execute)."""
        if self.current is None:
            return None
        return self.current.get_current_node()

    def get_current_direction(self):
        """
        Return the direction by which the current node in the current frame
        was reached.

        'DOWN' means the current node was the Target, or initial node
        'UP' means the node was requested to be run by one of it's targets
        aka a node to which it is the source.
        """
        if self.current is None:
            raise Exception("DEBUGGING: node direction requested, but no Node in frame.")
        else:
            return self.current.get_current_direction()

    def get_map(self):
        """
        Return the current map
        """
        return self.map

    def get_executed(self):
        """
        Returns a dict of followed edges and executed nodes
        """
        if self.map is None:
            return {"nodes": [], "edges": [], "last_node": None}
        return self.map.get_executed(self)

    def is_blocked(self):
        """Return whether the current frame is currently blocked."""
        if self.current is not None:
            return self.current.is_blocked()
        else:
            return False

    def is_paused(self):
        """Return whether the runtime is paused."""
        return self.paused

    def has_anomaly(self):
        """Indicate if this runtime has an anomaly."""
        return hasattr(self, 'anomaly') and self.anomaly is not None

    def get_anomaly(self):
        """Return the anomaly or None if there is not one."""
        return self.anomaly

    def set_anomaly(self, anomaly, last_type, last_value, exc_traceback):
        """
        set the anomaly (aka exception) and flag the runtime as stopped
        """
        self.anomaly = anomaly
        self.stopped = True
        tb(exc_traceback, last_type, last_value)

    def is_done(self):
        """Return whether the current map is completed."""
        if self.anomaly is not None:
            return True
        if self.stopped:
            return True
        if self.paused:
            return False
        return (self.current is None or self.current.current is None) and not self.stack

    def reset_all_nodes_runstate(self):
        for nid, node in self.map.nodes.items():
            RS.s(node).reset(runtime=self, exc_cnt=self.get_exec_count())

    def export_as_graphviz(self, directory="/Users/nate/Desktop/", ext="pdf"):
        """
        render the current graph as a DOT graphviz for
        troupblshooting purposes
        """
        bkup_dbg = self.debug
        self.debug = True
        rendered_edges = {}
        green = "#00bb00ff"
        bgcolor = "#111321ff"
        fgcolor = "#ddeef0ff"
        lightgrey = "#555555ff"
        pink = "#cc7777ff"

        class ed():
            def __init__(self, src, tgt, label="frm.next"):
                self.source = src
                self.target = tgt
                self.rule = label

            def been_followed(self):
                return True

        from graphviz import Digraph
        dot = Digraph(name=f"{self.map.name}", edge_attr={'arrowhead': 'vee'})
        dot.attr(size='6,6')
        dot.attr(bgcolor=bgcolor)
        dot.attr(color=fgcolor)
        dot.attr(fontcolor=fgcolor)
        dot.attr(margin="0,0")
        # starting with the current frame and current node
        # traverse back N levels adding prior nodes with their runstate and followed edges
        # Then look forward N levels
        # Runstate should have different bgcolors
        # followed vs unfollowed will be solid vs dashed

        def add_node(d, n, frm, prefix):
            kwargs = {
                "shape": "circle",
                "style": "solid",
                "color": fgcolor,
                "fontcolor": fgcolor
            }
            sfx = "\n------ run state ------\n"
            # d.attr('node', shape=shape, style=style, color=color, fillcolor="white")
            if n.has_executed():
                kwargs["style"] = "dashed"
                # d.attr('node', style="dashed")
                sfx = f"{sfx}executed\n"
            if n._run_state._blocked:
                kwargs["style"] = "filled"
                kwargs["color"] = "red"
                kwargs["shape"] = "octagon"
                # d.attr('node', style="filled", color="red")
                sfx = f"{sfx}blocked\n"
            if n._run_state._deferred:
                kwargs["style"] = "filled"
                kwargs["color"] = pink
                # d.attr('node', style="filled", color=pink, shape="octagon")
                sfx = f"{sfx}deferred\n"
            if n._run_state._pending_run:
                kwargs["style"] = "filled"
                kwargs["color"] = lightgrey
                kwargs["shape"] = "square"
                # d.attr('node', style="filled", color=lightgrey, shape="square")
                sfx = f"{sfx}pending_run\n"
            if n.is_preempted():
                kwargs["style"] = "filled"
                kwargs["color"] = green
                # d.attr('node', style="filled", color="green")
                sfx = f"{sfx}preempted\n"
            if n == frm.current:
                sfx = f"{sfx}frm current\n"
                kwargs["fillcolor"] = bgcolor
                kwargs["color"] = "orange"
                kwargs["shape"] = "doublecircle"
                # d.attr('node', style="filled", color="orange", shape="doublecircle")
            if n == self.current.current and self.current == frm:
                kwargs["style"] = "filled"
                kwargs["fillcolor"] = "orange"
                sfx = f"{sfx}**current**\n"
            if hasattr(n, 'vis_repr'):
                sfx = f"{sfx}`{n.vis_repr()}`\n"
            if hasattr(n, 'value'):
                sfx = f"{sfx}val: {n.value}\n"

            # d.node(f"{prefix}{n.id}", label=repr(n))
            shorty = n.id[0:6] + '...' + n.id[-6:]
            label = f"type: {n.__class__.__name__}\nid: {shorty}\nname: {n.name}{sfx}"
            if label is None:
                label = f"type: {n.__class__.__name__}\nid: {shorty}{sfx}"
            kwargs["label"] = label
            d.node(f"{prefix}{n.id}", **kwargs)
            # d.attr('node', shape=shape, style=style, color=color, fillcolor="white")

        def add_edge(d, e, prefix, color=fgcolor, skip_redundant=True):
            idx = f"{prefix},{e.source.id},{e.target.id}"
            if idx in rendered_edges and skip_redundant:
                return
            rendered_edges[idx] = e
            if e.been_followed():
                d.attr('edge', style="dashed")
            n1 = e.source.id
            n2 = e.target.id
            d.attr('edge', color=color, fontcolor=fgcolor)
            d.edge(f"{prefix}{n1}", f"{prefix}{n2}", label=e.rule)
            d.attr('edge', color=fgcolor, style="solid", fontcolor=fgcolor)

        def recurse_back(d, n, frm, depth=1, maxdepth=3, prefix=''):
            if depth >= maxdepth:
                add_node(d, n, frm, prefix)
                return
            if hasattr(n, '_followedSources'):
                for e in n._followedSources:
                    recurse_back(d, e.source, frm, depth=depth+1, maxdepth=maxdepth, prefix=prefix)
                    add_edge(d, e, prefix)
            if hasattr(n, '_unfollowedSources'):
                for e in n._unfollowedSources:
                    recurse_back(d, e.source, frm, depth=depth+1, maxdepth=maxdepth, prefix=prefix)
                    add_edge(d, e, prefix, color="red")
            add_node(d, n, frm, prefix)

        def recurse_forw(d, n, frm, depth=1, maxdepth=3, prefix=''):
            if depth >= maxdepth:
                add_node(d, n, frm, prefix)
                return
            if hasattr(n, '_followedTargets'):
                for e in n._followedTargets:
                    recurse_forw(d, e.target, frm, depth=depth+1, maxdepth=maxdepth, prefix=prefix)
                    add_edge(d, e, prefix, color="blue")
            if hasattr(n, '_unfollowedTargets'):
                for e in n._unfollowedTargets:
                    recurse_forw(d, e.target, frm, depth=depth+1, maxdepth=maxdepth, prefix=prefix)
                    add_edge(d, e, prefix, color=green)
            add_node(d, n, frm, prefix)

        def show_next(d, frm, pfx):
            n1 = frm.current
            for n2 in frm.next_nodes:
                # need to subscript to get te n2 as next_nodes is a tuple of node and direction
                e = ed(n1, n2[0])
                add_edge(d, e, pfx, color=pink)
                n1 = n2[0]

        # Render a legend
        with dot.subgraph(name="Legend") as d:
            d.engine = "neato"
            d.attr(fontcolor=fgcolor, color=fgcolor, shape="rectangle", label="Legend")

            d.attr('node', style="solid", shape="circle", fontcolor=fgcolor)
            d.node("1", label="pending run", shape="square", style="filled", color=lightgrey, fontcolor=fgcolor)
            d.node("2", label="Current Node\nCurrent Frame",
                   shape="doublecircle", color="orange", fillcolor="orange", style="filled", fontcolor=fgcolor)
            d.node("3", label="Current Node\nPrior Frame",
                   shape="doublecircle", color="orange", fillcolor="white", fontcolor=fgcolor)
            d.node("4", label="Preempted", shape="square", color=green, style="filled", fontcolor=fgcolor)
            d.node("5", label="Deferred", color=pink, style="filled", fontcolor=fgcolor)
            d.node("6", label="Blocked", shape="octagon", color="red", style="filled", fontcolor=fgcolor)
            d.node("7", label="Executed", style="dashed", color=fgcolor, fontcolor=fgcolor)
            d.node("8", label="Blank", style="invis")

            d.edge("1", "2", label="ahead\n  followed", color="blue", fontcolor=fgcolor)
            d.edge("3", "4", label="ahead\n  unfollowed", color=green, fontcolor=fgcolor)
            d.edge("5", "6", label="behind\n  followed", color=fgcolor, fontcolor=fgcolor)
            d.edge("7", "8", label="behind\n  unfollowed", color="red", fontcolor=fgcolor)

            d.attr('node', style="invis")
            d.node("i_.")
            d.node("j_.")
            d.edge("i_.", "j_.", label="runtime\n  unfollowed", color="lightgray", style="dashed", fontcolor=fgcolor)
            d.node("k_.")
            d.node("l_.")
            d.edge("k_.", "l_.", label="frame\nnext", color=pink, style="dotted", fontcolor=fgcolor)

            idx = 0
            prior = None
            for n, _, _ in self.path:
                add_node(d, n, self.current, 'path')
                if idx > 0:
                    e = ed(prior, n, label=f"{idx}")
                    add_edge(d, e, "path", color="orange", skip_redundant=False)
                idx += 1
                prior = n

        for frm in self.stack:
            if frm is not self.current:
                pfx = str(self.stack.index(frm))
            else:
                pfx = ''
            with dot.subgraph(name=f"sl:{pfx}") as d:
                d.attr(color="blue", label=f"Stack Level: {pfx}")
                recurse_back(d, frm.current, frm, depth=1, maxdepth=8, prefix=pfx)
                recurse_forw(d, frm.current, frm, depth=1, maxdepth=8, prefix=pfx)
                show_next(d, frm, pfx)

        self.debug = bkup_dbg

        if directory is None:
            dot.view()
        else:
            if not hasattr(self, '_export_as_graphviz_counter'):
                self._export_as_graphviz_counter = 0
            fname = self.map.name.replace(' ', '_') + f'.{self._export_as_graphviz_counter}'
            self._export_as_graphviz_counter += 1
            dot.render(filename=fname, directory=directory, format=ext)
            # dot.save(filename=fname+".gv", directory=directory)

    def get_version_string(self):
        return f"{version.MAJOR}.{version.MINOR}.{version.REVISION}"
