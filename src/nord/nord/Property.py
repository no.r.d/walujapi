"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

import sys
import nord.nord.Strings


class Property(object):
    def __init__(s, obj, name, default=None, description=''):
        s.name = name
        s.isSet = False
        s.default = default
        if len(description) > 0:
            s.description = _(description)
        else:
            s.description = ''
        s.holder = obj

    def get(s):
        if not s.isSet or not hasattr(s.holder, s.name):
            return s.default
        else:
            return getattr(s.holder, s.name)

    def set(s, value):
        s.isSet = True
        setattr(s.holder, s.name, value)

    def check(s):
        """
            checks the holding object for the presence of a member with name
            if present, return True, else, false
        """
        return hasattr(s.holder, s.name)

    def __repr__(s):
        '''
            returns a string representation of the property
        '''
        if s.isSet:
            return "Property({}) : '{}'".format(s.name, getattr(s.holder, s.name))
        else:
            return "Property({}) is not set{}".format(s.name, "" if s.default is None else ", default: {}".format(s.default))

sys.modules[__name__] = Property
