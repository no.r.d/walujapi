"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

The Map is the analog of program, script, module and/or library from other languages.

Map'self are graph'self made up of nodes connected by edges which have rules relating
the types of nodes with the type of edge. Map'self can reference other maps.
"""

import sys
import nord.nord.utils.SpaceManager as use
from nord.nord.exceptions import UseException, MapException
from nord.sigurd.utils.log import info, notice, warn
import nord.nord.InstanceOf as InstanceOf
from nord.nord.changetrack import MapChanges, MapChange
import nord.sigurd.utils.namegen as ng


def section_compare(adict, bdict):
    to_add = []
    to_del = []
    to_change = {}
    changes = []
    for k, v in bdict.items():
        if type(v) is not dict and hasattr(v, 'to_dict'):
            v = v.to_dict()
        if k not in adict:
            to_add.append({k: v})
        else:
            to_change[k] = v

    for k, v in adict.items():
        if type(v) is not dict and hasattr(v, 'to_dict'):
            v = v.to_dict()
        if k not in bdict:
            to_del.append({k: v})
        elif k not in to_change:
            # Perhaps this is supposed to be unreachable...
            to_change[k] = v
        elif to_change[k] == v:
            # this should be safe as we've determined
            # that this node exists in "b"
            del to_change[k]

    for k, v in to_change.items():
        B = bdict[k]
        A = adict[k]
        if type(B) is not dict and hasattr(B, 'to_dict'):
            B = B.to_dict()

        if type(A) is not dict and hasattr(A, 'to_dict'):
            A = A.to_dict()

        add_to_a = {k: B[k] for k in set(B) - set(A)}
        del_from_a = {k: A[k] for k in set(A) - set(B)}
        change_a = {k: B[k] for k in set(A) & set(B) if A[k] != B[k]}
        priors_a = {k: A[k] for k in set(A) & set(B) if A[k] != B[k]}
        changes.append({k: {"add": add_to_a, "del": del_from_a, "change": change_a, "priors": priors_a}})

    return {"add": to_add, "del": to_del, "change": changes}


class Map(object):
    """
    Map'self are the representation of the programs to be executed.

    A graph stored within a dictionary is used to
    generate the element mesh of objects which are then executed or debugged.
    """

    @staticmethod
    def find_edge_rules(src, tgt, ctype, spaces):
        # check to see if the involved nodes (from a language perspective) are good with the connection
        if not tgt.connect_allowed(src, ctype, 'as_tgt') or\
                not src.connect_allowed(tgt, ctype, 'as_src'):
            return False, None, None, None

        source = src.__class__.__name__.capitalize()
        target = tgt.__class__.__name__.capitalize()

        edge = None
        srule = None
        trule = None
        srulename = f'rules.{source}{ctype.capitalize()}'
        trulename = f'rules.{ctype.capitalize()}{target}'
        edgename = f'edges.{ctype.capitalize()}'
        # Create a list of spaces to search (the keys of endict will be the unique set of spaces)
        endict = dict()
        for n in spaces:
            endict[n] = 1

        orig_space = use.current_space()
        srcFound = False
        tgtFound = False
        edgeFound = False
        for space in list(endict.keys()):
            use.space(space)
            if not srcFound:
                try:
                    srule = use.module(f"nord.{space}.{srulename}")
                    srcFound = True
                    notice(f"Source rule {srulename} found in {space}")
                except UseException:
                    info(f"Source rule {srulename} not found in {space}")

            if not edgeFound:
                try:
                    edge = use.module(f"nord.{space}.{edgename}")
                    edgeFound = True
                    notice(f"Edge type {edgename} found in {space}")
                except UseException:
                    info(f"Edge type {edgename} not found in {space}")

            if not tgtFound:
                try:
                    trule = use.module(f"nord.{space}.{trulename}")
                    tgtFound = True
                    notice(f"Target rule {trulename} found in {space}")
                except UseException:
                    info(f"Target rule {trulename} not found in {space}")

        use.space(orig_space)

        # since flow is a special, "universally allowed" edge, set return
        # flags accordingly
        if ctype.endswith("flow") and edgeFound:
            srcFound = True
            tgtFound = True

        if not srcFound:
            warn(f"Unable to find source rule named: {srulename}")
        if not edgeFound:
            warn(f"Unable to find an edge named: {edgename}")
        if not tgtFound:
            warn(f"Unable to find target rule named: {trulename}")

        return srcFound and tgtFound and edgeFound, edge, srule, trule

    @staticmethod
    def diff_to_changes(diff, graph=None):
        rv = []
        if graph:
            restore = graph._is_loading
            graph._is_loading = True
        for add in diff["nodes"]['add']:
            for k, v in add.items():
                node = Map.make_node_from_dict(v, graph)
                rv.append(MapChange("add_node", node))
        for rem in diff["nodes"]['del']:
            for k, v in rem.items():
                node = Map.make_node_from_dict(v, graph)
                rv.append(MapChange("delete_node", node))
        if graph:
            """
            if there is not a graph, then it is impossible to process changes
            as no basis node is available to change from.
            """
            for change in diff["nodes"]['change']:
                for nid, payload in change.items():
                    node = graph.nodes[nid]
                    for k, v in payload["add"].items():
                        rv.append(MapChange("change_node", node, attrib=k, prev=None, curr=v))
                    for k, v in payload["change"].items():
                        if not hasattr(node, k):
                            raise MapException(_("Apply diff failed as"  # noqa: F821
                                                 " requested field '{}' is missing").format(k))
                        rv.append(MapChange("change_node", node, attrib=k, prev=getattr(node, k), curr=v))
                    for k, v in payload["del"].items():
                        rv.append(MapChange("change_node", node, attrib=k, prev=getattr(node, k), rem_attr=True))
        else:
            raise MapException(_("Cannot convert diff to changes without graph to compare to."))  # noqa: F821

        # edge creation without a map/graph will not work
        for add in diff["edges"]['add']:
            for k, v in add.items():
                edge, source, target = Map.make_edge_from_dict(v, graph, diff=diff)
                rv.append(MapChange("add_edge", edge))

        for rem in diff["edges"]['del']:
            for k, v in rem.items():
                edge, source, target = Map.make_edge_from_dict(v, graph, diff=diff)
                rv.append(MapChange("delete_edge", edge))

        if graph:
            graph._is_loading = restore
        return [x.to_dict() for x in rv]

    def __init__(self, inp, verbose=False):
        """Create the map by traversing the input dictionary."""
        self.prep_for_init()
        if inp is not None:
            self.load(inp, verbose)

    def add_change_handler(self, handler):
        """
        Append the passed handler to the list of change handers.

        Each handler will be passed every change.
        """
        if handler not in self._handlers:
            self._handlers.append(handler)

    def ship_changes(self):
        changes = self._map_changes.get_changes()
        if changes:
            for handler in self._handlers:
                handler(changes)

    def prep_for_init(self):
        """Prepare the map to be executed."""
        if not hasattr(self, '_initialized'):
            use.space('nord')
            self.Container = use.module('nord.nord.nodes.Container')
            self.nodes = dict()
            self.edges = dict()
            self.rules = dict()
            self.extensions = dict()
            self.start_node = None
            self._initialized = True
            self._is_loading = False
            self._map_changes = MapChanges(self)
            self._handlers = list()

    def ccap_node_added(self, node):
        """
        Capture node added change
        """
        if self._is_loading:
            return
        self._map_changes.add_node(node)

    def ccap_node_changed(self, node, attribute, prev_val, new_val, noparse=False):
        """
        Capture node changed change
        """
        if self._is_loading:
            return
        self._map_changes.change_node(node, attribute, prev_val, new_val, noparse)
        self.ship_changes()

    def ccap_node_deleted(self, node):
        """
        Capture node deleted change
        """
        if self._is_loading:
            return
        self._map_changes.delete_node(node)

    def ccap_edge_added(self, edge):
        """
        Capture edge added change
        """
        if self._is_loading:
            return
        self._map_changes.add_edge(edge)

    def ccap_edge_deleted(self, edge):
        """
        Capture edge deleted change
        """
        if self._is_loading:
            return
        self._map_changes.delete_edge(edge)

    def ccap_add_node(self, change):
        """
        Apply node added change to this map
        """
        stored_val = self._is_loading
        self._is_loading = True
        self.load_node(change["entity"])
        self._is_loading = stored_val

    def ccap_change_node(self, change):
        """
        Apply node changed change to this map
        """
        stored_val = self._is_loading
        self._is_loading = True
        node = self.nodes.get(change["item_id"], None)
        if node is not None:
            if change["delattr"]:
                node.remove_attribute(change["attribute"])
            else:
                node.change_attribute(change["attribute"], change["change"])
        self._is_loading = stored_val

    def ccap_delete_node(self, change):
        """
        Apply node deleted change to this map
        """
        stored_val = self._is_loading
        self._is_loading = True
        node = self.nodes.get(change["item_id"], None)
        if node is not None:
            del self.nodes[node.id]
            node.delete()
            node.disconnect()
        self._is_loading = stored_val

    def ccap_add_edge(self, change):
        """
        Apply edge added change to this map
        """
        stored_val = self._is_loading
        self._is_loading = True
        self.load_edge(change["entity"])
        self._is_loading = stored_val

    def ccap_delete_edge(self, change):
        """
        Apply edge deleted change to this map
        """
        stored_val = self._is_loading
        self._is_loading = True
        edge = self.edges.get(change["item_id"], None)
        if edge is not None:
            del self.edges[change["item_id"]]
            edge.delete()
        self._is_loading = stored_val

    def has_changes(self):
        return self._map_changes.has_changes()

    def unload(self):
        """Clear out the current map's data."""
        self._is_loading = True
        del self.Container
        self.nodes.clear()
        self.edges.clear()
        self.rules.clear()
        self.start_node = None
        self.Container = use.module('nord.nord.nodes.Container')
        self._is_loading = False

    def add_edge(self, source, target, rule, edge):
        if (source.id, target.id, rule) not in self.edges:
            self.edges[(source.id, target.id, rule)] = edge
            self.ship_changes()

    def set_node(self, nid, node):
        self.nodes[nid] = node
        self.ship_changes()

    def rem_node(self, nid):
        """
        remove the node and all references to it...

        TODO: Instances need to be handled as well ..
        """
        if nid in self.nodes:
            node = self.nodes[nid]
            node.disconnect()
            # This delete statement prevents recursion
            # that would be triggered by the node.delete
            # as that function also calls this one...
            # Which is probably not great, but, ATT I am
            # uncertain if there are other appropriate
            # calls to delete outside of this codepath
            del self.nodes[nid]
            node.delete()
            self.ship_changes()

    def rem_edge(self, edge):
        """
        Remove the passed edge from the list of edges.

        This should not be called directly. Only via the node's delete
        call flow.
        """
        if (edge.source.id, edge.target.id, edge.rule) in self.edges:
            del self.edges[(edge.source.id, edge.target.id, edge.rule)]
        if (edge.target.id, edge.source.id, edge.rule) in self.edges:
            del self.edges[(edge.target.id, edge.source.id, edge.rule)]
        edge.delete()
        self.ship_changes()

    @staticmethod
    def make_node_from_dict(n, graph=None):
        node = None
        if 'space' not in n:
            provider = 'nord'
        else:
            provider = n['space']

        use.space(provider)

        if "type" not in n:
            # This defaulting of the node type is primarily in place
            # to facilitate testing.
            modpath = 'nord.nord.Node'
        else:
            modpath = f"nord.{provider}.nodes.{n['type'].title()}"
        mod = use.module(modpath)
        if mod is not None:
            # this will look through the loaded modules for a module
            # named after the type field in the node element
            # it will create a new instance of that type
            # and apply the data provided by the map to that node
            # and lastly store the node by it'self ID in the nodes dictionary

            try:
                node = mod(graph)
            except TypeError:  # pragma: no cover
                print("Invalid node initialization for node of type: {}"
                      .format(n['type'].title()))  # pragma: no cover
            node.apply(n, write_direct=True)

        else:  # pragma: no cover
            print("{} node type not implemented".format(n['type'].title()))
        return node

    def load_instance(self, n):
        if n['instanceof'] in self.nodes and\
                isinstance(self.nodes[n['instanceof']], InstanceOf):
            parent = self.nodes[n['instanceof']]
            child = parent.add_instance(n)
            child.type = parent.type
            self.set_node(n['id'], child)
        else:
            print("Instance of requested without known parent: {}".format(n))

    def load_node(self, n):
        if "id" in n and "name" in n and "instanceof" not in n:
            node = Map.make_node_from_dict(n, graph=self)
            if node:
                self.set_node(n['id'], node)

        elif "id" in n and "name" in n and "instanceof" in n:
            self.load_instance(n)

    @staticmethod
    def make_edge_from_dict(e, graph=None, diff=None):
        if graph is None:
            raise MapException(_("graph must exist to greate edges"))  # noqa: F821

        sid = e["source"].split(':')[0]
        tid = e["target"].split(':')[0]
        rule = e["rule"]

        nodes = {k: v for k, v in graph.nodes.items()}
        if diff is not None:
            """
            Augment the set of nodes, with any new ones in
            the diff, if it is available
            """
            for add in diff["nodes"]['add']:
                for k, v in add.items():
                    node = Map.make_node_from_dict(v, graph)
                    nodes[node.id] = node

        if sid in nodes and tid in nodes:
            if 'space' not in e:
                provider = 'nord'
            else:
                provider = e['space']

            use.space(provider)

            source = nodes[sid]
            target = nodes[tid]
            connable, mod, _, _ = Map.find_edge_rules(source,
                                                      target,
                                                      rule,
                                                      list(use.get_spaces()))
            if mod is not None and connable:

                # this will create a new edge of type "rule"
                # between the source and the target nodes

                edge = mod(graph)
                edge.apply(e)
                edge.set_nodes(source, target, e['rule'])
                return edge, source, target
            else:  # pragma: no cover
                print("{} edge type not implemented".format(e['rule'].title()))
                raise MapException('Missing implementation ' +
                                   'for edge: {} -> {} -> {} '.format(source.__class__.__name__,
                                                                      rule,
                                                                      target.__class__.__name__))
        else:  # pragma: no cover
            print(f"Unknown node {e['source'].split(':')[0]} or"
                  f" {e['target'].split(':')[0]} requested by edge: {e}."
                  f" Known nodes are: {nodes}")
        return None, None, None

    def load_edge(self, e):
        sid = e["source"].split(':')[0]
        tid = e["target"].split(':')[0]
        if (sid, tid, e['rule']) in self.edges:
            return
        edge, source, target = Map.make_edge_from_dict(e, self)
        # this suggests that a source and target can only be
        # connected once per rule
        if edge is not None:
            self.add_edge(source, target, e['rule'], edge)
        if e['rule'] not in self.rules:
            self.rules[e['rule']] = list()
        self.rules[e['rule']].append(edge)

    def get_name(self):
        """Return the name of this map. establish a random one, if non-already assigned"""
        if not hasattr(self, 'name'):
            self.name = ng.get()
        return self.name

    def get_id(self):
        """Return the ID of this map, generate one if non exists"""
        if not hasattr(self, "id"):
            self.id = ng.id()
        return self.id

    def load(self, inp, verbose, bypass_cdc=True):
        """Process the input dict structure into Nodes and Edges."""
        self._is_loading = bypass_cdc
        self.verbose = verbose

        # Do not overwrite the name if it has already been set.
        if not hasattr(self, 'name'):
            if "name" in inp:
                self.name = inp['name']
            else:
                self.name = ng.get()

        if not hasattr(self, "id"):
            if "id" in inp:
                self.id = inp["id"]
            else:
                self.id = ng.id()

        # prepare the spaces with any extensions requested by the map/graph.
        # permissions and compatible versions are too be implemented.
        # as are overrides
        if "extensions" in inp:
            for extn in inp["extensions"]:
                ext = inp["extensions"][extn]
                self.extensions[extn] = ext
                use.space(extn)
                if "locations" in ext:
                    for provides in ext["locations"]:
                        for loc in ext["locations"][provides]:
                            use.add_location(loc, provides)

        # If payment details are included in the input
        # data, establish an EconomicSession
        if "signed_tx_block" in inp:
            # A transaction block has been provided by the
            # requestor. This is a prepayment, the runtime
            # will need to refund the unused portion when
            # the map is done.
            import nord.pasion.visibles.wallet as wallet
            wallet.establish_session_from_tx(inp["signed_tx_block"], inp["refunds_to"], inp["payment_to"])

        if "map" in inp:
            if "nodes" in inp['map']:
                delayed_nodes = []
                for n in inp['map']['nodes']:
                    if "id" in n and "name" in n and "type" in n and "instanceof" not in n:
                        self.load_node(n)
                    elif "id" in n and "instanceof" in n:
                        # This node is an instance of a prior node. Associate if
                        # the referenced node is instanceable.
                        delayed_nodes.append(n)
                    else:  # pragma: no cover
                        print("Malformed node detected: {}".format(n))

                for n in delayed_nodes:
                    self.load_instance(n)

                if "edges" in inp['map']:
                    for e in inp['map']['edges']:
                        if "source" in e and "target" in e and "rule" in e:
                            # sid = e['source'].split(":")[0]
                            # tid = e['target'].split(":")[0]
                            # rule = e["rule"]
                            self.load_edge(e)

                            # sys.exit(1)
                        else:  # pragma: no cover
                            print("Malformed edge detected: {}".format(e))

                    # pick a node to begin map execution.
                    self.detect_start_node()

                else:
                    print("No edge data found in input data")
                    # sys.exit(1)
            else:
                print("No node data found in input data")
                # sys.exit(1)
        else:
            print("No map found in input data")
            # sys.exit(1)

        if verbose:
            print("Map created with {} Nodes and {} Edges".format(len(self.nodes), len(self.edges)))
        self._is_loading = False

    def get_sourceless_nodes(self):
        """
        Get all nodes that have no source edges.
        """
        candidates = []
        found_one = False
        for nname in self.nodes:
            node = self.nodes[nname]
            if len(node.get_source_rules()) == 0 and node.get_cfg_value('cannot_start_map') is None:
                candidates.append(node)
                found_one = True

        candidates = sorted(candidates)
        return candidates, found_one

    def detect_start_node(self):
        """
        Find nodes with no edges targetting it.
        Make it/them the start node(s).

        Should this approach not find a node, set the first node in the
        map as the starting node.... who knows what the ramifications
        of this approach are...?
        """
        if self.start_node is None:
            self.start_node = []
            candidates, found_one = self.get_sourceless_nodes()
            if not found_one:
                print("Run requested for map, but no initial container specified")

            # first look for any containers with no inbound edges.
            # if there is one named "main" (any case), set it to start
            # otherwise, choose the first container
            # if there are no containers, then, choose the
            # first non-container node with no inbound edges.
            found_main = False
            for node in candidates:
                if type(node) == self.Container and\
                        hasattr(node, 'name') and\
                        node.name.lower() == 'main':
                    self.start_node.insert(0, node)
                    found_main = True
                else:
                    self.start_node.append(node)

            if not found_main:
                for node in candidates:
                    if type(node) == self.Container:
                        self.start_node.remove(node)
                        self.start_node.insert(0, node)
                        break

            if len(self.start_node) == 0 and self.nodes:
                # fallback to the first node in the list....
                self.start_node = [self.nodes[list(self.nodes.keys())[0]]]

        return sorted(self.start_node)

    def run(self, runtime, args):
        """
        Execute this map when called from the World (top level, not from another Map).

        In short this method just starts the flow of rules. From that point execution
        is handled within all the rules modules.

        """
        if self.start_node is not None:
            runtime.initialize(self, verbose=self.verbose)
            # clear out any runstates
            for _, n in self.nodes.items():
                if hasattr(n, '_run_state'):
                    del n._run_state
            runtime.begin(self.start_node)

            # self.start_node.execute(runtime=runtime, args=args)
        else:
            # There is no container at the top level
            # So, find a node that is not a target
            # and start the execution there...
            self.detect_start_node()
            if self.start_node is not None:
                runtime.initialize(self, verbose=self.verbose)
                runtime.begin(self.start_node)
            else:
                print("Run requested for map, but no initial container specified")
                raise RuntimeError("Unable to detect a node from which to start the map.")

    def init_debug(self, runtime, args):
        """.The init_debug method starts the map in debugging mode."""
        if self.start_node is not None:
            import nord.nord.utils.Debugger as dbg
            runtime.initialize(self, verbose=True)
            dbg.start(self, runtime, args)

    def debug_next(self):
        """Step to the next element in the flow, and execute whatever rules it has defined."""
        pass

    def get_node(self, nid):
        """
        Fetch the node with id nid from the map, if it exists.

        Return None if it does not.
        """
        if nid in self.nodes:
            return self.nodes[nid]
        else:
            return None

    def get_edge(self, sid, tid, rule):
        """
        Fetch and return the edge
        identified by sid (source ID),
        tid (target ID) and rule.

        If such an edge does not exist, return None
        """
        if (sid, tid, rule) in self.edges:
            return self.edges[(sid, tid, rule)]
        else:
            return None

    def get_executed(self, runtime):
        """
        Return a dict made up of a list of followed edges and executed nodes
        """
        rv = {
            "edges": [edge_id for edge_id, edge in self.edges.items() if edge.been_followed(runtime)],
            "nodes": [node_id for node_id, node in self.nodes.items() if node.has_executed()],
            "last_node": None
        }
        if runtime.get_current_node():
            rv["last_node"] = runtime.get_current_node().id
        return rv

    def __repr__(self):
        """Traverse the map (graph) and print out all the nodes, edges and rules."""
        rv = "Nodes\n================\n"
        for k in sorted(self.nodes.keys()):
            rv += "-----\n{}\n-----\n".format(repr(self.nodes[k]))

        rv += "\n\n\nEdges\n===============\n"

        for k in sorted(self.edges.keys()):
            rv += "  ---->\n{}\n".format(repr(self.edges[k]))

        rv += "\n\nRules\n===============\n"

        for k in sorted(self.rules.keys()):
            rv += "\n(====)\n-- {} Rule --\n".format(k.title())
            for i in self.rules[k]:
                rv += "\n  R: {}".format(str(i))

        return rv

    def to_dict(self):
        """
        Return the dictionary representation of this map.

        Traverse the map (graph) and convert all the nodes and
        edges into a dictionary
        """
        rv = dict()
        rv["map"] = {"nodes": [], "edges": []}
        rv["name"] = self.get_name()
        rv["id"] = self.get_id()
        rv["extensions"] = {}
        for n in sorted(self.nodes.keys()):
            rv["map"]["nodes"].append(self.nodes[n].to_dict())
        for e in sorted(self.edges.keys()):
            rv["map"]["edges"].append(self.edges[e].to_dict())
        for s in sorted(use.get_spaces()):
            if s == "nord":
                continue
            sd = use.space_to_dict(s)
            if sd is not None:
                rv["extensions"][s] = sd

        return rv

    def get_changes(self, guid=None):
        return self._map_changes.get_changes(guid=guid)

    def apply_change(self, change):
        """
        This is probably terrible ... but,
        find the function named ccap_*
        where * is the change_type
        """
        if "change_type" in change:
            ct = change["change_type"]
            if hasattr(self, f'ccap_{ct}'):
                func = getattr(self, f'ccap_{ct}')
                func(change)
            else:
                raise MapException(_("Unable to process change: {}").format(change))  # noqa: F821
        else:
            raise MapException(_("Malformed change: {}").format(change))  # noqa: F821

    @staticmethod
    def _section_compare_for_testing(a, b):
        return section_compare(a, b)

    @staticmethod
    def diff_maps(a, b):
        """
        Compare two input maps, and return the difference
        needed to be applied to "a" in order to end up with "b"
        """
        diff = None
        # first see which nodes need to be added to "a"
        # then, see which nodes need to be removed from "a"
        # lastly, capture the changes to the common nodes
        node_diff = section_compare(a.nodes, b.nodes)
        edge_diff = section_compare(a.edges, b.edges)
        space_diff = section_compare(a.extensions, b.extensions)

        diff = {"nodes": node_diff, "edges": edge_diff, "extensions": space_diff}
        # and for any other meta data, like name, version etc
        return diff

    def apply_diff(self, diff):
        # need to be able to convert diff to changes
        # need to be able to apply diff (via converting to changes)
        changes = Map.diff_to_changes(diff, self)
        self.apply_changes(changes)

    def connect_cdc_loopback(self):
        """
        Inject the loopback handler to be the first CDC handler
        such that changes are applied to this graph, prior to
        subsequent handlers (like sigurd....) so the map will
        possess any elements prior to the handlers from being
        called which might depend upon these nodes.
        """
        if len(self._handlers) == 0 or self.loopback_apply_changes != self._handlers[0]:
            self._handlers.insert(0, self.loopback_apply_changes)

    def loopback_apply_changes(self, changes):
        """
        Primarily intended to support testing, or, completly local
        execution (vismap operating on its own _working_map without using wrigan)
        This presumes that the sigurd handlers will be called after
        these changes are applied to the localgraph...
        """
        for change in changes:
            self.apply_change(change)

    def apply_changes(self, changes, notsrcguid=None):
        for change in changes:
            if notsrcguid is None or \
                    change.get("change_src_id", "NO") != notsrcguid:
                self.apply_change(change)
        if changes:
            for handler in self._handlers:
                handler(changes)


sys.modules[__name__] = Map
