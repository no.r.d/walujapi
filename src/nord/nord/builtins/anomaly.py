"""
Copyright 2022 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""


def anomaly(*args, **kwargs):
    """
    the anomaly builtin method raises an
    exception in the backend, which
    when run, creates an anomaly in the
    UI
    """
    fmt = ""
    for itm in args:
        fmt += "{}"

    for k in kwargs:
        fmt += k + "= {} "
        args += (kwargs[k],)

    raise Exception(fmt.format(*args))
