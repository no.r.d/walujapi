"""
Copyright 2022 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""
import nord.nord.nodes.Data as Data
import nord.sigurd.utils.namegen as ng

_newnode_runtime = None


def newnode_set_runtime(runtime):
    """
    This function will be detected and called by the Builtin node
    such that when newnode is called, it has access to the runtime,
    and thereby the running Map.
    """
    global _newnode_runtime
    _newnode_runtime = runtime


def newnode(*args, **kwargs):
    """
    the data builtin method injects a
    new data node into the map.
    """
    global _newnode_runtime
    karte = _newnode_runtime.get_map()

    p = [0.0, 0.0, 0.0]
    fmt = ""
    pos = None
    for itm in args:
        if "x" in itm and "y" in itm and "z" in itm:
            p = [itm["x"], itm["y"], itm["z"]]
            pos = itm
        else:
            fmt += "{}"

    for k in kwargs:
        fmt += k + "= {} "
        args += (kwargs[k],)


    args = list(args)
    if pos:
        args.remove(pos)

    if args:
        name = fmt.format(*args)
    else:
        name = ng.get()

    karte.load_node({
        "id": ng.id(),
        "name": name,
        "type": "data",
        "position": p
    })
