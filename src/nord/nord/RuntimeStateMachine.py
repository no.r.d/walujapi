"""
Copyright 2022 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--
This implements a state machine which evaluates the current state of
a node and the runtime to determine what should happen next.
"""
import sys
import nord.nord.RunState as RS

SCHEDULE_NEXT = 0
DONE = 1
ANOMALY = 2
BLOCKED = 3
DEFERRED = 4


class ExecStates:
    @staticmethod
    def not_executed(runtime, node):
        return EventStates.eval(runtime, node)

    @staticmethod
    def preempted(runtime, node):
        return NumUnexecSourcesStates.eval(runtime, node)

    @staticmethod
    def executed(runtime, node):
        return AnomalyStates.eval(runtime, node)

    @staticmethod
    def blocked(runtime, node):
        return BlockedStates.eval(runtime, node)

    @staticmethod
    def deferred(runtime, node):
        RS.s(node).unset_deferred()
        targets = node.follow_unfollowed_targets(runtime, runtime.verbose)
        node.schedule_unexec_targets(runtime, targets=targets)
        return NodesToScheduleStates.eval(runtime, node)

    @staticmethod
    def eval(runtime, node):
        if runtime is None or node is None:
            raise Exception(f"Invalid inputs to RuntimeStateMachine: ({runtime}, {node})")
        if not node.has_executed():
            return ExecStates.not_executed(runtime, node)
        elif node.is_preempted():
            return ExecStates.preempted(runtime, node)
        elif node.is_blocked():
            return ExecStates.blocked(runtime, node)
        elif RS.s(node).deferred():
            ExecStates.deferred(runtime, node)
        elif node.has_executed():
            return ExecStates.executed(runtime, node)
        else:
            raise Exception(f"Unknown ExecState of node {node}")


class EventStates:
    @staticmethod
    def in_event(runtime, node):
        return EventingNodeStates.eval(runtime, node)

    @staticmethod
    def no_event(runtime, node):
        return NumUnexecSourcesStates.eval(runtime, node)

    @staticmethod
    def eval(runtime, node):
        #
        # Need to look at runtime, not node to see
        # if there is an event
        #
        if runtime.is_handling_event() or runtime.has_pending_event():
            return EventStates.in_event(runtime, node)
        else:
            return EventStates.no_event(runtime, node)


class InEventPath:
    @staticmethod
    def yes(runtime, node):
        """
        Basically, jump to execute ....
        """
        return NumUnexecSourcesStates.zero(runtime, node)

    @staticmethod
    def no(runtime, node):
        return ContinueFlagStates.eval(runtime, node)

    @staticmethod
    def eval(runtime, node):
        if runtime.in_event_path(node):
            return InEventPath.yes(runtime, node)
        else:
            return InEventPath.no(runtime, node)


class EventingNodeStates:
    @staticmethod
    def yes(runtime, node):
        return EventForThisNodeStates.eval(runtime, node)

    @staticmethod
    def no(runtime, node):
        return InEventPath.eval(runtime, node)

    @staticmethod
    def eval(runtime, node):
        if node.handles_events():
            return EventingNodeStates.yes(runtime, node)
        else:
            return EventingNodeStates.no(runtime, node)


class NumUnexecSourcesStates:
    @staticmethod
    def zero(runtime, node):
        try:
            if node.prep_to_execute(runtime, runtime.verbose):
                if runtime.get_current_node().has_unexecuted_sources():
                    runtime.get_current_node().schedule_unexec_sources(runtime)
                    return SCHEDULE_NEXT
            # because a node, when prepping to execute can swap itself out for another node
            # (walujapi::nodes::reference, for example) we need to continue processing
            # the state against the swapped to node, and the new nodes, could swap themselves out
            # as well ... need to loop until runtime current is == node... And, since these nodes
            # might need to be re-evaluated from a state machine perspective
            if runtime.get_current_node() is not node:
                runtime.setNextNode(runtime.get_current_node(), 'DOWN')
                return SCHEDULE_NEXT
            if runtime.verbose:
                print("\nExecuting node: {}\n".format(repr(node)))
            node.execute(runtime)
        except Exception as e:
            last_type, last_value, exc_traceback = sys.exc_info()
            runtime.set_anomaly(e, last_type, last_value, exc_traceback)
            return ANOMALY
        return DeferredStates.eval(runtime, node)

    @staticmethod
    def one_plus(runtime, node):
        node.flag_preempted()
        # A node's prep_to_execute method can return false
        # to prevent the default scheduling from occurring
        if node.prep_to_execute(runtime, runtime.verbose):
            node.schedule_unexec_sources(runtime)
        return AnomalyStates.eval(runtime, node)

    @staticmethod
    def eval(runtime, node):
        if not node.has_unexecuted_sources():
            return NumUnexecSourcesStates.zero(runtime, node)
        else:
            return NumUnexecSourcesStates.one_plus(runtime, node)


class EventForThisNodeStates:
    @staticmethod
    def yes(runtime, node):
        try:
            if runtime.verbose:
                print("\nExecuting node: {}\n".format(repr(node)))
            node.execute(runtime)
        except Exception as e:
            last_type, last_value, exc_traceback = sys.exc_info()
            runtime.set_anomaly(e, last_type, last_value, exc_traceback)
            return ANOMALY
        return DeferredStates.eval(runtime, node)

    @staticmethod
    def no(runtime, node):
        return ContinueFlagStates.eval(runtime, node)

    @staticmethod
    def eval(runtime, node):
        if runtime.in_this_node_event(node):
            return EventForThisNodeStates.yes(runtime, node)
        else:
            return EventForThisNodeStates.no(runtime, node)


class ContinueFlagStates:
    @staticmethod
    def yes(runtime, node):
        return BackPropogatingStates.eval(runtime, node)

    @staticmethod
    def no(runtime, node):
        node.flag_exe(runtime)
        return NodesToScheduleStates.eval(runtime, node)

    @staticmethod
    def eval(runtime, node):
        if node.get_cfg_value('continue_exec_wo_event'):
            return ContinueFlagStates.yes(runtime, node)
        else:
            return ContinueFlagStates.no(runtime, node)


class BackPropogatingStates:
    @staticmethod
    def yes(runtime, node):
        return NumUnexecSourcesStates.eval(runtime, node)

    @staticmethod
    def no(runtime, node):
        node.flag_exe(runtime)
        node.follow_unfollowed_targets(runtime, runtime.verbose)
        node.schedule_unexec_targets(runtime)
        return AnomalyStates.eval(runtime, node)

    @staticmethod
    def eval(runtime, node):
        if runtime.get_heading() == 'UP':
            return BackPropogatingStates.yes(runtime, node)
        else:
            return BackPropogatingStates.no(runtime, node)


class BlockedStates:
    @staticmethod
    def yes(runtime, node):
        return BLOCKED

    @staticmethod
    def no(runtime, node):
        if not node.is_preempted():
            node.flag_exe(runtime)
        targets = node.follow_unfollowed_targets(runtime, runtime.verbose)
        node.schedule_unexec_targets(runtime, targets=targets)
        return AnomalyStates.eval(runtime, node)

    @staticmethod
    def eval(runtime, node):
        if node.is_blocked():
            return BlockedStates.yes(runtime, node)
        else:
            return BlockedStates.no(runtime, node)


class DeferredStates:
    @staticmethod
    def yes(runtime, node):
        node.flag_exe(runtime)
        return DEFERRED

    @staticmethod
    def no(runtime, node):
        return BlockedStates.eval(runtime, node)

    @staticmethod
    def eval(runtime, node):
        if RS.s(node).deferred():
            return DeferredStates.yes(runtime, node)
        else:
            return DeferredStates.no(runtime, node)


class NodesToScheduleStates:
    @staticmethod
    def yes(runtime, node):
        # schedule next node
        return SCHEDULE_NEXT

    @staticmethod
    def no(runtime, node):
        # Stop runtime engine
        return DONE

    @staticmethod
    def eval(runtime, node):
        if runtime.current and runtime.current.get_next_nodes():
            return NodesToScheduleStates.yes(runtime, node)
        else:
            return NodesToScheduleStates.no(runtime, node)


class AnomalyStates:
    @staticmethod
    def yes(runtime, node):
        # Stop runtime engine
        return ANOMALY

    @staticmethod
    def no(runtime, node):
        return NodesToScheduleStates.eval(runtime, node)

    @staticmethod
    def eval(runtime, node):
        if runtime.has_anomaly():
            return AnomalyStates.yes(runtime, node)
        else:
            return AnomalyStates.no(runtime, node)


def eval(runtime, node):
    """
    The primary entry point to the runtime state machine.

    Given a runtime and node, this function evaluate's their combined states
    and returns a code indicating what should be done next.
    """
    if runtime.current is None:
        return DONE
    return ExecStates.eval(runtime, node)
