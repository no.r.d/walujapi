"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--
The base class for the secondary construct of the Graph "lanquage", the Edge.

Edges are the connections between vertices of the graph. Edges move data and
execution between nodes in Nord. Like Nodes, Edges are expected to be subclassed.
This module provides the common capabilities all edges should have. Subclasses
are welcome to override these methods as needed.
"""
from nord.nord.exceptions import UseException as UseException
from nord.nord.exceptions import RuntimeException as RuntimeException
from nord.nord.exceptions import EdgeException as EdgeException
from nord.nord.exceptions import ImplementationException as ImplementationException
import nord.nord.utils.SpaceManager as use
import sys
import nord.nord.Base as Base


class Edge(Base):
    """
    Edges are the connections between vertices of the graph. Edges connect Nodes in the Map.

    This class is the base class used to implement relationships between different types of Nodes.
    An example is the Assign subclass.

    >>> Edge() #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE
    Edge: [None]  --Edge-->  [None]
    """

    # Setting this to true will de-reference the rules to functions, only the first time
    # # the edge is followed, instead of everytime. TBD if this has negative consequences.
    _CI_edge_cache_found_rules = True

    def __init__(self, graph):
        """
        All nodes and edges are associated with a graph.

        When they are created, changed, or removed, the graph
        is notified, such that it can inform any and all clients.
        """
        self._graph = graph
        self._src_rule_override = None
        self._tgt_rule_override = None
        self._proxy_source = None
        self._proxy_target = None
        if graph is not None:
            self._graph.ccap_edge_added(self)

    def override_src_rule(self, newrule):
        """
        Sets the override to be used instead of
        type(self).__name__ on the source end
        of the edge when following the edge.

        If no such rule exists (src.__class__.__name__ + self._src_rule_override)
        the follow will throw an exception at runtime.
        """
        self._src_rule_override = newrule

    def override_tgt_rule(self, newrule):
        """
        Sets the override to be used instead of
        type(self).__name__ on the target end
        of the edge when following the edge.

        If no such rule exists ( self._tgt_rule_override + tgt.__class__.__name__)
        the follow will throw an exception at runtime.
        """
        self._tgt_rule_override = newrule

    def proxy_source(self, source):
        """
        """
        self._proxy_source = source

    def proxy_target(self, target):
        """
        """
        self._proxy_target = target

    def set_untracked_attr(self, attribute, value):
        """
        Used to associate values with a Node which will not
        be tracked by the change capture __setattr__ system.
        """
        self.__dict__[attribute] = value

    def delete(self):
        """
        Notify the Map/Graph that this node has been deleted
        """
        self.get_base_source().rem_target(self.get_base_target(), self)
        self.get_base_target().rem_source(self.get_base_source(), self)
        if self._graph:
            self._graph.ccap_edge_deleted(self)

    def get_graph(self):
        """
        Return the graph object with which this edge is associated
        """
        return self._graph

    def set_nodes(self, src, tgt, rule):
        """
        Assign the nodes at either end of the edge.

        >>> from nord.Node import Node
        >>> src = Node()
        >>> src.apply({"id": 1, "name": "fred", "value": 77})
        >>> tgt = Node()
        >>> tgt.apply({"id": 2, "name": "larry", "value": 92})
        >>> e = Edge()
        >>> e.set_nodes(src, tgt, 'Dummy')
        >>> repr(e) #doctest: +NORMALIZE_WHITESPACE
        'Edge: [Node(Node): <id: 1, name: fred>]  --Edge-->  [Node(Node): <id: 2, name: larry>]'

        """
        self.source = src
        self.target = tgt
        self.rule = rule
        src.set_target(tgt, rule, self)
        if self._graph is not None:
            self._graph.add_edge(self.source, self.target, self.rule, self)

    def flag_followed(self, runtime):
        """Flag this edge as having been followed."""
        if not hasattr(self, "_followed"):
            pass
        self._followed = runtime.get_exec_count()

        # tell the source about this edge having been followed
        self.get_base_source().set_followed(self, source=True)
        self.get_base_target().set_followed(self, source=False)

    def clear_followed(self):
        """Remove the Flag that this edge has been followed."""
        if hasattr(self, "_followed"):
            delattr(self, "_followed")

        # tell the source about this edge having been unfollowed
        self.get_base_source().set_unfollowed(self, source=True)
        self.get_base_target().set_unfollowed(self, source=False)

    def been_followed(self, runtime=None):
        """Indicate if this edge has been followed yet."""
        if hasattr(self, "_followed"):
            if runtime is not None:
                return self._followed >= runtime.get_exec_count()
            else:
                return True
        else:
            return False

    def set_cargo(self, cargo):
        """
        Set this edge's payload to the passed cargo data.

        Used for the source rules to store the meaningful
        data for use by the target rules
        """
        self._cargo = cargo

    def get_cargo(self):
        """
        Return this edge's cargo, if it exists.

        Used for the target rules to retrieve the meaningful
        data placed by the target rules
        """
        if hasattr(self, '_cargo'):
            return self._cargo
        else:
            return None

    def has_cargo(self):
        """Return whether or not this edge has cargo."""
        return hasattr(self, '_cargo')

    def apply_delegate(self, rule):
        """Given the input rule, attempt to employ the delegation requested by its edge."""
        rv = False
        if hasattr(rule.edge, 'delegate'):
            method = rule.edge.delegate['uid']
            if rule.edge.delegate['direction'] == 'as_src':
                node = rule.edge.get_source()
            else:
                node = rule.edge.get_target()
            if hasattr(node, method):
                func = getattr(node, method)
                rv = func(rule)
                if rv is None:
                    # If the implementor did not elect to return anything, assume
                    # execution should continue, so, return False.
                    rv = True
            else:
                raise RuntimeError(_("Extension implementation error."  # noqa: F821
                                     " no method named {} in delegate: {}").format(method, node))
        else:
            raise RuntimeError(_("Extension implementation error."  # noqa: F821
                                 " apply_delegate requested, but no delegates available."))
        return rv

    def traverse(self, srule, trule):
        """
        Abstract method called when the edge is followed.

        This method can be overridden by subclasses to perform whatever special magic
        they will perform to take the return from the source rule and pass that to the target rule

        if they return false, (which should really be if the rules return false), then, the edge
        will not get flagged as followed (unless they are also not using the factory follow method...)
        """
        if srule is None or trule is None:
            raise EdgeException(_("Edge traversal impossible with 'None' rule"))  # noqa: F821
        if srule.apply():
            return trule.apply()
        return False

    def follow(self, runtime, verbose=False):
        """
        Main "execution" method for all edges.

        Following an edge is a recursive (via the way the runtime works, not directly recursive)
        function that ensures all the source node source edges have been followed.

        However, if an edge is in an event path, and the runtime is not in an event,
        just flag the edge as followed, but do not follow it yet.
        """
        if verbose:
            print("   ---> following: {}".format(self))

        if not runtime.is_handling_event() and\
                runtime.in_event_path(self.source) and\
                runtime.in_event_path(self.target):
            """
            This edge is along the event path, but, the runtime
            is not processing an event, so, flag the edges as followed
            but, do not follow it.
            """
            self.flag_followed(runtime)
            return True

        if hasattr(self, '_srule') and\
                hasattr(self, '_trule') and\
                self.get_cfg_value('edge_cache_found_rules') and\
                self._proxy_target is None and\
                self._proxy_target is None:
            rv = self.traverse(self._srule, self._trule)
            if rv is None:
                raise ImplementationException(f"{_('Rule supporting edge {self.__class__.__name__} does not return boolean')}")  # noqa: F821, E501
            if rv:
                self.flag_followed(runtime)
            return rv

        # Only add edges that terminate with the current node, if they haven't been followed before

        # Handle source rule override if set
        if self._src_rule_override is None:
            src_rule = type(self).__name__
        else:
            src_rule = self._src_rule_overridecapitalize()

        if self._proxy_source is None:
            src_node = type(self.source).__name__
            wrk_source = self.source
        else:
            src_node = type(self._proxy_source).__name__
            wrk_source = self._proxy_source
        src_modname = "{}{}".format(src_node, src_rule)
        if 'space' in dir(wrk_source):
            provider = wrk_source.space
        elif hasattr(wrk_source, 'get_space'):
            provider = wrk_source.get_space()
        else:
            provider = 'nord'

        use.space(provider)
        try:
            mod = use.module('nord.{}.rules.{}'.format(provider, src_modname))
        except UseException:
            raise RuntimeException(
                "Executing node: {} - No src rule execution source module named '{}.rules.{}' available"  # noqa: E501
                .format(str(runtime.current), provider, src_modname))

        if mod is not None:

            srule = mod(wrk_source, runtime, self)

        else:
            raise RuntimeException(
                "Executing node: {} - No src rule execution source module named '{}.rules.{}' available"  # noqa: E501
                .format(str(runtime.current), provider, src_modname))  # pragma: no cover

        # Handle source rule override if set
        if self._tgt_rule_override is None:
            tgt_rule = type(self).__name__
        else:
            tgt_rule = self._tgt_rule_override.capitalize()

        if self._proxy_target is None:
            tgt_node = type(self.target).__name__
            wrk_target = self.target
        else:
            tgt_node = type(self._proxy_target).__name__
            wrk_target = self._proxy_target
        tgt_modname = "{}{}".format(tgt_rule, tgt_node)
        if 'space' in dir(wrk_target):
            provider = wrk_target.space
        elif hasattr(wrk_target, 'get_space'):
            provider = wrk_target.get_space()
        else:
            provider = 'nord'

        use.space(provider)

        try:
            mod = use.module('nord.{}.rules.{}'.format(provider, tgt_modname))
        except UseException:
            raise RuntimeException(
                "Executing node: {} - No tgt rule execution source module named '{}.rules.{}' available"  # noqa: 501
                .format(str(runtime.current), provider, tgt_modname))  # pragma: no cover

        if mod is not None:

            trule = mod(wrk_target, runtime, self)

        else:
            raise RuntimeException(
                "Executing node: {} - No tgt rule execution source module named '{}.rules.{}' available"  # noqa: 501
                .format(str(runtime.current), provider, tgt_modname))  # pragma: no cover

        self._srule = srule
        self._trule = trule

        rv = self.traverse(srule, trule)
        if rv is None:
            raise ImplementationException(f"{_('Rule supporting edge {self.__class__.__name__} does not return boolean')}")  # noqa: F821, E501
        if rv:
            self.flag_followed(runtime)
        return rv

    def source_has_executed(self):
        if not hasattr(self, '_operate_in_reverse'):
            return self.source.has_executed()
        else:
            return self.target.has_executed()

    def target_has_executed(self):
        if not hasattr(self, '_operate_in_reverse'):
            return self.target.has_executed()
        else:
            return self.source.has_executed()

    def get_source(self):
        if hasattr(self, '_operate_in_reverse') and self._proxy_target:
            return self._proxy_target
        elif self._proxy_source:
            return self._proxy_source
        else:
            return self.get_base_source()

    def get_base_source(self):
        if not hasattr(self, '_operate_in_reverse'):
            return self.source
        else:
            return self.target

    def get_target(self):
        if hasattr(self, '_operate_in_reverse') and self._proxy_source:
            return self._proxy_source
        elif self._proxy_target:
            return self._proxy_target
        else:
            return self.get_base_target()

    def get_base_target(self):
        if not hasattr(self, '_operate_in_reverse'):
            return self.target
        else:
            return self.source

    def disconnect(self):
        """
        remove this edge from source and target.
        """
        if self._graph:
            self._graph.rem_edge(self)
        self.get_base_source().rem_target(self.get_base_target(), self)
        self.get_base_target().rem_source(self.get_base_source(), self)

    def to_dict(self):
        """
        Convert this edge to a dictionary for saving / serializing.

        Return a dictionary of all simple datatype members, or
        known dictionaries, as well as the source, target, and rule
        """
        rv = {}
        for k in (k for k in sorted(self.__dict__.keys()) if not k.startswith("_")):
            v = getattr(self, k)
            if type(v) in (str, int, float, bool):
                rv[k] = v
            elif k in ("delegate",) and type(v) == dict:
                rv[k] = v.copy()

        rv["source"] = "{}:{}".format(self.source.id, self.source.name if hasattr(self.source, "name") else "___")
        rv["target"] = "{}:{}".format(self.target.id, self.target.name if hasattr(self.target, "name") else "___")
        rv["rule"] = self.rule.lower()
        if self._spacename != "nord" or\
                self.source.get_space() != 'nord' or\
                self.target.get_space() != 'nord':
            rv["space"] = self._spacename
            rv["to_space"] = self.target.get_space()
            rv["from_space"] = self.source.get_space()

        return rv

    def debug_to_dict(self):
        """
        Convert this edge to a dictionary for saving / serializing.

        Return a dictionary of all simple datatype members, or
        known dictionaries, as well as the source, target, and rule
        """
        rv = {}

        rv["string"] = repr(self)
        if hasattr(self, '_cargo'):
            if callable(self._cargo):
                rv["cargo"] = f"{self._cargo.__name__}()"
            else:
                rv["cargo"] = self._cargo
        else:
            rv["cargo"] = "- empty -"

        if hasattr(self, '_followed'):
            rv["exec_count"] = self._followed
        else:
            rv["exec_count"] = "-"

        if self.source.get_op_runtime():
            rv["rt_exec_cnt"] = self.source.get_op_runtime().get_exec_count()
        elif self.target.get_op_runtime():
            rv["rt_exec_cnt"] = self.target.get_op_runtime().get_exec_count()
        else:
            rv["rt_exec_cnt"] = "??"

        rv["src_exe"] = self.source_has_executed()
        rv["tgt_exe"] = self.target_has_executed()

        rv["followed"] = self.been_followed()

        return rv

    def render_dbg_text(self, txt, dat):
        """
        Given the dict of dbg info from the backend,
        return a renderable string and whether this edge has been followed
        """
        if dat['src_exe']:
            src_exe = "x--"
        else:
            src_exe = "---"

        if dat['tgt_exe']:
            tgt_exe = '--x>'
        else:
            tgt_exe = '--->'

        pfx = ""
        sfx = ""
        if dat["followed"]:
            pfx = "--------------\n"
            sfx = "\n--------------"
        else:
            pfx = ".   .   .   .\n"
            sfx = "\n.   .   .   ."

        txt = f"{pfx}{dat['exec_count']}/{dat['rt_exec_cnt']}  " + \
            f"self/rt exe cnt\n{src_exe}[{dat['cargo']}]{tgt_exe}{sfx}\n\n" + txt

        return txt, dat["followed"]

    def __repr__(self):
        """Return the representation of this edge."""
        src = None
        if hasattr(self, "source"):
            src = self.source

        tgt = None
        if hasattr(self, "target"):
            tgt = self.target

        followed = ""
        if self.been_followed():
            followed = "**"
        rv = "{}: [{}]  --{}-->  [{}]{}"\
             .format(type(self).__name__, str(src), type(self).__name__, str(tgt), followed)
        return rv

    def __lt__(self, other):
        """
        compare sources first, if different.

        If the same, compare the targets, if they are different,

        If the targets are also the same, compare the rules.
        """
        if self.source != other.source:
            return self.source < other.source
        elif self.target != other.target:
            return self.target < other.target
        else:
            return self.rule < other.rule

    def __gt__(self, other):
        """
        compare sources first, if different.

        If the same, compare the targets, if they are different,

        If the targets are also the same, compare the rules.
        """
        if self.source != other.source:
            return self.source > other.source
        elif self.target != other.target:
            return self.target > other.target
        else:
            return self.rule > other.rule


sys.modules[__name__] = Edge
