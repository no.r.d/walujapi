"""
Copyright 2020 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--
The RunState class is used to decorate nodes
with the various state variables needed by
the Runtime and Node execution methods.
"""
import sys


class RunState(object):
    """
    Class that stores a node's execution state for the frame and runtime.

    Each Node, when provided to the runtime to be executed can have a number
    of differents states. Instances of this class record and manipulate that
    state.
    """

    @staticmethod
    def prepare(node):
        """Inject an instance of the runstate into the node."""
        node._run_state = RunState()

    @staticmethod
    def s(node):
        """Shorthand to fetch a node's runstate."""
        if not hasattr(node, '_run_state'):
            RunState.prepare(node)
        return node._run_state

    def __init__(self):
        """
        Initialize the default state.

        _deferred - When set, allows a node's next_after_execute
          method after the frame stack drops back into the deferred
          node's frame. (For instance, a container is executed, so the
          container pushes a new frame on the stack, and the contents
          of the container can then be executed, and when done, the
          container's downstream targets will be run)

        _pending_run - Used by the Frame to prevent re-adding an already
          added node to the next_nodes list (infinite loop prevention)

        _preempted - Used to indicate to the runtime that this node has
          upstream nodes that haven't yet been run. So, it preempts itself,
          sets the upstreams to next, and lets the upstream work on the
          upstream nodes first.

        _executed - used to determine if the node has been run
          during this "iteration" of the runtime. Iterations are incremented
          when the runtime runs out of nodes to run (but allows for eventing
          nodes to resume execution)

        _blocked - Indicates that this node es expecting input to
          delivered to it. (via the runtime set_input method)

        _runtime - stores the runtime for future reference.
        """
        self.reset()

    def reset(self, runtime=None, exc_cnt=-1):
        self._deferred = False
        self._pending_run = False
        self._preempted = False
        self._executed = exc_cnt
        self._blocked = False
        self._runtime = runtime
        self._upstream_eventors = {}

    def set_upstream_eventor(self, eventor_id):
        self._upstream_eventors[eventor_id] = True

    def downstream_of_eventor(self, eventor_id):
        """
        return true if the passed eventor has marked this
        node as downstream
        """
        return eventor_id in self._upstream_eventors

    def has_eventor_upstream(self):
        """
        Return true if there are any eventors identified for this node.
        """
        return len(self._upstream_eventors) > 0

    def set_executed(self, rt):
        if rt is not None:
            self._runtime = rt
            self._executed = rt.get_exec_count()
        else:
            self._executed = -1

    def unset_executed(self, rt):
        if rt is not None:
            self._runtime = rt
            self._executed = rt.get_exec_count() - 1
        else:
            self._executed -= 1

    def executed(self):
        if self._runtime is None:
            return False
        else:
            rtxc = self._runtime.get_exec_count()
            rv = self._executed >= rtxc
            return rv

    def set_pending_run(self):
        self._pending_run = True

    def unset_pending_run(self):
        self._pending_run = False

    def pending_run(self):
        return self._pending_run

    def set_preempted(self):
        self._preempted = True

    def unset_preempted(self):
        self._preempted = False

    def preempted(self):
        return self._preempted

    def set_blocked(self):
        self._blocked = True

    def unset_blocked(self):
        self._blocked = False

    def blocked(self):
        return self._blocked

    def set_deferred(self):
        self._deferred = True

    def unset_deferred(self):
        self._deferred = False

    def deferred(self):
        return self._deferred

    def runtime(self):
        return self._runtime

    def __getattr__(self, name):
        """
        In order to allow for space/extension specific usage, this
        catchall member-defining function can be used
        to set, unset, and check flags against nodes. There is
        no safety net to ensure that attribute names do not
        overlap, so, some naming convention should be employed.
        "<space>"_"<flagname>" might be a decent convention to follow.
        Wrap all undefined member attributes with three methods.

        The methods are:

        set_<name>()
        unset_<name>()
        <name>()

        where <name>() returns the current value for that
        attribute.
        """
        if name.startswith('set'):
            attr = name[3:]
            x = self

            def _set():
                setattr(x, attr, True)
            return _set
        elif name.startswith('unset'):
            attr = name[5:]
            x = self

            def _unset():
                setattr(x, attr, False)
            return _unset
        elif name.startswith('del'):
            attr = name[3:]
            x = self

            def _del():
                delattr(x, attr)
            return _del
        elif f'_{name}' in self.__dict__:
            x = getattr(self, f'_{name}')
            return lambda: x
        elif name.startswith('_'):
            raise AttributeError(name)
        else:
            return lambda: None


sys.modules[__name__] = RunState
