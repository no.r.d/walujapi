"""
Copyright 2020 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--
The Runtime Frame module manages the list of nodes
to execute.
"""
import nord.nord.RunState as RS
import nord.nord.RuntimeStateMachine as RSM
from collections import deque
import sys


class Frame(object):
    """
    Frames are the entries in the processing stack.

    Frames hold the prior states so that when the
    current node and next nodes are completed, returned, or
    anomalied, the runtime can pick up where it left off
    in the prior level of the stack.
    """

    def __init__(self):
        """Create a new frame item."""
        self.current = None
        self.ret_val = None
        self.next_nodes = deque()
        """
        Looking is the indication of how this node was presented to the frame.
        If the current node was added to next_nodes by one of it's target
        nodes, then the direction is 'UP', otherwise it is 'DOWN'

        This directionality is used when evaluating "Event Hold Candidacy"
        also known as EHC. EHC Nodes control flow and event propogation
        such that a running graph will only execute in an event driven fashion
        if so drawn.
        """
        self.looking = 'DOWN'

    def inject_node(self, node, direction):
        """Make 'node' the first one in the list of nodes to run next."""
        if not RS.s(node).pending_run():
            self.next_nodes.append((node, direction))
            RS.s(node).set_pending_run()
        elif RS.s(node).preempted():
            RS.s(node).unset_preempted()

    def append_node(self, node, direction):
        """Add the 'node' to the end of the list of nodes to run next."""
        if not RS.s(node).pending_run():
            self.next_nodes.appendleft((node, direction))
            RS.s(node).set_pending_run()
        elif RS.s(node).preempted():
            RS.s(node).unset_preempted()

    def has_started(self):
        """Return True if current is not None."""
        return self.current is not None

    def prep_next(self):
        """Prepare to run the next node in the frame."""
        # self.nextNode = None
        # self.currentReturn = None
        # self.anomaly = None

    """
    def next(self):
        if len(self.next_nodes) > 0:
            self.current = self.next_nodes[0]
            del self.next_nodes[0]
            return True
        else:
            return False
    """

    def set_next(self):
        """
        Set the current pointer to the next node in the frame.

        But, only set the current pointer if the current
        is not deferred.
        """
        if self.current and RS.s(self.current).deferred():
            return
        if len(self.next_nodes) > 0:
            saved_current = None
            if self.current and not RS.s(self.current).executed():
                saved_current = self.current
                saved_looking = self.looking
                # In case this had already been pushed off to later,
                # clear the pending run flag, so we can put this back at the
                # then end of the run list.
                RS.s(self.current).unset_pending_run()
            self.current, self.looking = self.next_nodes.pop()
            # Need to get this node onto runtime path of
            # executed nodes....?
            if saved_current:
                self.append_node(saved_current, saved_looking)
        else:
            self.current = None

    def replace_current(self, node, direction='DOWN'):
        """Replace the current pointer with the passed node."""
        if self.current is not None:
            # This functionality is potentially dangerous... use at your own
            # risk. This is used by the Reference node to enable
            # de-referencing. Other uses haven't been thought through.

            # This is also used by Athena to trigger event handling. See _base.py
            pass
            # raise Exception("PROBABLE IMPLEMENTATION BUG...."
            #                 "frame.replace_current probably shouldn't be called when current is set.")
        self.current = node
        self.looking = direction

    def set_deferred(self):
        """Mark the current node as deferred."""
        RS.s(self.current).set_deferred()

    def set_return(self, retval):
        """Set the return value for this frame."""
        self.ret_val = retval

    def get_return(self):
        """Get the return value of this frame."""
        return self.ret_val

    def handle_empty_current(self):
        """
        If the current pointer is None
        try to populate it from next_nodes.
        """
        if self.current is None:
            if len(self.next_nodes):
                self.current, self.looking = self.next_nodes.pop()
                return True
            else:
                return False
        else:
            return True

    def handle_exec_tail(self, runtime):
        """
        perform the steps after complete run_node
        branches are followed.
        """
        RS.s(self.current).unset_pending_run()

        if not RS.s(self.current).blocked():
            self.current.next_after_execute(runtime, runtime.verbose)

    def run_node(self, runtime):
        """Execute the current node and update state accordingly."""

        # skip all this starting stuff if this node has been set to deferred
        # as it will be picked up later when complete stack is called
        if not self.handle_empty_current():
            return False, RSM.SCHEDULE_NEXT

        state = RSM.eval(runtime, self.current)
        if state == RSM.DEFERRED:
            rv = True
        else:
            RS.s(self.current).unset_pending_run()
            rv = False

        return rv, state






        # If blocked or deferred, clear pending run state
        # to enable subsequent processing
        if not RS.s(self.current).deferred() or RS.s(self.current).blocked():
            if runtime.verbose:
                print("\nExecuting node: {}\n".format(repr(self.current)))

            # Ask the node to get ready to execute
            self.current.prep_to_execute(runtime, runtime.verbose)
            # runtime.export_as_graphviz()
            if not RS.s(self.current).preempted():
                # Call the node'self execute method
                self.current.execute(runtime)
                # runtime.export_as_graphviz()
            else:
                # this node has been preempted by unfollowed sources.
                return False

            if RS.s(self.current).deferred():
                return True
        else:
            RS.s(self.current).unset_deferred()

        self.handle_exec_tail(runtime)
        # runtime.export_as_graphviz()
        return False

    def set_done(self):
        """
        None'ify self.current such that any calls to done
        will return True, indicating this frame is finished
        """
        self.current = None

    def done(self):
        """Check if the current node is out of edges to run next."""
        if self.current is not None:
            if len(self.next_nodes) == 0:
                return not self.current.has_out_edges()
            else:
                return False
        else:
            return True

    def get_current_node(self):
        """Return the frame's current node."""
        return self.current

    def get_current_direction(self):
        """Return the frame's current node direction from which the current node was reached."""
        return self.looking

    def get_next_nodes(self):
        """
        Return the current queue of nodes to run next
        """
        return self.next_nodes

    def is_blocked(self):
        """Return whether or not execution is blocked on the current node."""
        if self.current:
            return RS.s(self.current).blocked()
        else:
            return False


sys.modules[__name__] = Frame
