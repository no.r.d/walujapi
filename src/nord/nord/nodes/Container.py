"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""
import nord.nord.Receiver as Receiver
import nord.nord.Property as Property
import nord.nord.exceptions.RuntimeException as RuntimeException
import sys


class Container(Receiver):
    """
    Container Map elements are used to group related elements together. Containers are somewhat analogous to
    namespaces and blocks from the textual programming paradigm. A container might be subclassed to create
    methods, "classes", "packages", "applications", and so on

    >>> Container() #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE
    Container:

    """
    def __init__(s, graph):
        super().__init__(graph)
        s._properties = {
            'menuicon': Property(s,
                                 'menuicon',
                                 default='unknown',
                                 description=_(  # noqa: F821
                                        "The name of the icon to use when "
                                        "Use'ing this container in another Map"))
        }

    def prep_to_execute(self, runtime, verbose):
        """
        Prior to calling the default node prep_to_execute method

        check to see if anything is being passed to this container, and pass those
        values to the sourceless nodes (in order)


        This should compliment decoration based argument passing.


        So, to be implemented:

           Container should update it's delegates on container exit such that
           all sourceless nodes which are descendents of Data are presented as
           delegates (( THIS SHOULD ALL BE IN RECEIVER ))

           All delegates should just passthrough to the represented node

           However, if pass edges are directly hitting the receiver, then
           those cargos should be applied in order to the candidate sourceless
           nodes (not already handled as delegates.)

        """
        # If there aren't any cached argument pass throughs
        # identify and align delegated pass edges.
        # if there are edges directly targeting this container
        # then, in source sorted order, connect those edges
        # to the sorted list of un-connected contained nodes.
        # CDC will need to be disabled when changing the
        # targets to the contained nodes such that, these
        # runtime graph changes are not propogated to the
        # front end. (Not sure this graph manipulation is a good idea...)
        wrk_srcless = []
        wrk_tgtme = []
        if not hasattr(self, '_contained_sourceless'):
            self._contained_sourceless = {}
            for edge in self.get_targets('contains'):
                node = edge.get_target()

                if list(node.get_source_rules()) == ["contains"] and\
                        (node.eligible_for("assign") or node.eligible_for("pass")):
                    # this is a sourceless node.
                    self._contained_sourceless[node.id] = node
                    wrk_srcless.append(node)

        for rule in self.get_source_rules():
            for edge in self.get_sources(rule):
                if hasattr(edge, "delegate") and edge.delegate["uid"] in self._contained_sourceless:
                    node = self._contained_sourceless[edge.delegate["uid"]]
                    edge.proxy_target(node)
                    if not node.eligible_for("pass") and node.eligible_for("assign"):
                        edge.override_tgt_rule("assign")
                    elif not node.eligible_for("assign"):
                        raise RuntimeException(_("invalid sourceless node {}").format(node))  # noqa: F821
                    wrk_srcless.remove(node)
                elif rule in ["pass", "assign"]:
                    # capture edges that are trying to get arguments into
                    # this container
                    wrk_tgtme.append(edge)

        if len(wrk_srcless) >= len(wrk_tgtme):
            # All inbound edges to the container can be satisfied.
            for node, edge in zip(wrk_srcless, wrk_tgtme):
                edge.proxy_target(node)
        else:
            raise RuntimeException(_("Too many arguments to container: {}").format(self))  # noqa: F821

        return True
        # new runtime state machine obviates the need to
        # call the superclass prep_to_execute.
        # super().prep_to_execute(runtime, verbose)

    def execute(s, runtime):
        """
            The runtime looks (automatically) for flow rules
            from the container to any of the nodes within the container

            This method will establish flow rules to all contained nodes
            without any other edges targetting them, if there are no flow
            edges to any of the contained nodes

            The nodes will be sorted by name, then ID if there are more
            than one to be kicked off
        """

        # don't bother with this if there are any flow targets
        # contained by this node
        for edge in s.get_targets('flow'):
            for e2 in edge.get_target().get_sources('contains'):
                if e2.source == s:
                    s.flag_exe(runtime)
                    return

        # add a nev "frame" to the stack for this container
        runtime.pushStack()

        # create an empty list to store any nodes we will need to
        # push into a "new stack" to execute before continuing on with this containers
        # targets
        newFlowReceivers = list()
        # So, first get a list of all the edges from
        # this container to the nodes it contains
        for edge in s.get_targets('contains'):
            topTarget = True
            # Then, eliminate all nodes from candidacy for
            # being the start of a "new stack" which have
            # non-containing edges, or, whose containing
            # edges have already been followed
            for rule in edge.get_target().get_source_rules():
                if rule != 'contains':
                    # for every edge in this rule type
                    # check to see if any haven't yet been followed.
                    # if none have, then, this is a candidate to
                    # run in a "new stack"
                    alreadyFollowed = True
                    for e4 in edge.get_target().get_sources(rule):
                        if e4.source == s and not e4.been_followed(runtime):
                            alreadyFollowed = False
                            e4.flag_followed(runtime)
                    if alreadyFollowed:
                        topTarget = False
                else:
                    # check each containing edge, and if any from this container
                    # have not been followed set for candidacy to start a "new stack"
                    """
                    alreadyFollowed = True
                    for e3 in edge.target.get_sources('contains'):
                        if e3.source == s and not e3.been_followed(runtime):
                            alreadyFollowed = False
                    if not alreadyFollowed:
                        e3.flag_followed(runtime)
                        topTarget = True

                        break
                    """

            if topTarget:
                newFlowReceivers.append(edge.get_target())

        for tgt in sorted(newFlowReceivers):
            # e = Flow()
            # e.set_nodes(s, tgt, 'flow')
            runtime.topframe_append_node(tgt, 'DOWN')

        s.flag_exe(runtime)

        for edge in s.get_targets("contains"):
            edge.flag_followed(runtime)


sys.modules[__name__] = Container
