"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

import nord.nord.Receiver as Receiver
from nord.nord.exceptions import RuntimeException as RuntimeException
import sys
import nord.sigurd.utils.namegen as ng


class Return(Receiver):
    """
    Return nodes are the last nodes executed within a
    container. They pass their value to their container's
    assign and pass edges

    """
    def __init__(s, graph, name=None, ntype=None):
        super().__init__(graph)
        if name is None:
            s.set_untracked_attr('name', ng.get(s))
        else:
            s.set_untracked_attr('name', name)
        if ntype is not None:
            s.set_untracked_attr('type', ntype)
        s.set_untracked_attr('verbose', False)

    def execute(s, runtime):
        """
            grab the assigned value and
            set it to the cargo for any and all
            pass and assign edges from the
            container which holds this return

            A potential bug here is that the graph structure
            allows the Return to be contained by more than
            one container... So, throw a runtime exception
            if we detect more than one contains src
        """
        # Grab the runtime's verbosity so it can be used in hosOutEdges
        s.verbose = runtime.verbose

        containers = s.get_sources('contains')

        val = s.get_arg_values()

        if len(containers) > 1:
            raise RuntimeException(_("Cannot share Return Receiver with multiple containers"))  # noqa: F821

        for container in [x.source for x in containers]:
            for passedge in container.get_targets('pass'):
                passedge.set_cargo(val)

            for assignedge in container.get_targets('assign'):
                assignedge.set_cargo(val)

        s.flag_exe(runtime)

    def next_after_execute(self, runtime, verbose):
        """
        Once the frame is done looking at thes node
        Cause the current pointer to be None'd
        such that the frame ceases to be operated upon
        """
        super().next_after_execute(runtime, verbose)
        runtime.set_frame_return(self.get_arg_values())

    def has_out_edges(s):
        """
            over rides the check for out edges to follow any that exist
            , BUT, signal the runtime that it does not.
        """
        trules = (x for x in s.get_target_rules() if x not in ('flow'))
        for r in trules:
            for edge in s.get_targets(r):
                if not edge.been_followed():
                    edge.follow(s, s.verbose)
        return False


sys.modules[__name__] = Return
