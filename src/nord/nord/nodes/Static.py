"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

import nord.nord.Node as Node
import nord.nord.Property as Property
import nord.sigurd.utils.namegen as ng
import sys


class Static(Node):
    """
    Static vertices in the Map are used to "hardcode" values into the Map to be
    used / referenced by other vertices.

    >>> s = Static()
    >>> s.apply({'value': "HelloWorld"})
    >>> s #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE
    Static:
        .value = HelloWorld

    """
    def __init__(s, graph, name=None, ntype=None, value=None):
        s._properties = {
            'value': Property(s,
                              'value',
                              default=None,
                              description=_("""The value of the static 'variable'"""))  # noqa: F821
        }

        super().__init__(graph)
        if name is None:
            s.set_untracked_attr('name', ng.get(s))
        else:
            s.set_untracked_attr('name', name)
        if ntype is not None:
            s._type = ntype

    def get(s):
        """
        Get the value from this data node
        """
        return s.get_property('value')


sys.modules[__name__] = Static
