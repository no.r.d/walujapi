"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

import nord.nord.Receiver as Receiver
from nord.nord.exceptions import BuiltinException as BuiltinException
import nord.nord.Property as Property
import sys

from nord.nord.builtins import *  # noqa: F403


class Builtin(Receiver):
    """
    Builtin vertices in the Map are used to provide standard system functions like stdlib in "C"

    >>> Builtin() #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE
    Builtin:

    """
    def __init__(s, graph):
        super().__init__(graph)
        s._properties = {
            'method': Property(s,
                               'method',
                               default='out',
                               description=_("The builtin method to "  # noqa: F405
                                             "execute. Default is 'out'"))
        }

    def addArg(s, arg, edge):
        """
        initializes the Builtin argument list if need be,
        and adds this argument to the list
        """
        if not hasattr(s, "args"):
            s.args = list()
        if len(s.args) > 0 and not hasattr(edge, "name") and not hasattr(edge, "position"):
            raise BuiltinException("Pre-execution exception: passing more than 1 argument "
                                   f"to Builtin:{s.get_property('method')} via {str(edge)} "
                                   "requires edge with name or position attribute")

        s.args.append((arg, edge))

    def execute(s, runtime):
        """
        When a builtin is executed, it looks for a Module in the builtin directory (package)
        with the name in s.method.

        The builtin node will pass all parameters that have been passed to it
        to the named "method" module

        >>> from ..Map import Map
        >>> import nord.nord.Runtime as Runtime
        >>> m = json.load(open("samplemaps/helloworld.n", "r"))
        >>> karte = Map(m)  #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE
        >>> vessal = Runtime()
        >>> karte.run(vessal, None) #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE
        Hello World!

        >>> m = json.load(open("samplemaps/helloworld_2static_pos.n", "r"))
        >>> karte = Map(m)  #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE
        >>> vessal = Runtime()
        >>> karte.run(vessal, None) #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE
        Hello World!

        >>> m = json.load(open("samplemaps/helloworld_2static_name.n", "r"))
        >>> karte = Map(m)  #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE
        >>> vessal = Runtime()
        >>> karte.run(vessal, None) #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE
        first= Hello  second= World!

        """
        mod = globals()[s.get_property('method')]
        args, kwargs = s.get_arg_values()

        """
        Check if the requested builtin needs any contextual
        data prior to executing.
        """
        if f"{s.get_property('method')}_set_runtime" in globals():
            premod = globals()[f"{s.get_property('method')}_set_runtime"]
            premod(runtime)

        val = mod(*args, **kwargs)
        s._returned_value = val

        s.flag_exe(runtime)


sys.modules[__name__] = Builtin
