"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

Connect nodes in external entities with edges in the current graph.

This node should only need to be executed one time per map run
as the dereferenced contents will be innjected into the running
map/graph.
"""

import nord.nord.Receiver as Receiver
import nord.nord.Property as Property
from nord.nord.exceptions import RuntimeException as RuntimeException
import nord.nord.utils.SpaceManager as use
import sys


class Reference(Receiver):
    """
    Connect nodes in external entities with edges in the current graph.

    This node should only need to be executed one time per map run
    as the dereferenced contents will be innjected into the running
    map/graph.
    """

    def __init__(self, graph):
        """Constructor for Reference class."""
        super().__init__(graph)
        self._properties = {
            'parentid': Property(self,
                                 'parentid',
                                 description=_(  # noqa: F821
                                     "The id of the source object which holds what \
                                        this reference refers to")),
            'itemid': Property(self,
                               'itemid',
                               description=_(  # noqa: F821
                                   "The id of the object referred to within \
                                 the parent (source) object")),
            'menuicon': Property(self,
                                 'menuicon',
                                 default='unknown',
                                 description=_(  # noqa: F821
                                     "The name of the icon to use when rendering \
                                     this Reference"))
        }
        self._connected = False
        self._reference = None
        self._refHolder = None
        self._runtime = None

    def dereference(self, runtime):
        """
        Extract the container (parent) and the item from within it.

        This assumes that the parent object (graph, or external) has
        been loaded into the current runtime's map.
        """
        parent = runtime.map.get_node(self.parentid)
        item = parent.get_node(self.itemid, runtime)
        self._reference = item
        self._refHolder = parent
        return parent, item

    def prep_to_execute(self, runtime, verbose):
        """
        Perform the dereferencing if needed, cache the references.

        References can be executed. In order to do so, they have to
        "reach back" into the parent, find the itemid node, and then
        apply the rules targeting this reference to the referenced item
        which is all handled by the runtime
        """
        # super().prep_to_execute(runtime, verbose)
        super().schedule_unexec_sources(runtime)
        if runtime.runstate().s(self).preempted():
            return

        self._runtime = runtime
        if self.is_set_property('parentid') and self.is_set_property('itemid'):
            parent, item = self.dereference(runtime)

            if not self._connected:
                """
                    for all the edges which
                    target this reference
                    re-target them at the
                    referenced Receiver.... Likewise,
                    for all edges which this Receiver
                    is the source of, set the source
                    to be the referenced Receiver instead
                    this means that any subsequent
                    executions will directly operate
                    upon the referenced node. In theory,
                    a reference will only be executed once
                    and thus be de-referenced
                """

                for rule in self.get_source_rules():
                    for edge in self.get_sources(rule):
                        if edge not in item.get_sources(rule):
                            edge.set_nodes(edge.get_source(), item, rule)

                for rule in self.get_target_rules():
                    for edge in self.get_targets(rule):
                        if edge not in item.get_targets(rule):
                            edge.set_nodes(item, edge.get_target(), rule)

                self._connected = True
                # runtime.pushStack()
                runtime.replace_current_node(item)
                old_space = use.current_space()
                use.space(parent.get_space())
                item.schedule_unexec_sources(runtime, verbose)
                use.space(old_space)

            else:
                """
                    I'm not yet certain this is an error condition
                    but I'll treat it as such until need suggests
                    a Reference could be de-referenced more than
                    once
                """
                raise RuntimeException(
                    _("Attempted to re-de-reference {}").  # noqa: F821
                    format(self))  # pragma: no cover
        else:
            raise RuntimeException(_("Reference not properly intialized"))  # noqa: F821
        return False

    def execute(self, runtime):
        """Override Receiver.execute so the referenced item will be called next."""
        1 == 1
        pass


sys.modules[__name__] = Reference
