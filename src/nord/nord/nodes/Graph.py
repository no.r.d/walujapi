"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

import nord.nord.Node as Node
import nord.nord.Property as Property
import json
import nord.nord.utils.SpaceManager as use
from nord.nord.exceptions import RuntimeException as RuntimeException
import sys


class Graph(Node):
    """
    Graph Map elements are used reference external Maps (.n files for now) and
    allow them to be utilized by the current graph. Similar to pythons "import"
    functionality.

    """
    def __init__(s, graph):
        super().__init__(graph)
        s._properties = {
            'mapfile': Property(s, 'mapfile', description=_("""The 'use'able path to a NoRD mapfile. Usually possessing a *.n extension."""))
            , 'menuicon': Property(s, 'menuicon', default='unknown', description=_("""The name of the icon to use when Use'ing this Graph in another Map"""))
        }
        s._map = None

    def get_node(s, nid, runtime):
        """"""
        # if s._map is None:
        #     import nord.nord.Map as Map
        #     fname = s.get_property('mapfile')
        #     s._map = Map(json.load(use.file(fname)))
        # node = s._map.get_node(nid)
        node = runtime.map.get_node(nid)
        if node is None:
            raise RuntimeException(_("Requested Node with id: {} does not exist in {}").format(nid, s.mapfile))
        else:
            return node

sys.modules[__name__] = Graph
