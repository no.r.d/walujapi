"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

Provide for integration of external packages into runtime and visualization tooling.

Configuration, models, nodes, edges, and rules within a content space
are made available to the Use and loading routines.
"""

from pathlib import Path
from nord.nord.utils.UseUtil import Use
# from nord.ContentSpace import Space

_cs = Use(spacename="nord")  # Set up the default space
_in_space = "nord"
_spaces_loaded = {}

_catalog = {
    "nord": _cs,
}  # Dictionary of all known spaces


def get_mod_subs(module):
    """Return the unique exposed contents of the passed module."""
    rv = dir(module)
    if hasattr(module, '__all__'):
        rv += module.__all__
    # cast to set and then back to list to get unique values, then sort them
    # filtering out the elements that start with '_'
    rv = [x for x in list(set(rv)) if not x.startswith('_')]
    rv.sort()
    return rv


def get_spaces():
    """Return all the known spaces."""
    return _catalog.keys()


def current_space():
    """Return the name of the current space."""
    global _in_space
    return _in_space


def space_to_dict(space):
    """Convert the space parameters into a dictionary and return it."""
    if space in _catalog:
        return _catalog[space].to_dict()
    else:
        return None


def add_config(filename, prefix=None, parent_too=False):
    """
    Load spaces and their paths from provided file.

    The file format is:
    {space}
    {provides}:{path}
    {provides}:{path}

    {space}
    {provides}:{path}
    {provides}:{path}
    {provides}:{path}
    """
    with open(filename, 'r') as f:
        for line in [l.strip() for l in f if len(l.strip()) > 0]:  # noqa: E741
            if line.startswith("#") or\
                    line.startswith(';'):
                continue
            if ":" in line:
                arr = line.split('!')
                if prefix is not None:
                    parr = arr[1].split('://')
                    path = f"{parr[0]}://{prefix}{parr[1]}"
                else:
                    path = arr[1]
                add_location(path, arr[0])
                if parent_too:
                    path = Path(prefix).parent.resolve()
                    add_location(f"{parr[0]}://{path}", arr[0])
            else:
                space(line)


def space(name):
    """Create a new content space if needed and switch to it."""
    global _cs, _in_space
    if name not in _catalog:
        _catalog[name] = Use(spacename=name)
    _cs = _catalog[name]
    _in_space = name


def space_loaded(space):
    """
    Keep track of whether or not a space
    has had its configuration loaded. It would
    be better if load_space_config could be called
    idempotently....
    """
    global _spaces_loaded
    if space not in _spaces_loaded:
        _spaces_loaded[space] = 1
        return False
    else:
        return True


def space_taken(name):
    """Return if the requested name is in use."""
    return name in _catalog


def set_protocol(name, classinst):
    """Add the provided protocol handler to the current space."""
    _cs.set_protocol(name, classinst)


def add_override(name, overrides, location, provider):
    """Override, with name, the overrides' item with the passed location and provider."""
    _cs.add_override(name, overrides, location, provider)


def add_location(location, provides):
    """Add the passed location to the current space."""
    _cs.add_location(location, provides)


def prepend_location(location, provides):
    """Insert the passed location to the beginning of the current space's search path."""
    _cs.prepend_location(location, provides)


def add_provider(provider, means, defloc):
    """Add the provider named provider to the current space's providers."""
    _cs.add_provider(provider, means, defloc)


def file_exists(filename):
    """Return whether the file requested exists."""
    return _cs.file_exists(filename)


def file(name, mode='r'):
    """Return an opened file, that is findable in the searchpath via 'name'."""
    return _cs.file(name)


def module(name):
    """Import the named module, return the handle to imported code."""
    return _cs.module(name)


def path(name):
    """
    Return local path to named asset.

    (the protocols will make the resource local if it is not already)
    """
    return _cs.path(name)


def get(provider, name):
    """Generic get method where the consumer specifies which provider to use."""
    return _cs.get(provider, name)
