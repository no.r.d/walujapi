"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--
Collection of helpful tools.
"""

import sys
import json


def remove_flapping_edges(mapfname):
    """
    given a ".n" map file, load it, and evaluate all the edges, removing any that lack
    a source, target or both.
    """
    m = None
    with open(mapfname, 'r') as fp:
        m = json.load(fp)
        nodes = m.get('map', {}).get('nodes', [])
        nodes = {n['id']: n for n in nodes}
        edges = m.get('map', {}).get('edges', {})
        validedges = []
        for edge in edges:
            sid = edge["source"].split(':')[0]
            tid = edge["target"].split(':')[0]

            if sid in nodes and tid in nodes:
                validedges.append(edge)

        del m["map"]["edges"]
        m["map"]["edges"] = validedges

    if m is not None:
        with open(mapfname, 'w') as fp:
            json.dump(m, fp, indent=2)


if __name__ == '__main__':

    _menu = {
        'rfe': (remove_flapping_edges, "Remove Flapping Edges"),
    }

    def show_help():
        print("Available tools:")
        for k, v in _menu.items():
            print(f"{k} - {v[1]}")
        print("")

    if len(sys.argv) == 1:
        show_help()
        sys.exit(1)

    func = _menu.get(sys.argv[1], show_help)[0]
    func(*sys.argv[2:])
