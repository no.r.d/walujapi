"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

import sys
import os
import traceback

_karte = None
_rt = None
_args = None
_loop = True


def _mapOK():
    if _karte is not None:
        return True


def _next():
    if _mapOK():
        _rt.next()


def _continue():
    _rt.unpause()
    if _mapOK():
        _rt.continue_exec()


def _print():
    if _mapOK():
        print("Current Node:\n\n{}".format(repr(_rt.current.current)))


def _quit():
    global _loop
    _loop = False
    return False


def _help():
    print("Debugger Help:")
    for k in _key2func:
        print("press {} - {}".format(k, _key2func[k][1]))


_key2func = {
    'n': (_next, 'To execute the next Node'),
    'q': (_quit, 'To abort the dubugging process'),
    'p': (_print, 'To print the next Node'),
    'c': (_continue, 'To continue execution until completed or breakpoint'),
    'h': (_help, 'To print this help text')
}


def on_press(key):
    pass


def on_release(key):
    if hasattr(key, 'char') and key.char.strip() in _key2func:
        return _key2func[key.char.strip()][0]()
    # elif key == keyboard.Key.enter:
    # pass
    else:
        print()


class Key(object):
    def __init__(s, char):
        s.char = char


def start(karte, runtime, args):
    global _karte, _rt, _args

    _karte = karte
    _rt = runtime
    _args = args

    _rt.pause()
    startNode = None
    if hasattr(_karte, 'startContainer'):
        startNode = _karte.startContainer
    else:
        _karte.detect_start_node()
        startNode = _karte.start_node
    if startNode:
        _rt.begin(debug=True)
    else:
        raise RuntimeError(_("Unable to find initial node with which to start map debugging."))  # noqa: F821

    while _loop:
        char = sys.stdin.read(1)
        on_release(Key(char))
        if len(_rt.input_wanters()) > 0:
            for nodeid in _rt.input_wanters():
                inp = input(f"Enter input for {nodeid}> ")
                if len(inp) > 0:
                    _rt.set_input(nodeid, inp)
        for nodeid in _rt.get_outputters():
            output = _rt.get_output(nodeid)
            print(f"node: '{nodeid}' outputs: '{output}'")
        if _rt.stopped:
            break


def tb(t, tp=None, val=None, msg=None):
    stck = None
    if tp is None or val is None:
        stck = traceback.format_tb(t)
    else:
        stck = traceback.format_exception(tp, val, t)

    cnt = 0
    s = None
    for i in stck:
        if s is None:
            s = "Stack Trace [%d] %d: %s" % (os.getpid(), cnt, i)
        else:
            s = "%s Stack Trace [%d] %d: %s" % (s, os.getpid(), cnt, i)
        cnt = cnt + 1
    if msg is not None and s is not None:
        s = "%s: %s" % (msg, s)
    if msg is not None and s is None:
        s = msg

    print(s)
