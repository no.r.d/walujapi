"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--
Main module to execute maps without an attached GUI.
"""


from nord.nord import Map, Config
from nord.nord.Runtime import Runtime
import os
import sys
import json


def handle_io(karte, vessal):
    if len(vessal.input_wanters()) > 0:
        while len(vessal.input_wanters()) > 0:
            for uid in vessal.input_wanters():
                inp = input(f"{karte.get_node(uid)}\n\nNeeds input:>>")
                vessal.set_input(uid, inp)
        vessal.continue_exec()
        handle_io(karte, vessal)

    if len(vessal.get_outputters()) > 0:
        for uid in vessal.get_outputters():
            # for some reason this is falling off the end ef the list...
            # I suspect due to the recursion...
            if vessal.has_output(uid):
                print(f"{karte.get_node(uid)}\nhas output:\n\n{vessal.get_output(uid)}\n")


def run_cli():
    if len(sys.argv) > 3 or len(sys.argv) < 2:
        print("Pass a .n file as the first argument, and optionally debug flag as the second")
        sys.exit(1)
    else:
        # add the location of the executable and map files to the appropriate
        # use paths via config
        exepath = os.path.sep.join(sys.argv[0].split(os.path.sep)[:-1])
        Config.add_to_code_path('dir://{}'.format(exepath))

        inpath = os.path.sep.join(sys.argv[1].split(os.path.sep)[:-1])
        Config.add_to_file_path('dir://{}'.format(inpath))

        m = json.load(open(sys.argv[1]))
        karte = Map(m, verbose=False)
        karte.connect_cdc_loopback()
        vessal = Runtime()

        if len(sys.argv) == 2:

            print("Running map")

            print(repr(karte) if karte.verbose else "")
            print()
            print()

            print("______________ Starting Map ___________")
            print()
            print()
            karte.run(vessal, sys.argv[1:])
            handle_io(karte, vessal)
            print()
            print()
            print("______________ Map Complete ___________")
            print(repr(karte) if karte.verbose else "")

        else:

            print("Debugging map")

            print(repr(karte) if karte.verbose else "")
            print()
            print()

            print("____ DBG _____ Starting Map ____ DBG _____")
            print()
            print()
            karte.init_debug(vessal, sys.argv[1:])
            print()
            print()
            print("____ DBG _____ Map Complete ____ DBG _____")
            print(repr(karte) if karte.verbose else "")
