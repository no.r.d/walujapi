"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

This module abstracts the protocol from the three usable resource providers (by default).

More providers and protocols can be implemented as superclasses. Perhaps
as compositions instead...
"""

import os
import sys
import importlib
from pathlib import Path
import pkg_resources
from nord.nord.exceptions import UseException as UseException


class UseProtocolException(Exception):
    """The exception class for this module."""

    pass


class Protocol(object):
    """The protocol class has the minimum needed methods a real protocol must have."""

    def __init__(self):
        """The default separator is all the base contructor has."""
        self.sep = '/'

    def prep(self, path):
        """Perform any setup prior to regular operations."""
        pass

    def fetch(self, path, name):
        """Fetch the resource and return it."""
        raise UseProtocolException(
            "Use Protocol '{}' implementation error, fetch method not implemented"
            .format(self.__class__.__name__)
        )

    def exists(self, path, name):
        """Return true if the requested resource exists."""
        raise UseProtocolException(
            "Use Protocol '{}' implementation error, exists method not implemented"
            .format(self.__class__.__name__)
        )

    def finish(self, path):
        """Execute any needed teardown when done."""
        pass


class PkgProtocol(Protocol):
    """The Package Protocol. This protocol
    looks for content using Python's pkg_resources.
    Meaning, this will find content installed with the
    software, as specifed in setup.py

    The purpose of this protocol is to be used to fetch content
    included with the software. Maybe as a last resort, so as to
    allow for user customization."""

    def __init__(self, space):
        Protocol.__init__(self)
        self.space = space

    def fetch(self, path, name):
        """Return the full path on the local filesystem."""
        # print("DIR: fetching: {}".format(path + os.path.sep + name))
        if not name.startswith(f"nord{os.path.sep}{self.space}") and not \
                self.space == 'nord' and not name.startswith(f'nord{os.path.sep}sigurd'):
            bpath = f"nord{os.path.sep}{self.space}{os.path.sep}{name}"
            barr = bpath.split(os.path.sep)
        elif self.space == 'nord' and not name.startswith(f"nord{os.path.sep}{self.space}") and \
                not name.startswith(f'nord{os.path.sep}sigurd'):
            bpath = f"nord{os.path.sep}{self.space}{os.path.sep}{name}"
            barr = bpath.split(os.path.sep)
        else:
            bpath = name
            barr = bpath.split(os.path.sep)

        bdir = ".".join(barr[:-2])
        try:
            fullpath = pkg_resources.resource_filename(bdir, barr[-2])
            path = Path(fullpath + os.path.sep + barr[-1])
            return str(path.resolve())
        except ValueError:
            return None

    def exists(self, path, name):
        """Return true if the file or directory requested exists."""
        if not name.startswith(f"nord{os.path.sep}{self.space}") and not \
                self.space == 'nord' and not name.startswith(f'nord{os.path.sep}sigurd'):
            bpath = f"nord{os.path.sep}{self.space}{os.path.sep}{name}"
            barr = bpath.split(os.path.sep)
        elif self.space == 'nord' and not name.startswith(f"nord{os.path.sep}{self.space}") and \
                not name.startswith(f'nord{os.path.sep}sigurd'):
            bpath = f"nord{os.path.sep}{self.space}{os.path.sep}{name}"
            barr = bpath.split(os.path.sep)
        else:
            bpath = name
            barr = bpath.split(os.path.sep)

        if len(barr) < 2:
            return False

        bdir = ".".join(barr[:-2])
        try:
            fullpath = pkg_resources.resource_filename(bdir, os.path.sep.join(barr[-2:]))
            path = Path(fullpath)
            rv = path.is_dir() or path.is_file() or path.is_symlink()
        except ValueError:
            rv = False
        except ModuleNotFoundError:
            rv = False
        except TypeError:
            rv = False
        # print("{} is not a file".format(path+os.path.sep+name))
        return rv


class DirProtocol(Protocol):
    """The Directory protocol."""

    def fetch(self, path, name):
        """Return the full path on the local filesystem."""
        # print("DIR: fetching: {}".format(path + os.path.sep + name))
        return path + os.path.sep + name

    def exists(self, path, name):
        """Return true if the file or directory requested exists."""
        if not Path(path).is_dir():
            # print("{} is not a directory".format(path))
            return False
        elif Path(path + os.path.sep + name).is_file():
            return True
        elif Path(path + os.path.sep + name).is_dir():
            return True
        else:
            return False


class NullProtocol(Protocol):
    """Protocol to use for placeholder so sys.path can be used for code"""

    def fetch(self, path, name):
        return None

    def exists(self, path, name):
        return False


"""
    The Use class abstracts searchpaths for "providers". Providers can be/are
    things like code, files, paths, and so on. Use allows for overriding and
    dynamic locations to be "used" for the same resource. The primary reason for
    this is to allow for "themability" or "configurability" such that modules,
    files, pathes, themes, artifacts etc can be replaced, relocated, or requests for them
    can be redirected depending on configuration, and installation scenarios

    For the "code" provider, Use wraps the import functionality in Python to allow
    nord configurable paths be used for modules as well as to better control
    namespacing of potentially redundant names. Lastly, it will allow overriding.

    Overriding can be used, for instance, by sigurd to replace the builtin.out
    with its own builtin.out which presents the output in the 3d environment
    rather than on STDOUT as the walujapi.builtins.out would.

    Use works through providers and paths. A provider is the mechanism \
    through which a resource is brought into the runtime/environment
    the searchPath for each provider is the sequence of locations the
    provider will consider when trying to satisfy a request for a named resource

    Each protocol used in the search path can behave differently, for instance
    protocols that fetch data remotely can be cached, or explicitly not cached
"""


class Use(object):
    """The utility class to load assets."""

    def __init__(self, spacename="nord"):
        """Initialize the Use instance."""
        self._providers = {
            'files': self._file,
            'code': self._import,
            'path': self._path
        }

        # Account for pip install -e and default module install locations.
        devpath = [f'dir://{x}{os.path.sep}nord{os.path.sep}{spacename}' for x in sys.path
                   if Path(f'{x}{os.path.sep}nord{os.path.sep}{spacename}').exists()]
        prodpath = [f'dir://{x}{os.path.sep}nord{os.path.sep}{spacename}' for x in sys.path
                    if Path(f'{x}{os.path.sep}nord{os.path.sep}{spacename}').exists()]

        self.space = spacename
        if self.space == "nord":
            self._searchPath = {
                'files': ['dir://.'] + list(set(devpath + prodpath)),
                'code': ['null://.'],
                'path': ['dir://.', 'dir:///'] + list(set(devpath + prodpath))
            }
        else:
            self._searchPath = {
                'files': list(set(devpath + prodpath)),
                'code':  ['null://.'],
                'path':  list(set(devpath + prodpath))
            }

        self._searchPath['path'].append('pkg://')
        self._searchPath['files'].append('pkg://')

        self._requestedResources = {}

        self._overrides = {}

        self._protocols = {
            'dir': DirProtocol(),
            'null': NullProtocol(),
            'pkg': PkgProtocol(self.space)
        }

    def set_protocol(self, name, classinst):
        """Establish the passed instance as the named protocol passed."""
        self._protocols[name] = classinst

    def _protocol_start(self, path, name):
        """Activate the selected protocols with the named path."""
        arr = path.split('://')
        protocol, locarr = arr[0], arr[1:]

        if protocol not in self._protocols:
            raise UseException("Use protocol {} not found".format(protocol))

        pexe = self._protocols[protocol]

        if not hasattr(pexe, 'sep'):
            raise UseProtocolException("Use Protocol '{}' lacks path separator."
                                       .format(pexe.__class__.__name__))

        location = pexe.sep.join(locarr)

        if 'fetch' not in dir(pexe):
            raise UseProtocolException("Use Protocol '{}' lacks fetch method."
                                       .format(pexe.__class__.__name__))

        if 'prep' not in dir(pexe):
            raise UseProtocolException("Use Protocol '{}' lacks prep method."
                                       .format(pexe.__class__.__name__))

        if 'finish' not in dir(pexe):
            raise UseProtocolException("Use Protocol '{}' lacks finish method."
                                       .format(pexe.__class__.__name__))

        pexe.prep(location)
        return pexe, location

    def _exists(self, path, name):
        pexe, location = self._protocol_start(path, name)
        rv = pexe.exists(location, name)
        pexe.finish(location)

        return rv

    def _fetch(self, path, name):
        pexe, location = self._protocol_start(path, name)
        rv = pexe.fetch(location, name)
        pexe.finish(location)

        return rv

    def add_override(self, name, ovrrddnLoc, location, provider):
        """
        Set a specific location and provider to use instead.

        For a specific resource with a given name.
        """
        self._overrides[name] = {ovrrddnLoc: (location, provider)}

    def add_location(self, location, provides):
        if provides not in self._providers:
            self._searchPath[provides] = []
        if location not in self._searchPath[provides]:
            self._searchPath[provides].append(location)

    def prepend_location(self, location, provides):
        if provides not in self._providers:
            self._searchPath[provides] = []
        self._searchPath[provides].insert(0, location)

    def add_provider(self, provider, means, defloc):
        if provider not in self._providers:
            self._providers[provider] = means
            self._searchPath[provider] = [defloc]

    def file_exists(self, filename):
        """Return whether or not the requested file exists."""
        for loc in self._searchPath["files"]:
            if self._exists(loc, filename):
                return True
        return False

    def _file(self, location, name, mode='r'):
        '''
            The file (files) provider searches the path for the first
            entry with the requested name, and returns an open file object

            presently the opened file is read-only
        '''
        if self._exists(location, name):
            return open(self._fetch(location, name), mode)
        else:
            return None

    def _import(self, location, name):
        '''
            The importer (code) provider searches the path for the first matching
            python file (module) and imports and returns that module
        '''

        if name.endswith('.py'):
            name = name.replace('.py', '')
        lookloc = name.replace('.', os.path.sep)

        name = name.replace(os.path.sep, '.')

        rv = None
        for lookloc in [lookloc, lookloc+'.py']:
            if self._exists(location, lookloc) or location == 'dir://':

                if location != 'dir://':
                    looklen = len(lookloc.split(os.path.sep))
                    file = Path(location.split('://')[1]+os.path.sep+lookloc).resolve()
                    parent, _ = file.parents[looklen - 1], file.parents[looklen]
                    sys.path.append(str(parent))

                    name = self._fetch(location, lookloc).replace('.py', '')
                    arr = name.split(os.path.sep)
                    pkg = None
                    if len(arr) > looklen:
                        pkg = ".".join(arr[:-1 * looklen])
                        name = ".".join(arr[-1 * looklen:])
                    # rv = importlib.import_module(name.split('.')[-1], package=name.split('.')[:-1])
                    rv = importlib.import_module(name, package=name.split('.')[0])

                    # import * into the returned module.
                    if hasattr(rv, '__all__'):
                        for n in rv.__all__:
                            setattr(rv, n, importlib.import_module(name + '.' + n))
                    try:
                        sys.path.remove(str(parent))
                    except ValueError:  # pragma: no cover
                        # in case it is Already removed, not sure it would/could be
                        pass
                else:
                    try:
                        # This branch allows for modules installed to the environment
                        # to be imported by name. like: use.module('nord.nord.nodes.Container')
                        # This is if and only if the location is "dir://""
                        name = lookloc.replace('.py', '')
                        name = name.replace(os.path.sep, '.')
                        pkg = None
                        rv = importlib.import_module(name, package=f"nord.{name.split('.')[0]}")

                        # import * into the returned module.
                        if hasattr(rv, '__all__'):
                            for n in [x for x in rv.__all__ if not hasattr(rv, x)]:
                                try:
                                    setattr(rv, n, importlib.import_module(name + '.' + n))
                                except ModuleNotFoundError:
                                    raise UseException(f"{pkg}.{name}.{n} Could not be imported")

                    except ModuleNotFoundError:
                        try:
                            modname = '.'.join(name.split('.')[:-1])
                            if len(modname) > 0:
                                rv = importlib.import_module(modname)
                                if hasattr(rv, 'nord'):
                                    rv = rv.nord
                                objname = name.split('.')[-1]
                                if hasattr(rv, objname):
                                    rv = getattr(rv, objname)
                                else:
                                    raise UseException(f"{objname} not found in {pkg}.{name}")
                            else:
                                rv = importlib.import_module(name)
                        except ModuleNotFoundError:
                            raise UseException(f"{pkg}.{name} not importable")
                return rv

        # attempt to load this as a system module.... return None if not possible...
        try:
            rv = importlib.import_module(name)
            return rv
        except ModuleNotFoundError as e:  # noqa: F841
            # print(e)
            # print(f"system module {name} not importable")
            # raise UseException(f"system module {name} not importable")
            pass

        return None

    def _path(self, location, name):
        '''
            The path provider searches the searchpath for the first matching
            name, and returns the local filesystem path to the requested filename
        '''
        if self._exists(location, name):
            return self._fetch(location, name)
        else:
            return None

    def file(self, name, mode='r'):
        """
            the "exposed" function used to get an opened file (findable in the searchpath via 'name')
        """
        return self.get('files', name)

    def module(self, name):
        """
            imports the named module and returns the handle to the imported
            code in this exposed function
        """
        return self.get('code', name)

    def path(self, name):
        """
        Returns the local path to the named asset (the protocols
        will make the resource local if it is not already)
        """
        return self.get('path', name)

    def get(self, provider, name):
        """
        This is the primary function for this class. The developer will call employ this
        module like so:

        import nord.nord.utils.UseUtil as use
        builtin = use.get('code', 'nodes/Builtin.py')

        From there on, 'builtin' can be used like any other module.


        # the following line breaks doctest...:
         u2 = get('code', 'nord.utils.UseUtil')
        # So it was changed to:
         import nord.nord.utils.UseUtil as u2



        >>> get('path', 'nord/utils/Use.py')
        './nord/utils/Use.py'
        >>> f = get('files', 'nord/utils/Use.py')
        >>> f.close()
        >>> f = path('/tmp/23525345oentuhuhantoeh34535')
        >>> f
        './/tmp/23525345oentuhuhantoeh34535'
        >>> u3 = module('nord/utils/Use')
        >>> f = u3.file('nord/utils/Use.py')
        >>> f.close()

        """
        if provider not in self._providers:
            raise UseException(f"Unknown provider {provider} requested of"
                               f" 'Use' for resource: {name}")
        else:
            means = self._providers[provider]

        rv = None
        for path in self._searchPath[provider]:
            tmeans = means
            if name in self._overrides and path in self._overrides[name]:
                path, provider = self._overrides[name][path]
                tmeans = self._providers[provider]
            rv = tmeans(path, name)
            if rv is not None:
                break

        if rv is None:
            raise UseException("{}:{} not found in search path: {}"
                               .format(provider, name, self._searchPath[provider]))

        return rv

    def to_dict(self):
        """Convert the settings into a single dictionary."""
        rv = {}
        rv["locations"] = {}
        for k in self._searchPath:
            rv["locations"][k] = self._searchPath[k]
        return rv
