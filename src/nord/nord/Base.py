"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--
The Base class to Nodes, Edges, and Rules providing the spacename.
"""

import sys
from nord.nord.exceptions import PropertyException as PropertyException
from nord.nord.Config import Configurable

g_vis_sort_dir_x = 1.0
g_vis_sort_dir_y = 1.0
g_vis_sort_dir_z = -1.0


class Base(Configurable):
    """
    Basic class providing class level variables.

    This should be re-implemented within all extensions.
    """

    def __new__(cls, *args, **kwargs):
        """
        Preset the descendent's _spacename to its base package name.

        This is done by interrogating the module path+name.

        It is assumed that the naming convention:

        <project>.<space>.<proj path ....>.<module> will be adhered to.

        So, "nord.kamal.visibles.greaterthan"
        is the full name of the kamal extension's greaterthan visualization.
        """
        instance = super(Base, cls).__new__(cls)
        instance.__dict__['_spacename'] = instance.__class__.__module__.split('.')[1]
        instance.extract_config_items()
        return instance

    def apply(self, data, write_direct=False):
        """
        Create and assign instance member attributes from passed data dict.

        The apply method takes a dictionary as input
        and sets the values of the dictionary as the members
        of this instance of the node

        >>> n = Node()
        >>> n.apply({"id": 1, "name": "fred", "value": 77})
        >>> n.id
        1
        >>> n.name
        'fred'
        >>> n.value
        77


        """
        for n in data.keys():
            isset = False
            if hasattr(self, '_properties'):
                if n in self._properties:
                    self._properties[n].set(data[n])
                    isset = True

            if not isset:
                if write_direct:
                    self.__dict__[n] = data[n]
                else:
                    setattr(self, n, data[n])

    def get_property(self, name):
        """Fetch the named base property."""
        if '_properties' in self.__dict__ and name in self._properties:
            return self._properties[name].get()
        else:
            raise PropertyException(_("{} has no property named: {}")  # noqa: F821
                                    .format(self.__class__.__name__, name))

    def set_property(self, name, value):
        """Set the named base property."""
        if '_properties' in self.__dict__ and name in self._properties:
            self._properties[name].set(value)
        else:
            raise PropertyException(_("Cannot set '{1}' for {0} as it has no property named: {1}")  # noqa: F821, E501
                                    .format(self.__class__.__name__, name))

    def is_set_property(self, name):
        """Check if the named base property has been set (not only if it exists)."""
        if '_properties' in self.__dict__ and name in self._properties:
            return self._properties[name].isSet
        else:
            raise PropertyException(_("{} has no property named: {}")  # noqa: F821
                                    .format(self.__class__.__name__, name))

    def check_property(self, name):
        """Check if the named base property exists as a property and as an instance member."""
        if '_properties' in self.__dict__ and name in self._properties:
            return self._properties[name].check()
        else:
            raise PropertyException(_("{} has no property named: {}")  # noqa: F821
                                    .format(self.__class__.__name__, name))

    def __lt__(self, other):
        """Comparitor for less than."""
        # Check to see if the visual positions can
        # be used to determine the sort order

        global g_vis_sort_dir_x
        global g_vis_sort_dir_y
        global g_vis_sort_dir_z

        self_x = None
        self_y = None
        self_z = None

        if hasattr(self, 'position'):
            self_x = self.position[0]
            self_y = self.position[1]
            self_z = self.position[2]

        if hasattr(other, 'position'):
            other_x = other.position[0]
            other_y = other.position[1]
            other_z = other.position[2]

        if hasattr(self, 'position') and not hasattr(other, 'position'):
            return False
        elif not hasattr(self, 'position') and hasattr(other, 'position'):
            return True

        if self_x is not None and other_x is not None and\
                self_y is not None and other_y is not None and\
                self_z is not None and other_z is not None:
            dx = (self_x - other_x) * g_vis_sort_dir_x
            dy = (self_y - other_y) * g_vis_sort_dir_y
            dz = (self_z - other_z) * g_vis_sort_dir_z

            return {abs(dx): dx, abs(dy): dy, abs(dz): dz}[max(abs(dx), abs(dy), abs(dz))] < 0

        # If not positions check if they have names
        if hasattr(self, 'name') and not hasattr(other, 'name'):
            return False
        elif not hasattr(self, 'name') and hasattr(other, 'name'):
            return True
        elif hasattr(self, 'name') and hasattr(other, 'name'):
            return self.name < other.name

        # If not names check if they have ids
        if hasattr(self, 'id') and not hasattr(other, 'id'):
            return False
        elif not hasattr(self, 'id') and hasattr(other, 'id'):
            return True
        elif hasattr(self, 'id') and hasattr(other, 'id'):
            return self.id < other.id

        return False

    def __gt__(self, other):
        """Comparitor for greater than."""
        # Check is to see if the visual positions can
        # be used to determine the sort order
        global g_vis_sort_dir_x
        global g_vis_sort_dir_y
        global g_vis_sort_dir_z

        self_x = None
        self_y = None
        self_z = None

        if hasattr(self, 'position'):
            self_x = self.position[0]
            self_y = self.position[1]
            self_z = self.position[2]

        if hasattr(other, 'position'):
            other_x = other.position[0]
            other_y = other.position[1]
            other_z = other.position[2]

        if hasattr(self, 'position') and not hasattr(other, 'position'):
            return True
        elif not hasattr(self, 'position') and hasattr(other, 'position'):
            return False

        if self_x and other_x and\
                self_y and other_y and\
                self_z and other_z:
            dx = (self_x - other_x) * g_vis_sort_dir_x
            dy = (self_y - other_y) * g_vis_sort_dir_y
            dz = (self_z - other_z) * g_vis_sort_dir_z

            return {abs(dx): dx, abs(dy): dy, abs(dz): dz}[max(abs(dx), abs(dy), abs(dz))] > 0

        # If not positions check if they have names
        if hasattr(self, 'name') and not hasattr(other, 'name'):
            return True
        elif not hasattr(self, 'name') and hasattr(other, 'name'):
            return False
        elif hasattr(self, 'name') and hasattr(other, 'name'):
            return self.name > other.name

        # If not names check if they have ids
        if hasattr(self, 'id') and not hasattr(other, 'id'):
            return True
        elif not hasattr(self, 'id') and hasattr(other, 'id'):
            return False
        elif hasattr(self, 'id') and hasattr(other, 'id'):
            return self.id > other.id

        return False


sys.modules[__name__] = Base
