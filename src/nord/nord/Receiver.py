"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--
The Receiver extends the base Node by adding the ability to 'receive' passed parameters.

Essentially this class enables inheriting node types to accept and organize passed
parameters. The initial descendents of this node type are containers (for functions and classes),
Return nodes, Reference nodes (to graphs, externals, containers, etc)
"""

import sys
import nord.nord.Node as Node
from nord.nord.exceptions import RuntimeException as RuntimeException


class Argument(object):
    """Instances of this class hold the value and position of data passed to receivers."""

    def __init__(self):
        """Initialize the argument."""
        self.ord_pos = None
        self.ord_name = None
        self.vis_pos_x = None
        self.vis_pos_y = None
        self.vis_pos_z = None
        self.vis_sort_dir_x = 1.0
        self.vis_sort_dir_y = -1.0
        self.vis_sort_dir_z = -1.0
        self.payload_set = False

    def __lt__(self, other):
        """Comparitor for less than."""
        # first check if the two have specified positions
        if self.ord_pos and not other.ord_pos:
            return False
        if not self.ord_pos and other.ord_pos:
            return True
        if self.ord_pos and other.ord_pos:
            return self.ord_pos < other.ord_pos

        # Next check to see if the visual positions can
        # be used to determine the sort order
        if self.vis_pos_x and other.vis_pos_x and\
                self.vis_pos_y and other.vis_pos_y and\
                self.vis_pos_z and other.vis_pos_z:
            dx = (self.vis_pos_x - other.vis_pos_x) * self.vis_sort_dir_x
            dy = (self.vis_pos_y - other.vis_pos_y) * self.vis_sort_dir_y
            dz = (self.vis_pos_z - other.vis_pos_z) * self.vis_sort_dir_z

            return {abs(dx): dx, abs(dy): dy, abs(dz): dz}[max(abs(dx), abs(dy), abs(dz))] < 0

        # Lastly check if they have names
        if self.ord_name and not other.ord_name:
            return False
        if not self.ord_name and other.ord_name:
            return True
        if self.ord_name and other.ord_name:
            return self.ord_name < other.ord_name

        return False

    def __gt__(self, other):
        """Comparitor for greater than."""
        # first check if the two have specified positions
        if self.ord_pos and not other.ord_pos:
            return True
        if not self.ord_pos and other.ord_pos:
            return False
        if self.ord_pos and other.ord_pos:
            return self.ord_pos > other.ord_pos

        # Next check is to see if the visual positions can
        # be used to determine the sort order
        if self.vis_pos_x and other.vis_pos_x and\
                self.vis_pos_y and other.vis_pos_y and\
                self.vis_pos_z and other.vis_pos_z:
            dx = (self.vis_pos_x - other.vis_pos_x) * self.vis_sort_dir_x
            dy = (self.vis_pos_y - other.vis_pos_y) * self.vis_sort_dir_y
            dz = (self.vis_pos_z - other.vis_pos_z) * self.vis_sort_dir_z

            return {abs(dx): dx, abs(dy): dy, abs(dz): dz}[max(abs(dx), abs(dy), abs(dz))] > 0

        # Lastly check if they have names
        if self.ord_name and not other.ord_name:
            return True
        if not self.ord_name and other.ord_name:
            return False
        if self.ord_name and other.ord_name:
            return self.ord_name > other.ord_name

        return False

    def use_edge(self, edge):
        """Populate this argument's sorting characteristics from the passed edge."""
        if hasattr(edge, 'ord_position'):
            self.ord_pos = edge.ord_position
        if hasattr(edge, 'ord_name'):
            self.ord_name = edge.ord_name
        if hasattr(edge.get_source(), 'position'):
            (self.vis_pos_x, self.vis_pos_y, self.vis_pos_z) = edge.get_source().position
        self.edge = edge

    def get_key(self):
        """Return the lookup key for this argument based upon the positional precidence."""
        if self.ord_pos:
            return self.ord_pos
        if self.ord_name:
            return self.ord_name
        if self.vis_pos_x and self.vis_pos_y and self.vis_pos_z:
            return (self.vis_pos_x, self.vis_pos_y, self.vis_pos_z)

    def set_payload(self, value):
        """Store the payload and indicate that it has been stored."""
        self.payload = value
        self.payload_set = True

    def set_named_payload(self, name, value):
        self.ord_name = name
        self.set_payload(value)

    def has_payload(self):
        """Return whether this argument been populated with a value."""
        return self.payload_set

    def get_payload(self):
        """Get the payload if it has been set."""
        if not self.payload_set:
            raise RuntimeException(_("Argument value requested, but never established."))  # noqa: F821
        else:
            return self.payload


class Receiver(Node):
    """Provider of parameter reception capabilities."""

    def init_argument_cache(self, edge):
        """Prepare this Node's cache of arguments."""
        self._arguments = {}
        self._slots = {}
        self._arglist = list()
        # Organize the edges by:
        #   The "slot" attribute associated with the edge, if it exists
        for e in self._sourceRules[edge.rule]:
            arg = Argument()
            arg.use_edge(e)
            self._arguments[e] = arg
            self._slots[arg.get_key()] = arg
            self._arglist.append(arg)

    def get_argument(self, rule):
        """Get the known argument via its affiliated rule"""
        if not hasattr(self, '_arguments'):
            self.init_argument_cache(rule.edge)
        arg = self._arguments[rule.edge]
        return arg

    def set_arg(self, edge, value):
        """
        Given an edge and value, identify which argument should be assigned the value.

        Using the target of the edge to evaluate all the sibling edges of the same type,
        organize the edges, saving the organized state for future reference, and identify
        which of the known / expected parameter slots should receive the passed value.
        """
        if not hasattr(self, '_arguments'):
            self.init_argument_cache(edge)
        if isinstance(value, Receiver):
            # The cargo passed is also a receiver, so just set my internals
            # to be the same as the inbound...
            # this could be a potential bug. I'm not sure exactly how, but,
            # this just link to the inbound receiver feels a little dirty
            self._arguments = value._arguments
            self._slots = value._slots
            self._arglist = value._arglist
        else:
            arg = self._arguments[edge]
            arg.set_payload(value)

    def has_args(self):
        """Return true if this receiver has arguments being passed to it."""
        if not hasattr(self, '_arguments'):
            return False
        if len(self._arguments) > 0:
            return True
        elif len(self._sourceRules['Pass']) > 0:
            return True
        else:
            return False

    def get_arg_values(self):
        """Return an ordered list of positional arguments, and a dictionary of named ones."""
        ord_rv = ()
        nam_rv = {}

        if not hasattr(self, '_arglist'):
            return (), {}

        for arg in sorted(self._arglist):
            if arg.ord_pos or arg.ord_name is None:
                ord_rv += (arg.get_payload(),)
            else:
                nam_rv[arg.ord_name] = arg.get_payload()

        return ord_rv, nam_rv


sys.modules[__name__] = Receiver
