#!/bin/bash


# Requires pytest and coverage
# installed with pip3 install pytest coverage

coverage run --source src/ --include "src/*" -m pytest -v
coverage report -m
