#!/usr/bin/env bash
if [ ${#} -ne 1 ]; then
    echo "Must pass one, ond only one argument on the command line."
    exit 1
fi

NODE="${1}"
NODE=$(echo ${NODE} | tr '[:upper:]' '[:lower:]')
CNODE=$(echo $(tr a-z A-Z <<< ${NODE:0:1})${NODE:1})
SPACE=$(basename $(pwd))

cat <<EOF > src/nord/${SPACE}/nodes/${CNODE}.py
"""
Copyright $(date +%Y) NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--
"""
import nord.nord.Node as Node
import sys


class ${CNODE}(Node):
    """"""
    def execute(self, runtime):
        pass


sys.modules[__name__] = ${CNODE}
EOF

NODEID=$(head /dev/random | shasum | cut -d ' ' -f 1)

cat <<EOF > tests/maps/test_gen_node_${NODE}.n
{
    "extensions": {
        "${SPACE}": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///$(pwd)",
                    "pkg://"
                ],
                "path": [
                    "dir:///$(pwd)",
                    "pkg://"
                ]
            }
        }
    },
    "map": {
        "edges": [],
        "nodes": [
            {
                "id": "${NODEID}",
                "name": "Test ${CNODE}",
                "type": "${NODE}",
                "space": "${SPACE}",
                "position": [ 0.0, -0.5, -0.5]
            }
        ]
    }

}
EOF

cat <<EOF > tests/nodes/${CNODE}_test.py
"""
Copyright $(date +%Y) NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--
"""
import nord.${SPACE}.nodes.${CNODE} as ${CNODE}
import nord.nord.Map as Map
from nord.nord import Runtime as RT
Runtime = RT.Runtime
import json  # noqa: E402
import pytest  # noqa: E402


def test_${NODE}_run_gen():
    try:
        m = json.load(open("tests/maps/test_gen_node_${NODE}.n", "r"))
    except FileNotFoundError:
        m = json.load(open("${SPACE}/tests/maps/test_gen_node_${NODE}.n", "r"))
    karte = Map(m)
    vessal = Runtime()
    karte.run(vessal, None)
    node = karte.get_node('${NODEID}')
    assert 1 == 1

EOF

if ! grep -q \<${NODE}\> src/nord/${SPACE}/config/modelmap.conf $2>&1; then
    echo "${NODE}:models!data.egg" >> src/nord/${SPACE}/config/modelmap.conf
fi

cat <<EOF > src/nord/${SPACE}/visibles/${NODE}.py
"""
Copyright $(date +%Y) NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--
"""
from nord.sigurd.shell.visnode import VisNode
from nord.sigurd.utils.verboser import funlog


class ${CNODE}(VisNode):
    """Implement the visualization."""

    def __init__(self, uid, app):
        super().__init__(uid, '${NODE}', app, space="${SPACE}")

        self.add_delegate("basic_auth_token",
                          "basicauthtoken",
                          'pass',
                          'as_tgt',
                          tiptext=_("Pass basic auth\ntoken here"))  # noqa: F821

    @funlog
    def update_tooltip(self, tooltip):
        if hasattr(self, 'mapentry'):
            tooltip.set_text(f"{self} ")
            return
        tooltip.set_text(f"{self.name}: " + _("** Unset **"))  # noqa: F821
EOF


cat <<EOF > tests/visibles/vpl_${CNODE}_test.py
"""
Copyright $(date +%Y) NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--
"""

import pytest
from panda3d.core import Point3
import nord.sigurd.shell.vismap as vm
from nord.wrigan.visibles.utils import dim_map, undim_map
import nord.sigurd.managers.objectmanager as om


@pytest.mark.panda3d()
def test_add_${NODE}_node(init_panda3d):
    # app = init_panda3d
    vm.create_node("${CNODE}", Point3(0.0, 0.0, 0.0), space="${SPACE}")
    karte = vm.get_working_map()
    # app.one_loop_iteration()
    r = render  # noqa: F821
    newNode = karte.nodes[list(karte.nodes.keys())[0]]
    dim_map()
    undim_map()
    # This assert detects if there are more NodePaths than there should be.
    # This was a problem detected when the Node Constructor was
    # called twice in the Static Node init method. Which in turn
    # caused the CDC capability to have two add_node changes
    # for the same node... which in turn, caused two model's to be
    # created with the same ID, which is bad...
    assert len(r.findAllMatches(f"{newNode.id}")) == 1
    assert om.fetch(newNode.id).data == r.findAllMatches(f"{newNode.id}")[0]

EOF

echo ${0} ${*} >> .mk_skel_gen.log