{
	"map": {
		"nodes": [
			  { "id": "1", "name": "main",    "type": "container" }
			, { "id": "2", "name": "out",     "type": "builtin", "method": "out"}
			, { "id": "3", "name": "static1", "type": "static",  "value": "Hello World!"}
		],
		"edges": [
			  { "source": "1:main",    "target": "3:static1", "rule": "contains" }
			, { "source": "1:main",    "target": "2:out",     "rule": "contains" }
			, { "source": "1:main",    "target": "2:out",     "rule": "flow" }
			, { "source": "3:static1", "target": "2:out",     "rule": "pass" }
		]
	},
	"name": "HelloWorldSimple"
}