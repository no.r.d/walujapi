{
    "extensions": {
        "fregata": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir://walujapi/samplemaps",
                    "dir:///Users/nate/Desktop/Dev/NoRD/fregata/src/nord/fregata",
                    "pkg://"
                ],
                "path": [
                    "dir://walujapi/samplemaps",
                    "dir:///Users/nate/Desktop/Dev/NoRD/fregata/src/nord/fregata",
                    "pkg://"
                ]
            }
        },
        "iteru": {
            "locations": {
                "code": [
                    "null://.",
                    "dir://../iteru/src"
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/iteru/src/nord/iteru",
                    "pkg://",
                    "dir://../iteru/src/iteru",
                    "dir://../iteru/src"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/iteru/src/nord/iteru",
                    "pkg://",
                    "dir://../iteru/src",
                    "dir://../iteru/src/iteru"
                ]
            }
        },
        "pasion": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/pasion/src/nord/pasion",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/pasion/src/nord/pasion",
                    "pkg://"
                ]
            }
        },
        "shoshoni": {
            "locations": {
                "code": [
                    "null://.",
                    "dir://../shoshoni/src"
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/shoshoni/src/nord/shoshoni",
                    "pkg://",
                    "dir://../shoshoni/src/shoshoni",
                    "dir://../shoshoni/src"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/shoshoni/src/nord/shoshoni",
                    "pkg://",
                    "dir://../shoshoni/src",
                    "dir://../shoshoni/src/shoshoni"
                ]
            }
        },
        "wrigan": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/wrigan/src/nord/wrigan",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/wrigan/src/nord/wrigan",
                    "pkg://"
                ]
            }
        }
    },
    "map": {
        "edges": [
            {
                "from_space": "shoshoni",
                "rule": "use",
                "source": "28f952fb-7cda-4fd3-bdc3-dabb5b21aefb:External: Friendly Flooded Ceiling",
                "space": "nord",
                "target": "9a824fb2-8096-4fad-a616-0543c0a6aa39:Container: Friendly Viscous Slobber",
                "to_space": "nord"
            },
            {
                "rule": "flow",
                "source": "5073fffd-4c53-494e-9647-08cb09b435c2:<<class 'type'> <class 'datetime.datetime'>>:Fat Chipped Slobber",
                "target": "429f9bc9-f7f1-4e1f-976e-e72f220d3085:Builtin: Super Dental Sparrow"
            },
            {
                "rule": "assign",
                "source": "5073fffd-4c53-494e-9647-08cb09b435c2:<<class 'type'> <class 'datetime.datetime'>>:Fat Chipped Slobber",
                "target": "51f71e25-b2a7-4f7c-8ac4-a9a77653d9fa:Data: Smart Oderous Wall"
            },
            {
                "rule": "pass",
                "source": "51f71e25-b2a7-4f7c-8ac4-a9a77653d9fa:Data: Smart Oderous Wall",
                "target": "429f9bc9-f7f1-4e1f-976e-e72f220d3085:Builtin: Super Dental Sparrow"
            },
            {
                "from_space": "iteru",
                "rule": "pass",
                "source": "5bc9efd4-49a7-418f-9093-e2a9c5a8ddce:Convert: Hard Scientific Cloud",
                "space": "nord",
                "target": "5073fffd-4c53-494e-9647-08cb09b435c2:<<class 'type'> <class 'datetime.datetime'>>:Fat Chipped Slobber",
                "to_space": "nord"
            },
            {
                "from_space": "iteru",
                "rule": "pass",
                "source": "7b85162c-a4a0-4014-be8d-3051fe11fbe8:Convert: Split Sublimated Mountain",
                "space": "nord",
                "target": "5073fffd-4c53-494e-9647-08cb09b435c2:<<class 'type'> <class 'datetime.datetime'>>:Fat Chipped Slobber",
                "to_space": "nord"
            },
            {
                "from_space": "nord",
                "rule": "pass",
                "source": "7f281587-2207-47ed-9f56-1f67f27f8e71:Static: Hard Ranged Bag",
                "space": "nord",
                "target": "7b85162c-a4a0-4014-be8d-3051fe11fbe8:Convert: Split Sublimated Mountain",
                "to_space": "iteru"
            },
            {
                "rule": "contains",
                "source": "9a824fb2-8096-4fad-a616-0543c0a6aa39:Container: Friendly Viscous Slobber",
                "target": "429f9bc9-f7f1-4e1f-976e-e72f220d3085:Builtin: Super Dental Sparrow"
            },
            {
                "rule": "contains",
                "source": "9a824fb2-8096-4fad-a616-0543c0a6aa39:Container: Friendly Viscous Slobber",
                "target": "5073fffd-4c53-494e-9647-08cb09b435c2:<<class 'type'> <class 'datetime.datetime'>>:Fat Chipped Slobber"
            },
            {
                "rule": "contains",
                "source": "9a824fb2-8096-4fad-a616-0543c0a6aa39:Container: Friendly Viscous Slobber",
                "target": "51f71e25-b2a7-4f7c-8ac4-a9a77653d9fa:Data: Smart Oderous Wall"
            },
            {
                "from_space": "nord",
                "rule": "contains",
                "source": "9a824fb2-8096-4fad-a616-0543c0a6aa39:Container: Friendly Viscous Slobber",
                "space": "nord",
                "target": "5bc9efd4-49a7-418f-9093-e2a9c5a8ddce:Convert: Hard Scientific Cloud",
                "to_space": "iteru"
            },
            {
                "from_space": "nord",
                "rule": "contains",
                "source": "9a824fb2-8096-4fad-a616-0543c0a6aa39:Container: Friendly Viscous Slobber",
                "space": "nord",
                "target": "7b85162c-a4a0-4014-be8d-3051fe11fbe8:Convert: Split Sublimated Mountain",
                "to_space": "iteru"
            },
            {
                "rule": "contains",
                "source": "9a824fb2-8096-4fad-a616-0543c0a6aa39:Container: Friendly Viscous Slobber",
                "target": "7f281587-2207-47ed-9f56-1f67f27f8e71:Static: Hard Ranged Bag"
            },
            {
                "rule": "contains",
                "source": "9a824fb2-8096-4fad-a616-0543c0a6aa39:Container: Friendly Viscous Slobber",
                "target": "a566cfad-494f-4667-9570-0439286f7269:Static: Gothic Squished Bag"
            },
            {
                "rule": "contains",
                "source": "9a824fb2-8096-4fad-a616-0543c0a6aa39:Container: Friendly Viscous Slobber",
                "target": "a57486cd-a262-49e6-bd1e-bb401a577fb6:Static: Hangry Forked Slobber"
            },
            {
                "from_space": "nord",
                "rule": "contains",
                "source": "9a824fb2-8096-4fad-a616-0543c0a6aa39:Container: Friendly Viscous Slobber",
                "space": "nord",
                "target": "d2ebb8cd-a7bc-4e91-8842-bd94ec1c8bf7:Toint: Gothic Nordic Cheese",
                "to_space": "iteru"
            },
            {
                "from_space": "nord",
                "rule": "contains",
                "source": "9a824fb2-8096-4fad-a616-0543c0a6aa39:Container: Friendly Viscous Slobber",
                "space": "nord",
                "target": "e4e44ee1-a319-4dfa-8758-7ff1dc6db3c4:Convert: Flat Flooded Laser",
                "to_space": "iteru"
            },
            {
                "from_space": "nord",
                "rule": "pass",
                "source": "a566cfad-494f-4667-9570-0439286f7269:Static: Gothic Squished Bag",
                "space": "nord",
                "target": "e4e44ee1-a319-4dfa-8758-7ff1dc6db3c4:Convert: Flat Flooded Laser",
                "to_space": "iteru"
            },
            {
                "rule": "flow",
                "source": "a57486cd-a262-49e6-bd1e-bb401a577fb6:Static: Hangry Forked Slobber",
                "target": "5073fffd-4c53-494e-9647-08cb09b435c2:<<class 'type'> <class 'datetime.datetime'>>:Fat Chipped Slobber"
            },
            {
                "from_space": "nord",
                "rule": "pass",
                "source": "a57486cd-a262-49e6-bd1e-bb401a577fb6:Static: Hangry Forked Slobber",
                "space": "nord",
                "target": "5bc9efd4-49a7-418f-9093-e2a9c5a8ddce:Convert: Hard Scientific Cloud",
                "to_space": "iteru"
            },
            {
                "from_space": "nord",
                "rule": "contains",
                "source": "c0b1d83d-1c27-4022-98bc-3dba988f5b63:Container: Under Viscous Hair",
                "space": "nord",
                "target": "28f952fb-7cda-4fd3-bdc3-dabb5b21aefb:External: Friendly Flooded Ceiling",
                "to_space": "shoshoni"
            },
            {
                "rule": "contains",
                "source": "c0b1d83d-1c27-4022-98bc-3dba988f5b63:Container: Under Viscous Hair",
                "target": "9a824fb2-8096-4fad-a616-0543c0a6aa39:Container: Friendly Viscous Slobber"
            },
            {
                "from_space": "iteru",
                "rule": "cast",
                "source": "d2ebb8cd-a7bc-4e91-8842-bd94ec1c8bf7:Toint: Gothic Nordic Cheese",
                "space": "iteru",
                "target": "5bc9efd4-49a7-418f-9093-e2a9c5a8ddce:Convert: Hard Scientific Cloud",
                "to_space": "iteru"
            },
            {
                "from_space": "iteru",
                "rule": "cast",
                "source": "d2ebb8cd-a7bc-4e91-8842-bd94ec1c8bf7:Toint: Gothic Nordic Cheese",
                "space": "iteru",
                "target": "7b85162c-a4a0-4014-be8d-3051fe11fbe8:Convert: Split Sublimated Mountain",
                "to_space": "iteru"
            },
            {
                "from_space": "iteru",
                "rule": "cast",
                "source": "d2ebb8cd-a7bc-4e91-8842-bd94ec1c8bf7:Toint: Gothic Nordic Cheese",
                "space": "iteru",
                "target": "e4e44ee1-a319-4dfa-8758-7ff1dc6db3c4:Convert: Flat Flooded Laser",
                "to_space": "iteru"
            },
            {
                "from_space": "iteru",
                "rule": "pass",
                "source": "e4e44ee1-a319-4dfa-8758-7ff1dc6db3c4:Convert: Flat Flooded Laser",
                "space": "nord",
                "target": "5073fffd-4c53-494e-9647-08cb09b435c2:<<class 'type'> <class 'datetime.datetime'>>:Fat Chipped Slobber",
                "to_space": "nord"
            },
            {
                "rule": "assign",
                "source": "5073fffd-4c53-494e-9647-08cb09b435c2:<<class 'type'> <class 'datetime.datetime'>>:Fat Chipped Slobber",
                "target": "51f71e25-b2a7-4f7c-8ac4-a9a77653d9fa:Data: Smart Oderous Wall"
            },
            {
                "rule": "flow",
                "source": "5073fffd-4c53-494e-9647-08cb09b435c2:<<class 'type'> <class 'datetime.datetime'>>:Fat Chipped Slobber",
                "target": "429f9bc9-f7f1-4e1f-976e-e72f220d3085:Builtin: Super Dental Sparrow"
            },
            {
                "from_space": "nord",
                "rule": "pass",
                "source": "a566cfad-494f-4667-9570-0439286f7269:Static: Gothic Squished Bag",
                "space": "nord",
                "target": "e4e44ee1-a319-4dfa-8758-7ff1dc6db3c4:Convert: Flat Flooded Laser",
                "to_space": "iteru"
            },
            {
                "from_space": "nord",
                "rule": "pass",
                "source": "7f281587-2207-47ed-9f56-1f67f27f8e71:Static: Hard Ranged Bag",
                "space": "nord",
                "target": "7b85162c-a4a0-4014-be8d-3051fe11fbe8:Convert: Split Sublimated Mountain",
                "to_space": "iteru"
            },
            {
                "from_space": "iteru",
                "rule": "pass",
                "source": "e4e44ee1-a319-4dfa-8758-7ff1dc6db3c4:Convert: Flat Flooded Laser",
                "space": "nord",
                "target": "5073fffd-4c53-494e-9647-08cb09b435c2:<<class 'type'> <class 'datetime.datetime'>>:Fat Chipped Slobber",
                "to_space": "nord"
            },
            {
                "from_space": "iteru",
                "rule": "pass",
                "source": "7b85162c-a4a0-4014-be8d-3051fe11fbe8:Convert: Split Sublimated Mountain",
                "space": "nord",
                "target": "5073fffd-4c53-494e-9647-08cb09b435c2:<<class 'type'> <class 'datetime.datetime'>>:Fat Chipped Slobber",
                "to_space": "nord"
            },
            {
                "from_space": "iteru",
                "rule": "cast",
                "source": "d2ebb8cd-a7bc-4e91-8842-bd94ec1c8bf7:Toint: Gothic Nordic Cheese",
                "space": "iteru",
                "target": "e4e44ee1-a319-4dfa-8758-7ff1dc6db3c4:Convert: Flat Flooded Laser",
                "to_space": "iteru"
            },
            {
                "from_space": "iteru",
                "rule": "cast",
                "source": "d2ebb8cd-a7bc-4e91-8842-bd94ec1c8bf7:Toint: Gothic Nordic Cheese",
                "space": "iteru",
                "target": "7b85162c-a4a0-4014-be8d-3051fe11fbe8:Convert: Split Sublimated Mountain",
                "to_space": "iteru"
            },
            {
                "from_space": "iteru",
                "rule": "cast",
                "source": "d2ebb8cd-a7bc-4e91-8842-bd94ec1c8bf7:Toint: Gothic Nordic Cheese",
                "space": "iteru",
                "target": "5bc9efd4-49a7-418f-9093-e2a9c5a8ddce:Convert: Hard Scientific Cloud",
                "to_space": "iteru"
            },
            {
                "rule": "flow",
                "source": "a57486cd-a262-49e6-bd1e-bb401a577fb6:Static: Hangry Forked Slobber",
                "target": "5073fffd-4c53-494e-9647-08cb09b435c2:<<class 'type'> <class 'datetime.datetime'>>:Fat Chipped Slobber"
            },
            {
                "from_space": "nord",
                "rule": "pass",
                "source": "a57486cd-a262-49e6-bd1e-bb401a577fb6:Static: Hangry Forked Slobber",
                "space": "nord",
                "target": "5bc9efd4-49a7-418f-9093-e2a9c5a8ddce:Convert: Hard Scientific Cloud",
                "to_space": "iteru"
            },
            {
                "from_space": "iteru",
                "rule": "pass",
                "source": "5bc9efd4-49a7-418f-9093-e2a9c5a8ddce:Convert: Hard Scientific Cloud",
                "space": "nord",
                "target": "5073fffd-4c53-494e-9647-08cb09b435c2:<<class 'type'> <class 'datetime.datetime'>>:Fat Chipped Slobber",
                "to_space": "nord"
            },
            {
                "rule": "pass",
                "source": "51f71e25-b2a7-4f7c-8ac4-a9a77653d9fa:Data: Smart Oderous Wall",
                "target": "429f9bc9-f7f1-4e1f-976e-e72f220d3085:Builtin: Super Dental Sparrow"
            },
            {
                "from_space": "shoshoni",
                "rule": "use",
                "source": "28f952fb-7cda-4fd3-bdc3-dabb5b21aefb:External: Friendly Flooded Ceiling",
                "space": "nord",
                "target": "9a824fb2-8096-4fad-a616-0543c0a6aa39:Container: Friendly Viscous Slobber",
                "to_space": "nord"
            }
        ],
        "nodes": [
            {
                "id": "28f952fb-7cda-4fd3-bdc3-dabb5b21aefb",
                "modname": "datetime",
                "name": "External: Friendly Flooded Ceiling",
                "position": [
                    -2.4810118675231934,
                    0.0,
                    -1.9227843284606934
                ],
                "space": "shoshoni",
                "type": "external"
            },
            {
                "id": "429f9bc9-f7f1-4e1f-976e-e72f220d3085",
                "name": "Builtin: Super Dental Sparrow",
                "position": [
                    7.8437180519104,
                    -0.6015663146972656,
                    -8.064321517944336
                ],
                "type": "builtin"
            },
            {
                "id": "5073fffd-4c53-494e-9647-08cb09b435c2",
                "itemid": "52bafd0d-b3c2-4ce0-8494-cbfacc61d32f",
                "name": "<<class 'type'> <class 'datetime.datetime'>>:Fat Chipped Slobber",
                "parentid": "28f952fb-7cda-4fd3-bdc3-dabb5b21aefb",
                "position": [
                    -6.934427261352539,
                    0.0,
                    1.3025307655334473
                ],
                "referenced_item": "datetime",
                "type": "reference"
            },
            {
                "id": "51f71e25-b2a7-4f7c-8ac4-a9a77653d9fa",
                "name": "Data: Smart Oderous Wall",
                "position": [
                    7.674807548522949,
                    -0.4150390625,
                    4.666077613830566
                ],
                "type": "data"
            },
            {
                "id": "5bc9efd4-49a7-418f-9093-e2a9c5a8ddce",
                "name": "Convert: Hard Scientific Cloud",
                "position": [
                    -20.507883071899414,
                    0.7384033203125,
                    -4.5947136878967285
                ],
                "space": "iteru",
                "type": "convert"
            },
            {
                "id": "7b85162c-a4a0-4014-be8d-3051fe11fbe8",
                "name": "Convert: Split Sublimated Mountain",
                "position": [
                    -20.19044303894043,
                    0.43225860595703125,
                    0.9133983850479126
                ],
                "space": "iteru",
                "type": "convert"
            },
            {
                "id": "7f281587-2207-47ed-9f56-1f67f27f8e71",
                "name": "Static: Hard Ranged Bag",
                "position": [
                    -28.893959045410156,
                    -2.4534950256347656,
                    0.4836501181125641
                ],
                "type": "static",
                "value": "02"
            },
            {
                "id": "9a824fb2-8096-4fad-a616-0543c0a6aa39",
                "name": "Container: Friendly Viscous Slobber",
                "position": [
                    6.69873046875,
                    0.0,
                    1.6746826171875
                ],
                "type": "container"
            },
            {
                "id": "a566cfad-494f-4667-9570-0439286f7269",
                "name": "Static: Gothic Squished Bag",
                "position": [
                    -27.8514347076416,
                    -3.952014923095703,
                    8.865455627441406
                ],
                "type": "static",
                "value": "2019"
            },
            {
                "id": "a57486cd-a262-49e6-bd1e-bb401a577fb6",
                "name": "Static: Hangry Forked Slobber",
                "position": [
                    -29.318429946899414,
                    -2.2191123962402344,
                    -11.226203918457031
                ],
                "type": "static",
                "value": "03"
            },
            {
                "id": "c0b1d83d-1c27-4022-98bc-3dba988f5b63",
                "name": "Container: Under Viscous Hair",
                "position": [
                    1.5878485441207886,
                    0.0,
                    -2.7787320613861084
                ],
                "type": "container"
            },
            {
                "id": "d2ebb8cd-a7bc-4e91-8842-bd94ec1c8bf7",
                "name": "Toint: Gothic Nordic Cheese",
                "position": [
                    -29.194698333740234,
                    10.189581871032715,
                    4.576087951660156
                ],
                "space": "iteru",
                "type": "toint"
            },
            {
                "id": "e4e44ee1-a319-4dfa-8758-7ff1dc6db3c4",
                "name": "Convert: Flat Flooded Laser",
                "position": [
                    -17.304424285888672,
                    -1.6411056518554688,
                    5.243081092834473
                ],
                "space": "iteru",
                "type": "convert"
            }
        ]
    },
    "name": "Green Aerated Bouy"
}