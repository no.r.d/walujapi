{
	"map": {
		"nodes": [
			  { "id": "1", "name": "main", "type": "container"}
			, { "id": "2", "name": "var1", "type": "data"}
			, { "id": "3", "name": "out", "type": "builtin", "method": "out"}
			, { "id": "4", "name": "static1", "type": "static", "value": "Hello World!"}
		],
		"edges": [
			  { "source": "1:main", "target": "2:var1", "rule": "contains"}
			, { "source": "1:main", "target": "4:static1", "rule": "contains"}
			, { "source": "1:main", "target": "3:out", "rule": "contains"}
			, { "source": "4:static1", "target": "2:var1", "rule": "assign"}
			, { "source": "2:var1", "target": "3:out", "rule": "pass"}
		]
	},
	"name": "HelloWorld"
}
