"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

from nord.nord import Runtime as RT
Runtime = RT.Runtime
import nord.nord.Config as config  # noqa: E402
import json  # noqa: E402
import nord.nord.Map as Map  # noqa: E402
import nord.nord.Node as Node  # noqa: E402
import nord.nord.utils.SpaceManager as use  # noqa: E402


def test_setReturn():
    r = Runtime()
    n = Node(Map(None))
    r.initialize(Map(None))
    r.stopped = True
    r.begin(n)
    r.setNextNode(n, 'DOWN')
    r.setReturn(1)
    assert r.current.ret_val == 1


def test_verbose_RunNode(capsys):
    try:
        m = json.load(open("samplemaps/helloworld.n", "r"))
    except FileNotFoundError:
        m = json.load(open("walujapi/samplemaps/helloworld.n", "r"))
    karte = Map(m, verbose=True)
    vessal = Runtime()
    karte.run(vessal, None)

    captured = capsys.readouterr()
    print(captured.out)
    assert captured.out == '''Map created with 4 Nodes and 7 Edges

Executing node: Container:
    .id = 1
    .name = main
    .type = container
  Targets
     - Contains -> Node(Data): <id: 2, name: var1>
     - Contains -> Node(Static): <id: 4, name: static1>
     - Contains -> Node(Builtin): <id: 3, name: out>
     - Flow -> Node(Data): <id: 2, name: var1>

   ---> following (contained) : Contains: [Node(Container): <id: 1, name: main>]  --Contains-->  [Node(Builtin): <id: 3, name: out>]
   ---> following (contained) : Contains: [Node(Container): <id: 1, name: main>]  --Contains-->  [Node(Static): <id: 4, name: static1>]
   ---> following (contained) : Contains: [Node(Container): <id: 1, name: main>]  --Contains-->  [Node(Data): <id: 2, name: var1>]
   ---> Flow following: Flow: [Node(Container): <id: 1, name: main>]  --Flow-->  [Node(Data): <id: 2, name: var1>]

Executing node: Static:
    .id = 4
    .name = static1
    .type = static
    .value = Hello World!
  Sources
     - Contains <- Node(Container): <id: 1, name: main>
  Targets
     - Assign -> Node(Data): <id: 2, name: var1>

   ---> following: Assign: [Node(Static): <id: 4, name: static1>]  --Assign-->  [Node(Data): <id: 2, name: var1>]

Executing node: Data:
    .id = 2
    .name = var1
    .type = data
    .value = Hello World!
  Sources
     - Contains <- Node(Container): <id: 1, name: main>
     - Flow <-!!-- Node(Container): <id: 1, name: main>
     - Assign <- Node(Static): <id: 4, name: static1>
  Targets
     - Flow -> Node(Builtin): <id: 3, name: out>
     - Pass -> Node(Builtin): <id: 3, name: out>

   ---> following: Pass: [Node(Data): <id: 2, name: var1>]  --Pass-->  [Node(Builtin): <id: 3, name: out>]
   ---> Flow following: Flow: [Node(Data): <id: 2, name: var1>]  --Flow-->  [Node(Builtin): <id: 3, name: out>]

Executing node: Builtin:
    .id = 3
    .method = out
    .name = out
    .type = builtin
  Sources
     - Contains <- Node(Container): <id: 1, name: main>
     - Flow <-!!-- Node(Data): <id: 2, name: var1>
     - Pass <- Node(Data): <id: 2, name: var1>

Hello World!
'''


def test_max_path_length():
    config.max_path_length = 2
    try:
        m = json.load(open("samplemaps/helloworld.n", "r"))
    except FileNotFoundError:
        m = json.load(open("walujapi/samplemaps/helloworld.n", "r"))
    karte = Map(m, verbose=True)

    vessal = Runtime()
    karte.run(vessal, None)
    assert len(vessal.path) == 2


def test_next_with_anomaly(capsys):
    capsys.readouterr()

    try:
        m = json.load(open("samplemaps/helloanomaly.n", "r"))
    except FileNotFoundError:
        m = json.load(open("walujapi/samplemaps/helloanomaly.n", "r"))
    karte = Map(m)
    vessal = Runtime()
    vessal.initialize(karte)
    vessal.pause()
    vessal.begin(start=karte.start_node)
    vessal.resume()
    vessal.next()
    vessal.finish()
    captured = capsys.readouterr()
    assert captured.out == "Runtime Anomaly 'Node(Anomaly): <id: 3, name: anomaly>' raised\n"


def test_process_stack(capsys):
    capsys.readouterr()
    use.add_location('dir://walujapi/', 'files')
    try:
        m = json.load(open("tests/maps/importother2.n", "r"))
    except FileNotFoundError:
        m = json.load(open("walujapi/tests/maps/importother2.n", "r"))
    karte = Map(m, verbose=False)
    vessal = Runtime()
    karte.run(vessal, [])
    captured = capsys.readouterr()
    assert captured.out == """I love to eat cheese!!!\n(('I love to eat cheese!!!',), {})\n"""

    try:
        m = json.load(open("tests/maps/test_return.n", "r"))
    except FileNotFoundError:
        m = json.load(open("walujapi/tests/maps/test_return.n", "r"))
    karte = Map(m, verbose=False)
    vessal = Runtime()
    karte.run(vessal, [])
    captured = capsys.readouterr()
    assert captured.out == "Flobbly Doo\nHi Mike!\n(('Hi Mike!',), {})\n(('Flobbly Doo',), {})\n"


def test_complete_stack():
    rt = Runtime()
    rt.frame_finished()


def test_exec_count():
    try:
        m = json.load(open("samplemaps/helloworld.n", "r"))
    except FileNotFoundError:
        m = json.load(open("walujapi/samplemaps/helloworld.n", "r"))
    karte = Map(m, verbose=True)

    vessal = Runtime()
    karte.run(vessal, None)
    assert vessal.get_exec_count() == 1
    vessal.appendNextNode(karte.nodes['4'], 'DOWN')
    vessal.stopped = False
    vessal.continue_exec()
    assert vessal.get_exec_count() == 2


def test_in_event():
    try:
        m = json.load(open("samplemaps/helloworld.n", "r"))
    except FileNotFoundError:
        m = json.load(open("walujapi/samplemaps/helloworld.n", "r"))
    karte = Map(m, verbose=True)

    vessal = Runtime()
    karte.run(vessal, None)
    assert vessal.get_exec_count() == 1

    event = {
        "nodeid": "4",
        "type": "no_type",
        "payload": "Some stuff"
    }

    vessal.add_in_event(event)
    vessal.stopped = False
    vessal.paused = False
    vessal.continue_exec()

    assert vessal.anomaly.args[0] == 'Runtime Error: malformed event'

    event = {
        "node_id": "44",
        "type": "no_type",
        "payload": "Some stuff"
    }

    vessal.add_in_event(event)
    vessal.stopped = False
    vessal.paused = False
    vessal.continue_exec()

    assert vessal.anomaly.args[0] == 'Runtime Error: unknown node'

    event = {
        "node_id": "2",
        "type": "no_type",
        "payload": "Some stuff"
    }

    vessal.add_in_event(event)
    vessal.stopped = False
    vessal.paused = False
    vessal.continue_exec()

    assert vessal.anomaly.args[0] == 'Runtime Error: event sent to node which cannot process it.'

    # This allows next_from_in_events to run, but, does not exercise
    # any downstream nodes in the Map.
    def ha(rt, ev):
        rt.anomaly = Exception(ev['payload'])
        return False

    karte.nodes['2'].handle_event = ha

    vessal.add_in_event(event)
    vessal.stopped = False
    vessal.paused = False
    vessal.continue_exec()

    assert vessal.anomaly.args[0] == 'Some stuff'


def test_rs_blocked():
    """
    Data nodes when run without inbound edges
    will be blocking, waiting for user input.

    Providing input will clear the blocking state.
    """
    graph = {
        "map": {
            "edges": [],
            "nodes": [
                {
                    "id": 1,
                    "name": "TEST BLOCKING",
                    "type": "data"
                }
            ]
        }
    }

    karte = Map(graph, verbose=True)
    vessal = Runtime()
    karte.run(vessal, None)
    assert karte.nodes[1]._run_state._blocked is True
    assert karte.nodes[1]._run_state._deferred is False
    assert karte.nodes[1]._run_state._executed == -1
    assert karte.nodes[1]._run_state._preempted is False
    assert karte.nodes[1]._run_state._pending_run is False
    assert karte.nodes[1].value is None

    vessal.set_input(1, "CONTENT")
    vessal.continue_exec()
    assert karte.nodes[1]._run_state._blocked is False
    assert karte.nodes[1]._run_state._deferred is False
    assert karte.nodes[1]._run_state._executed == 0
    assert karte.nodes[1]._run_state._preempted is False
    assert karte.nodes[1]._run_state._pending_run is False
    assert karte.nodes[1].value == "CONTENT"

    graph = {
        "map": {
            "edges": [
                {
                    "rule": "assign",
                    "source": "2:VAL_TO_DATA",
                    "target": "1:TEST BLOCKING"
                }
            ],
            "nodes": [
                {
                    "id": "1",
                    "name": "TEST BLOCKING",
                    "type": "data"
                },
                {
                    "id": "2",
                    "name": "VAL_TO_DATA",
                    "type": "static",
                    "value": "MORE CONTENT"
                }
            ]
        }
    }

    karte = Map(graph, verbose=True)
    vessal = Runtime()
    karte.run(vessal, None)
    assert karte.nodes["1"]._run_state._blocked is False
    assert karte.nodes["1"]._run_state._deferred is False
    assert karte.nodes["1"]._run_state._executed == 0
    assert karte.nodes["1"]._run_state._preempted is False
    assert karte.nodes["1"]._run_state._pending_run is False
    assert karte.nodes["1"].value == "MORE CONTENT"

    graph = {
        "map": {
            "edges": [
                {
                    "rule": "assign",
                    "source": "2:TEST BLOCKING",
                    "target": "1:VAL_TO_DATA"
                }
            ],
            "nodes": [
                {
                    "id": "2",
                    "name": "TEST BLOCKING",
                    "type": "data"
                },
                {
                    "id": "1",
                    "name": "VAL_TO_DATA",
                    "type": "data",
                }
            ]
        }
    }

    karte = Map(graph, verbose=True)
    vessal = Runtime()
    karte.run(vessal, None)
    assert karte.nodes["2"]._run_state._blocked is True
    assert karte.nodes["2"]._run_state._deferred is False
    assert karte.nodes["2"]._run_state._executed == -1
    assert karte.nodes["2"]._run_state._preempted is False
    assert karte.nodes["2"]._run_state._pending_run is False
    assert karte.nodes["1"].value is None
    assert karte.nodes["2"].value is None

    vessal.set_input("2", "OTHER CONTENT")
    vessal.continue_exec()
    assert karte.nodes["1"]._run_state._blocked is False
    assert karte.nodes["1"]._run_state._deferred is False
    assert karte.nodes["1"]._run_state._executed == 0
    assert karte.nodes["1"]._run_state._preempted is False
    assert karte.nodes["1"]._run_state._pending_run is False
    assert karte.nodes["1"].value == "OTHER CONTENT"


def test_debug_breakpoint():
    try:
        data = json.load(open("samplemaps/helloworld.n", "r"))
    except FileNotFoundError:
        data = json.load(open("walujapi/samplemaps/helloworld.n", "r"))
    m1 = Map(data, verbose=True)
    rt = Runtime()
    rt.initialize(m1)
    exe = rt.get_executed()
    """
    Even though begin executes all the nodes in a graph,
    it ends up resetting the state, so the runtime engine reports
    nothing has been executed.... perhaps this is not desireable,
    but, the get_executed method is primarily intended for determining
    what stuff was run up to a given breakpoint in debug mode.
    """
    assert len(exe["edges"]) == 0
    assert len(exe["nodes"]) == 0
    assert exe["last_node"] is None

    n = m1.get_node('3')
    n.set_breakpoint()
    rt.initialize(m1)
    rt.begin(debug=True)
    exe = rt.get_executed()
    assert len(exe["edges"]) == 4
    assert len(exe["nodes"]) == 2
    assert exe["last_node"] == '3'
    assert n._breakpoint_set
    assert not n.has_executed()
    assert not rt.stopped
    assert rt.current is not None
    assert rt.current.current == n

    cnt = 0
    while cnt < 2:
        rt.continue_exec()
        cnt += 1

    assert rt.current is None
    assert rt.stopped

    # now run it without the debug flag
    rt.initialize(m1)
    rt.begin(debug=False)
    assert rt.stopped
    assert rt.current is None
    assert n._breakpoint_set
    assert n._run_state._executed == 0


def test_keep_nexting():
    """
        +----+-------+--------------+---+--------+
        | dbg  | bp set  | bp detected  |       | rv     |
        +-----+------+--------------+---+--------+
        | T       | F          | F                      |       | F      |   1
        | T       | T         | T                       |       | F      |  2
        | T       | F         | T                       |       | T      |  3
        | T       | T         | F                       |       | ERROR  | 4
        | F       | F         | F                       |        | T      |  5
        | F       | T         | F                       |       | ERROR  | 6
        | F       | F         | T                       |       | T      | 7
        | F       | T         | T                       |       | T      | 8
        +----+---------+--------------+---+--------+
    """

    rt = Runtime()
    rt.initialize(None)
    n = Node(None)
    n2 = Node(None)
    rt.setNextNode(n, 'DOWN')
    rt.appendNextNode(n2, 'DOWN')
    rt.frame_next()

    # 1.
    rt.debug = True
    assert not rt.keep_nexting()

    # 2.
    n.set_breakpoint()
    rt.breakpoint_detected = True
    assert not rt.keep_nexting()

    # 3.
    n.unset_breakpoint()
    assert rt.keep_nexting()

    # 5.
    rt.debug = False
    rt.breakpoint_detected = False
    assert rt.keep_nexting()

    # 7.
    rt.breakpoint_detected = True
    assert rt.keep_nexting()

    # 8.
    n.set_breakpoint()
    rt.breakpoint_detected = True
    assert rt.keep_nexting()
