"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

import pytest


@pytest.mark.skip(reason="External has been moved into the shoshoni extension.")
def test_modfile_property():
    from nord.nord.exceptions import PropertyException as PropertyException
    import nord.nord.nodes.External as External
    g = External()
    g.set_property('modfile', '/dev/null')
    assert g.get_property('modfile') == '/dev/null'

    try:
        g.set_property('mapfile', '/dev/null')
        g.get_property('mapfile') == '/dev/null'
    except PropertyException as e:
        assert str(e) == "Cannot set 'mapfile' for External as it has no property named: mapfile"
