"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

import nord.nord.Map as Map
import json
import pytest
from nord.nord import Runtime as RT
Runtime = RT.Runtime
import nord.nord.utils.SpaceManager as use  # noqa: E402
from nord.nord.exceptions import RuntimeException as RuntimeException  # noqa: E402


def test_mapfile_property():
    import nord.nord.nodes.Graph as Graph
    from nord.nord.exceptions import PropertyException as PropertyException
    g = Graph(Map(None))
    g.set_property('mapfile', '/dev/null')
    assert g.get_property('mapfile') == '/dev/null'

    try:
        g.get_property('modfile')
    except PropertyException as e:
        assert str(e) == 'Graph has no property named: modfile'

    try:
        g.is_set_property('modfile')
    except PropertyException as e:
        assert str(e) == 'Graph has no property named: modfile'

    try:
        g.check_property('modfile')
    except PropertyException as e:
        assert str(e) == 'Graph has no property named: modfile'

    assert g.check_property('mapfile')
    assert g.is_set_property('mapfile')

    g.apply({'id': 1, 'mapfile': 'bananas'})
    assert g.get_property('mapfile') == 'bananas'


def test_bad_imported_graph(capsys):
    try:
        data = json.load(open("tests/maps/importother.n", "r"))
    except FileNotFoundError:
        data = json.load(open("walujapi/tests/maps/importother.n", "r"))

    data['map']['nodes'][2]['itemid'] = 'DOESNOTEXIST'
    m1 = Map(data, verbose=True)
    try:
        m1.run(Runtime(), [])
        captured = capsys.readouterr()
    except RuntimeException as e:
        assert str(e) == "Requested Node with id: DOESNOTEXIST does not exist in tests/maps/importme.n"

    try:
        data = json.load(open("tests/maps/importother.n", "r"))
    except FileNotFoundError:
        data = json.load(open("walujapi/tests/maps/importother.n", "r"))

    del data['map']['nodes'][2]['parentid']
    m1 = Map(data, verbose=True)
    try:
        m1.run(Runtime(), [])
        captured = capsys.readouterr()
    except RuntimeException as e:
        assert str(e) == "Reference not properly intialized"


@pytest.mark.skip(reason="INCOMPLETE TEST")
def test_imported_graph(capsys):
    use.add_location('dir://walujapi/', 'files')
    try:
        data = json.load(open("tests/maps/importother2.n", "r"))
    except FileNotFoundError:
        data = json.load(open("walujapi/tests/maps/importother2.n", "r"))
    m1 = Map(data, verbose=True)
    m1.run(Runtime(), [])
    captured = capsys.readouterr()
    assert captured.out == """Map created with 5 Nodes and 7 Edges

Executing node: Graph:
    .id = 37bc0554-6892-4b2d-9adb-e997fb4d3993
    .mapfile = tests/maps/importme2.n
    .name = Graph: Super Sublimated Cave
    .position = [11.762126922607422, 0.0, -0.6525980830192566]
    .type = graph
  Targets
     - Use -> Node(Container): <id: 5735e3a9-076a-4735-aa7f-a31c0a32fe8d, name: Container: Light Onerous Table>


Executing node: Container:
    .id = 5735e3a9-076a-4735-aa7f-a31c0a32fe8d
    .name = Container: Light Onerous Table
    .position = [-10.702614784240723, 0.0, 6.28797721862793]
    .type = container
  Sources
     - Use <-!!-- Node(Graph): <id: 37bc0554-6892-4b2d-9adb-e997fb4d3993, name: Graph: Super Sublimated Cave>
  Targets
     - Contains -> Node(Builtin): <id: defbd811-1213-4f9b-9bc5-7dd650f846ff, name: Builtin: Aloof Chipped Siege>
     - Contains -> Node(Reference): <id: 0ffb71e8-ecdf-4e3a-9411-920bf00152d4, name: Cool Oderous Siege>
     - Contains -> Node(Reference): <id: 19419c10-2771-41bc-b4f5-de34c548f912, name: Cuddly Scientific Laser>

   ---> following: Use: [Node(Graph): <id: 37bc0554-6892-4b2d-9adb-e997fb4d3993, name: Graph: Super Sublimated Cave>]  --Use-->  [Node(Container): <id: 5735e3a9-076a-4735-aa7f-a31c0a32fe8d, name: Container: Light Onerous Table>]
Map created with 13 Nodes and 17 Edges

Executing node: Reference:
    .id = 0ffb71e8-ecdf-4e3a-9411-920bf00152d4
    .itemid = 1644c7b9-783a-4729-a232-b8f9b44f22dd
    .name = Cool Oderous Siege
    .parentid = 37bc0554-6892-4b2d-9adb-e997fb4d3993
    .position = [-8.645009994506836, 0.0, 2.3109662532806396]
    .type = reference
  Sources
     - Contains <-!!-- Node(Container): <id: 5735e3a9-076a-4735-aa7f-a31c0a32fe8d, name: Container: Light Onerous Table>
  Targets
     - Pass -> Node(Builtin): <id: defbd811-1213-4f9b-9bc5-7dd650f846ff, name: Builtin: Aloof Chipped Siege>
     - Flow -> Node(Builtin): <id: defbd811-1213-4f9b-9bc5-7dd650f846ff, name: Builtin: Aloof Chipped Siege>

   ---> following (contained) : Contains: [Node(Container): <id: 5735e3a9-076a-4735-aa7f-a31c0a32fe8d, name: Container: Light Onerous Table>]  --Contains-->  [Node(Reference): <id: 0ffb71e8-ecdf-4e3a-9411-920bf00152d4, name: Cool Oderous Siege>]
   ---> following (contained) : Contains: [Node(Container): <id: 5735e3a9-076a-4735-aa7f-a31c0a32fe8d, name: Container: Light Onerous Table>]  --Contains-->  [Node(Container): <id: 1644c7b9-783a-4729-a232-b8f9b44f22dd, name: Helper>]**

Executing node: Static:
    .id = ed6b79c9-4f77-4466-a8f2-bae3273563d5
    .name = Str1
    .type = static
    .value = I love to eat cheese!!!
  Sources
     - Contains <-!!-- Node(Container): <id: 1644c7b9-783a-4729-a232-b8f9b44f22dd, name: Helper>
  Targets
     - Flow -> Node(Builtin): <id: ca5f5e29-1efd-4e41-acca-4bca49175e71, name: Builtin: Friendly Tuned Laser>
     - Assign -> Node(Data): <id: 880b498a-c7ec-418a-92d5-1347faa9ea60, name: Data: Gothic Vehicular Sound>

   ---> following (contained) : Contains: [Node(Container): <id: 1644c7b9-783a-4729-a232-b8f9b44f22dd, name: Helper>]  --Contains-->  [Node(Static): <id: ed6b79c9-4f77-4466-a8f2-bae3273563d5, name: Str1>]

Executing node: Data:
    .id = 880b498a-c7ec-418a-92d5-1347faa9ea60
    .name = Data: Gothic Vehicular Sound
    .type = data
    .value = None
  Sources
     - Contains <-!!-- Node(Container): <id: 1644c7b9-783a-4729-a232-b8f9b44f22dd, name: Helper>
     - Assign <-!!-- Node(Static): <id: ed6b79c9-4f77-4466-a8f2-bae3273563d5, name: Str1>
  Targets
     - Pass -> Node(Builtin): <id: ca5f5e29-1efd-4e41-acca-4bca49175e71, name: Builtin: Friendly Tuned Laser>
     - Pass -> Node(Return): <id: 58cc40bf-08ba-4d1c-aa9f-b5e4f90f0673, name: Return: Mild Sanded Sparrow>

   ---> following (contained) : Contains: [Node(Container): <id: 1644c7b9-783a-4729-a232-b8f9b44f22dd, name: Helper>]  --Contains-->  [Node(Data): <id: 880b498a-c7ec-418a-92d5-1347faa9ea60, name: Data: Gothic Vehicular Sound>]
   ---> following: Assign: [Node(Static): <id: ed6b79c9-4f77-4466-a8f2-bae3273563d5, name: Str1>]  --Assign-->  [Node(Data): <id: 880b498a-c7ec-418a-92d5-1347faa9ea60, name: Data: Gothic Vehicular Sound>]

Executing node: Builtin:
    .id = ca5f5e29-1efd-4e41-acca-4bca49175e71
    .name = Builtin: Friendly Tuned Laser
    .type = builtin
  Sources
     - Pass <-!!-- Node(Data): <id: 880b498a-c7ec-418a-92d5-1347faa9ea60, name: Data: Gothic Vehicular Sound>
     - Contains <-!!-- Node(Container): <id: 1644c7b9-783a-4729-a232-b8f9b44f22dd, name: Helper>
     - Flow <-!!-- Node(Static): <id: ed6b79c9-4f77-4466-a8f2-bae3273563d5, name: Str1>
  Targets
     - Flow -> Node(Return): <id: 58cc40bf-08ba-4d1c-aa9f-b5e4f90f0673, name: Return: Mild Sanded Sparrow>

   ---> following: Pass: [Node(Data): <id: 880b498a-c7ec-418a-92d5-1347faa9ea60, name: Data: Gothic Vehicular Sound>]  --Pass-->  [Node(Builtin): <id: ca5f5e29-1efd-4e41-acca-4bca49175e71, name: Builtin: Friendly Tuned Laser>]
   ---> following (contained) : Contains: [Node(Container): <id: 1644c7b9-783a-4729-a232-b8f9b44f22dd, name: Helper>]  --Contains-->  [Node(Builtin): <id: ca5f5e29-1efd-4e41-acca-4bca49175e71, name: Builtin: Friendly Tuned Laser>]
I love to eat cheese!!!
   ---> Flow following: Flow: [Node(Builtin): <id: ca5f5e29-1efd-4e41-acca-4bca49175e71, name: Builtin: Friendly Tuned Laser>]  --Flow-->  [Node(Return): <id: 58cc40bf-08ba-4d1c-aa9f-b5e4f90f0673, name: Return: Mild Sanded Sparrow>]

Executing node: Return:
    .id = 58cc40bf-08ba-4d1c-aa9f-b5e4f90f0673
    .name = Return: Mild Sanded Sparrow
    .type = return
    .verbose = False
  Sources
     - Flow <-!!-- Node(Builtin): <id: ca5f5e29-1efd-4e41-acca-4bca49175e71, name: Builtin: Friendly Tuned Laser>
     - Pass <-!!-- Node(Data): <id: 880b498a-c7ec-418a-92d5-1347faa9ea60, name: Data: Gothic Vehicular Sound>
     - Contains <-!!-- Node(Container): <id: 1644c7b9-783a-4729-a232-b8f9b44f22dd, name: Helper>

   ---> following: Pass: [Node(Data): <id: 880b498a-c7ec-418a-92d5-1347faa9ea60, name: Data: Gothic Vehicular Sound>]  --Pass-->  [Node(Return): <id: 58cc40bf-08ba-4d1c-aa9f-b5e4f90f0673, name: Return: Mild Sanded Sparrow>]
   ---> following (contained) : Contains: [Node(Container): <id: 1644c7b9-783a-4729-a232-b8f9b44f22dd, name: Helper>]  --Contains-->  [Node(Return): <id: 58cc40bf-08ba-4d1c-aa9f-b5e4f90f0673, name: Return: Mild Sanded Sparrow>]

Executing node: Builtin:
    .id = defbd811-1213-4f9b-9bc5-7dd650f846ff
    .name = Builtin: Aloof Chipped Siege
    .position = [-11.213495254516602, -1.2502784729003906, -4.962981224060059]
    .type = builtin
  Sources
     - Contains <-!!-- Node(Container): <id: 5735e3a9-076a-4735-aa7f-a31c0a32fe8d, name: Container: Light Onerous Table>
     - Pass <-!!-- Node(Container): <id: 1644c7b9-783a-4729-a232-b8f9b44f22dd, name: Helper>
     - Flow <-!!-- Node(Container): <id: 1644c7b9-783a-4729-a232-b8f9b44f22dd, name: Helper>
  Targets
     - Flow -> Node(Reference): <id: 19419c10-2771-41bc-b4f5-de34c548f912, name: Cuddly Scientific Laser>

   ---> following (contained) : Contains: [Node(Container): <id: 5735e3a9-076a-4735-aa7f-a31c0a32fe8d, name: Container: Light Onerous Table>]  --Contains-->  [Node(Builtin): <id: defbd811-1213-4f9b-9bc5-7dd650f846ff, name: Builtin: Aloof Chipped Siege>]
   ---> following: Pass: [Node(Container): <id: 1644c7b9-783a-4729-a232-b8f9b44f22dd, name: Helper>]  --Pass-->  [Node(Builtin): <id: defbd811-1213-4f9b-9bc5-7dd650f846ff, name: Builtin: Aloof Chipped Siege>]
(('I love to eat cheese!!!',), {})
   ---> Flow following: Flow: [Node(Builtin): <id: defbd811-1213-4f9b-9bc5-7dd650f846ff, name: Builtin: Aloof Chipped Siege>]  --Flow-->  [Node(Reference): <id: 19419c10-2771-41bc-b4f5-de34c548f912, name: Cuddly Scientific Laser>]

Executing node: Reference:
    .id = 19419c10-2771-41bc-b4f5-de34c548f912
    .itemid = 5c29aca5-6599-4f88-ac80-251c4138aaff
    .name = Cuddly Scientific Laser
    .parentid = 37bc0554-6892-4b2d-9adb-e997fb4d3993
    .position = [7.754087924957275, 0.3971405029296875, 3.2734317779541016]
    .type = reference
  Sources
     - Flow <-!!-- Node(Builtin): <id: defbd811-1213-4f9b-9bc5-7dd650f846ff, name: Builtin: Aloof Chipped Siege>
     - Contains <-!!-- Node(Container): <id: 5735e3a9-076a-4735-aa7f-a31c0a32fe8d, name: Container: Light Onerous Table>

   ---> following (contained) : Contains: [Node(Container): <id: 5735e3a9-076a-4735-aa7f-a31c0a32fe8d, name: Container: Light Onerous Table>]  --Contains-->  [Node(Reference): <id: 19419c10-2771-41bc-b4f5-de34c548f912, name: Cuddly Scientific Laser>]
   ---> following (contained) : Contains: [Node(Container): <id: 5735e3a9-076a-4735-aa7f-a31c0a32fe8d, name: Container: Light Onerous Table>]  --Contains-->  [Node(Container): <id: 5c29aca5-6599-4f88-ac80-251c4138aaff, name: Main>]**

Executing node: Static:
    .id = 1dc01a5f-d883-4dd2-8e86-3ed910b1c84d
    .name = Static
    .type = static
    .value = CheeseFace
  Sources
     - Contains <-!!-- Node(Container): <id: 5c29aca5-6599-4f88-ac80-251c4138aaff, name: Main>

   ---> following (contained) : Contains: [Node(Container): <id: 5c29aca5-6599-4f88-ac80-251c4138aaff, name: Main>]  --Contains-->  [Node(Static): <id: 1dc01a5f-d883-4dd2-8e86-3ed910b1c84d, name: Static>]
"""
