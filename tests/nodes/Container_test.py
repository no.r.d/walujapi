"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

import nord.nord.nodes.Container as Container
import nord.nord.Map as Map
import json
from nord.nord import Runtime as RT
Runtime = RT.Runtime


def test_Container():
    a = Container(Map(None))
    a.id = 1
    assert repr(a) == '''Container:\n    .id = 1\n    .type = container'''


def test_parallel_starts(capsys):
    try:
        data = json.load(open("tests/maps/test_start_mult_contained.n", "r"))
    except FileNotFoundError:
        data = json.load(open("walujapi/tests/maps/test_start_mult_contained.n", "r"))
    m1 = Map(data, verbose=True)
    m1.run(Runtime(), [])