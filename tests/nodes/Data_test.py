"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

import nord.nord.nodes.Data as Data
import nord.nord.Map as Map
import re


def test_Data():
    a = Data(Map(None))
    a.id = 1
    assert re.match('''Data:
    .id = 1
    .name = [a-zA-Z0-9- ]+
    .type = data
    .value = None''', repr(a)) is not None


def test_data_explicit_type_and_name():
    a = Data(Map(None), name="fred", ntype=str)
    a.id = 1
    assert repr(a) == '''Data:
    .id = 1
    .name = fred
    .type = <class 'str'>
    .value = None'''


def test_data_explicit_name():
    a = Data(Map(None), name="fred")
    a.id = 1
    assert repr(a) == '''Data:
    .id = 1
    .name = fred
    .type = data
    .value = None'''
