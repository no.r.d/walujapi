"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

import nord.nord.nodes.Builtin as Builtin
from nord.nord.exceptions import BuiltinException as BuiltinException
import nord.nord.Map as Map
from nord.nord import Runtime as RT
Runtime = RT.Runtime
import json  # noqa: E402
import pytest  # noqa: E402


def test_Builtin_helloworld(capsys):
    try:
        m = json.load(open("samplemaps/helloworld.n", "r"))
    except FileNotFoundError:
        m = json.load(open("walujapi/samplemaps/helloworld.n", "r"))
    karte = Map(m)
    vessal = Runtime()
    karte.run(vessal, None)
    captured = capsys.readouterr()
    assert captured.out == 'Hello World!\n'


def test_Builtin_helloworld1(capsys):
    try:
        m = json.load(open("samplemaps/helloworld_2static_pos.n", "r"))
    except FileNotFoundError:
        m = json.load(open("walujapi/samplemaps/helloworld_2static_pos.n", "r"))
    karte = Map(m)
    vessal = Runtime()
    karte.run(vessal, None)
    captured = capsys.readouterr()
    assert captured.out == 'Hello World!\n'


def test_Builtin_helloworld2(capsys):
    try:
        m = json.load(open("samplemaps/helloworld_2static_pos.n", "r"))
    except FileNotFoundError:
        m = json.load(open("walujapi/samplemaps/helloworld_2static_pos.n", "r"))
    karte = Map(m)
    vessal = Runtime()
    karte.run(vessal, None)
    captured = capsys.readouterr()
    assert captured.out == 'Hello World!\n'


def test_Builtin_helloworld3(capsys):
    try:
        m = json.load(open("samplemaps/helloworld_2static_name.n", "r"))
    except FileNotFoundError:
        m = json.load(open("walujapi/samplemaps/helloworld_2static_name.n", "r"))
    karte = Map(m)
    vessal = Runtime()
    karte.run(vessal, None)
    captured = capsys.readouterr()
    assert captured.out == 'Hello World!\n'


@pytest.mark.skip(reason="This test is outdated now that Builtin is a Receiver subclass.")
def test_Builtin_addArg_exception(capsys):
    try:
        m = json.load(open("samplemaps/helloworld_2static_pos.n", "r"))
    except FileNotFoundError:
        m = json.load(open("walujapi/samplemaps/helloworld_2static_pos.n", "r"))
    del m['map']['edges'][4]['position']
    karte = Map(m)
    vessal = Runtime()
    try:
        karte.run(vessal, None)
        assert 'Better not hit this test' == 1
    except BuiltinException as e:
        assert str(e) == 'Pre-execution exception: passing more than 1 argument to Builtin:out via Pass: [Node(Static): <id: 4, name: static2>]  --Pass-->  [Node(Builtin): <id: 2, name: out>] requires edge with name or position attribute'  # noqa: E501


def test_Builtin_noargs(capsys):
    try:
        m = json.load(open("samplemaps/helloworld.n", "r"))
    except FileNotFoundError:
        m = json.load(open("walujapi/samplemaps/helloworld.n", "r"))
    del m['map']['edges'][6]
    karte = Map(m)
    vessal = Runtime()
    karte.run(vessal, None)
    captured = capsys.readouterr()
    assert captured.out == '\n\n'


def test_Builtin():
    a = Builtin(Map(None))
    a.id = 1
    assert repr(a) == 'Builtin:\n    .id = 1\n    .type = builtin'
