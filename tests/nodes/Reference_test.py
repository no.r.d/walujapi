"""
Copyright 2022 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--
"""

import json
import nord.nord.Runtime as RT
import nord.nord.Map as Map


Runtime = RT.Runtime


def test_dereference_edge_follow_state():
    """
    This test makes sure that a swapped current node (via replace_current_node)
    in Reference.prep_to_execute has followed all in and out edges.
    """
    try:
        m = json.load(open("tests/maps/test_dataobject_nested.n", "r"))
    except FileNotFoundError:
        m = json.load(open("walujapi/tests/maps/test_dataobject_nested.n", "r"))
    karte = Map(m)
    resultant_dobj = karte.nodes['8a5b877c-3139-4194-9c0d-714ba7a93b30']
    now_dobj = karte.nodes['95e41b9b-1cda-4c0b-9e0f-f8b596b617fb']

    ref_node = karte.nodes['a38fe084-b958-42cf-bd01-33f99a07b2e6']

    vessal = Runtime()
    vessal.initialize(karte, verbose=True)
    vessal.pause()
    vessal.begin(debug=True)

    while not vessal.is_done():
        node = vessal.get_current_node()
        vessal.next()

    deref_node = karte.nodes['73fe8334-0c81-4cbc-8b12-3fa7f77baff5']
    assert set(deref_node._followedTargets).issubset(set(ref_node._unfollowedTargets))
    assert len(deref_node._followedTargets) == len(ref_node._unfollowedTargets) - 1 # all be the flow edge
    assert len(ref_node._followedSources) == len(deref_node._followedSources)