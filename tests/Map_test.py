"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

import nord.nord.Map as Map
import json
from nord.nord import Runtime as RT
import nord.nord.utils.SpaceManager as use
from nord.nord.exceptions import UseException as UseException
from nord.nord.exceptions import MapException as MapException
import io

Runtime = RT.Runtime
use.add_location("dir://./src", "code")


def test_Map():
    try:
        data = json.load(open("samplemaps/helloworld.n", "r"))
    except FileNotFoundError:
        data = json.load(open("walujapi/samplemaps/helloworld.n", "r"))
    m1 = Map(data, verbose=True)

    assert (
        repr(m1)
        == """Nodes
================
-----
Container:
    .id = 1
    .name = main
    .type = container
  Targets
     - Contains -> Node(Data): <id: 2, name: var1>
     - Contains -> Node(Static): <id: 4, name: static1>
     - Contains -> Node(Builtin): <id: 3, name: out>
     - Flow -> Node(Data): <id: 2, name: var1>
-----
-----
Data:
    .id = 2
    .name = var1
    .type = data
    .value = None
  Sources
     - Contains <-!!-- Node(Container): <id: 1, name: main>
     - Flow <-!!-- Node(Container): <id: 1, name: main>
     - Assign <-!!-- Node(Static): <id: 4, name: static1>
  Targets
     - Flow -> Node(Builtin): <id: 3, name: out>
     - Pass -> Node(Builtin): <id: 3, name: out>
-----
-----
Builtin:
    .id = 3
    .method = out
    .name = out
    .type = builtin
  Sources
     - Contains <-!!-- Node(Container): <id: 1, name: main>
     - Flow <-!!-- Node(Data): <id: 2, name: var1>
     - Pass <-!!-- Node(Data): <id: 2, name: var1>
-----
-----
Static:
    .id = 4
    .name = static1
    .type = static
    .value = Hello World!
  Sources
     - Contains <-!!-- Node(Container): <id: 1, name: main>
  Targets
     - Assign -> Node(Data): <id: 2, name: var1>
-----



Edges
===============
  ---->
Contains: [Node(Container): <id: 1, name: main>]  --Contains-->  [Node(Data): <id: 2, name: var1>]
  ---->
Flow: [Node(Container): <id: 1, name: main>]  --Flow-->  [Node(Data): <id: 2, name: var1>]
  ---->
Contains: [Node(Container): <id: 1, name: main>]  --Contains-->  [Node(Builtin): <id: 3, name: out>]
  ---->
Contains: [Node(Container): <id: 1, name: main>]  --Contains-->  [Node(Static): <id: 4, name: static1>]
  ---->
Flow: [Node(Data): <id: 2, name: var1>]  --Flow-->  [Node(Builtin): <id: 3, name: out>]
  ---->
Pass: [Node(Data): <id: 2, name: var1>]  --Pass-->  [Node(Builtin): <id: 3, name: out>]
  ---->
Assign: [Node(Static): <id: 4, name: static1>]  --Assign-->  [Node(Data): <id: 2, name: var1>]


Rules
===============

(====)
-- Assign Rule --

  R: Assign: [Node(Static): <id: 4, name: static1>]  --Assign-->  [Node(Data): <id: 2, name: var1>]
(====)
-- Contains Rule --

  R: Contains: [Node(Container): <id: 1, name: main>]  --Contains-->  [Node(Data): <id: 2, name: var1>]
  R: Contains: [Node(Container): <id: 1, name: main>]  --Contains-->  [Node(Static): <id: 4, name: static1>]
  R: Contains: [Node(Container): <id: 1, name: main>]  --Contains-->  [Node(Builtin): <id: 3, name: out>]
(====)
-- Flow Rule --

  R: Flow: [Node(Container): <id: 1, name: main>]  --Flow-->  [Node(Data): <id: 2, name: var1>]
  R: Flow: [Node(Data): <id: 2, name: var1>]  --Flow-->  [Node(Builtin): <id: 3, name: out>]
(====)
-- Pass Rule --

  R: Pass: [Node(Data): <id: 2, name: var1>]  --Pass-->  [Node(Builtin): <id: 3, name: out>]"""
    )  # noqa: E501

    del data["name"]
    m2 = Map(data)
    assert m2.name != m1.name

    try:
        try:
            data2 = json.load(open("samplemaps/helloworld.n", "r"))
        except FileNotFoundError:
            data2 = json.load(open("walujapi/samplemaps/helloworld.n", "r"))
        data2["map"]["nodes"].append(
            {"id": 1, "name": "test", "type": "BETTER_NOT_EXIST"}
        )
        data2["map"]["nodes"].append({"type": "BETTER_NOT_EXIST"})
        _ = Map(data2)
    except UseException:
        pass

    try:
        try:
            data3 = json.load(open("samplemaps/helloworld.n", "r"))
        except FileNotFoundError:
            data3 = json.load(open("walujapi/samplemaps/helloworld.n", "r"))
        data3["map"]["edges"].append(
            {"source": "1", "target": "1", "rule": "BETTER_NOT_EXIST"}
        )
        data3["map"]["edges"].append({"source": "99", "target": "1", "rule": "assign"})
        data3["map"]["edges"].append({"cheese": "banana"})
        _ = Map(data3)
    except MapException:
        pass

    try:
        data4 = json.load(open("samplemaps/helloworld.n", "r"))
    except FileNotFoundError:
        data4 = json.load(open("walujapi/samplemaps/helloworld.n", "r"))
    del data4["map"]["edges"]
    _ = Map(data4)

    del data4["map"]["nodes"]
    _ = Map(data4)

    del data4["map"]
    _ = Map(data4)

    m1.startContainer = None
    m1.run(Runtime(), [])

    d = m1.to_dict()

    print()
    print(
        "====================================================================================="
    )
    print(
        "====================================================================================="
    )
    print(json.dumps(d, indent=2))
    print(
        "====================================================================================="
    )
    print(
        "====================================================================================="
    )

    del d[
        "extensions"
    ]  # needed to deal with extensions that may have been loaded by a map somewhere...
    del d["id"]
    assert d == {
        "map": {
            "nodes": [
                {"id": "1", "name": "main", "type": "container"},
                {"id": "2", "name": "var1", "type": "data", "value": "Hello World!"},
                {"id": "3", "method": "out", "name": "out", "type": "builtin"},
                {
                    "id": "4",
                    "name": "static1",
                    "type": "static",
                    "value": "Hello World!",
                },
            ],
            "edges": [
                {"rule": "contains", "source": "1:main", "target": "2:var1"},
                {"rule": "flow", "source": "1:main", "target": "2:var1"},
                {"rule": "contains", "source": "1:main", "target": "3:out"},
                {"rule": "contains", "source": "1:main", "target": "4:static1"},
                {"rule": "flow", "source": "2:var1", "target": "3:out"},
                {"rule": "pass", "source": "2:var1", "target": "3:out"},
                {"rule": "assign", "source": "4:static1", "target": "2:var1"},
            ],
        },
        "name": "HelloWorld",
    }


def test_debug_map(monkeypatch):
    monkeypatch.setattr("sys.stdin", io.StringIO("n\nn\nn\nn\nn\nn\nn\nn\nn\nn\nn\n"))
    try:
        data = json.load(open("samplemaps/helloworld.n", "r"))
    except FileNotFoundError:
        data = json.load(open("walujapi/samplemaps/helloworld.n", "r"))
    m1 = Map(data, verbose=True)
    rt = Runtime()
    m1.init_debug(rt, [])
    assert rt.stopped
    assert rt.current is None
    assert rt._exec_count == 1
    assert rt.anomaly is None

    # validate that the continue function of the debugger completes
    # the map
    monkeypatch.setattr("sys.stdin", io.StringIO("n\nn\nn\nn\nn\nc\n"))
    m1 = Map(data, verbose=True)
    rt = Runtime()
    m1.init_debug(rt, [])
    assert rt.stopped
    assert rt.current is None
    assert rt._exec_count == 1
    assert rt.anomaly is None


def test_diff_add_item():
    a = {}
    b = {1: "a"}
    diff = Map._section_compare_for_testing(a, b)
    assert diff == {"add": [{1: "a"}], "del": [], "change": []}


def test_diff_del_item():
    b = {}
    a = {1: "a"}
    diff = Map._section_compare_for_testing(a, b)
    assert diff == {"del": [{1: "a"}], "add": [], "change": []}


def test_diff_change_item():
    a = {1: {"a": 1, "b": 2, "c": 3}, 2: {1: 1}}
    b = {1: {"b": 4}, 2: {1: 1}}
    diff = Map._section_compare_for_testing(a, b)
    assert diff == {
        "add": [],
        "del": [],
        "change": [
            {
                1: {
                    "add": {},
                    "del": {"a": 1, "c": 3},
                    "change": {"b": 4},
                    "priors": {"b": 2},
                }
            }
        ],
    }


def test_diff_map():
    """
    This test should probably be broken into separate tests
    for add, delete and change. Were the section_compare method
    made available this would be possible.
    """
    try:
        data = json.load(open("samplemaps/diff_datetime_a.n", "r"))
    except FileNotFoundError:
        data = json.load(open("walujapi/samplemaps/diff_datetime_a.n", "r"))
    m1 = Map(data, verbose=True)

    try:
        data = json.load(open("samplemaps/diff_datetime_b.n", "r"))
    except FileNotFoundError:
        data = json.load(open("walujapi/samplemaps/diff_datetime_b.n", "r"))
    m2 = Map(data, verbose=True)

    diff = Map.diff_maps(m1, m2)
    import pprint

    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(diff)
    assert diff == {
        "edges": {
            "add": [
                {
                    (
                        "9a824fb2-8096-4fad-a616-0543c0a6aa39",
                        "d2ebb8cd-a7bc-4e91-8842-bd94ec1c8bf7",
                        "contains",
                    ): {
                        "from_space": "nord",
                        "rule": "contains",
                        "source": "9a824fb2-8096-4fad-a616-0543c0a6aa39:Container: "
                        "Friendly "
                        "Viscous "
                        "Slobber",
                        "space": "nord",
                        "target": "d2ebb8cd-a7bc-4e91-8842-bd94ec1c8bf7:Toint: "
                        "Gothic "
                        "Nordic "
                        "Cheese",
                        "to_space": "iteru",
                    }
                },
                {
                    (
                        "d2ebb8cd-a7bc-4e91-8842-bd94ec1c8bf7",
                        "5bc9efd4-49a7-418f-9093-e2a9c5a8ddce",
                        "cast",
                    ): {
                        "from_space": "iteru",
                        "rule": "cast",
                        "source": "d2ebb8cd-a7bc-4e91-8842-bd94ec1c8bf7:Toint: "
                        "Gothic "
                        "Nordic "
                        "Cheese",
                        "space": "iteru",
                        "target": "5bc9efd4-49a7-418f-9093-e2a9c5a8ddce:Convert: "
                        "Hard "
                        "Scientific "
                        "Cloud",
                        "to_space": "iteru",
                    }
                },
                {
                    (
                        "d2ebb8cd-a7bc-4e91-8842-bd94ec1c8bf7",
                        "7b85162c-a4a0-4014-be8d-3051fe11fbe8",
                        "cast",
                    ): {
                        "from_space": "iteru",
                        "rule": "cast",
                        "source": "d2ebb8cd-a7bc-4e91-8842-bd94ec1c8bf7:Toint: "
                        "Gothic "
                        "Nordic "
                        "Cheese",
                        "space": "iteru",
                        "target": "7b85162c-a4a0-4014-be8d-3051fe11fbe8:Convert: "
                        "Split "
                        "Sublimated "
                        "Mountain",
                        "to_space": "iteru",
                    }
                },
                {
                    (
                        "d2ebb8cd-a7bc-4e91-8842-bd94ec1c8bf7",
                        "e4e44ee1-a319-4dfa-8758-7ff1dc6db3c4",
                        "cast",
                    ): {
                        "from_space": "iteru",
                        "rule": "cast",
                        "source": "d2ebb8cd-a7bc-4e91-8842-bd94ec1c8bf7:Toint: "
                        "Gothic "
                        "Nordic "
                        "Cheese",
                        "space": "iteru",
                        "target": "e4e44ee1-a319-4dfa-8758-7ff1dc6db3c4:Convert: "
                        "Flat "
                        "Flooded "
                        "Laser",
                        "to_space": "iteru",
                    }
                },
            ],
            "change": [],
            "del": [
                {
                    (
                        "9a824fb2-8096-4fad-a616-0543c0a6aa39",
                        "04d19947-2be5-42a1-8e8f-4c99bcd0d8b2",
                        "contains",
                    ): {
                        "rule": "contains",
                        "source": "9a824fb2-8096-4fad-a616-0543c0a6aa39:Container: "
                        "Friendly "
                        "Viscous "
                        "Slobber",
                        "target": "04d19947-2be5-42a1-8e8f-4c99bcd0d8b2:This "
                        "is "
                        "a "
                        "broken "
                        "map",
                    }
                }
            ],
        },
        "extensions": {
            "add": [],
            "change": [
                {
                    "iteru": {
                        "add": {},
                        "change": {
                            "locations": {
                                "code": ["null://.", "dir://../iteru/src"],
                                "files": [
                                    "dir:///Users/nate/Desktop/Dev/NoRD/iteru/src/nord/iteru",
                                    "pkg://",
                                    "dir://../iteru/src/iteru",
                                    "dir://../iteru/src",
                                ],
                                "path": [
                                    "dir:///Users/nate/Desktop/Dev/NoRD/iteru/src/nord/iteru",
                                    "pkg://",
                                    "dir://../iteru/src",
                                    "dir://../iteru/src/iteru",
                                ],
                            }
                        },
                        "del": {},
                        "priors": {
                            "locations": {
                                "code": ["null://.", "dir://."],
                                "files": [
                                    "dir:///Users/nate/Desktop/Dev/NoRD/iteru/src/nord/iteru",
                                    "pkg://",
                                    "dir://../iteru/src/iteru",
                                    "dir://../iteru/src",
                                ],
                                "path": [
                                    "dir:///Users/nate/Desktop/Dev/NoRD/iteru/src/nord/iteru",
                                    "pkg://",
                                    "dir://../iteru/src",
                                    "dir://../iteru/src/iteru",
                                ],
                            }
                        },
                    }
                }
            ],
            "del": [],
        },
        "nodes": {
            "add": [
                {
                    "d2ebb8cd-a7bc-4e91-8842-bd94ec1c8bf7": {
                        "id": "d2ebb8cd-a7bc-4e91-8842-bd94ec1c8bf7",
                        "name": "Toint: " "Gothic " "Nordic " "Cheese",
                        "position": [
                            -29.194698333740234,
                            10.189581871032715,
                            4.576087951660156,
                        ],
                        "space": "iteru",
                        "type": "toint",
                    }
                }
            ],
            "change": [
                {
                    "28f952fb-7cda-4fd3-bdc3-dabb5b21aefb": {
                        "add": {},
                        "change": {
                            "position": [-2.4810118675231934, 0.0, -1.9227843284606934]
                        },
                        "del": {},
                        "priors": {
                            "position": [
                                -3.6762800216674805,
                                -0.158538818359375,
                                5.899912357330322,
                            ]
                        },
                    }
                },
                {
                    "51f71e25-b2a7-4f7c-8ac4-a9a77653d9fa": {
                        "add": {},
                        "change": {
                            "position": [
                                7.674807548522949,
                                -0.4150390625,
                                4.666077613830566,
                            ]
                        },
                        "del": {},
                        "priors": {
                            "position": [
                                7.578783988952637,
                                -0.248931884765625,
                                1.1726133823394775,
                            ]
                        },
                    }
                },
            ],
            "del": [
                {
                    "04d19947-2be5-42a1-8e8f-4c99bcd0d8b2": {
                        "id": "04d19947-2be5-42a1-8e8f-4c99bcd0d8b2",
                        "name": "This " "is " "a " "broken " "map",
                        "position": [
                            -9.62778091430664,
                            -31.40869140625,
                            -5.628478050231934,
                        ],
                        "type": "comment",
                    }
                }
            ],
        },
    }
