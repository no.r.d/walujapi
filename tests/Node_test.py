"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

import nord.nord.Node as Node
import nord.nord.Edge as Edge
from nord.nord import Runtime as RT
import nord.nord.Map as Map
import nord.nord.RunState as RunState


EHC_graph = {
    "map": {
        "edges": [
            {
                "rule": "flow",
                "source": "0:E",
                "target": "1:E"
            },
            {
                "rule": "flow",
                "source": "1:E",
                "target": "2:E"
            },
            {
                "rule": "flow",
                "source": "2:E",
                "target": "3:E"
            },
            {
                "rule": "flow",
                "source": "3:E",
                "target": "4:E"
            },
            {
                "rule": "flow",
                "source": "4:E",
                "target": "2:SSS"
            },
            {
                "rule": "flow",
                "source": "9:E",
                "target": "4:E"
            },
            {
                "rule": "flow",
                "source": "9:E",
                "target": "2:E"
            },
            {
                "rule": "assign",
                "source": "5:N",
                "target": "1:N"
            },
            {
                "rule": "assign",
                "source": "6:N",
                "target": "1:N"
            },
            {
                "rule": "flow",
                "source": "7:N",
                "target": "2:N"
            },
            {
                "rule": "flow",
                "source": "7:N",
                "target": "8:N"
            }
        ],
        "nodes": [
            {
                "id": "0",
                "name": "event1",
                "type": "data"
            },
            {
                "id": "1",
                "name": "instream1",
                "type": "data"
            },
            {
                "id": "2",
                "name": "instream2",
                "type": "data"
            },
            {
                "id": "3",
                "name": "instream3",
                "type": "data"
            },
            {
                "id": "4",
                "name": "instream4",
                "type": "data"
            },
            {
                "id": "5",
                "name": "non-event1",
                "type": "data"
            },
            {
                "id": "6",
                "name": "non-event2",
                "type": "data"
            },
            {
                "id": "7",
                "name": "non-event3",
                "type": "data"
            },
            {
                "id": "8",
                "name": "non-event4",
                "type": "data"
            },
            {
                "id": "9",
                "name": "event2",
                "type": "data"
            }
        ]
    }
}


def test_getTargets_empty():
    n = Node(Map(None))
    assert n.get_targets('NA') == []


def test_getSources_empty():
    n = Node(Map(None))
    assert n.get_sources('NA') == []


def test_exe_flagging():
    n = Node(Map(None))
    rt = RT.Runtime()
    n.flag_exe(rt)
    assert RunState.s(n).executed()
    n.clear_exe(rt)
    assert not RunState.s(n).executed()
    assert not RunState.s(n).executed()
    n.execute(rt)
    assert n.has_executed()


def test_no_rules():
    n = Node(Map(None))
    assert n.get_source_rules() == []


def test_unfollowed():
    src = Node(Map(None))
    e = Edge(Map(None))
    src.set_unfollowed(e, True)
    src.set_unfollowed(e, False)
    src.set_followed(e, True)
    src.set_followed(e, False)


def test_determine_event_hold_candidacy():
    """
    Tests the recursive method which evaluates (and flags)
    nodes as candidates for event driven execution.
    """

    # First test by initializing with a leaf node
    karte = Map(EHC_graph, True)
    vessal = RT.Runtime()
    event_emitter1 = karte.nodes["0"]
    event_emitter2 = karte.nodes["9"]
    event_emitter1.handle_event = lambda k: None
    event_emitter2.handle_event = lambda k: None
    event_leaf = karte.nodes["4"]
    event_leaf.determine_event_hold_candidacy(vessal, event_leaf)
    assert karte.nodes["0"]._ehc_enabled is True
    assert karte.nodes["0"]._event_sources == ['0']
    assert not hasattr(karte.nodes["0"]._run_state, '_nord_ehc_inf_recurse_brake')
    assert karte.nodes["1"]._ehc_enabled is True
    assert karte.nodes["1"]._event_sources == ['0']
    assert not hasattr(karte.nodes["1"]._run_state, '_nord_ehc_inf_recurse_brake')
    assert karte.nodes["2"]._ehc_enabled is True
    assert sorted(karte.nodes["2"]._event_sources) == sorted(['0', '9'])
    assert not hasattr(karte.nodes["2"]._run_state, '_nord_ehc_inf_recurse_brake')
    assert karte.nodes["3"]._ehc_enabled is True
    assert sorted(karte.nodes["3"]._event_sources) == sorted(['0', '9'])
    assert not hasattr(karte.nodes["3"]._run_state, '_nord_ehc_inf_recurse_brake')
    assert karte.nodes["4"]._ehc_enabled is True
    assert sorted(karte.nodes["4"]._event_sources) == sorted(['0', '9'])
    assert not hasattr(karte.nodes["4"]._run_state, '_nord_ehc_inf_recurse_brake')
    assert karte.nodes["5"]._ehc_enabled is False
    assert not hasattr(karte.nodes["5"], '_event_sources')
    assert not hasattr(karte.nodes["5"]._run_state, '_nord_ehc_inf_recurse_brake')
    assert karte.nodes["6"]._ehc_enabled is False
    assert not hasattr(karte.nodes["6"], '_event_sources')
    assert not hasattr(karte.nodes["6"]._run_state, '_nord_ehc_inf_recurse_brake')
    assert karte.nodes["7"]._ehc_enabled is False
    assert not hasattr(karte.nodes["7"], '_event_sources')
    assert not hasattr(karte.nodes["7"]._run_state, '_nord_ehc_inf_recurse_brake')
    assert karte.nodes["8"]._ehc_enabled is False
    assert not hasattr(karte.nodes["8"], '_event_sources')
    assert not hasattr(karte.nodes["8"]._run_state, '_nord_ehc_inf_recurse_brake')
    assert karte.nodes["9"]._ehc_enabled is True
    assert karte.nodes["9"]._event_sources == ['9']
    assert not hasattr(karte.nodes["9"]._run_state, '_nord_ehc_inf_recurse_brake')

    # Then, make sure the same results occur when starting in the
    # middle of the graph
    karte = Map(EHC_graph, True)
    vessal = RT.Runtime()
    event_emitter1 = karte.nodes["0"]
    event_emitter2 = karte.nodes["9"]
    event_emitter1.handle_event = lambda k: None
    event_emitter2.handle_event = lambda k: None
    event_mid = karte.nodes["2"]
    event_mid.determine_event_hold_candidacy(vessal, event_mid)
    assert karte.nodes["0"]._ehc_enabled is True
    assert karte.nodes["0"]._event_sources == ['0']
    assert not hasattr(karte.nodes["0"]._run_state, '_nord_ehc_inf_recurse_brake')
    assert karte.nodes["1"]._ehc_enabled is True
    assert karte.nodes["1"]._event_sources == ['0']
    assert not hasattr(karte.nodes["1"]._run_state, '_nord_ehc_inf_recurse_brake')
    assert karte.nodes["2"]._ehc_enabled is True
    assert sorted(karte.nodes["2"]._event_sources) == sorted(['0', '9'])
    assert not hasattr(karte.nodes["2"]._run_state, '_nord_ehc_inf_recurse_brake')
    assert karte.nodes["3"]._ehc_enabled is True
    assert sorted(karte.nodes["3"]._event_sources) == sorted(['0', '9'])
    assert not hasattr(karte.nodes["3"]._run_state, '_nord_ehc_inf_recurse_brake')
    assert karte.nodes["4"]._ehc_enabled is True
    assert sorted(karte.nodes["4"]._event_sources) == sorted(['0', '9'])
    assert not hasattr(karte.nodes["4"]._run_state, '_nord_ehc_inf_recurse_brake')
    assert karte.nodes["5"]._ehc_enabled is False
    assert not hasattr(karte.nodes["5"], '_event_sources')
    assert not hasattr(karte.nodes["5"]._run_state, '_nord_ehc_inf_recurse_brake')
    assert karte.nodes["6"]._ehc_enabled is False
    assert not hasattr(karte.nodes["6"], '_event_sources')
    assert not hasattr(karte.nodes["6"]._run_state, '_nord_ehc_inf_recurse_brake')
    assert karte.nodes["7"]._ehc_enabled is False
    assert not hasattr(karte.nodes["7"], '_event_sources')
    assert not hasattr(karte.nodes["7"]._run_state, '_nord_ehc_inf_recurse_brake')
    assert karte.nodes["8"]._ehc_enabled is False
    assert not hasattr(karte.nodes["8"], '_event_sources')
    assert not hasattr(karte.nodes["8"]._run_state, '_nord_ehc_inf_recurse_brake')
    assert karte.nodes["9"]._ehc_enabled is True
    assert karte.nodes["9"]._event_sources == ['9']
    assert not hasattr(karte.nodes["9"]._run_state, '_nord_ehc_inf_recurse_brake')


def test_is_ehc_or_runnable():
    """
    """
    karte = Map(EHC_graph, True)
    vessal = RT.Runtime()
    vessal.initialize(karte)
    event_emitter1 = karte.nodes["0"]
    event_emitter2 = karte.nodes["9"]
    event_emitter1.handle_event = lambda k, x: None
    event_emitter2.handle_event = lambda k, x: None
    assert karte.nodes["0"].is_ehc_or_runnable(vessal) is False
    assert karte.nodes["1"].is_ehc_or_runnable(vessal) is False
    assert karte.nodes["2"].is_ehc_or_runnable(vessal) is False
    assert karte.nodes["3"].is_ehc_or_runnable(vessal) is False
    assert karte.nodes["4"].is_ehc_or_runnable(vessal) is False
    assert karte.nodes["5"].is_ehc_or_runnable(vessal) is True
    assert karte.nodes["6"].is_ehc_or_runnable(vessal) is True
    assert karte.nodes["7"].is_ehc_or_runnable(vessal) is True
    assert karte.nodes["8"].is_ehc_or_runnable(vessal) is True
    assert karte.nodes["9"].is_ehc_or_runnable(vessal) is False

    vessal.add_in_event({"node_id": "0"})
    vessal.next_from_in_events()
    assert karte.nodes["0"].is_ehc_or_runnable(vessal) is True
    assert karte.nodes["1"].is_ehc_or_runnable(vessal) is True
    assert karte.nodes["2"].is_ehc_or_runnable(vessal) is True
    assert karte.nodes["3"].is_ehc_or_runnable(vessal) is True
    assert karte.nodes["4"].is_ehc_or_runnable(vessal) is True
    assert karte.nodes["5"].is_ehc_or_runnable(vessal) is True
    assert karte.nodes["6"].is_ehc_or_runnable(vessal) is True
    assert karte.nodes["7"].is_ehc_or_runnable(vessal) is True
    assert karte.nodes["8"].is_ehc_or_runnable(vessal) is True
    assert karte.nodes["9"].is_ehc_or_runnable(vessal) is False

    vessal.add_in_event({"node_id": "9"})
    vessal.next_from_in_events()
    assert karte.nodes["0"].is_ehc_or_runnable(vessal) is False
    assert karte.nodes["1"].is_ehc_or_runnable(vessal) is False
    assert karte.nodes["2"].is_ehc_or_runnable(vessal) is True
    assert karte.nodes["3"].is_ehc_or_runnable(vessal) is True
    assert karte.nodes["4"].is_ehc_or_runnable(vessal) is True
    assert karte.nodes["5"].is_ehc_or_runnable(vessal) is True
    assert karte.nodes["6"].is_ehc_or_runnable(vessal) is True
    assert karte.nodes["7"].is_ehc_or_runnable(vessal) is True
    assert karte.nodes["8"].is_ehc_or_runnable(vessal) is True
    assert karte.nodes["9"].is_ehc_or_runnable(vessal) is True


def test_in_event_path():
    """
    """
    karte = Map(EHC_graph, True)
    vessal = RT.Runtime()
    vessal.initialize(karte)
    event_emitter1 = karte.nodes["0"]
    event_emitter2 = karte.nodes["9"]
    event_emitter1.handle_event = lambda k, x: None
    event_emitter2.handle_event = lambda k, x: None
    assert karte.nodes["0"].in_event_path(vessal) is False
    assert karte.nodes["1"].in_event_path(vessal) is False
    assert karte.nodes["2"].in_event_path(vessal) is False
    assert karte.nodes["3"].in_event_path(vessal) is False
    assert karte.nodes["4"].in_event_path(vessal) is False
    assert karte.nodes["5"].in_event_path(vessal) is False
    assert karte.nodes["6"].in_event_path(vessal) is False
    assert karte.nodes["7"].in_event_path(vessal) is False
    assert karte.nodes["8"].in_event_path(vessal) is False
    assert karte.nodes["9"].in_event_path(vessal) is False

    vessal.add_in_event({"node_id": "0"})
    vessal.next_from_in_events()
    assert karte.nodes["0"].in_event_path(vessal) is True
    assert karte.nodes["1"].in_event_path(vessal) is True
    assert karte.nodes["2"].in_event_path(vessal) is True
    assert karte.nodes["3"].in_event_path(vessal) is True
    assert karte.nodes["4"].in_event_path(vessal) is True
    assert karte.nodes["5"].in_event_path(vessal) is False
    assert karte.nodes["6"].in_event_path(vessal) is False
    assert karte.nodes["7"].in_event_path(vessal) is False
    assert karte.nodes["8"].in_event_path(vessal) is False
    assert karte.nodes["9"].in_event_path(vessal) is False

    vessal.add_in_event({"node_id": "9"})
    vessal.next_from_in_events()
    assert karte.nodes["0"].in_event_path(vessal) is False
    assert karte.nodes["1"].in_event_path(vessal) is False
    assert karte.nodes["2"].in_event_path(vessal) is True
    assert karte.nodes["3"].in_event_path(vessal) is True
    assert karte.nodes["4"].in_event_path(vessal) is True
    assert karte.nodes["5"].in_event_path(vessal) is False
    assert karte.nodes["6"].in_event_path(vessal) is False
    assert karte.nodes["7"].in_event_path(vessal) is False
    assert karte.nodes["8"].in_event_path(vessal) is False
    assert karte.nodes["9"].in_event_path(vessal) is True


def test_event_hold_candidate():
    """
    Test that the wrapper function sets the graph
    EHC states as expected when called.
    """
    karte = Map(EHC_graph, True)
    vessal = RT.Runtime()
    event_emitter1 = karte.nodes["0"]
    event_emitter2 = karte.nodes["9"]
    event_emitter1.handle_event = lambda k: None
    event_emitter2.handle_event = lambda k: None
    assert karte.nodes["0"].event_hold_candidate(vessal) is True
    assert karte.nodes["1"].event_hold_candidate(vessal) is True
    assert karte.nodes["2"].event_hold_candidate(vessal) is True
    assert karte.nodes["3"].event_hold_candidate(vessal) is True
    assert karte.nodes["4"].event_hold_candidate(vessal) is True
    assert karte.nodes["5"].event_hold_candidate(vessal) is False
    assert karte.nodes["6"].event_hold_candidate(vessal) is False
    assert karte.nodes["7"].event_hold_candidate(vessal) is False
    assert karte.nodes["8"].event_hold_candidate(vessal) is False
    assert karte.nodes["9"].event_hold_candidate(vessal) is True
    node = Node(Map(None))
    assert node.event_hold_candidate(vessal) is False

    karte = Map(EHC_graph, True)
    vessal = RT.Runtime()
    event_emitter1 = karte.nodes["0"]
    event_emitter2 = karte.nodes["9"]
    event_emitter1.handle_event = lambda k: None
    event_emitter2.handle_event = lambda k: None
    assert karte.nodes["5"].event_hold_candidate(vessal) is False
    assert karte.nodes["8"].event_hold_candidate(vessal) is False
    assert karte.nodes["9"].event_hold_candidate(vessal) is True
    assert karte.nodes["0"].event_hold_candidate(vessal) is True
    assert karte.nodes["2"].event_hold_candidate(vessal) is True
    assert karte.nodes["1"].event_hold_candidate(vessal) is True
    assert karte.nodes["3"].event_hold_candidate(vessal) is True
    assert karte.nodes["4"].event_hold_candidate(vessal) is True
    assert karte.nodes["6"].event_hold_candidate(vessal) is False
    assert karte.nodes["7"].event_hold_candidate(vessal) is False

    karte = Map(EHC_graph, True)
    vessal = RT.Runtime()
    event_emitter1 = karte.nodes["0"]
    event_emitter2 = karte.nodes["9"]
    event_emitter1.handle_event = lambda k: None
    event_emitter2.handle_event = lambda k: None
    assert karte.nodes["0"].event_hold_candidate(vessal) is True

    karte = Map(EHC_graph, True)
    vessal = RT.Runtime()
    event_emitter1 = karte.nodes["0"]
    event_emitter2 = karte.nodes["9"]
    event_emitter1.handle_event = lambda k: None
    event_emitter2.handle_event = lambda k: None
    assert karte.nodes["1"].event_hold_candidate(vessal) is True

    karte = Map(EHC_graph, True)
    vessal = RT.Runtime()
    event_emitter1 = karte.nodes["0"]
    event_emitter2 = karte.nodes["9"]
    event_emitter1.handle_event = lambda k: None
    event_emitter2.handle_event = lambda k: None
    assert karte.nodes["2"].event_hold_candidate(vessal) is True

    karte = Map(EHC_graph, True)
    vessal = RT.Runtime()
    event_emitter1 = karte.nodes["0"]
    event_emitter2 = karte.nodes["9"]
    event_emitter1.handle_event = lambda k: None
    event_emitter2.handle_event = lambda k: None
    assert karte.nodes["3"].event_hold_candidate(vessal) is True

    karte = Map(EHC_graph, True)
    vessal = RT.Runtime()
    event_emitter1 = karte.nodes["0"]
    event_emitter2 = karte.nodes["9"]
    event_emitter1.handle_event = lambda k: None
    event_emitter2.handle_event = lambda k: None
    assert karte.nodes["4"].event_hold_candidate(vessal) is True

    karte = Map(EHC_graph, True)
    vessal = RT.Runtime()
    event_emitter1 = karte.nodes["0"]
    event_emitter2 = karte.nodes["9"]
    event_emitter1.handle_event = lambda k: None
    event_emitter2.handle_event = lambda k: None
    assert karte.nodes["5"].event_hold_candidate(vessal) is False

    karte = Map(EHC_graph, True)
    vessal = RT.Runtime()
    event_emitter1 = karte.nodes["0"]
    event_emitter2 = karte.nodes["9"]
    event_emitter1.handle_event = lambda k: None
    event_emitter2.handle_event = lambda k: None
    assert karte.nodes["6"].event_hold_candidate(vessal) is False

    karte = Map(EHC_graph, True)
    vessal = RT.Runtime()
    event_emitter1 = karte.nodes["0"]
    event_emitter2 = karte.nodes["9"]
    event_emitter1.handle_event = lambda k: None
    event_emitter2.handle_event = lambda k: None
    assert karte.nodes["7"].event_hold_candidate(vessal) is False

    karte = Map(EHC_graph, True)
    vessal = RT.Runtime()
    event_emitter1 = karte.nodes["0"]
    event_emitter2 = karte.nodes["9"]
    event_emitter1.handle_event = lambda k: None
    event_emitter2.handle_event = lambda k: None
    assert karte.nodes["8"].event_hold_candidate(vessal) is False

    karte = Map(EHC_graph, True)
    vessal = RT.Runtime()
    event_emitter1 = karte.nodes["0"]
    event_emitter2 = karte.nodes["9"]
    event_emitter1.handle_event = lambda k: None
    event_emitter2.handle_event = lambda k: None
    assert karte.nodes["9"].event_hold_candidate(vessal) is True


def test_eligible_for():
    import nord.nord.nodes.Data as Data
    d = Data(None)
    assert d.eligible_for("pass")
    assert d.eligible_for("assign")
