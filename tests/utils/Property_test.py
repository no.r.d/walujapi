"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

def test_property_default():
    import nord.nord.Property as Property
    class A(object):
        pass

    p = Property(A(), 'testname')
    assert p.get() is None
    assert str(p) == "Property(testname) is not set"
    p.set("a")
    assert str(p) == "Property(testname) : 'a'"


def test_property_check():
    import nord.nord.Property as Property
    class A(object):
        def __init__(s):
            s.testpass = '1'

    o = A()

    p = Property(o, 'testpass')
    assert p.check()
    p = Property(o, 'testfault')
    assert p.check() == False


def test_load_property():
    import nord.nord.Map as Map
    import json
    from nord.nord import Runtime as RT
    Runtime = RT.Runtime
    try:
        data = json.load(open("tests/maps/property.n", "r"))
    except FileNotFoundError:
        data = json.load(open("walujapi/tests/maps/property.n", "r"))
    m1 = Map(data, verbose=True)
    assert str(m1) == '''Nodes
================
-----
Graph:
    .id = 8c70c4a6-e17b-4eb9-95f4-9de832b6f610
    .modfile = /abc/def/xyz
    .name = Graph: Light Pounded Ceiling
    .position = [-0.18426291644573212, 0.0, 2.3416759967803955]
    .type = graph
-----



Edges
===============


Rules
===============
'''


def test_load_unset_property():
    import nord.nord.Map as Map
    import json
    from nord.nord import Runtime as RT
    Runtime = RT.Runtime
    try:
        data = json.load(open("tests/maps/property.n", "r"))
    except FileNotFoundError:
        data = json.load(open("walujapi/tests/maps/property.n", "r"))
    del data['map']['nodes'][0]['modfile']
    m1 = Map(data, verbose=True)
    assert str(m1) == '''Nodes
================
-----
Graph:
    .id = 8c70c4a6-e17b-4eb9-95f4-9de832b6f610
    .name = Graph: Light Pounded Ceiling
    .position = [-0.18426291644573212, 0.0, 2.3416759967803955]
    .type = graph
-----



Edges
===============


Rules
===============
'''
