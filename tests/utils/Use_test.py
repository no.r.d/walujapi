"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

import nord.nord.utils.UseUtil
from nord.nord.utils.UseUtil import Protocol, UseProtocolException, DirProtocol, PkgProtocol
from nord.nord.exceptions import UseException as UseException
import pytest
from pathlib import Path
use = nord.nord.utils.UseUtil.Use()


def test_Use():
    use.add_location('dir://src', 'path')
    use.add_location('dir://src', 'files')
    use.add_location('dir://src', 'code')
    use.add_location('dir://walujapi/src', 'path')
    use.add_location('dir://walujapi/src', 'files')
    use.add_location('dir://walujapi/src', 'code')
    use.add_location('dir://walujapi', 'path')
    use.add_location('dir://walujapi', 'files')
    use.add_location('dir://walujapi', 'code')
    assert use.get('path', 'nord/nord/utils/UseUtil.py').replace('walujapi/', '').endswith('src/nord/nord/utils/UseUtil.py')
    f = use.get('files', 'nord/nord/utils/UseUtil.py')
    f.close()
    f = use.path('samplemaps/helloworld.n')
    assert f.replace('walujapi/', '').endswith('samplemaps/helloworld.n')
    um = use.module('nord/nord/utils/UseUtil')
    u3 = um.Use()
    u3.add_location('dir://src/', 'files')
    u3.add_location('dir://walujapi/src/', 'files')
    f = u3.file('nord/nord/utils/UseUtil.py')
    f.close()


def test_Protocol_baseclass():
    p = Protocol()
    try:
        p.fetch('a', 'b')
    except UseProtocolException as e:
        assert str(e) == "Use Protocol 'Protocol' implementation error, fetch method not implemented"

    try:
        p.exists('a', 'b')
    except UseProtocolException as e:
        assert str(e) == "Use Protocol 'Protocol' implementation error, exists method not implemented"


def test_DirProtocol():
    dp = DirProtocol()
    assert dp.exists('/dev/null', 'null') == False


def test_PkgProtocol():
    pp = PkgProtocol('nord')
    assert pp.exists('pkg://', 'nord/nord/config/testing.conf') is True

    pp = PkgProtocol('fregata')
    assert pp.exists('pkg://', 'nord/wrigan/config/eventmap.conf') is False

    # The following injects a dependency on sigurd being installed
    pp = PkgProtocol('nord')
    assert pp.exists('pkg://', 'nord/sigurd/config/space.conf') is True

    assert pp.exists('pkg://', 'bill') is False

    # this test requires that wrigan be installed
    pp = PkgProtocol('wrigan')
    assert pp.exists('pkg://', 'config/eventmap.conf') is True

    pp = PkgProtocol('nord')
    assert pp.exists('pkg://', 'config/modelmap.conf')



def test_setProtocol():
    use.set_protocol('jimmy', object)
    assert 'jimmy' in use._protocols


def test_extprotocal():
    try:
        use._exists('missing://abe', 'something')
    except UseException as e:
        assert str(e) == 'Use protocol missing not found'

    class BadProt(Protocol):
        def fetch(s, path, name):
            return None
        def exists(s, path, name):
            return True

    class WorseProt(object):
        pass

    wp = WorseProt()
    use.set_protocol('worse', wp)

    bp = BadProt()
    use.set_protocol('bad', bp)

    use.add_location('worse://.', 'path')

    try:
        use._exists('worse://abe', 'something')
    except UseProtocolException as e:
        assert str(e) == "Use Protocol 'WorseProt' lacks path separator."

    setattr(wp, 'sep', '/')
    try:
        use._exists('worse://abe', 'something')
    except UseProtocolException as e:
        assert str(e) == "Use Protocol 'WorseProt' lacks fetch method."

    setattr(wp, 'fetch', lambda x: x)
    try:
        use._exists('worse://abe', 'something')
    except UseProtocolException as e:
        assert str(e) == "Use Protocol 'WorseProt' lacks prep method."

    setattr(wp, 'prep', lambda x: x)
    try:
        use._exists('worse://abe', 'something')
    except UseProtocolException as e:
        assert str(e) == "Use Protocol 'WorseProt' lacks finish method."

@pytest.mark.skip(reason="This test is incompatible with VS-Code's testing from parent directory to walujapi")
def test_override():
    use = nord.nord.utils.UseUtil.Use()

    use.add_override('helloworld.n', 'dir://.', 'dir://samplemaps', 'path')
    assert use.get('path', 'helloworld.n').endswith('samplemaps/helloworld.n')


def test_newProviderLocation():
    use.add_location('dir://.', 'music')
    use.prepend_location('dir:///tmp', 'music')

    def nProv(location, name):
        return use._fetch(location, name)

    use.add_provider('music', nProv, 'dir:///opt')
    assert use.get('music', 'freddy')


def test_code_with_py():
    use.add_location('dir://src/', 'code')
    um = use.module('nord.nord/utils/UseUtil.py')
    try:
        u4 = um.Use()
        u4.get('jazz', 'skiddly-dee')
    except UseException as e:
        assert str(e) == "Unknown provider jazz requested of 'Use' for resource: skiddly-dee"


def test_code_package_dir():
    use.add_location('dir://src/', 'code')
    nord = use.module('nord.nord')
    assert 'nodes' in dir(nord)


def test_not_found():
    try:
        use = nord.nord.utils.UseUtil.Use()
        use.get('files', 'This_File_doesnt_exist')
    except UseException as e:
        assert str(e).startswith("files:This_File_doesnt_exist not found in search path:")


def test_via_pgk_resource():
    try:
        use = nord.nord.utils.UseUtil.Use()
        path = use.path('nord/nord/config/testing.conf')
        rv = Path(path).exists()
        assert rv
    except Exception as e:
        assert 1 == 3
        assert e is not None
