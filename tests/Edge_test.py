"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

import nord.nord.Edge as Edge
import nord.nord.edges.Assign as Assign
import nord.nord.edges.Pass as Pass
from nord.nord.exceptions import EdgeException, RuntimeException
import nord.nord.nodes.Static as Static
import nord.nord.nodes.Builtin as Builtin
import nord.nord.nodes.Data as Data
from nord.nord import Runtime as RT
import nord.nord.utils.SpaceManager as use
import nord.nord.Map as Map

Runtime = RT.Runtime
use.add_location('dir://./src', 'code')


def test_apply():
    n = Edge(Map(None))
    n.apply({"id": 1, "name": "fred", "value": 77})
    assert n.id == 1
    assert n.name == "fred"
    assert n.value == 77


def test_setNodes():
    src = Data(Map(None))
    src.apply({"id": 1, "name": "fred", "value": 77})
    tgt = Data(Map(None))
    tgt.apply({"id": 2, "name": "larry", "value": 92})
    e = Assign(Map(None))
    e.set_nodes(src, tgt, 'Dummy')

    xtr = Data(Map(None))
    e2 = Assign(Map(None))
    e2.set_nodes(xtr, src, "assign")

    e.follow(Runtime())

    assert e.been_followed()
    e.clear_followed()
    assert repr(e) == 'Assign: [Node(Data): <id: 1, name: fred>]  --Assign-->  [Node(Data): <id: 2, name: larry>]'
    assert e.to_dict() == {'rule': 'dummy', 'source': '1:fred', 'target': '2:larry'}


def test_base_traverse():
    n = Edge(Map(None))
    try:
        n.traverse(None, None)
    except EdgeException:
        pass


def test_empty_cargo():
    e = Edge(Map(None))
    assert e.get_cargo() is None


def test_follow_no_target_type():
    e = Assign(Map(None))
    d = Data(Map(None))
    s = Static(Map(None))
    e.set_nodes(d, s, 'assign')
    try:
        e.follow(Runtime())
    except RuntimeException as r:
        assert str(r) == "Executing node: None - No tgt rule execution source module named 'nord.rules.AssignStatic' available"  # noqa: E501


def test_follow_override():
    e = Assign(Map(None))
    d = Data(Map(None))
    s = Static(Map(None))
    s.set_property('value', 12)
    e.set_nodes(s, d, 'assign')
    e.follow(Runtime())
    # this validates normal assign edge operation
    assert d.value == 12

    # now, test that we can override Pass to behave like assign
    e = Pass(Map(None))
    d1 = Data(Map(None))
    d2 = Data(Map(None))
    e.set_nodes(d1, d2, "pass")
    # Normally you can only assign from Data to Data, not Pass
    # But, with an override of tgt_rule to assign, from pass,
    # we can then follow the Pass rule
    d1.set(44)
    e.override_tgt_rule("assign")
    e.follow(Runtime())
    assert d2.value == 44

    # now, test that we can override Pass to behave like Assign
    e = Pass(Map(None))
    d = Data(Map(None))
    b = Builtin(Map(None))
    e.set_nodes(d, b, "pass")
    # Normally you can only pass from Data to Builtin, not Assign
    # As such, by overriding to in-compatible Assign,
    # An exception should be thrown
    d.set(44)
    e.override_tgt_rule("assign")
    try:
        e.follow(Runtime())
    except RuntimeException as r:
        assert str(r) == "Executing node: None - No tgt rule execution source module named 'nord.rules.AssignBuiltin' available"  # noqa: E501


def test_override_and_replace_target():
    e = Pass(Map(None))
    b = Builtin(Map(None))
    s = Static(Map(None))
    s.set_property('value', 12)
    e.set_nodes(s, b, 'pass')
    e.follow(Runtime())

    d2 = Data(Map(None))

    # Normally you can only assign from Data to Data, not Pass
    # But, with an override of tgt_rule to assign, from pass,
    # we can then follow the Pass rule
    s.set_property('value', 15)
    e.override_tgt_rule("assign")
    e.proxy_target(d2)
    e.follow(Runtime())
    assert d2.value == 15
