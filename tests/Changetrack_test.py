"""
Copyright 2020 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

from  nord.nord.changetrack import MapChange
import nord.nord.Map as Map
import nord.nord.Node as Node
import nord.nord.Edge as Edge
import nord.nord.nodes.Data as Data

test_map = {
    "map": {
        "nodes": [
            {
                "id": "1",
                "type": "data",
                "name": "node 1"
            },
            {
                "id": "2",
                "type": "data",
                "name": "node 2"
            },
        ],
        "edges": [
            {
                "source": "1:node 1",
                "target": "2:node 2",
                "rule": "assign"
            }
        ]
    }
}

def test_add_node():
    karte = Map(test_map)
    mc = karte._map_changes
    n = Node(karte)
    assert n in mc._added_nodes
    assert len(mc._added_nodes) == 1
    assert len(mc._list_of_changes) == 1
    assert mc._list_of_changes[0].change_type == 'add_node'
    assert mc._list_of_changes[0].changed_entity == n


def test_change_node():
    changes = []

    def handler(change):
        changes.extend(change)

    karte = Map(test_map)
    karte.add_change_handler(handler)

    n1 = Data(karte)
    n1.set(17)
    n1.set(18)
    n1.set(18)

    n2 = karte.nodes['1']
    n2.set(19)
    n2.set(20)
    n2.set(20)

    assert len(changes) == 4
    assert changes[0]["change_type"] == "add_node"
    assert changes[0]["item_id"] == n1.id
    assert changes[0]["entity"]["id"] == n1.id
    assert changes[0]["entity"]["type"] == "data"
    assert changes[0]["entity"]["value"] == 17

    assert changes[1]["change_type"] == "change_node"
    assert changes[1]["attribute"] == "value"
    assert changes[1]["prior"] == 17
    assert changes[1]["change"] == 18
    assert changes[1]["delattr"] == False
    assert changes[1]["item_id"] == n1.id

    assert changes[2]["change_type"] == "change_node"
    assert changes[2]["attribute"] == "value"
    assert changes[2]["prior"] == None
    assert changes[2]["change"] == 19
    assert changes[2]["delattr"] == False
    assert changes[2]["item_id"] == '1'

    assert changes[3]["change_type"] == "change_node"
    assert changes[3]["attribute"] == "value"
    assert changes[3]["prior"] == 19
    assert changes[3]["change"] == 20
    assert changes[3]["delattr"] == False
    assert changes[3]["item_id"] == '1'

def test_delete_node():
    changes = []

    def handler(change):
        changes.extend(change)

    karte = Map(test_map)
    karte.add_change_handler(handler)

    n1 = Data(karte)
    karte.set_node(n1.id, n1)
    n_del = karte.nodes["1"]

    assert changes[0]["change_type"] == 'add_node'
    assert changes[0]["item_id"] == n1.id
    assert changes[0]["entity"]["id"] == n1.id
    assert changes[0]["entity"]["name"] == n1.name
    assert changes[0]["entity"]["type"] == "data"
    assert len(changes) == 1

    karte.rem_node("1")
    assert n_del.id not in karte.nodes
    assert len(changes) == 3
    assert changes[1]["change_type"] == 'delete_edge'
    assert changes[1]["item_id"] == ('1', '2', 'assign')
    assert changes[1]["entity"]["rule"] == "assign"
    assert changes[1]["entity"]["source"] == "1:node 1"
    assert changes[1]["entity"]["target"] == "2:node 2"

    assert changes[2]["change_type"] == 'delete_node'
    assert changes[2]["item_id"] == '1'
    assert changes[2]["entity"]["id"] == "1"
    assert changes[2]["entity"]["name"] == "node 1"
    assert changes[2]["entity"]["type"] == "data"
    assert len(karte.edges) == 0


def test_add_edge():
    changes = []

    def handler(change):
        changes.extend(change)

    karte = Map(test_map)
    karte.add_change_handler(handler)

    mc = karte._map_changes
    n1 = karte.nodes["1"]
    n2 = karte.nodes["2"]
    edge = Edge(karte)
    edge.set_nodes(n2, n1, 'assign')
    karte.add_edge(n2, n1, 'assign', edge)

    assert len(changes) == 1
    assert len(karte.edges) == 2
    assert changes[0]["item_id"][0] == edge.source.id
    assert changes[0]["item_id"][1] == edge.target.id
    assert changes[0]["item_id"][2] == edge.rule

def test_delete_edge():
    changes = []

    def handler(change):
        changes.extend(change)

    karte = Map(test_map)
    karte.add_change_handler(handler)

    n1 = karte.nodes["1"]
    n2 = karte.nodes["2"]
    edge = n1.get_targets('assign')[0]
    karte.rem_node(n1.id)

    assert len(changes) == 2
    assert changes[0]["item_id"][0] == edge.source.id
    assert changes[0]["item_id"][1] == edge.target.id
    assert changes[0]["item_id"][2] == edge.rule
    assert changes[0]["change_type"] == "delete_edge"
    assert changes[0]["entity"]["rule"] == "assign"
    assert changes[0]["entity"]["source"] == "1:node 1"
    assert changes[0]["entity"]["target"] == "2:node 2"

    assert changes[1]["item_id"] == n1.id
    assert changes[1]["change_type"] == "delete_node"
    assert changes[1]["entity"]["id"] == "1"
    assert changes[1]["entity"]["name"] == "node 1"
    assert changes[1]["entity"]["type"] == "data"


def test_apply_other_map_changes():
    changes = []

    def handler(change):
        changes.extend(change)

    karte = Map(test_map)
    karte.add_change_handler(handler)
    mc = karte._map_changes
    n_del = karte.nodes["1"]
    n2 = karte.nodes["2"]
    karte.rem_node(n_del.id)
    n1 = Data(karte)
    karte.set_node(n1.id, n1)
    n1.set(17)
    n1.set(18)
    n1.set(18)

    edge = Edge(karte)
    edge.set_nodes(n2, n1, 'assign')
    karte.add_edge(n2, n1, 'assign', edge)

    n2 = karte.nodes['2']
    n2.set(19)
    n2.set(20)
    n2.set(20)

    k2 = Map(test_map)

    # need to be able to apply changes
    # changes = karte.get_changes()
    k2.apply_changes(changes)

    k2d = k2.to_dict()
    kd = karte.to_dict()
    assert k2d["map"] == kd["map"]


def test_apply_diff_changes():
    karte = Map(test_map)
    mc = karte._map_changes
    n_del = karte.nodes["1"]
    n2 = karte.nodes["2"]
    n_del.delete()
    n1 = Data(karte)
    karte.set_node(n1.id, n1)
    n1.set(17)
    n1.set(18)
    n1.set(18)

    edge = Edge(karte)
    edge.set_nodes(n2, n1, 'assign')
    karte.add_edge(n2, n1, 'assign', edge)

    n2 = karte.nodes['2']
    n2.set(19)
    n2.set(20)
    n2.set(20)

    k2 = Map(test_map)
    diff = Map.diff_maps(k2, karte)

    k2.apply_diff(diff)

    k2d = k2.to_dict()
    kd = karte.to_dict()
    assert k2d["map"] == kd["map"]
