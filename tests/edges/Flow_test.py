"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

import nord.nord.edges.Flow as Flow
import nord.nord.Map as Map


map_tree_acyclic = {
    "map": {
        "extensions": {},
        "edges": [
            {
                "source": "1:root",
                "target": "2:lb1",
                "rule": "assign"
            },
            {
                "source": "1:root",
                "target": "3:rb1",
                "rule": "assign"
            },
            {
                "source": "2:lb1",
                "target": "4:lb1-lb1",
                "rule": "assign"
            },
            {
                "source": "2:lb1",
                "target": "5:lb1-rb1",
                "rule": "assign"
            }
        ],
        "nodes": [
            {
                "id": '1',
                "name": "root",
                "position": [0.0, 0.0, 0.0],
                "type": "data"
            },
            {
                "id": '2',
                "name": "lb1",
                "position": [0.0, 0.0, 0.0],
                "type": "data"
            },
            {
                "id": '3',
                "name": "rb1",
                "position": [0.0, 0.0, 0.0],
                "type": "data"
            },
            {
                "id": '4',
                "name": "lb1-lb1",
                "position": [0.0, 0.0, 0.0],
                "type": "data"
            },
            {
                "id": '5',
                "name": "lb1-rb1",
                "position": [0.0, 0.0, 0.0],
                "type": "data"
            }
        ]
    },
    "name": "Acyclic Tree Graph"
}


def test_Flow():
    a = Flow(Map(None))
    assert repr(a) == 'Flow: [None]  --Flow-->  [None]'


def test_detect_no_cycle():
    graph = Map(map_tree_acyclic)
    a = Flow(graph)
    nodes, cycle = a.detect_cycle(graph.get_node('1'))
    assert len(nodes) == 5
    assert not cycle

    # inject a loop lower into the tree to make sure that
    # no cycle is detected, as we only want to detect
    # cycles from root
    map_tree_acyclic["map"]["edges"].append({
        "source": "4:lb1-lb1",
        "target": "5:lb1-rb1",
        "rule": "assign"
    })

    graph = Map(map_tree_acyclic)
    a = Flow(graph)
    nodes, cycle = a.detect_cycle(graph.get_node('1'), path=[])
    assert len(nodes) == 5
    assert not cycle


def test_detect_single_cycle():
    map_tree_acyclic["map"]["edges"].append({
        "source": "4:lb1-lb1",
        "target": "1:root",
        "rule": "assign"
    })

    graph = Map(map_tree_acyclic)
    a = Flow(graph)
    nodes, cycle = a.detect_cycle(graph.get_node('1'), path=[])
    assert len(nodes) == 5
    assert cycle


def test_detect_multi_cycle():
    map_tree_acyclic["map"]["edges"].append({
        "source": "4:lb1-lb1",
        "target": "1:root",
        "rule": "assign"
    })

    map_tree_acyclic["map"]["edges"].append({
        "source": "5:lb1-rb1",
        "target": "1:root",
        "rule": "assign"
    })

    graph = Map(map_tree_acyclic)
    a = Flow(graph)
    nodes, cycle = a.detect_cycle(graph.get_node('1'), path=[])
    assert len(nodes) == 5
    assert cycle
