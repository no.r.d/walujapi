"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

import nord.nord.edges.Assign as Assign
import nord.nord.nodes.Comment as Comment
import nord.nord.edges.Contains as Contains
from nord.nord import Runtime as RT
import nord.nord.utils.SpaceManager as use
from nord.nord.exceptions import RuntimeException as RuntimeException
import nord.nord.Map as Map


Runtime = RT.Runtime
use.add_location('dir://./src', 'code')


def test_Contains():
    a = Contains(Map(None))
    assert repr(a) == 'Contains: [None]  --Contains-->  [None]'


def test_follow_exception():
    a = Assign(Map(None))
    rt = Runtime()

    a.set_nodes(Comment(Map(None)), Comment(Map(None)), 'assign')
    try:
        a.follow(rt)
        assert "This Code should not be reachable" == 'a'
    except RuntimeException as e:
        assert str(e) == "Executing node: None - No src rule execution source module named 'nord.rules.CommentAssign' available"  # noqa: E501
