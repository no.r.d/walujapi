"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

import nord.nord.edges.Docs as Docs
from nord.nord import Runtime as RT
import nord.nord.Map as Map
Runtime = RT.Runtime
import nord.nord.nodes.Comment as Comment


def test_CommentDocs():
    Na = Comment(Map(None))
    Na.id = 1
    Nb = Comment(Map(None))
    Nb.id = 2
    rt = Runtime()
    e = Docs(Map(None))
    e.set_nodes(Na, Nb, 'Commenting')
    e.follow(rt)

    assert repr(e) == "Docs: [Node(Comment): <id: 1>]  --Docs-->  [Node(Comment): <id: 2>]**"
