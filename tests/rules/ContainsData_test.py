"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

import nord.nord.rules.ContainsData as ContainsData
import nord.nord.edges.Contains as Contains
from nord.nord import Runtime as RT
Runtime = RT.Runtime
import nord.nord.nodes.Data as Data
import nord.nord.Map as Map

"""

    This test seems to be invalid as this does not appear to
    actually run the edge. There probably needs tobe some kind
    of runtime error thrown or something as
    there is no rule DataContains an the Node Data should not be
    able to contain anything

"""

def test_ContainsData():
    Na = Data(Map(None))
    Na.name = 'data1'
    Na.id = 1
    Nb = Data(Map(None))
    Nb.name = 'data2'
    Nb.id = 2
    rt = Runtime()
    e = Contains(Map(None))
    e.set_nodes(Na, Nb, 'Contains')
    e.follow(rt)

    assert repr(e) == "Contains: [Node(Data): <id: 1, name: data1>]  --Contains-->  [Node(Data): <id: 2, name: data2>]**"
