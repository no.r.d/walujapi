"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

import nord.nord.rules.ContainsStatic as ContainsStatic
import nord.nord.edges.Contains as Contains
from nord.nord import Runtime as RT
import nord.nord.Map as Map
Runtime = RT.Runtime
import nord.nord.nodes.Static as Static


"""

    This test seems to be invalid as this does not appear to
    actually run the edge. There probably needs tobe some kind
    of runtime error thrown or something as
    there is no rule StaticContains an the Node Static should not be
    able to contain anything

"""

def test_ContainsStatic():
    Na = Static(Map(None))
    Na.name = 'static1'
    Na.id = 1
    Nb = Static(Map(None))
    Nb.name = 'static2'
    Nb.id = 2
    rt = Runtime()
    e = Contains(Map(None))
    e.set_nodes(Na, Nb, 'Contains')
    e.follow(rt)

    assert repr(e) == "Contains: [Node(Static): <id: 1, name: static1>]  --Contains-->  [Node(Static): <id: 2, name: static2>]**"
