"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

import nord.nord.nodes.Static as Static
import nord.nord.nodes.Data as Data
from nord.nord import Runtime as RT
import nord.nord.edges.Assign as Assign
import nord.nord.Rule as Rule
import pytest
import nord.nord.Map as Map
Runtime = RT.Runtime


def test_AssignData():
    Na = Data(Map(None))
    Na.set("Larry")
    Nb = Data(Map(None))
    rt = Runtime()
    e = Assign(Map(None))
    e.set_nodes(Na, Nb, "RuleName")
    e.follow(rt)
    assert Nb.get() == 'Larry'


@pytest.mark.skip(reason="This test appears to have an invalid premise. ... ")
def test_AssignData_no_set():

    # the code below represents some of the attempts to
    # cover
    # class AssignStatic(Rule):
    #     pass

    # __builtins__['AssignStatic'] = type('AssignStatic', (Rule,), {})

    #sys.modules['AssignStatic'] = type('AssignStatic', (Rule,), {})

    #assert str(globals()) == ''

    Na = Data(Map(None))
    Na.set("Larry")
    Nb = Static(Map(None))

    rt = Runtime()
    e = Assign(Map(None))
    e.set_nodes(Na, Nb, "RuleName")
    e.follow(rt)
    assert Nb.get() == 'Larry'
