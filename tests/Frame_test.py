"""
Copyright 2020 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""
import nord.nord.Node as Node
import nord.nord.Frame as Frame
import nord.nord.RunState as RS
import nord.nord.Runtime as RT
import nord.nord.Map as Map


def test_inject_node():
    """
    First, Test the implementation of the function.
    In inject_nodes' case, make sure the node is
    injected, and it's state update. Make sure, an injected
    node cannot be injected again.

    The second scenario is to validate that node's
    are ordered as expected. Inject Node is intended
    to put the most recently injected nodes at the
    end of the queue to be run sooner. (as nodes are popped by
    the frame when getting the next node to run)
    """
    node = Node(Map(None))
    frame = Frame()
    frame.inject_node(node, 'DOWN')
    assert len(frame.next_nodes) == 1
    assert RS.s(node).pending_run() is True

    frame.inject_node(node, 'DOWN')
    assert len(frame.next_nodes) == 1

    RS.s(node).set_preempted()
    assert RS.s(node).preempted() is True
    frame.inject_node(node, 'DOWN')
    assert len(frame.next_nodes) == 1
    assert RS.s(node).preempted() is False

    n1 = Node(Map(None))
    n1.id = 'FIRST'
    n2 = Node(Map(None))
    n2.id = 'SECOND'
    n3 = Node(Map(None))
    n3.id = 'THIRD'

    frame = Frame()
    frame.inject_node(n3, 'DOWN')
    frame.inject_node(n2, 'DOWN')
    frame.inject_node(n1, 'DOWN')
    assert len(frame.next_nodes) == 3
    assert frame.next_nodes.pop()[0].id == 'FIRST'
    assert frame.next_nodes.pop()[0].id == 'SECOND'
    assert frame.next_nodes.pop()[0].id == 'THIRD'


def test_append_node():
    """
    First, Test the implementation of the function.
    In append_nodes' case, make sure the node is
    appended, and it's state updated. Make sure, an appended
    node cannot be appended or injected again.

    The second scenario is to validate that node's
    are ordered as expected. Append Node is intended
    to put the most recently appended nodes at the
    beginning of the queue to be run later. (as nodes are popped by
    the frame when getting the next node to run)
    """
    node = Node(Map(None))
    frame = Frame()
    frame.append_node(node, 'DOWN')
    assert len(frame.next_nodes) == 1
    assert RS.s(node).pending_run() is True

    frame.append_node(node, 'DOWN')
    assert len(frame.next_nodes) == 1

    frame.inject_node(node, 'DOWN')
    assert len(frame.next_nodes) == 1

    RS.s(node).set_preempted()
    assert RS.s(node).preempted() is True
    frame.append_node(node, 'DOWN')
    assert len(frame.next_nodes) == 1
    assert RS.s(node).preempted() is False

    n1 = Node(Map(None))
    n1.id = 'FIRST'
    n2 = Node(Map(None))
    n2.id = 'SECOND'
    n3 = Node(Map(None))
    n3.id = 'THIRD'

    frame = Frame()
    frame.append_node(n1, 'DOWN')
    frame.append_node(n2, 'DOWN')
    frame.append_node(n3, 'DOWN')
    assert len(frame.next_nodes) == 3
    assert frame.next_nodes.pop()[0].id == 'FIRST'
    assert frame.next_nodes.pop()[0].id == 'SECOND'
    assert frame.next_nodes.pop()[0].id == 'THIRD'


def test_set_next():
    frame = Frame()
    node = Node(Map(None))

    assert frame.current is None
    assert len(frame.next_nodes) == 0

    RS.s(node).set_deferred()
    # set a current node
    frame.replace_current(node)
    assert frame.current is node
    frame.set_next()
    # nothing should have happened
    # set_next's purpose is to manipulate current and next_nodes
    assert len(frame.next_nodes) == 0
    assert frame.current is node

    RS.s(node).unset_deferred()
    frame.set_next()
    # Now, current should be None as there are no nodes
    # in next_nodes to grab.
    assert frame.current is None
    assert len(frame.next_nodes) == 0

    n1 = Node(Map(None))
    n1.id = 'FIRST'
    n2 = Node(Map(None))
    n2.id = 'SECOND'
    n3 = Node(Map(None))
    n3.id = 'THIRD'

    frame = Frame()
    frame.append_node(n1, 'DOWN')
    frame.append_node(n2, 'DOWN')
    frame.append_node(n3, 'DOWN')

    assert frame.next_nodes[0][0].id == 'THIRD'  # Last to run
    assert frame.next_nodes[1][0].id == 'SECOND'
    assert frame.next_nodes[2][0].id == 'FIRST'

    assert len(frame.next_nodes) == 3

    frame.set_next()
    assert frame.next_nodes[0][0].id == 'THIRD'
    assert frame.next_nodes[1][0].id == 'SECOND'
    assert len(frame.next_nodes) == 2
    assert frame.current is n1

    frame.set_next()
    assert frame.next_nodes[0][0].id == 'FIRST'
    assert frame.next_nodes[1][0].id == 'THIRD'
    assert len(frame.next_nodes) == 2
    assert frame.current is n2

    frame.set_next()
    assert frame.next_nodes[0][0].id == 'SECOND'
    assert frame.next_nodes[1][0].id == 'FIRST'
    assert len(frame.next_nodes) == 2
    assert frame.current is n3

    frame.set_next()
    assert frame.next_nodes[0][0].id == 'THIRD'
    assert frame.next_nodes[1][0].id == 'SECOND'
    assert len(frame.next_nodes) == 2
    assert frame.current is n1

    vessal = RT.Runtime()
    n1.flag_exe(vessal)
    frame.set_next()
    assert frame.next_nodes[0][0].id == 'THIRD'
    assert len(frame.next_nodes) == 1
    assert frame.current is n2

    n2.flag_exe(vessal)
    frame.set_next()
    assert len(frame.next_nodes) == 0
    assert frame.current is n3

    n3.flag_exe(vessal)
    frame.set_next()
    assert len(frame.next_nodes) == 0
    assert frame.current is None


def test_replace_current():
    frame = Frame()
    n1 = Node(Map(None))
    n2 = Node(Map(None))

    assert frame.current is None
    assert frame.looking == 'DOWN'

    frame.replace_current(n1, 'UP')
    assert frame.current is n1
    assert frame.looking == 'UP'

    frame.replace_current(n2, 'DOWN')
    assert frame.current is n2
    assert frame.looking == 'DOWN'


def test_has_started():
    frame = Frame()
    node = Node(Map(None))
    assert frame.has_started() is False
    frame.inject_node(node, 'UP')
    frame.set_next()
    assert frame.has_started() is True
    frame.set_next()
    assert frame.has_started() is False


def test_set_deferred():
    node = Node(Map(None))
    assert RS.s(node).deferred() is False
    frame = Frame()
    frame.inject_node(node, 'UP')
    frame.set_next()
    frame.set_deferred()
    assert RS.s(node).deferred() is True


def test_set_return():
    frame = Frame()
    assert frame.ret_val is None
    frame.set_return("RETURN VALUE")
    assert frame.ret_val == "RETURN VALUE"


def test_get_return():
    frame = Frame()
    assert frame.ret_val is None
    frame.set_return("RETURN VALUE")
    assert frame.ret_val == "RETURN VALUE"
    assert frame.get_return() == "RETURN VALUE"


def test_run_node():
    """
    Execute the node, finding one to execute if current
    is not set yet. Return, signalling the caller,
    as dictated by the state of the executed node.

    The test cases in this test appear pretty useless at this point
    (post RuntimeStateMachine implementation....) Not sure how
    meaningful this test is any more....
    """
    # First test without current being establised
    runtime = RT.Runtime()
    runtime.initialize(Map(None))
    frame = Frame()
    assert frame.run_node(runtime) == (False, 0)

    # Then, test when the node is preempted
    # Generally by having un-executed sources
    node = Node(Map(None))
    frame = Frame()

    def gen_prep_to_execute(n):
        def prep_to_execute(runtime, verbose):
            RS.s(n).set_preempted()
        return prep_to_execute
    node.prep_to_execute = gen_prep_to_execute(node)

    frame.inject_node(node, 'UP')
    assert RS.s(node).pending_run() is True
    RS.s(node).set_preempted()
    assert frame.run_node(runtime) == (False, 1)
    assert node.has_executed() is False
    # assert RS.s(node).pending_run() is True

    # Now, see what happens when the node
    # is deferred, in the prep or execute phase
    # If the node sets itself as deferred, it
    # could have put a new frame on the stack
    # Deferral is really just deferring the
    # processing of the outbound edges
    # until something else has happened
    node = Node(Map(None))
    frame = Frame()

    def gen_prep_to_execute(n):
        def prep_to_execute(runtime, verbose):
            RS.s(n).set_deferred()
        return prep_to_execute
    node.prep_to_execute = gen_prep_to_execute(node)

    frame.inject_node(node, 'UP')
    assert frame.run_node(runtime) == (False, 1)
    assert node.has_executed() is False
    assert RS.s(node).pending_run() is False
    assert RS.s(node).deferred() is False

    # in this case, the node is deferred
    # prior to being run. Which means this is
    # a subsequent pass at this node. In which case,
    # it should pick up where it left off, and
    # pretty much, just run it's next_after_execute
    # method (if it is not blocked, which is not tested
    # here.

    # Much of this functionality is outside of Frame now, and covered by
    # the Runtime State Machine, so this test is likely obsolete...
    node = Node(Map(None))
    frame = Frame()
    assert RS.s(node).deferred() is False
    RS.s(node).set_deferred()
    assert RS.s(node).deferred() is True
    frame.inject_node(node, 'DOWN')
    assert frame.run_node(runtime) == (False, 1)
    assert RS.s(node).deferred() is True
    assert frame.run_node(runtime) == (False, 1)
    assert RS.s(node).deferred() is True
    assert RS.s(node).pending_run() is False


def test_done():
    """
    done() evaluates if the frame would
    have any work to do if set_next were called.
    It does not check (presently) if the current
    node has executed or not. Only, if it has
    anywhere else it might go.
    """
    frame = Frame()
    assert frame.done() is True
    node = Node(Map(None))
    frame.inject_node(node, 'UP')
    frame.set_next()
    assert frame.done() is True

    n1 = Node(Map(None))
    n2 = Node(Map(None))
    frame.append_node(n1, 'DOWN')
    frame.append_node(n2, 'UP')
    frame.append_node(node, 'UP')
    frame.set_next()
    assert frame.done() is False


def test_is_blocked():
    node = Node(Map(None))
    assert RS.s(node).blocked() is False
    frame = Frame()
    assert frame.is_blocked() is False
    frame.inject_node(node, 'UP')
    frame.set_next()
    assert RS.s(node).blocked() is False
    assert frame.is_blocked() is False
    RS.s(node).set_blocked()
    assert frame.is_blocked() is True
