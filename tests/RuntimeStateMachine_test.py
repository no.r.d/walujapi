"""
Copyright 2022 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""
import nord.nord.Node as Node
import nord.nord.RuntimeStateMachine as RSM
import nord.nord.Runtime as RT
import nord.nord.Map as Map
Runtime = RT.Runtime


def test_blocked():
    node = Node(None)
    vessal = Runtime()
    vessal.setNextNode(node, 'DOWN')
    vessal.current.set_next()
    node.flag_blocking()
    sched_state = RSM.eval(vessal, node)
    assert sched_state == RSM.BLOCKED

    node.clear_blocking()
    sched_state = RSM.eval(vessal, node)
    assert sched_state == RSM.DONE

    node.execute = lambda runtime: node.flag_blocking()
    del node._run_state
    sched_state = RSM.eval(vessal, node)
    assert sched_state == RSM.BLOCKED
    assert node.is_blocked()


def test_preempted():
    graph = {
        "map": {
            "edges": [
                {
                    "rule": "assign",
                    "source": "2:VAL_TO_DATA",
                    "target": "1:TEST BLOCKING"
                }
            ],
            "nodes": [
                {
                    "id": "1",
                    "name": "TEST BLOCKING",
                    "type": "data"
                },
                {
                    "id": "2",
                    "name": "VAL_TO_DATA",
                    "type": "static",
                    "value": "MORE CONTENT"
                }
            ]
        }
    }

    """
    Given a graph with two nodes, and the second node
    being the source of the first node.
    """

    karte = Map(graph, verbose=True)
    vessal = Runtime()
    vessal.initialize(karte)

    """
    Begin execution from the first node, so we
    can confirm that the state machine pre-empts
    this node, and schedules the source node (node2)
    for execution
    """
    node2 = karte.get_node("2")
    node1 = karte.get_node("1")
    vessal.setNextNode(node1, 'DOWN')
    vessal.current.set_next()
    # node.flag_preempted()
    sched_state = RSM.eval(vessal, node1)
    assert sched_state == RSM.SCHEDULE_NEXT
    assert node1.is_preempted()

    """
    Then, get the next scheduled node, and
    run it. It should be the second node, which is
    the source to the first node.
    """
    vessal.current.set_next()
    sched_state = RSM.eval(vessal, vessal.get_current_node())
    assert sched_state == RSM.SCHEDULE_NEXT
    assert node2.has_executed()
    assert vessal.anomaly is None

    """
    This should leave us in the position of having the
    first node, in the schedule, so we fetch that, put it
    through the state machine and confirm that it
    has executed, and that the map is Done
    """
    vessal.current.set_next()
    sched_state = RSM.eval(vessal, vessal.get_current_node())
    assert sched_state == RSM.DONE
    assert node1.has_executed()
    assert vessal.anomaly is None


def test_event_not_for_me():
    graph = {
        "map": {
            "edges": [
                {
                    "rule": "assign",
                    "source": "2:VAL_TO_DATA",
                    "target": "1:TEST BLOCKING"
                },
                {
                    "rule": "flow",
                    "source": "3:EVENTING_1",
                    "target": "2:VAL_TO_DATA"
                },
                {
                    "rule": "flow",
                    "source": "4:EVENTING_2",
                    "target": "1:TEST BLOCKING"
                },
                {
                    "rule": "assign",
                    "source": "5:SOURCELESS",
                    "target": "1:TEST BLOCKING"
                }
            ],
            "nodes": [
                {
                    "id": "1",
                    "name": "TEST BLOCKING",
                    "type": "data"
                },
                {
                    "id": "2",
                    "name": "VAL_TO_DATA",
                    "type": "static",
                    "value": "MORE CONTENT"
                },
                {
                    "id": "3",
                    "name": "EVENTING_1",
                    "type": "listener",
                    "space": "athena"
                },
                {
                    "id": "4",
                    "name": "EVENTING_2",
                    "type": "listener",
                    "space": "athena"
                },
                {
                    "id": "5",
                    "name": "SOURCELESS",
                    "type": "static",
                    "value": "INITIAL CONTENT"
                }
            ]
        }
    }

    """
    Given a graph with three non-eventingnodes, and the second node
    being the source of the first node and each of these having an eventing source.
    Then, there are eventing nodes pointed to each node.
    """

    karte = Map(graph, verbose=True)

    node2 = karte.get_node("2")
    node1 = karte.get_node("1")
    ev1 = karte.get_node("3")
    ev2 = karte.get_node("4")
    node3 = karte.get_node("5")

    vessal = Runtime()
    vessal.initialize(karte)
    assert node1.value is None

    vessal.begin(start=node3)

    assert node1.value == "INITIAL CONTENT"
    assert vessal._exec_count == 1
    assert node1._run_state._executed == 0
    assert node2._run_state._executed == 0
    assert node3._run_state._executed == 0
    assert ev1._run_state._executed == 0
    assert ev2._run_state._executed == 0
    assert vessal.anomaly is None

    event = {
        "node_id": "3",
        "type": "on_click"
    }

    """
    Test in event, against a node that cannot handle events
    Which, in this case has a source that is not executed

    So, first we need to make node2 an event handler.
    Then, we need to set the node one's upstream to 2
    """
    vessal.add_in_event(event)
    vessal.next_from_in_events()

    assert node1.value == "INITIAL CONTENT"
    assert vessal._exec_count == 1
    assert node1._run_state._executed == 0
    assert node2._run_state._executed == 0
    assert node3._run_state._executed == 0
    assert ev1._run_state._executed == 1
    assert ev2._run_state._executed == 0

    vessal.continue_exec()

    assert node1.value == "MORE CONTENT"
    assert vessal._exec_count == 2
    assert node1._run_state._executed == 1
    assert node2._run_state._executed == 1
    assert node3._run_state._executed == 0
    assert ev1._run_state._executed == 1
    assert ev2._run_state._executed == 0
    assert vessal.anomaly is None

    sched_state = RSM.eval(vessal, node1)
    assert sched_state == RSM.DONE
    assert node1.is_preempted()
    assert vessal.anomaly is None


def test_event_for_me():
    graph = {
        "map": {
            "edges": [
                {
                    "rule": "flow",
                    "source": "2:VAL_TO_DATA",
                    "target": "1:TEST BLOCKING"
                },
                {
                    "rule": "assign",
                    "source": "3:STATIC_CONTENT",
                    "target": "1:TEST BLOCKING"
                }
            ],
            "nodes": [
                {
                    "id": "1",
                    "name": "TEST BLOCKING",
                    "type": "data"
                },
                {
                    "id": "2",
                    "name": "VAL_TO_DATA",
                    "type": "listener",
                    "space": "athena"
                },
                {
                    "id": "3",
                    "name": "STATIC_CONTENT",
                    "type": "static",
                    "value": "SOME STUFF"
                }
            ]
        }
    }

    """
    Given a graph with three nodes, and the second node
    being the eventing source of the first node, and the third
    being the data provider to prevent blocking in the first node.
    """

    karte = Map(graph, verbose=True)
    vessal = Runtime()
    vessal.initialize(karte)

    node2 = karte.get_node("2")
    node1 = karte.get_node("1")
    node3 = karte.get_node("3")
    vessal.begin(start=node1)

    event = {
        "node_id": "2",
        "type": "on_click"
    }

    """
    Test in event, against a node that cannot handle events
    Which, in this case has a source that is not executed
    """

    assert vessal._exec_count == 1
    assert node1._run_state._executed == 0
    assert node2._run_state._executed == 0
    assert node3._run_state._executed == 0
    assert vessal.anomaly is None

    # vessal.add_in_event(event)
    # sched_state = RSM.eval(vessal, node2)
    # assert sched_state == RSM.DONE
    # assert node1.is_preempted()

    vessal.add_in_event(event)
    vessal.next_from_in_events()
    vessal.continue_exec()

    assert vessal._exec_count == 2
    assert node1._run_state._executed == 1
    assert node2._run_state._executed == 1
    assert vessal.anomaly is None


def test_event_no_continue_flag():
    graph = {
        "map": {
            "edges": [
                {
                    "rule": "flow",
                    "source": "2:VAL_TO_DATA",
                    "target": "1:TEST BLOCKING"
                },
                {
                    "rule": "assign",
                    "source": "3:STATIC_CONTENT",
                    "target": "1:TEST BLOCKING"
                }
            ],
            "nodes": [
                {
                    "id": "1",
                    "name": "TEST BLOCKING",
                    "type": "data"
                },
                {
                    "id": "2",
                    "name": "VAL_TO_DATA",
                    "type": "listener",
                    "space": "athena"
                },
                {
                    "id": "3",
                    "name": "STATIC_CONTENT",
                    "type": "static",
                    "value": "SOME STUFF"
                }
            ]
        }
    }

    """
    Given a graph with three nodes, and the second node
    being the eventing source of the first node, and the third
    being the data provider to prevent blocking in the first node.
    """

    karte = Map(graph, verbose=True)
    vessal = Runtime()
    vessal.initialize(karte)

    node2 = karte.get_node("2")
    node1 = karte.get_node("1")

    event = {
        "node_id": "2",
        "type": "on_click"
    }

    """
    Test in event, against a node that cannot handle events
    Which, in this case has a source that is not executed
    """
    vessal.add_in_event(event)
    vessal.next_from_in_events()
    sched_state = RSM.eval(vessal, node1)
    assert sched_state == RSM.SCHEDULE_NEXT

    """
    Test in event, against a node that can handle events,
    and is configured to continue execution. This
    should exercise backpropogation
    """
    vessal.initialize(karte)
    node2.handles_event = lambda event: 1
    node2.set_cfg_value('continue_exec_wo_event', False)
    # node1.handles_event = lambda event: 1
    del node1._run_state
    del node2._run_state
    event["nodeid"] = "3"
    vessal.add_in_event(event)
    vessal.next_from_in_events()
    sched_state = RSM.eval(vessal, node1)
    assert sched_state == RSM.SCHEDULE_NEXT
    # assert node1.is_preempted()

    vessal.current.set_next()
    node = vessal.get_current_node()
    sched_state = RSM.eval(vessal, node)
    assert sched_state == RSM.DONE
    assert node2.has_executed()
