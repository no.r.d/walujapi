"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

from pathlib import Path
import sys
# Path manipulation needed to allow vscode test
# UI to work from NoRD directory, rather than
# walujapi....
sys.path.append(str(Path().cwd()) + '/walujapi/src/')

from nord.nord import Runtime as RT  # noqa: E402
import json  # noqa: E402
import nord.nord.Map as Map  # noqa: E402
Runtime = RT.Runtime


def test_builtin_length():
    try:
        m = json.load(open("tests/maps/test_builtin_len.n", "r"))
    except FileNotFoundError:
        m = json.load(open("walujapi/tests/maps/test_builtin_len.n", "r"))

    karte = Map(m, verbose=True)
    vessal = Runtime()
    karte.run(vessal, None)

    resnode = karte.nodes['419185c1-d855-4457-9d58-11c82ca3174f']
    assert resnode.value == 3


def test_builtin_newnode():
    try:
        m = json.load(open("tests/maps/test_newnode_builtin.n", "r"))
    except FileNotFoundError:
        m = json.load(open("walujapi/tests/maps/test_newnode_builtin.n", "r"))

    karte = Map(m, verbose=True)
    vessal = Runtime()

    assert len(karte.nodes) == 3

    karte.run(vessal, None)

    assert len(karte.nodes) == 4

    newnode = None
    for k in karte.nodes:
        n = karte.nodes[k]
        if n.type == 'data' and n.name == 'NAMEOFNODE':
            newnode = n
    
    assert newnode is not None
    assert newnode.position == [-5.192852973937988, 0.20699691772460938, 2.2553625106811523]