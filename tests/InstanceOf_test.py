"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

import nord.nord.Map as Map
import nord.nord.utils.UseUtil
from nord.nord import Runtime as RT
import json
import pytest
Runtime = RT.Runtime
use = nord.nord.utils.UseUtil.Use()


# @pytest.mark.skip(reason="INCOMPLETE TEST")
def test_data_instancing():
    try:
        data = json.load(open("tests/maps/test_instancing.n", "r"))
    except FileNotFoundError:
        data = json.load(open("walujapi/tests/maps/test_instancing.n", "r"))
    m = Map(data)
    assert m.nodes['2d8580a4-f7e9-4c25-90b7-68b70e939ed6'].value is None
    m.run(Runtime(), [])
    assert m.nodes['2d8580a4-f7e9-4c25-90b7-68b70e939ed6'].value == '456'


def test_instance_math():
    try:
        m = json.load(open("tests/maps/test_instanceof.n", "r"))
    except FileNotFoundError:
        m = json.load(open("walujapi/tests/maps/test_instanceof.n", "r"))
    karte = Map(m)
    vessal = Runtime()

    orig = karte.get_node('053c9dd9-0eab-4dfc-8402-322acc823466')
    assert orig.value is None
    vessal.initialize(karte, verbose=True)
    vessal.begin()
    vessal.continue_exec()
    assert vessal.anomaly is None

    assert orig.value == 16
