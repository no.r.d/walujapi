{
    "extensions": {
        "athena": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/athena/src/nord/athena",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/athena/src/nord/athena",
                    "pkg://"
                ]
            }
        },
        "enrique": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/enrique/src/nord/enrique",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/enrique/src/nord/enrique",
                    "pkg://"
                ]
            }
        },
        "fregata": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir://../walujapi/tests/maps",
                    "dir:///Users/nate/Desktop/Dev/NoRD/fregata/src/nord/fregata",
                    "pkg://"
                ],
                "path": [
                    "dir://../walujapi/tests/maps",
                    "dir:///Users/nate/Desktop/Dev/NoRD/fregata/src/nord/fregata",
                    "pkg://"
                ]
            }
        },
        "iteru": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/iteru/src/nord/iteru",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/iteru/src/nord/iteru",
                    "pkg://"
                ]
            }
        },
        "jabir": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/jabir/src/nord/jabir",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/jabir/src/nord/jabir",
                    "pkg://"
                ]
            }
        },
        "kamal": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/kamal/src/nord/kamal",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/kamal/src/nord/kamal",
                    "pkg://"
                ]
            }
        },
        "pasion": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/pasion/src/nord/pasion",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/pasion/src/nord/pasion",
                    "pkg://"
                ]
            }
        },
        "shoshoni": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/shoshoni/src/nord/shoshoni",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/shoshoni/src/nord/shoshoni",
                    "pkg://"
                ]
            }
        },
        "sushrut": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/sushrut/src/nord/sushrut",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/sushrut/src/nord/sushrut",
                    "pkg://"
                ]
            }
        },
        "wrigan": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/wrigan/src/nord/wrigan",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/wrigan/src/nord/wrigan",
                    "pkg://"
                ]
            }
        },
        "xuyue": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/xuyue/src/nord/xuyue",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/xuyue/src/nord/xuyue",
                    "pkg://"
                ]
            }
        }
    },
    "map": {
        "edges": [
            {
                "from_space": "sushrut",
                "rule": "pass",
                "source": "a2b0e28d-880c-445d-82fc-7bd2fe8867b0:NAMEOFNODE",
                "space": "nord",
                "target": "f3e6eef4-0c4e-4d41-9d1c-97a05c0e8d8b:Add New Node to map",
                "to_space": "nord"
            },
            {
                "from_space": "athena",
                "rule": "pass",
                "source": "b3a7a759-7392-46f2-8f1b-8176062003c5:Location For new node",
                "space": "nord",
                "target": "f3e6eef4-0c4e-4d41-9d1c-97a05c0e8d8b:Add New Node to map",
                "to_space": "nord"
            }
        ],
        "nodes": [
            {
                "id": "a2b0e28d-880c-445d-82fc-7bd2fe8867b0",
                "name": "NAMEOFNODE",
                "position": [
                    2.1346945762634277,
                    -7.542499542236328,
                    4.166780471801758
                ],
                "space": "sushrut",
                "type": "staticstr",
                "value": "NAMEOFNODE"
            },
            {
                "id": "b3a7a759-7392-46f2-8f1b-8176062003c5",
                "name": "Location For new node",
                "position": [
                    -5.192852973937988,
                    0.20699691772460938,
                    2.2553625106811523
                ],
                "space": "athena",
                "type": "location"
            },
            {
                "id": "f3e6eef4-0c4e-4d41-9d1c-97a05c0e8d8b",
                "method": "newnode",
                "name": "Add New Node to map",
                "position": [
                    7.121370315551758,
                    1.0268058776855469,
                    -0.9547024965286255
                ],
                "space": "nord",
                "type": "builtin"
            }
        ]
    },
    "name": "Hangry Bifurcated Cheese"
}