{
    "extensions": {
        "athena": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/athena/src/nord/athena",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/athena/src/nord/athena",
                    "pkg://"
                ]
            }
        },
        "enrique": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/enrique/src/nord/enrique",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/enrique/src/nord/enrique",
                    "pkg://"
                ]
            }
        },
        "fregata": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir://tests/maps",
                    "dir:///Users/nate/Desktop/Dev/NoRD/fregata/src/nord/fregata",
                    "pkg://"
                ],
                "path": [
                    "dir://tests/maps",
                    "dir:///Users/nate/Desktop/Dev/NoRD/fregata/src/nord/fregata",
                    "pkg://"
                ]
            }
        },
        "iteru": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/iteru/src/nord/iteru",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/iteru/src/nord/iteru",
                    "pkg://"
                ]
            }
        },
        "jabir": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/jabir/src/nord/jabir",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/jabir/src/nord/jabir",
                    "pkg://"
                ]
            }
        },
        "kamal": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/kamal/src/nord/kamal",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/kamal/src/nord/kamal",
                    "pkg://"
                ]
            }
        },
        "pasion": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/pasion/src/nord/pasion",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/pasion/src/nord/pasion",
                    "pkg://"
                ]
            }
        },
        "shoshoni": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/shoshoni/src/nord/shoshoni",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/shoshoni/src/nord/shoshoni",
                    "pkg://"
                ]
            }
        },
        "sushrut": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/sushrut/src/nord/sushrut",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/sushrut/src/nord/sushrut",
                    "pkg://"
                ]
            }
        },
        "wrigan": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/wrigan/src/nord/wrigan",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/wrigan/src/nord/wrigan",
                    "pkg://"
                ]
            }
        },
        "xuyue": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/xuyue/src/nord/xuyue",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/xuyue/src/nord/xuyue",
                    "pkg://"
                ]
            }
        }
    },
    "map": {
        "edges": [
            {
                "position": 0,
                "rule": "pass",
                "source": "0ffb71e8-ecdf-4e3a-9411-920bf00152d4:Cool Oderous Siege",
                "target": "defbd811-1213-4f9b-9bc5-7dd650f846ff:Builtin: Aloof Chipped Siege"
            },
            {
                "rule": "use",
                "source": "37bc0554-6892-4b2d-9adb-e997fb4d3993:Graph: Super Sublimated Cave",
                "target": "5735e3a9-076a-4735-aa7f-a31c0a32fe8d:Container: Light Onerous Table"
            },
            {
                "rule": "contains",
                "source": "5735e3a9-076a-4735-aa7f-a31c0a32fe8d:Container: Light Onerous Table",
                "target": "0ffb71e8-ecdf-4e3a-9411-920bf00152d4:Cool Oderous Siege"
            },
            {
                "rule": "contains",
                "source": "5735e3a9-076a-4735-aa7f-a31c0a32fe8d:Container: Light Onerous Table",
                "target": "19419c10-2771-41bc-b4f5-de34c548f912:Cuddly Scientific Laser"
            },
            {
                "rule": "contains",
                "source": "5735e3a9-076a-4735-aa7f-a31c0a32fe8d:Container: Light Onerous Table",
                "target": "defbd811-1213-4f9b-9bc5-7dd650f846ff:Builtin: Aloof Chipped Siege"
            },
            {
                "rule": "flow",
                "source": "defbd811-1213-4f9b-9bc5-7dd650f846ff:Builtin: Aloof Chipped Siege",
                "target": "19419c10-2771-41bc-b4f5-de34c548f912:Cuddly Scientific Laser"
            }
        ],
        "nodes": [
            {
                "id": "0ffb71e8-ecdf-4e3a-9411-920bf00152d4",
                "itemid": "1644c7b9-783a-4729-a232-b8f9b44f22dd",
                "name": "Cool Oderous Siege",
                "parentid": "37bc0554-6892-4b2d-9adb-e997fb4d3993",
                "position": [
                    -8.645009994506836,
                    0.0,
                    2.3109662532806396
                ],
                "type": "reference"
            },
            {
                "id": "19419c10-2771-41bc-b4f5-de34c548f912",
                "itemid": "5c29aca5-6599-4f88-ac80-251c4138aaff",
                "name": "Cuddly Scientific Laser",
                "parentid": "37bc0554-6892-4b2d-9adb-e997fb4d3993",
                "position": [
                    7.754087924957275,
                    0.3971405029296875,
                    3.2734317779541016
                ],
                "type": "reference"
            },
            {
                "id": "37bc0554-6892-4b2d-9adb-e997fb4d3993",
                "mapfile": "tests/maps/importme2.n",
                "name": "Graph: Super Sublimated Cave",
                "position": [
                    11.762126922607422,
                    0.0,
                    -0.6525980830192566
                ],
                "type": "graph"
            },
            {
                "id": "5735e3a9-076a-4735-aa7f-a31c0a32fe8d",
                "name": "Container: Light Onerous Table",
                "position": [
                    -10.702614784240723,
                    0.0,
                    6.28797721862793
                ],
                "type": "container"
            },
            {
                "id": "defbd811-1213-4f9b-9bc5-7dd650f846ff",
                "name": "Builtin: Aloof Chipped Siege",
                "position": [
                    -11.213495254516602,
                    -1.2502784729003906,
                    -4.962981224060059
                ],
                "type": "builtin"
            }
        ]
    },
    "name": "Mega Sublimated Wave"
}