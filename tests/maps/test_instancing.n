{
    "map": {
        "edges": [
            {
                "rule": "flow",
                "source": "2d8580a4-f7e9-4c25-90b7-68b70e939ed6:Original Var",
                "target": "d4f2efd7-32f2-40c2-bd61-766d576ae969:Static Var:456"
            },
            {
                "rule": "assign",
                "source": "b085199c-6570-423e-b03e-f546f7b12501:Data: Mild Painted Cheese2",
                "target": "97760867-9f4a-405e-ad8e-3b3e1a55e838:value should == 456"
            },
            {
                "rule": "flow",
                "source": "34f108ad-13c9-4129-98ff-b7067017ae30:Data: Mild Painted Cheese",
                "target": "b085199c-6570-423e-b03e-f546f7b12501:Data: Mild Painted Cheese2"
            },
            {
                "rule": "assign",
                "source": "c335761d-5d73-48d1-8edd-34e18b0fa54e:Static Var:123",
                "target": "2d8580a4-f7e9-4c25-90b7-68b70e939ed6:Original Var"
            },
            {
                "rule": "assign",
                "source": "d4f2efd7-32f2-40c2-bd61-766d576ae969:Static Var:456",
                "target": "34f108ad-13c9-4129-98ff-b7067017ae30:Data: Mild Painted Cheese"
            }
        ],
        "nodes": [
            {
                "id": "2d8580a4-f7e9-4c25-90b7-68b70e939ed6",
                "name": "Original Var",
                "position": [
                    -7.754452705383301,
                    0.16887283325195312,
                    7.306362152099609
                ],
                "type": "data"
            },
            {
                "id": "34f108ad-13c9-4129-98ff-b7067017ae30",
                "instanceof": "2d8580a4-f7e9-4c25-90b7-68b70e939ed6",
                "name": "Data: Mild Painted Cheese",
                "position": [
                    -7.941957473754883,
                    0.0954132080078125,
                    0.26100361347198486
                ],
                "type": "data"
            },
            {
                "id": "97760867-9f4a-405e-ad8e-3b3e1a55e838",
                "name": "value should == 456",
                "position": [
                    -1.1060636043548584,
                    1.2426834106445312,
                    3.4453225135803223
                ],
                "type": "data"
            },
            {
                "id": "b085199c-6570-423e-b03e-f546f7b12501",
                "instanceof": "2d8580a4-f7e9-4c25-90b7-68b70e939ed6",
                "name": "Data: Mild Painted Cheese2",
                "position": [
                    -8.366474151611328,
                    -0.26248931884765625,
                    -5.589990139007568
                ],
                "type": "data"
            },
            {
                "id": "c335761d-5d73-48d1-8edd-34e18b0fa54e",
                "name": "Static Var:123",
                "position": [
                    -14.421072006225586,
                    -2.858245849609375,
                    6.584804058074951
                ],
                "type": "static",
                "value": "123"
            },
            {
                "id": "d4f2efd7-32f2-40c2-bd61-766d576ae969",
                "name": "Static Var:456",
                "position": [
                    -14.733187675476074,
                    -0.63690185546875,
                    -0.17145821452140808
                ],
                "type": "static",
                "value": "456"
            }
        ]
    },
    "name": "Smart Flooded Cheese"
}