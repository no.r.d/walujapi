{
    "map": {
        "edges": [
            {
                "rule": "contains",
                "source": "5735e3a9-076a-4735-aa7f-a31c0a32fe8d:Container: Light Onerous Table",
                "target": "adde28f9-3a90-4ed3-bb18-427b8d222073:Prime Onerous Mountain"
            },
            {
                "rule": "contains",
                "source": "5735e3a9-076a-4735-aa7f-a31c0a32fe8d:Container: Light Onerous Table",
                "target": "ccde5680-8973-4b70-834d-2fb73b2242cc:Friendly Flooded Plate"
            },
            {
                "rule": "use",
                "source": "37bc0554-6892-4b2d-9adb-e997fb4d3993:Graph: Super Sublimated Cave",
                "target": "5735e3a9-076a-4735-aa7f-a31c0a32fe8d:Container: Light Onerous Table"
            },
            {
                "rule": "pass",
                "source": "adde28f9-3a90-4ed3-bb18-427b8d222073:Prime Onerous Mountain",
                "target": "ccde5680-8973-4b70-834d-2fb73b2242cc:Friendly Flooded Plate"
            }
        ],
        "nodes": [
            {
                "id": "37bc0554-6892-4b2d-9adb-e997fb4d3993",
                "mapfile": "tests/maps/importme.n",
                "name": "Graph: Super Sublimated Cave",
                "position": [
                    11.762126922607422,
                    0.0,
                    -0.6525980830192566
                ],
                "type": "graph"
            },
            {
                "id": "5735e3a9-076a-4735-aa7f-a31c0a32fe8d",
                "name": "Container: Light Onerous Table",
                "position": [
                    -10.702614784240723,
                    0.0,
                    6.28797721862793
                ],
                "type": "container"
            },
            {
                "id": "adde28f9-3a90-4ed3-bb18-427b8d222073",
                "itemid": "5c29aca5-6599-4f88-ac80-251c4138aaff",
                "name": "Prime Onerous Mountain",
                "parentid": "37bc0554-6892-4b2d-9adb-e997fb4d3993",
                "position": [
                    10.764762878417969,
                    -0.90704345703125,
                    1.3795174360275269
                ],
                "type": "reference"
            },
            {
                "id": "ccde5680-8973-4b70-834d-2fb73b2242cc",
                "itemid": "1644c7b9-783a-4729-a232-b8f9b44f22dd",
                "name": "Friendly Flooded Plate",
                "parentid": "37bc0554-6892-4b2d-9adb-e997fb4d3993",
                "position": [
                    -11.506158828735352,
                    -1.0881004333496094,
                    -2.095442771911621
                ],
                "type": "reference"
            }
        ]
    },
    "name": "Mega Sublimated Wave"
}
