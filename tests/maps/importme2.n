{
    "map": {
        "edges": [
            {
                "rule": "flow",
                "source": "ca5f5e29-1efd-4e41-acca-4bca49175e71:Builtin: Friendly Tuned Laser",
                "target": "58cc40bf-08ba-4d1c-aa9f-b5e4f90f0673:21b24d53-0d6f-478f-8c01-eb0f0413419c"
            },
            {
                "rule": "contains",
                "source": "5c29aca5-6599-4f88-ac80-251c4138aaff:Main",
                "target": "1dc01a5f-d883-4dd2-8e86-3ed910b1c84d:Static"
            },
            {
                "rule": "contains",
                "source": "1644c7b9-783a-4729-a232-b8f9b44f22dd:Helper",
                "target": "ca5f5e29-1efd-4e41-acca-4bca49175e71:Builtin: Friendly Tuned Laser"
            },
            {
                "rule": "contains",
                "source": "1644c7b9-783a-4729-a232-b8f9b44f22dd:Helper",
                "target": "880b498a-c7ec-418a-92d5-1347faa9ea60:dede1cbc-3086-4f8d-bc85-da993d7a19c8"
            },
            {
                "rule": "contains",
                "source": "1644c7b9-783a-4729-a232-b8f9b44f22dd:Helper",
                "target": "58cc40bf-08ba-4d1c-aa9f-b5e4f90f0673:21b24d53-0d6f-478f-8c01-eb0f0413419c"
            },
            {
                "rule": "contains",
                "source": "1644c7b9-783a-4729-a232-b8f9b44f22dd:Helper",
                "target": "ed6b79c9-4f77-4466-a8f2-bae3273563d5:Str1"
            },
            {
                "rule": "pass",
                "source": "880b498a-c7ec-418a-92d5-1347faa9ea60:dede1cbc-3086-4f8d-bc85-da993d7a19c8",
                "target": "ca5f5e29-1efd-4e41-acca-4bca49175e71:Builtin: Friendly Tuned Laser"
            },
            {
                "rule": "pass",
                "source": "880b498a-c7ec-418a-92d5-1347faa9ea60:dede1cbc-3086-4f8d-bc85-da993d7a19c8",
                "target": "58cc40bf-08ba-4d1c-aa9f-b5e4f90f0673:21b24d53-0d6f-478f-8c01-eb0f0413419c"
            },
            {
                "rule": "flow",
                "source": "ed6b79c9-4f77-4466-a8f2-bae3273563d5:Str1",
                "target": "ca5f5e29-1efd-4e41-acca-4bca49175e71:Builtin: Friendly Tuned Laser"
            },
            {
                "rule": "assign",
                "source": "ed6b79c9-4f77-4466-a8f2-bae3273563d5:Str1",
                "target": "880b498a-c7ec-418a-92d5-1347faa9ea60:dede1cbc-3086-4f8d-bc85-da993d7a19c8"
            }
        ],
        "nodes": [
            {
                "id": "1644c7b9-783a-4729-a232-b8f9b44f22dd",
                "menuicon": "data",
                "name": "Helper",
                "position": [
                    9.002832412719727,
                    -0.813507080078125,
                    -3.72348690032959
                ],
                "type": "container"
            },
            {
                "id": "1dc01a5f-d883-4dd2-8e86-3ed910b1c84d",
                "name": "Static",
                "position": [
                    -0.26402607560157776,
                    0.5720558166503906,
                    1.9336012601852417
                ],
                "type": "static",
                "value": "CheeseFace"
            },
            {
                "id": "58cc40bf-08ba-4d1c-aa9f-b5e4f90f0673",
                "name": "Return: Mild Sanded Sparrow",
                "position": [
                    13.200087547302246,
                    -0.9893875122070312,
                    -4.236971855163574
                ],
                "type": "return"
            },
            {
                "id": "5c29aca5-6599-4f88-ac80-251c4138aaff",
                "menuicon": "action",
                "name": "Main",
                "position": [
                    -6.0960373878479,
                    0.0,
                    5.458792686462402
                ],
                "type": "container"
            },
            {
                "id": "815d732e-183a-440a-b538-ab63a82e5324",
                "name": "Comment: Light Flooded Rampart",
                "position": [
                    5.0990118980407715,
                    0.9308967590332031,
                    4.090157985687256
                ],
                "type": "comment"
            },
            {
                "id": "880b498a-c7ec-418a-92d5-1347faa9ea60",
                "name": "Data: Gothic Vehicular Sound",
                "position": [
                    1.59200119972229,
                    -0.15488433837890625,
                    -4.737729549407959
                ],
                "type": "data"
            },
            {
                "id": "ca5f5e29-1efd-4e41-acca-4bca49175e71",
                "name": "Builtin: Friendly Tuned Laser",
                "position": [
                    13.466560363769531,
                    0.0,
                    3.032662868499756
                ],
                "type": "builtin"
            },
            {
                "id": "ed6b79c9-4f77-4466-a8f2-bae3273563d5",
                "name": "Str1",
                "position": [
                    -4.468379497528076,
                    0.0,
                    2.5873606204986572
                ],
                "type": "static",
                "value": "I love to eat cheese!!!"
            }
        ]
    },
    "name": "Super Sanded Pangolin"
}