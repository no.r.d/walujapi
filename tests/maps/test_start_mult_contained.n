{
    "map": {
        "edges": [
            {
                "rule": "contains",
                "source": "8806cd27-84c2-4ba1-a520-447d20d0eb82:Container: Mild Scientific Pangolin",
                "target": "7a944f52-7bd0-4f87-9c30-5acc2afdf26d:Builtin: Hangry Onerous Sparrow"
            },
            {
                "rule": "contains",
                "source": "8806cd27-84c2-4ba1-a520-447d20d0eb82:Container: Mild Scientific Pangolin",
                "target": "e04b2cfa-99af-4558-a563-733f13909b22:Builtin: Super Nordic Cloud"
            },
            {
                "rule": "contains",
                "source": "8806cd27-84c2-4ba1-a520-447d20d0eb82:Container: Mild Scientific Pangolin",
                "target": "5cdee5ce-b0a2-4dbf-9aea-07c6516699ff:Data: Extra Scientific Table"
            },
            {
                "rule": "contains",
                "source": "8806cd27-84c2-4ba1-a520-447d20d0eb82:Container: Mild Scientific Pangolin",
                "target": "28d14df7-d398-4644-a87b-8f03c75c5ed0:Data: Extra Painted Rampart"
            },
            {
                "rule": "contains",
                "source": "8806cd27-84c2-4ba1-a520-447d20d0eb82:Container: Mild Scientific Pangolin",
                "target": "b7b03f15-252d-4555-abe6-caa63dd8344d:Static: Soggy Artsy Rampart"
            },
            {
                "rule": "contains",
                "source": "8806cd27-84c2-4ba1-a520-447d20d0eb82:Container: Mild Scientific Pangolin",
                "target": "85cb0eab-83cf-4499-a6f4-603615ea5086:Static: Under Cutaneous Sound"
            },
            {
                "rule": "flow",
                "source": "28d14df7-d398-4644-a87b-8f03c75c5ed0:Data: Extra Painted Rampart",
                "target": "7a944f52-7bd0-4f87-9c30-5acc2afdf26d:Builtin: Hangry Onerous Sparrow"
            },
            {
                "rule": "pass",
                "source": "28d14df7-d398-4644-a87b-8f03c75c5ed0:Data: Extra Painted Rampart",
                "target": "7a944f52-7bd0-4f87-9c30-5acc2afdf26d:Builtin: Hangry Onerous Sparrow"
            },
            {
                "rule": "flow",
                "source": "5cdee5ce-b0a2-4dbf-9aea-07c6516699ff:Data: Extra Scientific Table",
                "target": "e04b2cfa-99af-4558-a563-733f13909b22:Builtin: Super Nordic Cloud"
            },
            {
                "rule": "pass",
                "source": "5cdee5ce-b0a2-4dbf-9aea-07c6516699ff:Data: Extra Scientific Table",
                "target": "e04b2cfa-99af-4558-a563-733f13909b22:Builtin: Super Nordic Cloud"
            },
            {
                "rule": "assign",
                "source": "b7b03f15-252d-4555-abe6-caa63dd8344d:Static: Soggy Artsy Rampart",
                "target": "5cdee5ce-b0a2-4dbf-9aea-07c6516699ff:Data: Extra Scientific Table"
            },
            {
                "rule": "flow",
                "source": "b7b03f15-252d-4555-abe6-caa63dd8344d:Static: Soggy Artsy Rampart",
                "target": "5cdee5ce-b0a2-4dbf-9aea-07c6516699ff:Data: Extra Scientific Table"
            },
            {
                "rule": "assign",
                "source": "85cb0eab-83cf-4499-a6f4-603615ea5086:Static: Under Cutaneous Sound",
                "target": "28d14df7-d398-4644-a87b-8f03c75c5ed0:Data: Extra Painted Rampart"
            },
            {
                "rule": "flow",
                "source": "85cb0eab-83cf-4499-a6f4-603615ea5086:Static: Under Cutaneous Sound",
                "target": "28d14df7-d398-4644-a87b-8f03c75c5ed0:Data: Extra Painted Rampart"
            }
        ],
        "nodes": [
            {
                "id": "28d14df7-d398-4644-a87b-8f03c75c5ed0",
                "name": "Data: Extra Painted Rampart",
                "position": [
                    -3.522523880004883,
                    7.62939453125e-06,
                    8.476482391357422
                ],
                "type": "data"
            },
            {
                "id": "5cdee5ce-b0a2-4dbf-9aea-07c6516699ff",
                "name": "Data: Extra Scientific Table",
                "position": [
                    -8.786519050598145,
                    7.62939453125e-06,
                    5.983011245727539
                ],
                "type": "data"
            },
            {
                "id": "7a944f52-7bd0-4f87-9c30-5acc2afdf26d",
                "name": "Builtin: Hangry Onerous Sparrow",
                "position": [
                    13.852620124816895,
                    7.62939453125e-06,
                    8.793112754821777
                ],
                "type": "builtin"
            },
            {
                "id": "85cb0eab-83cf-4499-a6f4-603615ea5086",
                "name": "Static: Under Cutaneous Sound",
                "position": [
                    5.936837196350098,
                    7.62939453125e-06,
                    -4.43943452835083
                ],
                "type": "static",
                "value": "Test Two"
            },
            {
                "id": "8806cd27-84c2-4ba1-a520-447d20d0eb82",
                "name": "Container: Mild Scientific Pangolin",
                "position": [
                    -0.0932646319270134,
                    0.4947090148925781,
                    1.9119220972061157
                ],
                "type": "container"
            },
            {
                "id": "b7b03f15-252d-4555-abe6-caa63dd8344d",
                "name": "Static: Soggy Artsy Rampart",
                "position": [
                    -5.105680465698242,
                    7.62939453125e-06,
                    -3.779785394668579
                ],
                "type": "static",
                "value": "TestOne"
            },
            {
                "id": "e04b2cfa-99af-4558-a563-733f13909b22",
                "name": "Builtin: Super Nordic Cloud",
                "position": [
                    15.607285499572754,
                    7.62939453125e-06,
                    5.2969770431518555
                ],
                "type": "builtin"
            }
        ]
    },
    "name": "Sharp Murderous Wave"
}