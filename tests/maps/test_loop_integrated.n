{
    "extensions": {
        "athena": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/athena/src/nord/athena",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/athena/src/nord/athena",
                    "pkg://"
                ]
            }
        },
        "enrique": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/enrique/src/nord/enrique",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/enrique/src/nord/enrique",
                    "pkg://"
                ]
            }
        },
        "fregata": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir://../walujapi/tests/maps",
                    "dir:///Users/nate/Desktop/Dev/NoRD/fregata/src/nord/fregata",
                    "pkg://",
                    "dir://.."
                ],
                "path": [
                    "dir://../walujapi/tests/maps",
                    "dir:///Users/nate/Desktop/Dev/NoRD/fregata/src/nord/fregata",
                    "pkg://",
                    "dir://.."
                ]
            }
        },
        "iteru": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/iteru/src/nord/iteru",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/iteru/src/nord/iteru",
                    "pkg://"
                ]
            }
        },
        "jabir": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/jabir/src/nord/jabir",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/jabir/src/nord/jabir",
                    "pkg://"
                ]
            }
        },
        "kamal": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/kamal/src/nord/kamal",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/kamal/src/nord/kamal",
                    "pkg://"
                ]
            }
        },
        "pasion": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/pasion/src/nord/pasion",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/pasion/src/nord/pasion",
                    "pkg://"
                ]
            }
        },
        "shoshoni": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/shoshoni/src/nord/shoshoni",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/shoshoni/src/nord/shoshoni",
                    "pkg://"
                ]
            }
        },
        "sushrut": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/sushrut/src/nord/sushrut",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/sushrut/src/nord/sushrut",
                    "pkg://"
                ]
            }
        },
        "wrigan": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/wrigan/src/nord/wrigan",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/wrigan/src/nord/wrigan",
                    "pkg://"
                ]
            }
        },
        "xuyue": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/xuyue/src/nord/xuyue",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/xuyue/src/nord/xuyue",
                    "pkg://"
                ]
            }
        }
    },
    "map": {
        "edges": [
            {
                "from_space": "sushrut",
                "rule": "pass",
                "source": "13cf70eb-fd39-4968-a4f3-25edecd054f2:5",
                "space": "nord",
                "target": "c3f4412d-6f90-4443-a426-af3cc885f1e3:if less than",
                "to_space": "kamal"
            },
            {
                "from_space": "sushrut",
                "rule": "pass",
                "source": "3b72f0d5-305a-4d65-ba72-fef4ae8a63b0:1",
                "space": "nord",
                "target": "a3e73283-1bed-4be4-8bb4-885cc0d78e50:adder",
                "to_space": "xuyue"
            },
            {
                "from_space": "sushrut",
                "rule": "assign",
                "source": "675c9bb1-57ec-46aa-9d50-4191fb4061c0:0",
                "space": "nord",
                "target": "9a50a806-36fd-4317-93a0-f73fe5d4e915:*counter",
                "to_space": "sushrut"
            },
            {
                "from_space": "sushrut",
                "rule": "pass",
                "source": "7b1031fd-6b3b-4a68-80ea-bbbde09527a2:All Done",
                "space": "nord",
                "target": "f0e010bf-970d-4d9d-962a-b74ad31faf6b:Mega Tuned Meatpie",
                "to_space": "nord"
            },
            {
                "from_space": "sushrut",
                "rule": "pass",
                "source": "872856eb-992f-4998-9f00-9363a412ee43:counter",
                "space": "nord",
                "target": "a3e73283-1bed-4be4-8bb4-885cc0d78e50:adder",
                "to_space": "xuyue"
            },
            {
                "from_space": "xuyue",
                "rule": "assign",
                "source": "a3e73283-1bed-4be4-8bb4-885cc0d78e50:adder",
                "space": "nord",
                "target": "af2730e7-96a6-425c-8507-12b696e5112b:incr result",
                "to_space": "sushrut"
            },
            {
                "from_space": "sushrut",
                "rule": "assign",
                "source": "af2730e7-96a6-425c-8507-12b696e5112b:incr result",
                "space": "nord",
                "target": "be99ca72-fec8-4c7d-ab39-2b3295d9e5ac:*counter",
                "to_space": "sushrut"
            },
            {
                "from_space": "sushrut",
                "rule": "pass",
                "source": "af2730e7-96a6-425c-8507-12b696e5112b:incr result",
                "space": "nord",
                "target": "c3f4412d-6f90-4443-a426-af3cc885f1e3:if less than",
                "to_space": "kamal"
            },
            {
                "from_space": "kamal",
                "rule": "falseflow",
                "source": "c3f4412d-6f90-4443-a426-af3cc885f1e3:if less than",
                "space": "kamal",
                "target": "7b1031fd-6b3b-4a68-80ea-bbbde09527a2:All Done",
                "to_space": "sushrut"
            },
            {
                "from_space": "kamal",
                "rule": "trueflow",
                "source": "c3f4412d-6f90-4443-a426-af3cc885f1e3:if less than",
                "space": "kamal",
                "target": "872856eb-992f-4998-9f00-9363a412ee43:counter",
                "to_space": "sushrut"
            }
        ],
        "nodes": [
            {
                "id": "13cf70eb-fd39-4968-a4f3-25edecd054f2",
                "name": "5",
                "position": [
                    12.654202461242676,
                    0.1138763427734375,
                    -10.764413833618164
                ],
                "space": "sushrut",
                "type": "staticint",
                "value": "5"
            },
            {
                "id": "3b72f0d5-305a-4d65-ba72-fef4ae8a63b0",
                "name": "1",
                "position": [
                    -5.00333309173584,
                    -0.6484565734863281,
                    5.509457588195801
                ],
                "space": "sushrut",
                "type": "staticint",
                "value": "1"
            },
            {
                "id": "675c9bb1-57ec-46aa-9d50-4191fb4061c0",
                "name": "0",
                "position": [
                    -8.493227005004883,
                    -2.674549102783203,
                    9.689396858215332
                ],
                "space": "sushrut",
                "type": "staticint",
                "value": "0"
            },
            {
                "id": "7b1031fd-6b3b-4a68-80ea-bbbde09527a2",
                "name": "All Done",
                "position": [
                    19.29328155517578,
                    -1.8500099182128906,
                    -5.423208236694336
                ],
                "space": "sushrut",
                "type": "staticstr",
                "value": "All Done"
            },
            {
                "id": "872856eb-992f-4998-9f00-9363a412ee43",
                "name": "counter",
                "position": [
                    12.790970802307129,
                    0.7465782165527344,
                    5.635565280914307
                ],
                "space": "sushrut",
                "type": "dataint"
            },
            {
                "id": "9a50a806-36fd-4317-93a0-f73fe5d4e915",
                "instanceof": "872856eb-992f-4998-9f00-9363a412ee43",
                "name": "*counter",
                "position": [
                    -0.34255221486091614,
                    -2.1011810302734375,
                    9.775410652160645
                ],
                "space": "sushrut",
                "type": "dataint"
            },
            {
                "id": "a3e73283-1bed-4be4-8bb4-885cc0d78e50",
                "name": "adder",
                "position": [
                    1.5610222816467285,
                    0.470855712890625,
                    5.620331287384033
                ],
                "space": "xuyue",
                "type": "plus"
            },
            {
                "id": "af2730e7-96a6-425c-8507-12b696e5112b",
                "name": "incr result",
                "position": [
                    1.5760334730148315,
                    0.2833366394042969,
                    -5.510561466217041
                ],
                "space": "sushrut",
                "type": "dataint"
            },
            {
                "id": "be99ca72-fec8-4c7d-ab39-2b3295d9e5ac",
                "instanceof": "872856eb-992f-4998-9f00-9363a412ee43",
                "name": "*counter",
                "position": [
                    6.963627815246582,
                    0.9982337951660156,
                    0.09055940806865692
                ],
                "space": "sushrut",
                "type": "dataint"
            },
            {
                "id": "c3f4412d-6f90-4443-a426-af3cc885f1e3",
                "name": "if less than",
                "position": [
                    12.672780990600586,
                    -0.4149360656738281,
                    -5.471732139587402
                ],
                "space": "kamal",
                "type": "lessthan"
            },
            {
                "id": "f0e010bf-970d-4d9d-962a-b74ad31faf6b",
                "name": "Mega Tuned Meatpie",
                "position": [
                    19.524986267089844,
                    0.35852813720703125,
                    1.9280998706817627
                ],
                "space": "nord",
                "type": "builtin"
            }
        ]
    },
    "name": "Mild Bifurcated Table"
}