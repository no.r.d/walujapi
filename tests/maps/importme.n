{
    "extensions": {
        "athena": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/athena/src/nord/athena",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/athena/src/nord/athena",
                    "pkg://"
                ]
            }
        },
        "enrique": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/enrique/src/nord/enrique",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/enrique/src/nord/enrique",
                    "pkg://"
                ]
            }
        },
        "fregata": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir://../walujapi/tests/maps",
                    "dir:///Users/nate/Desktop/Dev/NoRD/fregata/src/nord/fregata",
                    "pkg://"
                ],
                "path": [
                    "dir://../walujapi/tests/maps",
                    "dir:///Users/nate/Desktop/Dev/NoRD/fregata/src/nord/fregata",
                    "pkg://"
                ]
            }
        },
        "iteru": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/iteru/src/nord/iteru",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/iteru/src/nord/iteru",
                    "pkg://"
                ]
            }
        },
        "jabir": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/jabir/src/nord/jabir",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/jabir/src/nord/jabir",
                    "pkg://"
                ]
            }
        },
        "kamal": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/kamal/src/nord/kamal",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/kamal/src/nord/kamal",
                    "pkg://"
                ]
            }
        },
        "pasion": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/pasion/src/nord/pasion",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/pasion/src/nord/pasion",
                    "pkg://"
                ]
            }
        },
        "shoshoni": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/shoshoni/src/nord/shoshoni",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/shoshoni/src/nord/shoshoni",
                    "pkg://"
                ]
            }
        },
        "sushrut": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/sushrut/src/nord/sushrut",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/sushrut/src/nord/sushrut",
                    "pkg://"
                ]
            }
        },
        "wrigan": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/wrigan/src/nord/wrigan",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/wrigan/src/nord/wrigan",
                    "pkg://"
                ]
            }
        },
        "xuyue": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/xuyue/src/nord/xuyue",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/xuyue/src/nord/xuyue",
                    "pkg://"
                ]
            }
        }
    },
    "map": {
        "edges": [
            {
                "rule": "contains",
                "source": "1644c7b9-783a-4729-a232-b8f9b44f22dd:Helper",
                "target": "b07f6ad1-d17b-4961-801c-25a32a8eab40:Hangry Molecular Presentation"
            },
            {
                "rule": "pass",
                "source": "1dc01a5f-d883-4dd2-8e86-3ed910b1c84d:Static",
                "target": "491a5bd8-d82f-4d32-b78e-92b4d14f99e2:Green Orbital Curtain"
            },
            {
                "rule": "contains",
                "source": "5c29aca5-6599-4f88-ac80-251c4138aaff:Main",
                "target": "1dc01a5f-d883-4dd2-8e86-3ed910b1c84d:Static"
            },
            {
                "rule": "contains",
                "source": "5c29aca5-6599-4f88-ac80-251c4138aaff:Main",
                "target": "491a5bd8-d82f-4d32-b78e-92b4d14f99e2:Green Orbital Curtain"
            }
        ],
        "nodes": [
            {
                "id": "1644c7b9-783a-4729-a232-b8f9b44f22dd",
                "menuicon": "data",
                "name": "Helper",
                "position": [
                    9.002832412719727,
                    -0.813507080078125,
                    -3.72348690032959
                ],
                "type": "container"
            },
            {
                "id": "1dc01a5f-d883-4dd2-8e86-3ed910b1c84d",
                "name": "Static",
                "position": [
                    -0.26402607560157776,
                    0.5720558166503906,
                    1.9336012601852417
                ],
                "type": "static",
                "value": "AAA"
            },
            {
                "id": "491a5bd8-d82f-4d32-b78e-92b4d14f99e2",
                "name": "Green Orbital Curtain",
                "position": [
                    4.179747581481934,
                    7.62939453125e-06,
                    -5.101300239562988
                ],
                "space": "nord",
                "type": "return",
                "verbose": false
            },
            {
                "id": "5c29aca5-6599-4f88-ac80-251c4138aaff",
                "menuicon": "action",
                "name": "Main",
                "position": [
                    -6.0960373878479,
                    0.0,
                    5.458792686462402
                ],
                "type": "container"
            },
            {
                "id": "815d732e-183a-440a-b538-ab63a82e5324",
                "name": "Comment: Light Flooded Rampart",
                "position": [
                    5.0990118980407715,
                    0.9308967590332031,
                    4.090157985687256
                ],
                "type": "comment"
            },
            {
                "id": "b07f6ad1-d17b-4961-801c-25a32a8eab40",
                "name": "Hangry Molecular Presentation",
                "position": [
                    -3.585575580596924,
                    0.20558929443359375,
                    3.0808773040771484
                ],
                "space": "nord",
                "type": "data"
            }
        ]
    },
    "name": "Super Sanded Pangolin"
}