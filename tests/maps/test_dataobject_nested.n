{
    "extensions": {
        "fregata": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir://../sushrut/test/maps",
                    "dir:///Users/nate/Desktop/Dev/NoRD/fregata/src/nord/fregata",
                    "pkg://",
                    "dir://.",
                    "dir:///Users/nate/.virtualenvs/nord_run/lib/python3.7/site-packages/nord/fregata",
                    "dir:///Users/nate/Desktop/Dev/NoRD/sandbox"
                ],
                "path": [
                    "dir://../sushrut/test/maps",
                    "dir:///Users/nate/Desktop/Dev/NoRD/fregata/src/nord/fregata",
                    "pkg://",
                    "dir://.",
                    "dir:///Users/nate/.virtualenvs/nord_run/lib/python3.7/site-packages/nord/fregata",
                    "dir:///Users/nate/Desktop/Dev/NoRD/sandbox"
                ]
            }
        },
        "pasion": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/pasion/src/nord/pasion",
                    "pkg://",
                    "dir:///Users/nate/.virtualenvs/nord_run/lib/python3.7/site-packages/nord/pasion"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/pasion/src/nord/pasion",
                    "pkg://",
                    "dir:///Users/nate/.virtualenvs/nord_run/lib/python3.7/site-packages/nord/pasion"
                ]
            }
        },
        "shoshoni": {
            "locations": {
                "code": [
                    "null://.",
                    "dir:///Users/nate/Desktop/Dev/NoRD/shoshoni/src/nord/shoshoni/",
                    "dir:///Users/nate/Desktop/Dev/NoRD/shoshoni/src/nord"
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/shoshoni/src/nord/shoshoni",
                    "pkg://",
                    "dir:///Users/nate/.virtualenvs/nord_run/lib/python3.7/site-packages/nord/shoshoni",
                    "dir:///Users/nate/Desktop/Dev/NoRD/shoshoni/src/nord/shoshoni/",
                    "dir:///Users/nate/Desktop/Dev/NoRD/shoshoni/src/nord"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/shoshoni/src/nord/shoshoni",
                    "pkg://",
                    "dir:///Users/nate/.virtualenvs/nord_run/lib/python3.7/site-packages/nord/shoshoni",
                    "dir:///Users/nate/Desktop/Dev/NoRD/shoshoni/src/nord/shoshoni/",
                    "dir:///Users/nate/Desktop/Dev/NoRD/shoshoni/src/nord"
                ]
            }
        },
        "sushrut": {
            "locations": {
                "code": [
                    "null://.",
                    "dir:///Users/nate/Desktop/Dev/NoRD/sushrut/src/nord/sushrut/",
                    "dir:///Users/nate/Desktop/Dev/NoRD/sushrut/src/nord"
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/sushrut/src/nord/sushrut",
                    "pkg://",
                    "dir:///Users/nate/Desktop/Dev/NoRD/sushrut/src/nord/sushrut/",
                    "dir:///Users/nate/Desktop/Dev/NoRD/sushrut/src/nord"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/sushrut/src/nord/sushrut",
                    "pkg://",
                    "dir:///Users/nate/Desktop/Dev/NoRD/sushrut/src/nord/sushrut/",
                    "dir:///Users/nate/Desktop/Dev/NoRD/sushrut/src/nord"
                ]
            }
        },
        "wrigan": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/wrigan/src/nord/wrigan",
                    "pkg://",
                    "dir:///Users/nate/.virtualenvs/nord_run/lib/python3.7/site-packages/nord/wrigan"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/wrigan/src/nord/wrigan",
                    "pkg://",
                    "dir:///Users/nate/.virtualenvs/nord_run/lib/python3.7/site-packages/nord/wrigan"
                ]
            }
        }
    },
    "map": {
        "edges": [
            {
                "from_space": "sushrut",
                "rule": "contains",
                "source": "2d250c51-8717-4553-a385-1d4ec12c66f3:8-13-2020 datetime object",
                "space": "nord",
                "target": "95e41b9b-1cda-4c0b-9e0f-f8b596b617fb:now",
                "to_space": "sushrut"
            },
            {
                "from_space": "sushrut",
                "rule": "flow",
                "source": "2d250c51-8717-4553-a385-1d4ec12c66f3:8-13-2020 datetime object",
                "space": "nord",
                "target": "95e41b9b-1cda-4c0b-9e0f-f8b596b617fb:now",
                "to_space": "sushrut"
            },
            {
                "from_space": "sushrut",
                "rule": "pass",
                "source": "2ffb1cdd-1add-43de-8ff0-52b982a136d3:month=9",
                "space": "nord",
                "target": "a38fe084-b958-42cf-bd01-33f99a07b2e6:datetime module",
                "to_space": "nord"
            },
            {
                "from_space": "sushrut",
                "rule": "pass",
                "source": "668ad610-9b55-4cf0-a68a-dcc6dbff5b73:year=2020",
                "space": "nord",
                "target": "a38fe084-b958-42cf-bd01-33f99a07b2e6:datetime module",
                "to_space": "nord"
            },
            {
                "from_space": "nord",
                "rule": "contains",
                "source": "7106fd2f-0bed-4afe-bcea-14ae4e133d82:MAIN",
                "space": "nord",
                "target": "2d250c51-8717-4553-a385-1d4ec12c66f3:8-13-2020 datetime object",
                "to_space": "sushrut"
            },
            {
                "from_space": "nord",
                "rule": "contains",
                "source": "7106fd2f-0bed-4afe-bcea-14ae4e133d82:MAIN",
                "space": "nord",
                "target": "2ffb1cdd-1add-43de-8ff0-52b982a136d3:month=9",
                "to_space": "sushrut"
            },
            {
                "from_space": "nord",
                "rule": "contains",
                "source": "7106fd2f-0bed-4afe-bcea-14ae4e133d82:MAIN",
                "space": "nord",
                "target": "668ad610-9b55-4cf0-a68a-dcc6dbff5b73:year=2020",
                "to_space": "sushrut"
            },
            {
                "from_space": "nord",
                "rule": "contains",
                "source": "7106fd2f-0bed-4afe-bcea-14ae4e133d82:MAIN",
                "space": "nord",
                "target": "8a5b877c-3139-4194-9c0d-714ba7a93b30:Dataobject: Extra Painted Bird",
                "to_space": "sushrut"
            },
            {
                "from_space": "nord",
                "rule": "contains",
                "source": "7106fd2f-0bed-4afe-bcea-14ae4e133d82:MAIN",
                "space": "nord",
                "target": "95e41b9b-1cda-4c0b-9e0f-f8b596b617fb:now",
                "to_space": "sushrut"
            },
            {
                "rule": "contains",
                "source": "7106fd2f-0bed-4afe-bcea-14ae4e133d82:MAIN",
                "target": "a38fe084-b958-42cf-bd01-33f99a07b2e6:datetime module"
            },
            {
                "from_space": "nord",
                "rule": "contains",
                "source": "7106fd2f-0bed-4afe-bcea-14ae4e133d82:MAIN",
                "space": "nord",
                "target": "d96a12f1-2d04-4504-85eb-61c1d414068c:day=13",
                "to_space": "sushrut"
            },
            {
                "from_space": "shoshoni",
                "rule": "use",
                "source": "75f97c78-a51a-476d-9194-b6358ca1e0bc:import datetime",
                "space": "nord",
                "target": "7106fd2f-0bed-4afe-bcea-14ae4e133d82:MAIN",
                "to_space": "nord"
            },
            {
                "from_space": "sushrut",
                "rule": "assign",
                "source": "95e41b9b-1cda-4c0b-9e0f-f8b596b617fb:now",
                "space": "nord",
                "target": "8a5b877c-3139-4194-9c0d-714ba7a93b30:Dataobject: Extra Painted Bird",
                "to_space": "sushrut"
            },
            {
                "from_space": "nord",
                "rule": "assign",
                "source": "a38fe084-b958-42cf-bd01-33f99a07b2e6:datetime module",
                "space": "nord",
                "target": "2d250c51-8717-4553-a385-1d4ec12c66f3:8-13-2020 datetime object",
                "to_space": "sushrut"
            },
            {
                "from_space": "nord",
                "rule": "flow",
                "source": "a38fe084-b958-42cf-bd01-33f99a07b2e6:datetime module",
                "space": "nord",
                "target": "95e41b9b-1cda-4c0b-9e0f-f8b596b617fb:now",
                "to_space": "sushrut"
            },
            {
                "from_space": "sushrut",
                "rule": "pass",
                "source": "d96a12f1-2d04-4504-85eb-61c1d414068c:day=13",
                "space": "nord",
                "target": "a38fe084-b958-42cf-bd01-33f99a07b2e6:datetime module",
                "to_space": "nord"
            }
        ],
        "nodes": [
            {
                "id": "2d250c51-8717-4553-a385-1d4ec12c66f3",
                "name": "8-13-2020 datetime object",
                "position": [
                    -0.7274140119552612,
                    11.88968276977539,
                    -4.350008964538574
                ],
                "space": "sushrut",
                "type": "dataobject"
            },
            {
                "id": "2ffb1cdd-1add-43de-8ff0-52b982a136d3",
                "name": "month=9",
                "position": [
                    -5.704847812652588,
                    -0.4404869079589844,
                    5.059983730316162
                ],
                "space": "sushrut",
                "type": "staticint",
                "value": "9"
            },
            {
                "id": "668ad610-9b55-4cf0-a68a-dcc6dbff5b73",
                "name": "year=2020",
                "position": [
                    -9.402274131774902,
                    -0.6414604187011719,
                    5.39057731628418
                ],
                "space": "sushrut",
                "type": "staticint",
                "value": "2020"
            },
            {
                "id": "7106fd2f-0bed-4afe-bcea-14ae4e133d82",
                "name": "MAIN",
                "position": [
                    -0.01298218872398138,
                    0.12591171264648438,
                    -3.9160337448120117
                ],
                "type": "container"
            },
            {
                "id": "75f97c78-a51a-476d-9194-b6358ca1e0bc",
                "menuicon": "menusubaddnodesshoshoni",
                "modname": "datetime",
                "name": "import datetime",
                "position": [
                    -7.368055820465088,
                    -0.42426300048828125,
                    -0.6813730001449585
                ],
                "space": "shoshoni",
                "type": "external"
            },
            {
                "id": "8a5b877c-3139-4194-9c0d-714ba7a93b30",
                "name": "Dataobject: Extra Painted Bird",
                "position": [
                    10.037057876586914,
                    -5.325466156005859,
                    -3.941821336746216
                ],
                "space": "sushrut",
                "type": "dataobject"
            },
            {
                "id": "95e41b9b-1cda-4c0b-9e0f-f8b596b617fb",
                "name": "now",
                "operate_as_variable": false,
                "position": [
                    6.250321388244629,
                    -0.16885757446289062,
                    1.9976906776428223
                ],
                "space": "sushrut",
                "type": "datafunction"
            },
            {
                "id": "a38fe084-b958-42cf-bd01-33f99a07b2e6",
                "itemid": "73fe8334-0c81-4cbc-8b12-3fa7f77baff5",
                "name": "datetime module",
                "parentid": "75f97c78-a51a-476d-9194-b6358ca1e0bc",
                "position": [
                    -7.761549472808838,
                    10.775100708007812,
                    0.23459361493587494
                ],
                "referenced_item": "datetime",
                "type": "reference"
            },
            {
                "id": "d96a12f1-2d04-4504-85eb-61c1d414068c",
                "name": "day=13",
                "position": [
                    -2.028496742248535,
                    -0.2794914245605469,
                    4.3250908851623535
                ],
                "space": "sushrut",
                "type": "staticint",
                "value": "13"
            }
        ]
    },
    "name": "Soggy Dental Table"
}