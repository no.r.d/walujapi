{
    "map": {
        "edges": [
            {
                "rule": "contains",
                "source": "5c29aca5-6599-4f88-ac80-251c4138aaff:Main",
                "target": "1dc01a5f-d883-4dd2-8e86-3ed910b1c84d:Equals"
            }
        ],
        "nodes": [
            {
                "id": "1644c7b9-783a-4729-a232-b8f9b44f22dd",
                "menuicon": "data",
                "name": "Helper",
                "position": [
                    9.002832412719727,
                    -0.813507080078125,
                    -3.72348690032959
                ],
                "type": "container"
            },
            {
                "id": "1dc01a5f-d883-4dd2-8e86-3ed910b1c84d",
                "name": "Equals",
                "position": [
                    -0.26402607560157776,
                    0.5720558166503906,
                    1.9336012601852417
                ],
                "type": "equals",
                "space": "kemal"
            },
            {
                "id": "5c29aca5-6599-4f88-ac80-251c4138aaff",
                "menuicon": "action",
                "name": "Main",
                "position": [
                    -6.0960373878479,
                    0.0,
                    5.458792686462402
                ],
                "type": "container"
            },
            {
                "id": "815d732e-183a-440a-b538-ab63a82e5324",
                "name": "Comment: Light Flooded Rampart",
                "position": [
                    5.0990118980407715,
                    0.9308967590332031,
                    4.090157985687256
                ],
                "type": "comment"
            }
        ]
    },
    "extensions": {
        "kemal": {
            "locations": {
                "files": ["dir://tests/extensions", "dir://../walujapi/tests/extensions"],
                "path": ["dir://tests/extensions", "dir://../walujapi/tests/extensions"],
                "code": ["dir://tests/extensions", "dir://../walujapi/tests/extensions"]
            },
            "permissions": {},
            "min_version": null,
            "max_version": null
        }
    },
    "name": "Super Sanded Pangolin"
}
