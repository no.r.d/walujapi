{
    "map": {
        "edges": [
            {
                "rule": "flow",
                "source": "29fb15e6-76bd-4742-931f-d83304213686:Builtin: Extra Viscous Aardvark",
                "target": "487a2501-d3f8-4a50-9d9c-2707b83fa565:Builtin: Aloof Blasted Bird"
            },
            {
                "rule": "flow",
                "source": "29fb15e6-76bd-4742-931f-d83304213686:Builtin: Extra Viscous Aardvark",
                "target": "f9da55cf-2a5b-4fb4-8165-121d03d107f3:Return: Under Cutaneous Wall"
            },
            {
                "rule": "contains",
                "source": "1a2f805e-43c3-42c0-825a-28d5f75a9e66:Container: Arrid Onerous Wave",
                "target": "29fb15e6-76bd-4742-931f-d83304213686:Builtin: Extra Viscous Aardvark"
            },
            {
                "rule": "flow",
                "source": "1a2f805e-43c3-42c0-825a-28d5f75a9e66:Container: Arrid Onerous Wave",
                "target": "31160615-f7ed-4230-9d21-c43306bbf7b8:Builtin: Arrid Forked Bird"
            },
            {
                "rule": "pass",
                "source": "1a2f805e-43c3-42c0-825a-28d5f75a9e66:Container: Arrid Onerous Wave",
                "target": "31160615-f7ed-4230-9d21-c43306bbf7b8:Builtin: Arrid Forked Bird"
            },
            {
                "rule": "contains",
                "source": "1a2f805e-43c3-42c0-825a-28d5f75a9e66:Container: Arrid Onerous Wave",
                "target": "89ec988f-660a-4cd1-8665-0f6025033b23:Container: Fat Forked Slobber"
            },
            {
                "rule": "contains",
                "source": "1a2f805e-43c3-42c0-825a-28d5f75a9e66:Container: Arrid Onerous Wave",
                "target": "f9da55cf-2a5b-4fb4-8165-121d03d107f3:Return: Under Cutaneous Wall"
            },
            {
                "rule": "contains",
                "source": "1a2f805e-43c3-42c0-825a-28d5f75a9e66:Container: Arrid Onerous Wave",
                "target": "4005f703-4db2-4945-97f8-3ed86d0c7630:Static: Distant Forked Cheese"
            },
            {
                "rule": "flow",
                "source": "89ec988f-660a-4cd1-8665-0f6025033b23:Container: Fat Forked Slobber",
                "target": "29fb15e6-76bd-4742-931f-d83304213686:Builtin: Extra Viscous Aardvark"
            },
            {
                "rule": "pass",
                "source": "89ec988f-660a-4cd1-8665-0f6025033b23:Container: Fat Forked Slobber",
                "target": "487a2501-d3f8-4a50-9d9c-2707b83fa565:Builtin: Aloof Blasted Bird"
            },
            {
                "rule": "contains",
                "source": "89ec988f-660a-4cd1-8665-0f6025033b23:Container: Fat Forked Slobber",
                "target": "7355fa5b-77b3-431d-8936-940031bdcc15:Return: Extra Nordic Lizard"
            },
            {
                "rule": "contains",
                "source": "89ec988f-660a-4cd1-8665-0f6025033b23:Container: Fat Forked Slobber",
                "target": "ce5704fa-634d-48e6-820e-fe5ed9840350:Static: Prime Rounded Bouy"
            },
            {
                "rule": "contains",
                "source": "1a2f805e-43c3-42c0-825a-28d5f75a9e66:Container: Arrid Onerous Wave",
                "target": "487a2501-d3f8-4a50-9d9c-2707b83fa565:Builtin: Aloof Blasted Bird"
            },
            {
                "rule": "pass",
                "source": "4005f703-4db2-4945-97f8-3ed86d0c7630:Static: Distant Forked Cheese",
                "target": "29fb15e6-76bd-4742-931f-d83304213686:Builtin: Extra Viscous Aardvark"
            },
            {
                "rule": "flow",
                "source": "4005f703-4db2-4945-97f8-3ed86d0c7630:Static: Distant Forked Cheese",
                "target": "89ec988f-660a-4cd1-8665-0f6025033b23:Container: Fat Forked Slobber"
            },
            {
                "rule": "pass",
                "source": "4005f703-4db2-4945-97f8-3ed86d0c7630:Static: Distant Forked Cheese",
                "target": "f9da55cf-2a5b-4fb4-8165-121d03d107f3:Return: Under Cutaneous Wall"
            },
            {
                "rule": "flow",
                "source": "ce5704fa-634d-48e6-820e-fe5ed9840350:Static: Prime Rounded Bouy",
                "target": "7355fa5b-77b3-431d-8936-940031bdcc15:Return: Extra Nordic Lizard"
            },
            {
                "rule": "pass",
                "source": "ce5704fa-634d-48e6-820e-fe5ed9840350:Static: Prime Rounded Bouy",
                "target": "7355fa5b-77b3-431d-8936-940031bdcc15:Return: Extra Nordic Lizard"
            }
        ],
        "nodes": [
            {
                "id": "1a2f805e-43c3-42c0-825a-28d5f75a9e66",
                "name": "Container: Arrid Onerous Wave",
                "position": [
                    -6.157458782196045,
                    0.0,
                    7.792791843414307
                ],
                "type": "container"
            },
            {
                "id": "29fb15e6-76bd-4742-931f-d83304213686",
                "name": "Builtin: Extra Viscous Aardvark",
                "position": [
                    -7.616207599639893,
                    0.0,
                    -1.5278481245040894
                ],
                "type": "builtin"
            },
            {
                "id": "31160615-f7ed-4230-9d21-c43306bbf7b8",
                "name": "Builtin: Arrid Forked Bird",
                "position": [
                    8.805791854858398,
                    -1.4007720947265625,
                    -3.5894789695739746
                ],
                "type": "builtin"
            },
            {
                "id": "4005f703-4db2-4945-97f8-3ed86d0c7630",
                "name": "Static: Distant Forked Cheese",
                "position": [
                    -3.752882957458496,
                    -0.3244361877441406,
                    5.209794044494629
                ],
                "type": "static",
                "value": "Flobbly Doo"
            },
            {
                "id": "487a2501-d3f8-4a50-9d9c-2707b83fa565",
                "name": "Builtin: Aloof Blasted Bird",
                "position": [
                    13.137648582458496,
                    1.47894287109375,
                    -5.288259506225586
                ],
                "type": "builtin"
            },
            {
                "id": "7355fa5b-77b3-431d-8936-940031bdcc15",
                "name": "Return: Extra Nordic Lizard",
                "position": [
                    -6.679537296295166,
                    0.0,
                    -2.863754987716675
                ],
                "type": "return"
            },
            {
                "id": "89ec988f-660a-4cd1-8665-0f6025033b23",
                "name": "Container: Fat Forked Slobber",
                "position": [
                    11.600099563598633,
                    -0.401092529296875,
                    2.972806215286255
                ],
                "type": "container"
            },
            {
                "id": "ce5704fa-634d-48e6-820e-fe5ed9840350",
                "name": "Static: Prime Rounded Bouy",
                "position": [
                    7.769759654998779,
                    0.0,
                    0.5297561883926392
                ],
                "type": "static",
                "value": "Hi Mike!"
            },
            {
                "id": "f9da55cf-2a5b-4fb4-8165-121d03d107f3",
                "name": "Return: Under Cutaneous Wall",
                "position": [
                    2.6871700286865234,
                    0.0,
                    -5.382017135620117
                ],
                "type": "return"
            }
        ]
    },
    "name": "Aloof Culinary Iceberg"
}