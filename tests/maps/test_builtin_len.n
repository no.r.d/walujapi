{
    "extensions": {
        "fregata": {
            "locations": {
                "code": [
                    "dir://../fregata/src"
                ],
                "files": [
                    "dir://../fregata/src/fregata",
                    "dir://../fregata/src"
                ],
                "path": [
                    "dir://../fregata/src",
                    "dir://../fregata/src/fregata"
                ]
            }
        },
        "sushrut": {
            "locations": {
                "code": [
                    "dir:///home/nate/Desktop/Dev/NoRD/sushrut/src/sushrut/",
                    "dir:///home/nate/Desktop/Dev/NoRD/sushrut/src"
                ],
                "files": [
                    "dir:///home/nate/Desktop/Dev/NoRD/sushrut/src/sushrut/",
                    "dir:///home/nate/Desktop/Dev/NoRD/sushrut/src"
                ],
                "path": [
                    "dir:///home/nate/Desktop/Dev/NoRD/sushrut/src/sushrut/",
                    "dir:///home/nate/Desktop/Dev/NoRD/sushrut/src"
                ]
            }
        },
        "wrigan": {
            "locations": {
                "code": [
                    "dir://../wrigan/src"
                ],
                "files": [
                    "dir://../wrigan/src/wrigan",
                    "dir://../wrigan/src"
                ],
                "path": [
                    "dir://../wrigan/src",
                    "dir://../wrigan/src/wrigan"
                ]
            }
        }
    },
    "map": {
        "edges": [
            {
                "rule": "assign",
                "source": "eedcc574-e2f3-4e17-a33a-97ae7532acf4:Length Builtin",
                "target": "419185c1-d855-4457-9d58-11c82ca3174f:Results"
            },
            {
                "from_space": "sushrut",
                "rule": "pass",
                "source": "b872054f-bfd0-4d74-939c-5d27235a212a:Array",
                "space": "nord",
                "target": "eedcc574-e2f3-4e17-a33a-97ae7532acf4:Length Builtin",
                "to_space": "nord"
            },
            {
                "delegate": {
                    "direction": "as_tgt",
                    "rule_type": "pass",
                    "uid": "append_to_array"
                },
                "from_space": "sushrut",
                "rule": "pass",
                "source": "1f7c9f16-4da6-4b4c-9a39-df1d56fd3dd2:123",
                "space": "nord",
                "target": "b872054f-bfd0-4d74-939c-5d27235a212a:Array",
                "to_space": "sushrut"
            },
            {
                "delegate": {
                    "direction": "as_tgt",
                    "rule_type": "pass",
                    "uid": "append_to_array"
                },
                "from_space": "sushrut",
                "rule": "pass",
                "source": "060ffb5e-f56a-45ca-828c-8632fc3db5d2:456",
                "space": "nord",
                "target": "b872054f-bfd0-4d74-939c-5d27235a212a:Array",
                "to_space": "sushrut"
            },
            {
                "delegate": {
                    "direction": "as_tgt",
                    "rule_type": "pass",
                    "uid": "append_to_array"
                },
                "from_space": "sushrut",
                "rule": "pass",
                "source": "52884e2c-a8be-4273-bce4-044e65ac80cf:789",
                "space": "nord",
                "target": "b872054f-bfd0-4d74-939c-5d27235a212a:Array",
                "to_space": "sushrut"
            }
        ],
        "nodes": [
            {
                "id": "060ffb5e-f56a-45ca-828c-8632fc3db5d2",
                "name": "456",
                "position": [
                    -15.4603910446167,
                    -2.5400772094726562,
                    0.22372254729270935
                ],
                "space": "sushrut",
                "type": "staticstr",
                "value": "456"
            },
            {
                "id": "1f7c9f16-4da6-4b4c-9a39-df1d56fd3dd2",
                "name": "123",
                "position": [
                    -14.694939613342285,
                    -0.846771240234375,
                    6.329190254211426
                ],
                "space": "sushrut",
                "type": "staticstr",
                "value": "123"
            },
            {
                "id": "419185c1-d855-4457-9d58-11c82ca3174f",
                "name": "Results",
                "position": [
                    6.186358451843262,
                    -0.32901763916015625,
                    0.6531413197517395
                ],
                "type": "data"
            },
            {
                "id": "52884e2c-a8be-4273-bce4-044e65ac80cf",
                "name": "789",
                "position": [
                    -15.281067848205566,
                    -0.9617042541503906,
                    -5.182909965515137
                ],
                "space": "sushrut",
                "type": "staticstr",
                "value": "789"
            },
            {
                "id": "b872054f-bfd0-4d74-939c-5d27235a212a",
                "name": "Array",
                "position": [
                    -9.444438934326172,
                    -0.10892105102539062,
                    0.22280438244342804
                ],
                "space": "sushrut",
                "type": "dataarray"
            },
            {
                "id": "eedcc574-e2f3-4e17-a33a-97ae7532acf4",
                "method": "length",
                "name": "Length Builtin",
                "position": [
                    -0.6822776794433594,
                    0.0,
                    0.5210121273994446
                ],
                "type": "builtin"
            }
        ]
    },
    "name": "Gothic Bioactive Sword"
}