{
    "extensions": {
        "athena": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/athena/src/nord/athena",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/athena/src/nord/athena",
                    "pkg://"
                ]
            }
        },
        "enrique": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/enrique/src/nord/enrique",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/enrique/src/nord/enrique",
                    "pkg://"
                ]
            }
        },
        "fregata": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir://../walujapi/tests/maps",
                    "dir:///Users/nate/Desktop/Dev/NoRD/fregata/src/nord/fregata",
                    "pkg://"
                ],
                "path": [
                    "dir://../walujapi/tests/maps",
                    "dir:///Users/nate/Desktop/Dev/NoRD/fregata/src/nord/fregata",
                    "pkg://"
                ]
            }
        },
        "iteru": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/iteru/src/nord/iteru",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/iteru/src/nord/iteru",
                    "pkg://"
                ]
            }
        },
        "jabir": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/jabir/src/nord/jabir",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/jabir/src/nord/jabir",
                    "pkg://"
                ]
            }
        },
        "kamal": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/kamal/src/nord/kamal",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/kamal/src/nord/kamal",
                    "pkg://"
                ]
            }
        },
        "pasion": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/pasion/src/nord/pasion",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/pasion/src/nord/pasion",
                    "pkg://"
                ]
            }
        },
        "shoshoni": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/shoshoni/src/nord/shoshoni",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/shoshoni/src/nord/shoshoni",
                    "pkg://"
                ]
            }
        },
        "sushrut": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/sushrut/src/nord/sushrut",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/sushrut/src/nord/sushrut",
                    "pkg://"
                ]
            }
        },
        "wrigan": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/wrigan/src/nord/wrigan",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/wrigan/src/nord/wrigan",
                    "pkg://"
                ]
            }
        },
        "xuyue": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/xuyue/src/nord/xuyue",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/xuyue/src/nord/xuyue",
                    "pkg://"
                ]
            }
        }
    },
    "map": {
        "edges": [
            {
                "from_space": "sushrut",
                "rule": "pass",
                "source": "053c9dd9-0eab-4dfc-8402-322acc823466:Prime Pounded Bag",
                "space": "nord",
                "target": "49552334-2f50-472d-885c-6f07d17a8fa3:Aloof Artsy Dog",
                "to_space": "xuyue"
            },
            {
                "from_space": "sushrut",
                "rule": "assign",
                "source": "25406795-1722-447b-99c6-68ddae033d64:3",
                "space": "nord",
                "target": "053c9dd9-0eab-4dfc-8402-322acc823466:Prime Pounded Bag",
                "to_space": "sushrut"
            },
            {
                "from_space": "xuyue",
                "rule": "assign",
                "source": "49552334-2f50-472d-885c-6f07d17a8fa3:Aloof Artsy Dog",
                "space": "nord",
                "target": "89cd03b0-0cfc-4b8b-be2a-90e8c9b88236:**Prime Pounded Bag",
                "to_space": "sushrut"
            },
            {
                "from_space": "sushrut",
                "rule": "pass",
                "source": "829ea3fd-a501-4e95-a53b-51943536407a:*Prime Pounded Bag",
                "space": "nord",
                "target": "49552334-2f50-472d-885c-6f07d17a8fa3:Aloof Artsy Dog",
                "to_space": "xuyue"
            }
        ],
        "nodes": [
            {
                "id": "053c9dd9-0eab-4dfc-8402-322acc823466",
                "name": "Prime Pounded Bag",
                "position": [
                    -8.799059867858887,
                    1.7629203796386719,
                    4.8088459968566895
                ],
                "space": "sushrut",
                "type": "dataint"
            },
            {
                "id": "25406795-1722-447b-99c6-68ddae033d64",
                "name": "3",
                "position": [
                    -15.079285621643066,
                    -2.689411163330078,
                    4.454709053039551
                ],
                "space": "sushrut",
                "type": "staticint",
                "value": "4"
            },
            {
                "id": "49552334-2f50-472d-885c-6f07d17a8fa3",
                "name": "Aloof Artsy Dog",
                "position": [
                    -2.5808701515197754,
                    0.8276405334472656,
                    1.4328675270080566
                ],
                "space": "xuyue",
                "type": "times"
            },
            {
                "id": "829ea3fd-a501-4e95-a53b-51943536407a",
                "instanceof": "053c9dd9-0eab-4dfc-8402-322acc823466",
                "name": "*Prime Pounded Bag",
                "position": [
                    -9.02338695526123,
                    0.0,
                    -0.41539496183395386
                ],
                "space": "sushrut",
                "type": "dataint"
            },
            {
                "id": "89cd03b0-0cfc-4b8b-be2a-90e8c9b88236",
                "instanceof": "053c9dd9-0eab-4dfc-8402-322acc823466",
                "name": "**Prime Pounded Bag",
                "position": [
                    5.603794097900391,
                    0.5059280395507812,
                    1.7824360132217407
                ],
                "space": "sushrut",
                "type": "dataint"
            }
        ]
    },
    "name": "Soggy Culinary Wave"
}