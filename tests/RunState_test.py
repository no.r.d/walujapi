"""
Copyright 2020 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""
import nord.nord.Node as Node
import nord.nord.Map as Map
import nord.nord.RunState as RS
import nord.nord.Runtime as RT
import nord.nord.Frame as Frame
Runtime = RT.Runtime


def test_executed():
    node = Node(Map(None))
    vessal = Runtime()

    assert not hasattr(node, '_run_state')

    node.execute(vessal)

    assert RS.s(node).executed() is True

    node = Node(Map(None))

    assert RS.s(node).executed() is False

    node.execute(vessal)

    assert node.has_executed() is True

    vessal.incr_exec_count()

    assert node.has_executed() is False
    assert node._run_state._executed == 0
    node.execute(vessal)
    assert node._run_state._executed == 1
    assert node.has_executed() is True


def test_pending_run():
    node = Node(Map(None))
    assert not hasattr(node, '_run_state')
    assert RS.s(node).pending_run() is False
    assert hasattr(node, '_run_state')
    assert node._run_state._pending_run is False

    node = Node(Map(None))
    frame = Frame()
    vessal = Runtime()
    # So that verbose is set to the default, run the initialize function .... not sure that
    # verbose should be handled that way in Runtime, but, we're testing RunState here....
    vessal.initialize(Map(None))
    assert not hasattr(node, '_run_state')

    # tell the Frame that this node will be the next one
    frame.inject_node(node, 'DOWN')
    assert hasattr(node, '_run_state')
    assert RS.s(node).pending_run() is True
    # "Cock the hammer" on this node, such that it will be run
    frame.set_next()
    vessal.current = frame
    assert RS.s(node).pending_run() is True
    frame.run_node(vessal)
    assert RS.s(node).executed() is True
    assert RS.s(node).pending_run() is False


def test_preempted():
    node = Node(Map(None))
    assert not hasattr(node, '_run_state')
    assert RS.s(node).preempted() is False
    assert hasattr(node, '_run_state')
    assert node._run_state._preempted is False

    graph = {
        "map": {
            "edges": [
                {
                    "source": "1:Source",
                    "target": "2:Target",
                    "rule": "assign"
                }
            ],
            "nodes": [
                {
                    "id": "1",
                    "name": "Source",
                    "type": "static",
                    "value": "STATIC"
                },
                {
                    "id": "2",
                    "name": "Target",
                    "type": "data"
                }
            ]
        }
    }

    karte = Map(graph)
    source = karte.nodes["1"]
    target = karte.nodes["2"]
    assert not hasattr(source, '_run_state')
    assert not hasattr(target, '_run_state')
    vessal = Runtime()
    # So that verbose is set to the default, run the initialize function .... not sure that
    # verbose should be handled that way in Runtime, but, we're testing RunState here....
    vessal.initialize(karte)
    vessal.begin(target, debug=True)
    assert hasattr(target, '_run_state')
    assert RS.s(target).preempted() is False

    # tell the Frame that this node will be the next one
    # "Cock the hammer" on this node, such that it will be run
    vessal.next()
    assert RS.s(target).preempted() is True
    assert RS.s(source).executed() is False
    assert RS.s(target).executed() is False
    vessal.next()
    assert RS.s(target).preempted() is False
    assert RS.s(source).executed() is True
    assert RS.s(target).executed() is False
    vessal.next()
    assert RS.s(source).executed() is True
    assert RS.s(target).preempted() is False
    assert RS.s(target).executed() is True
    vessal.next()
    vessal.next()
    assert vessal.is_done()


def test_blocked():
    node = Node(Map(None))
    assert not hasattr(node, '_run_state')
    assert RS.s(node).blocked() is False
    assert hasattr(node, '_run_state')
    assert node._run_state._blocked is False

    graph = {
        "map": {
            "edges": [
            ],
            "nodes": [
                {
                    "id": "2",
                    "name": "Needs Input",
                    "type": "data"
                }
            ]
        }
    }

    karte = Map(graph)
    target = karte.nodes["2"]
    assert not hasattr(target, '_run_state')
    vessal = Runtime()
    # So that verbose is set to the default, run the initialize function .... not sure that
    # verbose should be handled that way in Runtime, but, we're testing RunState here....
    vessal.initialize(karte)
    vessal.begin(target, debug=True)
    assert hasattr(target, '_run_state')
    assert RS.s(target).blocked() is False

    # tell the Frame that this node will be the next one
    # "Cock the hammer" on this node, such that it will be run
    vessal.next()
    assert RS.s(target).blocked() is True
    assert RS.s(target).executed() is False
    vessal.set_input("2", "VALUE IS NOW THIS")
    vessal.next()
    vessal.next()
    vessal.next()
    assert vessal.is_done()
    assert target.value == "VALUE IS NOW THIS"


def test_deferred():
    lower = Node(Map(None))
    lower.id = 'LOWER'

    def gen_new_execute(n):

        def new_execute(runtime):
            if not RS.s(n).deferred():
                runtime.pushStack()
                n.flag_exe(runtime)
        return new_execute
    lower.execute = gen_new_execute(lower)

    upper = Node(Map(None))
    upper.id = 'UPPER'
    vessal = Runtime()
    # So that verbose is set to the default, run the initialize function .... not sure that
    # verbose should be handled that way in Runtime, but, we're testing RunState here....
    vessal.initialize(Map(None))
    assert not hasattr(lower, '_run_state')
    assert not hasattr(upper, '_run_state')
    vessal.begin(lower, debug=True)
    assert RS.s(lower).deferred() is False
    vessal.next()
    assert RS.s(lower).deferred() is True
    assert RS.s(lower).executed() is True
    vessal.setNextNode(upper, 'DOWN')
    vessal.next()
    vessal.next()
    # vessal.next()
    assert RS.s(lower).deferred() is False
    assert vessal.is_done()


def test_dynamic():
    node = Node(Map(None))
    assert not hasattr(node, '_run_state')
    vessal = Runtime()
    node.flag_exe(vessal)
    assert hasattr(node, '_run_state')
    assert '_TEST_DYNAMIC' not in node._run_state.__dict__
    RS.s(node).TEST_DYNAMIC()
    assert hasattr(node._run_state, '_TEST_DYNAMIC') is False
    assert '_TEST_DYNAMIC' not in node._run_state.__dict__
    RS.s(node).set_TEST_DYNAMIC()
    assert '_TEST_DYNAMIC' in node._run_state.__dict__
    assert node._run_state._TEST_DYNAMIC is True
    RS.s(node).unset_TEST_DYNAMIC()
    assert node._run_state._TEST_DYNAMIC is False
    assert '_TEST_DYNAMIC' in node._run_state.__dict__
    RS.s(node).del_TEST_DYNAMIC()
    assert '_TEST_DYNAMIC' not in node._run_state.__dict__
    assert not hasattr(node, '_TEST_DYNAMIC')
