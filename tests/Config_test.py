"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

import nord.nord.Config as config
import nord.nord.utils.SpaceManager as use
import pytest


@pytest.mark.skip(reason="INCOMPLETE TEST")
def test_config_paths():
    config.add_to_file_path('dir://tests')
    config.add_to_code_path('dir://tests')
    config.add_to_file_path('dir://walujapi/tests')
    config.add_to_code_path('dir://walujapi/tests')
    assert use.path('Config_test.py').replace('walujapi/', '') == 'tests/Config_test.py'
    assert "NEEDS WORK" == "THIS TEST"


def test_cfg_itm():
    ci = config.ConfigItem("TEST", 3)
    exception = None
    try:
        ci.set_value(False)
        exception = False
    except AttributeError:
        exception = True
    assert exception

    assert ci.get_value() == 3
    ci.set_value(15)
    assert ci.get_value() == 15


@pytest.mark.skip(reason="INCOMPLETE TEST")
def test_configuration():
    cfg = config.Configurable()
    loadable = {
        "^config": {
            "nord": {
                "test_item1": {
                    "value": "jimmy",
                    "type": "str"
                }
            },
            "other": {
                "item_in_cfg": {
                    "value": "333",
                    "type": "int"
                }
            }
        }
    }

    cfg.load_config(loadable)
    assert cfg.get_cfg_value("item_in_cfg", space="other") == 333
    assert cfg.get_cfg_value("test_item1") == "jimmy"
    cfg.set_cfg_value("test_item1", "bobby")
    assert cfg.get_cfg_value("test_item1") == "bobby"
    cfg.set_cfg_value("item_in_cfg", 1234, space="other")
    assert cfg.get_cfg_value("item_in_cfg", space="other") == 1234
