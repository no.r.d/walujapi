"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

import nord.nord.Node as Node
import sys


class Equals(Node):
    """
    Equals vertices in the Map provide the ability to compare two passed values.


    >>> Equals() #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE
    Equals:

    """
    def execute(s, runtime):
        """
        Evaluate the passed value with the assigned value.

        Do some other stuff. This needs to be replaced once kemal is
        developed.
        """
        pass
        s.flag_exe(runtime)


sys.modules[__name__] = Equals
