"""
Copyright 2021 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""
import nord.nord.Runtime as RT
import nord.nord.Map as Map
import json
Runtime = RT.Runtime


def test_integrated_loop():
    try:
        m = json.load(open('tests/maps/test_loop_integrated.n', 'r'))
    except FileNotFoundError:
        m = json.load(open('walujapi/tests/maps/test_loop_integrated.n', 'r'))

    karte = Map(m, verbose=True)

    vessal = Runtime()
    karte.run(vessal, None)
    assert vessal.get_exec_count() == 1
    assert karte.nodes['872856eb-992f-4998-9f00-9363a412ee43'].value == 5
    assert karte.nodes['be99ca72-fec8-4c7d-ab39-2b3295d9e5ac'].get() == 5
    ekey = ('af2730e7-96a6-425c-8507-12b696e5112b', 'c3f4412d-6f90-4443-a426-af3cc885f1e3', 'pass')
    assert karte.edges[ekey]._cargo == 5
